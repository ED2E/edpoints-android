package com.edmark.loyaltyuser.view.activity.signup;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.text.method.KeyListener;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_DISTRIBUTOR_SIGN_UP;
import static com.edmark.loyaltyuser.constant.Common.API_EDA_VERIFY;
import static com.edmark.loyaltyuser.constant.Common.CONSENT_CLAUSE;
import static com.edmark.loyaltyuser.constant.Common.DISTRIBUTOR;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.EDA_BYPASS;
import static com.edmark.loyaltyuser.constant.Common.EDA_NAME;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.PRIVACY_POLICY;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_EDA_NAME;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TERMS_CONDITIONS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/05/2018.
 */

@SuppressWarnings("unchecked")
public class RegistrationActivity extends BaseActivity {
    public static final String TAG = RegistrationActivity.class.getSimpleName();


    @BindView(R.id.activity_register_editText_card_number)
    EditText editText_card_number;
    @BindView(R.id.activity_register_editText_activation_code)
    EditText editText_activation_code;
    @BindView(R.id.activity_registration_radioGroup)
    RadioGroup radioGroup_choice;
    @BindView(R.id.activity_registration_radioButton_yes)
    RadioButton radioButton_yes;
    @BindView(R.id.activity_registration_radioButton_no)
    RadioButton radioButton_no;
    @BindView(R.id.activity_register_editText_account_number)
    EditText editText_account_number;
    @BindView(R.id.activity_register_editText_username)
    EditText editText_username;
    @BindView(R.id.activity_register_editText_mobile_number)
    EditText editText_phone;
    @BindView(R.id.activity_register_editText_email_address)
    EditText editText_email;
    @BindView(R.id.activity_register_editText_date_of_birth)
    EditText editText_date_of_birth;
    @BindView(R.id.activity_register_editText_password)
    EditText editText_password;
    @BindView(R.id.activity_register_editText_confirm_password)
    EditText editText_confirm_password;
    @BindView(R.id.activity_register_editText_pin)
    EditText editText_pin;
    @BindView(R.id.activity_register_editText_confirm_pin)
    EditText editText_confirm_pin;
    @BindView(R.id.activity_register_view_date_of_birth)
    View view_date_of_birth;
    @BindView(R.id.activity_register_textview_condition)
    TextView textView_condition;
    @BindView(R.id.activity_register_checkBox_accept)
    CheckBox checkBox_accept;
    @BindView(R.id.activity_register_button_sign_up)
    Button button_sign_up;
    @BindView(R.id.activity_register_layout_eda)
    LinearLayout linearLayout_eda;
    @BindView(R.id.activity_register_button_validate_eda)
    Button button_validate_eda;

    private ApiInterface apiService;
    private boolean isVerified = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        setupToolBar(R.id.activity_register_toolbar);
        initGUI();

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGUI() {

        radioGroup_choice.setOnCheckedChangeListener((group, checkedId) -> {
            if(radioButton_yes.isChecked()) {
                linearLayout_eda.setVisibility(View.VISIBLE);
            }
            if(radioButton_no.isChecked()) {
                linearLayout_eda.setVisibility(View.GONE);
            }
        });

        view_date_of_birth.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                editText_date_of_birth.setText(new StringBuilder().append(month + 1).append("-")
                        .append(dayOfMonth).append("-").append(year));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        textView_condition.setText(Html.fromHtml(getResources().getString(R.string.label_condtition, PRIVACY_POLICY, TERMS_CONDITIONS, CONSENT_CLAUSE)));
        textView_condition.setClickable(true);
        textView_condition.setMovementMethod(LinkMovementMethod.getInstance());
        button_sign_up.setOnClickListener(v ->
                {
                    if (editText_account_number.isShown()) {
                        if (!isVerified) {
                            showToast(getResources().getString(R.string.error_input_card));
                            return;
                        }
                    }

                    if (!validateEmail()){
                        showToast(getResources().getString(R.string.error_invalid_email));
                        return;
                    }

                    String newpass = editText_password.getText().toString();
                    String confirmpass = editText_confirm_password.getText().toString();
                    if (newpass.length() < 6 || confirmpass.length() < 6) {
                        editText_password.setText("");
                        editText_confirm_password.setText("");
                        showToast(getResources().getString(R.string.error_invalid_pass));
                        return;
                    } else {
                        if (!newpass.equals(confirmpass)) {
                            editText_password.setText("");
                            editText_confirm_password.setText("");
                            showToast(getResources().getString(R.string.error_invalid_pass_mismatch));
                            return;
                        }
                    }

                    String newpin = editText_pin.getText().toString();
                    String confirmpin = editText_confirm_pin.getText().toString();
                    if (newpin.length() < 4 || confirmpin.length() < 4) {
                        editText_pin.setText("");
                        editText_confirm_pin.setText("");
                        showToast(getResources().getString(R.string.error_invalid_pin));
                        return;
                    } else {
                        if (!newpin.equals(confirmpin)) {
                            editText_pin.setText("");
                            editText_confirm_pin.setText("");
                            showToast(getResources().getString(R.string.error_invalid_pin_mismatch));
                            return;
                        }
                    }
                    initSignUp();
                }
        );

        button_sign_up.setEnabled(false);
        checkBox_accept.setOnCheckedChangeListener((buttonView, isChecked) -> button_sign_up.setEnabled(isChecked));

        button_validate_eda.setOnClickListener(v ->
        {
            if (button_validate_eda.getText().toString().equals(getResources().getString(R.string.label_change))) {
                editText_account_number.setEnabled(true);
                editText_account_number.requestFocus();
                button_validate_eda.setText(getResources().getString(R.string.label_validity_check));
                button_validate_eda.setBackground(getResources().getDrawable(R.drawable.bg_button_verify_red));
                isVerified = false;
            } else {
                String eda = editText_account_number.getText().toString();
                initVerifyEDA(eda);
            }
        });

        editText_account_number.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        editText_card_number.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
    }

    private boolean validateEmail() {
        String emailInput = editText_email.getText().toString().trim();

        if(emailInput.isEmpty()){
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            return false;
        } else {
            return true;
        }
    }


    private void initSignUp() {
        showProgress();

        String eda = "";
        if (editText_account_number.isShown()) {
            if (isVerified) {
                eda = editText_account_number.getText().toString();
            }
        }

        Call<ResponseBody> call = apiService.signup(
                API_DISTRIBUTOR_SIGN_UP,
                eda,
                editText_username.getText().toString(),
                editText_email.getText().toString(),
                editText_phone.getText().toString(),
                editText_card_number.getText().toString(),
                editText_activation_code.getText().toString(),
                getHash(editText_password.getText().toString()),
                getHash(editText_pin.getText().toString()),
                DISTRIBUTOR,
                editText_date_of_birth.getText().toString(),
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    String message = (String) obj.get(MESSAGE);
                    if (status.equals(SUCCESS)) {
                        finish();
                        showToast(message);
                    } else {
                        showToast(message);
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("onFailure:");
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                t.printStackTrace();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }

    private void initVerifyEDA(String eda) {
        showProgress();

        Call<ResponseBody> call = apiService.edaVerification(
                API_EDA_VERIFY,
                eda,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);
                    if (status.equals(SUCCESS)) {
                        String responseEDA = (String) obj.get(EDA);
                        String edaName = (String) obj.get(EDA_NAME);
                        showDialog(REQUEST_CODE_EDA_NAME, edaName, true);
                        isVerified = true;
                        button_validate_eda.setText(getResources().getString(R.string.label_change));
                        editText_account_number.setEnabled(false);
                        button_validate_eda.setBackground(getResources().getDrawable(R.drawable.bg_button_verify_green));
                        editText_username.requestFocus();
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        showToast(message);
                        isVerified = false;
                        editText_account_number.setEnabled(true);
                        button_validate_eda.setBackground(getResources().getDrawable(R.drawable.bg_button_verify_red));
                        editText_account_number.requestFocus();
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    //Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("onFailure:");
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }

}