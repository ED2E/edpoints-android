package com.edmark.loyaltyuser.view.activity.main

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.viewpager.widget.ViewPager

import com.edmark.loyaltyuser.R
import com.edmark.loyaltyuser.adapter.OnBoardingViewPagerAdapter
import com.edmark.loyaltyuser.base.BaseActivity
import com.edmark.loyaltyuser.util.AppPreference
import com.edmark.loyaltyuser.view.activity.login.LoginActivity

import com.edmark.loyaltyuser.constant.Common.DISTRIBUTOR
import com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE
import com.edmark.loyaltyuser.constant.Common.KEY_FIRST_TIME_LAUNCH
import com.edmark.loyaltyuser.constant.Common.KEY_REMEMBERED
import com.edmark.loyaltyuser.constant.Common.MERCHANT
import kotlinx.android.synthetic.main.activity_onboarding.*

/*
 *  Created by DEV-Michael-ED2E on 05/23/2018.
 */

class OnboardingActivity : BaseActivity() {

    private var layouts: IntArray? = null
    private var prefManager: AppPreference? = null

    //  viewpager change listener
    private var viewPagerPageChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {

        override fun onPageSelected(position: Int) {
            addBottomDots(position)

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts!!.size - 1) {
                // last page. make button text to GOT IT
                activity_onboarding_button_next.text = getString(R.string.button_label_finish)
                activity_onboarding_button_skip.visibility = View.GONE
            } else {
                // still pages are left
                activity_onboarding_button_next.text = getString(R.string.button_label_next)
                activity_onboarding_button_skip.visibility = View.VISIBLE
            }
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

        override fun onPageScrollStateChanged(arg0: Int) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Checking for first time launch - before calling setContentView()
        prefManager = AppPreference.getInstance(this)
        if (!prefManager!!.getBoolean(KEY_FIRST_TIME_LAUNCH, true)) {
            launchHomeScreen()
            finish()
        }

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        setContentView(R.layout.activity_onboarding)

        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = intArrayOf(R.layout.child_onboarding, R.layout.child_onboarding, R.layout.child_onboarding, R.layout.child_onboarding)

        // adding bottom dots
        addBottomDots(0)

        // making notification bar transparent
        changeStatusBarColor()

        val adapter = OnBoardingViewPagerAdapter(this@OnboardingActivity, layouts)
        activity_onboarding_view_pager.adapter = adapter
        activity_onboarding_view_pager.addOnPageChangeListener(viewPagerPageChangeListener)

        activity_onboarding_button_skip.setOnClickListener { launchHomeScreen() }
        activity_onboarding_button_next.setOnClickListener {
            // checking for last page
            // if last page home screen will be launched
            val current = getItem(+1)
            if (current < layouts!!.size) {
                // move to next screen
                activity_onboarding_view_pager.currentItem = current
            } else {
                launchHomeScreen()
            }
        }
    }

    private fun addBottomDots(currentPage: Int) {
        val dots = arrayOfNulls<TextView>(layouts!!.size)

        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)

        activity_onboarding_button_layout_dots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]!!.text = resources.getString(R.string.label_dot)
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(colorsInactive[currentPage])
            activity_onboarding_button_layout_dots.addView(dots[i])
        }

        if (dots.isNotEmpty())
            dots[currentPage]!!.setTextColor(colorsActive[currentPage])
    }

    private fun getItem(i: Int): Int {
        return activity_onboarding_view_pager.currentItem + i
    }

    private fun launchHomeScreen() {
        prefManager!!.putBoolean(KEY_FIRST_TIME_LAUNCH, false)
        initIntent()
    }

    /**
     * Making notification bar transparent
     */
    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
    }

    private fun initIntent() {
        val preference = AppPreference.getInstance(this)
        if (preference.getBoolean(KEY_REMEMBERED, false)) {
            val type = preference.getString(KEY_ACCOUNT_TYPE, "")
            if (type == "") {
                startActivity(Intent(context, LoginActivity::class.java))
                finish()
            } else {
                if (type == DISTRIBUTOR) {
                    Log.d(TAG, "this is distributor show card picker:")
                    val intent = Intent(context, MainActivity::class.java)
                    intent.putExtra(KEY_ACCOUNT_TYPE, DISTRIBUTOR)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(context, MainActivity::class.java)
                    intent.putExtra(KEY_ACCOUNT_TYPE, MERCHANT)
                    startActivity(intent)
                    finish()
                }
            }
        } else {
            startActivity(Intent(context, LoginActivity::class.java))
            finish()
        }
    }

    companion object {
        private val TAG = OnboardingActivity::class.java.simpleName
    }
}