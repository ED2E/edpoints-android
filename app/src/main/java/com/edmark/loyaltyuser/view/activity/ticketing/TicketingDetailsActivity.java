package com.edmark.loyaltyuser.view.activity.ticketing;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Ads;
import com.edmark.loyaltyuser.model.Ticketing;
import com.edmark.loyaltyuser.util.AppUtility;
import com.edmark.loyaltyuser.view.activity.main.MainActivity;
import com.edmark.loyaltyuser.view.activity.main.WebviewActivity;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionActivity;
import com.google.gson.Gson;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.KEY_ADS_DETAILS;
import static com.edmark.loyaltyuser.constant.Common.KEY_DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANS_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.STATUS_ACTIVE;
import static com.edmark.loyaltyuser.constant.Common.URL_FAIL;
import static com.edmark.loyaltyuser.constant.Common.URL_HEADER;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;

/*
 * Created by DEV-Michael-ED2E on 04/27/2018.
 */
@SuppressWarnings("unchecked")
public class TicketingDetailsActivity extends BaseActivity {
    public static final String TAG = TicketingDetailsActivity.class.getSimpleName();

    @BindView(R.id.activity_ticketing_detail_textview_name)
    TextView textView_name;
    @BindView(R.id.activity_ticketing_detail_textview_content)
    TextView textView_content;
    @BindView(R.id.activity_ticketing_detail_textview_time)
    TextView textView_time;
    @BindView(R.id.activity_ticketing_detail_textview_rate)
    TextView textView_rate;
    @BindView(R.id.activity_ticketing_detail_imageview_featuredimage)
    ImageView imageView_featuredimage;
    @BindView(R.id.activity_ticketing_detail_button_buy)
    Button button;

    private String merchant_code;
    private boolean mPageFinished = false;
    private Ticketing adData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticketing_detail);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_ticketing_detail_toolbar);
        merchant_code = initData(savedInstanceState);
        initGui();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private String initData(Bundle savedInstanceState) {
        Gson gson = new Gson();
        String merchant_code;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchant_code = null;
                adData = null;
            } else {
                merchant_code = extras.getString(KEY_MERCHANT_CODE);
                adData = gson.fromJson(extras.getString(KEY_ADS_DETAILS), Ticketing.class);
            }
        } else {
            merchant_code = (String) savedInstanceState.getSerializable(KEY_MERCHANT_CODE);
            adData = gson.fromJson((String) savedInstanceState.getSerializable(KEY_ADS_DETAILS), Ticketing.class);
        }
        return merchant_code;
    }

    private void initGui() {
        if (adData != null) {
            Glide.with(getContext()).load(adData.getImage_url()).into(imageView_featuredimage);

            textView_name.setText(adData.getEvent_name());
            textView_time.setText(getResources().getString(R.string.label_time,adData.getTime()));
            textView_content.setText(getResources().getString(R.string.label_date,adData.getDate()));
            textView_rate.setText(getResources().getString(R.string.label_regular_rate,getResources().getString(R.string.label_point,decimalOutputFormat(adData.getRate()))));
//            textView_content.setText(Html.fromHtml(adData.getContent()));
//            String url;
//            if (Objects.requireNonNull(adData.getWebsite_url()).contains(URL_HEADER))
//                url = adData.getWebsite_url();
//            else
//                url = URL_HEADER + adData.getWebsite_url();
            button.setOnClickListener(v->{
                Intent intent = new Intent(getContext(), TransactionActivity.class);
                intent.putExtra(KEY_TRANS_CUSTID, adData.getCustid());
                intent.putExtra(KEY_TRANSACTION_AMOUNT, adData.getRate());
                intent.putExtra(KEY_DESCRIPTION, adData.getEvent_name());
                startActivity(intent);
            });

            if(adData.getStatus().equals(STATUS_ACTIVE))
                button.setEnabled(true);
            else
                button.setEnabled(false);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_merchant_details, menu);//Menu Resource, Menu
        MenuItem item = menu.findItem(R.id.action_location);
        if (merchant_code != null) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:
                Intent returnIntent = new Intent();
                returnIntent.putExtra(KEY_MERCHANT_CODE, merchant_code);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
