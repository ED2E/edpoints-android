package com.edmark.loyaltyuser.view.fragment.main;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.CustomerInfo;
import com.edmark.loyaltyuser.model.HomeMessageEvent;
import com.edmark.loyaltyuser.model.MainMessageEvent;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImageContract;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImagePresenter;
import com.edmark.loyaltyuser.ui.imageview.CircleImageView;
import com.glide.slider.library.svg.GlideApp;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.DISTRIBUTOR;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_ED_POINT;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_INFO;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_LOYALTY_POINT;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_MASSAGE_CHAIR;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_PROFILE_IMAGE;
import static com.edmark.loyaltyuser.constant.Common.TAG_FRAGMENT_HOME;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_ACCOUNT;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_ADVERTISEMENT;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_COUNTRY;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_EDCOIN;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_FAMILY_TREE;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_KEY_IN;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_LEDGER;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_LOG_OUT;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_MASSAGE_CHAIR;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_POINT_TRANSFER;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_RELOAD;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_SCAN;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_SHOW_QR;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_SUPPORT;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_TICKETING;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_STATISTICAL_ANALYSIS;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_UTILITIES;
import static com.edmark.loyaltyuser.util.AppUtility.capitalize;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.getCompanyFlagDrawable;

/*
 * Created by DEV-Michael-ED2E on 04/04/2018.
 */
@SuppressWarnings("unchecked")
public class HomeFragment extends Fragment implements LoadImageContract {
    public static final String TAG = HomeFragment.class.getSimpleName();
    @BindView(R.id.fragment_home_layout_control_button)
    LinearLayout layout_control_button;
    @BindView(R.id.fragment_home_layout_Logout)
    LinearLayout layout_logout;
    @BindView(R.id.fragment_home_layout_massage_chair)
    LinearLayout layout_massage_chair;
    @BindView(R.id.fragment_home_layout_reload)
    LinearLayout layout_reload;
    @BindView(R.id.fragment_home_textview_user_type)
    TextView textView_user_type;
    @BindView(R.id.fragment_home_textview_name)
    TextView textView_name;
    @BindView(R.id.fragment_home_textview_eda)
    TextView textView_eda;
    @BindView(R.id.fragment_home_textview_ed_points)
    TextView textView_ed_points;
    @BindView(R.id.fragment_home_textview_loyalty_points)
    TextView textView_loyaty_points;
    @BindView(R.id.fragment_home_textview_loyalty_points_title)
    TextView textView_loyaty_points_title;
    @BindView(R.id.fragment_home_textview_ed_points_title)
    TextView textView_ed_points_title;
    @BindView(R.id.fragment_home_imageview_profile)
    CircleImageView imageView_profile;
    @BindView(R.id.fragment_home_imageview_show_qr)
    ImageView imageView_show_qr;
    @BindView(R.id.fragment_home_imageview_flag)
    ImageView imageView_flag;
    @BindView(R.id.fragment_home_progress_profile)
    ProgressBar progressBar;
    private Unbinder unbinder;
    private LoadImagePresenter imageLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        initGUI();
        return view;
    }

    @SuppressLint("InflateParams")
    private void initGUI() {
        imageLoader = new LoadImagePresenter(this);
        Log.d(TAG, "initGUI");
        View child_type;
        String account_type = Objects.requireNonNull(this.getTag()).replace(TAG_FRAGMENT_HOME + ":", "");
        textView_user_type.setText(capitalize(account_type));
        if (account_type.equals(DISTRIBUTOR)) {
            child_type = getLayoutInflater().inflate(R.layout.layout_control_distributor, null);
            RelativeLayout button_account = child_type.findViewById(R.id.fragment_home_distributor_account);
            RelativeLayout button_scan = child_type.findViewById(R.id.fragment_home_distributor_scan);
            RelativeLayout button_key_in = child_type.findViewById(R.id.fragment_home_distributor_keyin);
            RelativeLayout button_advertisement = child_type.findViewById(R.id.fragment_home_distributor_advertisement);
            RelativeLayout button_ledger = child_type.findViewById(R.id.fragment_home_distributor_ledger);
            RelativeLayout button_transaction = child_type.findViewById(R.id.fragment_home_distributor_transaction);
            RelativeLayout button_support = child_type.findViewById(R.id.fragment_home_distributor_support);
            RelativeLayout button_utilities = child_type.findViewById(R.id.fragment_home_distributor_utilities);
            RelativeLayout button_ticketing = child_type.findViewById(R.id.fragment_home_distributor_ticketing);
            //TODO: remove code for country
            //RelativeLayout button_country = child_type.findViewById(R.id.fragment_home_distributor_country);
            RelativeLayout button_family_tree = child_type.findViewById(R.id.fragment_home_distributor_family_tree);
            RelativeLayout button_edcoin = child_type.findViewById(R.id.fragment_home_distributor_edcoin);
            RelativeLayout button_point_transfer = child_type.findViewById(R.id.fragment_home_distributor_point_transfer);

            button_account.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_ACCOUNT, null)));
            button_scan.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_SCAN, null)));
            button_advertisement.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_ADVERTISEMENT, null)));
            button_key_in.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_KEY_IN, null)));
            button_ledger.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_LEDGER, null)));
            button_transaction.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_TRANSACTION, null)));
            button_support.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_SUPPORT, null)));
            button_utilities.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_UTILITIES, null)));
            button_ticketing.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_TICKETING, null)));
            //TODO: remove code for country
            //button_country.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_COUNTRY, null)));
            button_family_tree.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_FAMILY_TREE, null)));
            button_edcoin.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_EDCOIN, null)));
            button_point_transfer.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_POINT_TRANSFER, null)));

            if (getContext() != null)
                textView_ed_points_title.setText(getContext().getResources().getString(R.string.label_ed_point));
        } else {
            child_type = getLayoutInflater().inflate(R.layout.layout_control_merchant, null);
            RelativeLayout button_advertisement = child_type.findViewById(R.id.fragment_home_merchant_advertisement);
            RelativeLayout button_statistical = child_type.findViewById(R.id.fragment_home_merchant_statistical_analysis);
            RelativeLayout button_transaction = child_type.findViewById(R.id.fragment_home_merchant_transaction);
            RelativeLayout button_support = child_type.findViewById(R.id.fragment_home_merchant_support);

            button_advertisement.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_ADVERTISEMENT, null)));
            button_statistical.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_STATISTICAL_ANALYSIS, null)));
            button_transaction.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_TRANSACTION, null)));
            button_support.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_SUPPORT, null)));

            if (getContext() != null)
                textView_ed_points_title.setText(getContext().getResources().getString(R.string.label_ed_point_received));
            textView_loyaty_points_title.setVisibility(View.GONE);
            textView_loyaty_points.setVisibility(View.GONE);
            layout_reload.setVisibility(View.INVISIBLE);
        }

        imageView_show_qr.setVisibility(View.VISIBLE);
        layout_control_button.addView(child_type);
        layout_logout.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_LOG_OUT, null)));
        imageView_profile.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_PROFILE, null)));
        imageView_show_qr.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_SHOW_QR, null)));
        layout_reload.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_RELOAD, null)));
        layout_massage_chair.setOnClickListener(v -> EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_MASSAGE_CHAIR, null)));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MainMessageEvent event) {
        Timber.d("onMessageEvent:" + event.getMessage());
        switch (event.getMessage()) {
            case EVENT_SET_ED_POINT: {
                textView_ed_points.setText(decimalOutputFormat(String.valueOf(event.getData())));
                break;
            }
            case EVENT_SET_INFO: {
                Gson gson = new Gson();
                CustomerInfo info = gson.fromJson(event.getData(), CustomerInfo.class);
                textView_name.setText(info.getName());
                textView_eda.setText(info.getEda());
                if (info.getCompany_code() != null) {
                    Timber.d("getCompany_code:" + info.getCompany_code());
                    int resourceId = getCompanyFlagDrawable(getContext(), info.getCompany_code());
                    imageView_flag.setImageResource(resourceId);
                } else {
                    Timber.d("Show other flag:");
                }
                break;

            }
            case EVENT_SET_LOYALTY_POINT: {
                textView_loyaty_points.setText(decimalOutputFormat(String.valueOf(event.getData())));
                break;
            }
            case EVENT_SET_PROFILE_IMAGE: {

                progressBar.setVisibility(View.VISIBLE);
                imageLoader.loadImage(getContext(), TAG, 0, event.getData(), imageView_profile, R.drawable.ic_default_user, true);
                break;
            }
            case EVENT_SET_MASSAGE_CHAIR: {
                if (event.getData() == null) {
                    layout_massage_chair.setVisibility(View.GONE);
                } else {
                    layout_massage_chair.setVisibility(View.VISIBLE);
                    layout_massage_chair.clearAnimation();
                    Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.anim_blink);
                    animation.setRepeatCount(Animation.INFINITE);
                    layout_massage_chair.startAnimation(animation);
                }
                break;
            }


        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView()");
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        Timber.d("onStop()");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLoadfailed(int requestCode) {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResourceready(int requestCode) {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }
}