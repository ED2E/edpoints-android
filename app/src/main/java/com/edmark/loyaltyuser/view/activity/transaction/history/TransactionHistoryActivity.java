package com.edmark.loyaltyuser.view.activity.transaction.history;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewSectionedAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.TransactionHistory;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_TRANSACTION_HISTORY;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.getDate;
import static com.edmark.loyaltyuser.util.AppUtility.getDateSection;
import static com.edmark.loyaltyuser.util.AppUtility.getFormattedDate;
import static com.edmark.loyaltyuser.util.AppUtility.getTimeinMilli;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/18/2018.
 */
@SuppressWarnings("unchecked")
public class TransactionHistoryActivity extends BaseActivity {
    public static final String TAG = TransactionHistoryActivity.class.getSimpleName();


    @BindView(R.id.activity_transaction_history_textview_date_start)
    TextView textView_date_from;
    @BindView(R.id.activity_transaction_history_textview_date_end)
    TextView textView_date_to;
    @BindView(R.id.activity_transaction_history_textview_result_count)
    TextView textView_result_count;
    @BindView(R.id.activity_transaction_history_textview_total_point)
    TextView textView_total_point;
    @BindView(R.id.activity_transaction_history_textview_total_spent)
    TextView textView_total_spent;
    @BindView(R.id.activity_transaction_history_textview_message)
    TextView textView_message;
    @BindView(R.id.activity_transaction_history_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.activity_transaction_history_button_calendar)
    ImageView button_calendar;
    @BindView(R.id.activity_transaction_history_button_search)
    ImageView button_search;
    @BindView(R.id.activity_transaction_history_textview_spent_title)
    TextView textView_spend_title;
    private BaseRecyclerViewAdapter adapter;
    private BaseRecyclerViewSectionedAdapter mSectionedAdapter;

    @BindView(R.id.activity_transaction_history_toolbar_title)
    TextView textView_tootbar_title;

    private ApiInterface apiService;
    private AppPreference preference;
    private String account_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        initLoadType(savedInstanceState);
        setupToolBar(R.id.activity_transaction_history_toolbar);
        initGui();

    }

    private void initLoadType(Bundle savedInstanceState) {

        account_type = preference.getString(KEY_ACCOUNT_TYPE, "");

        /*if (savedInstanceState == null) {
            showToast("if");
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                showToast("null");
                account_type = "";
            } else {
                showToast("w/ value");
                account_type = extras.getString(KEY_ACCOUNT_TYPE);
            }
        } else {
            showToast("else");
            account_type = (String) savedInstanceState.getSerializable(KEY_ACCOUNT_TYPE);
        }*/
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGui() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        button_calendar.setOnClickListener(v ->
        {
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                Timber.d("cal" + cal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            initDatePicker(true, cal, null);
        });
        button_search.setOnClickListener(v ->
        {
            if ((textView_date_from.getText().length() != 0 && textView_date_to.getText().length() != 0)
                    || (!textView_date_from.getText().toString().isEmpty() && !textView_date_to.getText().toString().isEmpty())
                    || (!textView_date_from.getText().toString().equals("") && !textView_date_to.getText().toString().equals(""))) {

                initAPI();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.no_date), Toast.LENGTH_SHORT).show();
            }
        });

        textView_date_to.setOnClickListener(v -> {

            Calendar cal = Calendar.getInstance();
            Calendar calMin = Calendar.getInstance();
            try {
                calMin.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                calMin.add(Calendar.DATE, 1);  // number of days to add
                Timber.d("cal" + calMin.getTime());
                cal.setTime(getFormattedDate(textView_date_to.getText().toString(), DATE_FORMAT));// all done
                Timber.d("cal" + cal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            initDatePicker(false, cal, calMin);
        });

        textView_date_from.setOnClickListener(v -> {
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                Timber.d("cal" + cal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            initDatePicker(true, cal, null);
        });


        textView_date_from.setText(getFormattedDate(new Date(), DATE_FORMAT));
        textView_date_to.setText(getFormattedDate(new Date(), DATE_FORMAT));
        initAPI();

        if(account_type != null){
            if(account_type.equals(MERCHANT))
            {
                textView_spend_title.setVisibility(View.GONE);
                textView_total_spent.setVisibility(View.GONE);
            }
        }
    }

    public void setFromDate(int year, int month, int day) {
        textView_date_from.setText(new StringBuilder().append(month).append("-").append(day).append("-").append(year));
    }

    public void setToDate(int year, int month, int day) {
        textView_date_to.setText(new StringBuilder().append(month).append("-").append(day).append("-").append(year));

        if ((textView_date_from.getText().length() != 0 && textView_date_to.getText().length() != 0)
                || (!textView_date_from.getText().toString().isEmpty() && !textView_date_to.getText().toString().isEmpty())
                || (!textView_date_from.getText().toString().equals("") && !textView_date_to.getText().toString().equals(""))) {

            initAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_date), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void initAPI() {
        showProgress();
//        String type = preference.getString(KEY_ACCOUNT_TYPE, "");
        String custId = preference.getString(KEY_CUSID);
//        String custId = "1354881";//TODO to be remove
//        String type = "merchant";//TODO to be remove
        String dateFrom = "";
        String dateTo = "";
        try {
            dateFrom = getTimeinMilli(textView_date_from.getText().toString(), DATE_FORMAT);
            dateTo = getTimeinMilli(textView_date_to.getText().toString(), DATE_FORMAT);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Call<ResponseBody> call = apiService.transactions(API_TRANSACTION_HISTORY, custId, account_type, dateFrom, dateTo, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    textView_message.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = response.body().string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            String message = (String) obj.get(MESSAGE);
                            Timber.d("message:" + message);
                            setNoResult();
                            hideProgress();
                        } catch (JSONException e) {

                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            ArrayList<TransactionHistory> data = new ArrayList<>();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Gson gson = new Gson();
                                TransactionHistory bonus = gson.fromJson(jsonobject.toString(), TransactionHistory.class);
                                bonus.setType(account_type);
                                Timber.d("Data:" + gson.toJson(bonus));
                                data.add(bonus);
                            }

                            setData(data);

                        }
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    hideProgress();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException||t instanceof ConnectException){
                    textView_message.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
            }
        });


    }


    private void initDatePicker(boolean fromDate, Calendar calendar, Calendar calendarMin) {
        if (calendar == null)
            calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {
            if (fromDate) {
                setFromDate(year, month + 1, dayOfMonth);

                Calendar cal = Calendar.getInstance();
                Calendar calMin = Calendar.getInstance();
                try {
                    calMin.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                    calMin.add(Calendar.DATE, 1);  // number of days to add
                    Timber.d("cal" + calMin.getTime());
                    cal.setTime(getFormattedDate(textView_date_to.getText().toString(), DATE_FORMAT));// all done
                    Timber.d("cal" + cal.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                initDatePicker(false, cal, calMin);
            } else {
                setToDate(year, month + 1, dayOfMonth);
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        if (!fromDate) {
            Date newDate = calendarMin.getTime();
            datePickerDialog.getDatePicker().setMinDate(newDate.getTime() - (newDate.getTime() % (24 * 60 * 60 * 1000)));
            datePickerDialog.setTitle(R.string.dialog_date_to);
        }
        else
        {
            datePickerDialog.setTitle(R.string.dialog_date_from);
        }
        datePickerDialog.show();
    }

    private void setNoResult() {
        textView_message.setVisibility(View.VISIBLE);
        textView_message.setText(getResources().getString(R.string.label_no_result));
        textView_result_count.setText(String.valueOf(0));
        textView_total_point.setText(getResources().getString(R.string.label_point_loyalty2, "0.0"));
        textView_total_spent.setText(getResources().getString(R.string.label_point, "0.0"));
        if (adapter != null) {
            adapter.clear();
        }
        if (mSectionedAdapter != null) {
            mSectionedAdapter.notifyDataSetChanged();
        }
    }

    private void setData(ArrayList<TransactionHistory> data) {
        textView_message.setVisibility(View.GONE);
        adapter = new BaseRecyclerViewAdapter(data, R.layout.child_transaction, BR.TransactionHistory, (v, item, position) ->
        {
            Timber.d("data position:" + data.get(position).getTransaction_id());
            Timber.d("data position:" + data.get(position).getDist_name());
        });
        //Add your adapter to the sectionAdapter
        mSectionedAdapter = new BaseRecyclerViewSectionedAdapter(Objects.requireNonNull(getContext()), R.layout.child_header, R.id.child_header_text, adapter);
        //Apply this adapter to the RecyclerView
        recyclerView.setAdapter(mSectionedAdapter);
        //Get all dates for the header/section
        ArrayList<String> dates = new ArrayList<>();
        double totalPoint = 0.00;
        double totalSpentPoint = 0.00;
        for (TransactionHistory items : data) {
            try {
                dates.add(getDate(items.getDate(), DATE_FORMAT));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try{
                totalPoint = totalPoint + Double.valueOf(items.getLoyalty_earn_point().replace(",", "").replace(" ", ""));
            }catch (Exception e)
            {
                totalPoint = totalPoint + Double.valueOf(items.getLoyalty_earn_point_2().replace(",", "").replace(" ", ""));
            }

            try{
                totalSpentPoint = totalSpentPoint + Double.valueOf(items.getTotal_spending_amount().replace(",", "").replace(" ", ""));
            }catch (Exception e)
            {
                totalSpentPoint = totalSpentPoint + Double.valueOf(items.getTotal_spending_amount_2().replace(",", "").replace(" ", ""));
            }

        }

        String totalPointDisplay = getResources().getString(R.string.label_point_loyalty2, decimalOutputFormat(String.valueOf(totalPoint)));
        String totalSpentDisplay = getResources().getString(R.string.label_point, decimalOutputFormat(String.valueOf(totalSpentPoint)));
        textView_total_spent.setText(totalSpentDisplay);
        if(account_type.equals(MERCHANT))
        {
            textView_total_point.setText(totalSpentDisplay);
        }
        else
        {
            textView_total_point.setText(totalPointDisplay);
        }
        textView_result_count.setText(String.valueOf(data.size()));

        Collections.sort(data, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        adapter.animateTo(data);

        //Setting up the headers/sections
        List<BaseRecyclerViewSectionedAdapter.Section> sections = new ArrayList<>();

        for (String date : getDateSection(dates)) {
//            int occurrences = Collections.frequency(data, new Tier(date));
//            Timber.d("Date:" + date + " occurrences:" + occurrences);
            int position = 0;
            for (int i = 0; i < data.size(); i++) {
                try {
                    if (getDate(data.get(i).getDate(), DATE_FORMAT).equals(date)) {
                        position = i;
                        break;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            sections.add(new BaseRecyclerViewSectionedAdapter.Section(position, date));
        }

        mSectionedAdapter.setSections(sections.toArray(new BaseRecyclerViewSectionedAdapter.Section[sections.size()]));
        mSectionedAdapter.notifyDataSetChanged();
        adapter.setSectionAdapter(mSectionedAdapter);
        hideProgress();
    }
}
