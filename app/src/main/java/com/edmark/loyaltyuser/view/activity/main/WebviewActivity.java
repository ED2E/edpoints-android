package com.edmark.loyaltyuser.view.activity.main;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.util.AppUtility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.CONSENT_CLAUSE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.KEY_WEBVIEW_ACTION;
import static com.edmark.loyaltyuser.constant.Common.PRIVACY_POLICY;
import static com.edmark.loyaltyuser.constant.Common.TERMS_CONDITIONS;
import static com.edmark.loyaltyuser.rest.ApiClient.BASE_URL;


/*
 * Created by DEV-Michael-ED2E on 04/24/2018.
 */
public class WebviewActivity extends BaseActivity {
    private String TAG = WebviewActivity.class.getSimpleName();

    @BindView(R.id.activity_webview) WebView webView;
    @BindView(R.id.activity_webview_Progressbar) ProgressBar progressbar_loading;
    @BindView(R.id.activity_webview_toolbar_title) TextView textView_tootbar_title;
    private String mUrl;
    private String mToolTitle = "";
    private boolean isGetPasswordUrl = false;            // Flags to handle when to inflate alert dialog
    private View noInternet;


    private String mCM;
    private ValueCallback mUM;
    private ValueCallback<Uri[]> mUMA;
    private final static int FCR=1;

    //select whether you want to upload multiple files
    private boolean multiple_files = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        //TODO clean this class
        if (getIntent() != null) {
            mUrl = getIntent().getStringExtra(KEY_URL);
            mToolTitle = getIntent().getStringExtra(KEY_TITLE);
            if(mUrl==null)
            {
                Uri data = getIntent().getData();
                if (data != null) {
                    Timber.i("LOG", "FULL URI: " + data.toString());
                    mUrl = data.toString();
                    switch (mUrl) {
                        case PRIVACY_POLICY:
                            mToolTitle = getResources().getString(R.string.label_privacy_policy);
                            break;
                        case TERMS_CONDITIONS:
                            mToolTitle = getResources().getString(R.string.label_terms_condition);
                            break;
                        case CONSENT_CLAUSE:
                            mToolTitle = getResources().getString(R.string.label_consent_clause);
                            break;
                    }
                    mUrl = mUrl.replace(KEY_WEBVIEW_ACTION,"");
                }
            }
            Timber.v("Url is ", mToolTitle +" "+ mUrl);
        }
        initGUI();
    }

    @SuppressLint({"SetJavaScriptEnabled", "WrongViewCast"})
    private void initGUI() {
        initToolBar();

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);

        if(Build.VERSION.SDK_INT >= 21){
            webSettings.setMixedContentMode(0);
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }else if(Build.VERSION.SDK_INT >= 19){
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.setWebViewClient(new CustomWebViewClient());
        webView.loadUrl(mUrl);
        webView.setWebChromeClient(new WebChromeClient(){
            //For Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg){
                mUM = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                WebviewActivity.this.startActivityForResult(Intent.createChooser(i,"File Chooser"), FCR);
            }
            // For Android 3.0+, above method not supported in some android 3+ versions, in such case we use this
            public void openFileChooser(ValueCallback uploadMsg, String acceptType){
                mUM = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                WebviewActivity.this.startActivityForResult(
                        Intent.createChooser(i, "File Browser"),
                        FCR);
            }
            //For Android 4.1+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture){
                mUM = uploadMsg;
//                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//                i.addCategory(Intent.CATEGORY_OPENABLE);
//                i.setType("image/*");

                Intent i;
                if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                    i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                } else {
                    i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                }
                i.putExtra("return-data", true);
                i.setType("image/*");
                if(multiple_files) {
                    i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                }
                WebviewActivity.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), FCR);
            }
            //For Android 5.0+
            public boolean onShowFileChooser(
                    WebView webView, ValueCallback<Uri[]> filePathCallback,
                    FileChooserParams fileChooserParams){
                if(mUMA != null){
                    mUMA.onReceiveValue(null);
                }
                mUMA = filePathCallback;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(takePictureIntent.resolveActivity(WebviewActivity.this.getPackageManager()) != null){
                    File photoFile = null;
                    try{
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCM);
                    }catch(IOException ex){
                        Log.e(TAG, "Image file creation failed", ex);
                    }
                    if(photoFile != null){
                        mCM = "file:" + photoFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    }else{
                        takePictureIntent = null;
                    }
                }
                //Original
//                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
//                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
//                contentSelectionIntent.setType("image/*");
//                if(multiple_files) {
//                    contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                }


                Intent contentSelectionIntent;
                if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                    contentSelectionIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                } else {
                    contentSelectionIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                }
                contentSelectionIntent.putExtra("return-data", true);
                contentSelectionIntent.setType("image/*");
                if(multiple_files) {
                    contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                }

                Intent[] intentArray;
                if(takePictureIntent != null){
                    intentArray = new Intent[]{takePictureIntent};
                }else{
                    intentArray = new Intent[0];
                }

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                if(multiple_files && Build.VERSION.SDK_INT >= 18) {
                    chooserIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                }
                startActivityForResult(chooserIntent, FCR);
                return true;
            }
        });
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        if (webView.canGoBack()) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            webView.goBack();
            Timber.v(TAG, webView.getUrl());
        } else {
            finish();
        }
    }

    private void initToolBar()
    {
        setupToolBar(R.id.activity_webview_toolbar);
        textView_tootbar_title.setText(mToolTitle);
    }

    public class CustomWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(final WebView view, int errorCode,
                                    String description, final String failingUrl) {
            Timber.e("errorCode", String.valueOf(errorCode));
            noInternet = View.inflate(WebviewActivity.this, R.layout.view_no_internet, null);
            noInternet.findViewById(R.id.refresh_button_new).setOnClickListener(v -> {
                view.loadUrl(failingUrl);
                view.removeView(noInternet);
            });
            if (view.findViewById(noInternet.getId()) == null) {
                view.loadUrl("file:///android_asset/fail.html");
                view.addView(noInternet);
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            progressbar_loading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressbar_loading.setVisibility(View.GONE);
            if(!AppUtility.getUrlTitle(webView.getUrl()).equals(""))
            {
                textView_tootbar_title.setText(AppUtility.getUrlTitle(webView.getUrl()));
            }
            Timber.d(TAG,"onPageFinished:"+webView.getUrl());
            Timber.d(TAG,"onPageFinished:"+AppUtility.getUrlTitle(webView.getUrl()));
            if (isGetPasswordUrl) {
                isGetPasswordUrl = false;
                initPinDialog();
            }
            if (webView.getUrl().contains(BASE_URL + "e-commerce/index.php/checkout/onepage/success")) {
                textView_tootbar_title.setText(getString(R.string.reward_order_success));
            }
        }

        /**
         * Any changes done in this method need to be done for Android N method as well just below to this
         * This will work for Below Android 7.0 (Nougat)
         *
         * @param view - the webView
         * @param url  - url to be redirected
         * @return - status
         */
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        webView.removeView(noInternet);

        if (webView.canGoBack()) {
            webView.goBack();
            Timber.v(TAG, webView.getUrl());
        } else {
            super.onBackPressed();
        }
    }
    private void initPinDialog() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Should check the API", Toast.LENGTH_SHORT).show();
            }
        });
        button_dismiss.setOnClickListener(v ->
                dialog.dismiss());
        dialog.show();
    }


    public class Callback extends WebViewClient{
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
            Toast.makeText(getApplicationContext(), "Failed loading app!", Toast.LENGTH_SHORT).show();
        }
    }
    // Create an image file
    private File createImageFile() throws IOException{
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "img_"+timeStamp+"_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName,".jpg",storageDir);
    }
    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event){
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode){
                case KeyEvent.KEYCODE_BACK:
                    if(webView.canGoBack()){
                        webView.goBack();
                    }else{
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(Build.VERSION.SDK_INT >= 21){
            Uri[] results = null;
            //Check if response is positive
            if(resultCode== Activity.RESULT_OK){
                if(requestCode == FCR){
                    if(null == mUMA){
                        return;
                    }
                    if(intent == null || intent.getData() == null){
                        //Capture Photo if no image available
                        if(mCM != null){
                            results = new Uri[]{Uri.parse(mCM)};
                        }
                    }else{
                        String dataString = intent.getDataString();
                        if(dataString != null){
                            results = new Uri[]{Uri.parse(dataString)};
                        } else {
                            if(multiple_files) {
                                if (intent.getClipData() != null) {
                                    final int numSelectedFiles = intent.getClipData().getItemCount();
                                    results = new Uri[numSelectedFiles];
                                    for (int i = 0; i < numSelectedFiles; i++) {
                                        results[i] = intent.getClipData().getItemAt(i).getUri();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            mUMA.onReceiveValue(results);
            mUMA = null;
        }else{
            if(requestCode == FCR){
                if(null == mUM) return;
                Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                mUM.onReceiveValue(result);
                mUM = null;
            }
        }
    }
}
