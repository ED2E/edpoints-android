package com.edmark.loyaltyuser.view.activity.account;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CardType;
import com.edmark.loyaltyuser.presenter.account.AccountContract;
import com.edmark.loyaltyuser.presenter.account.AccountPresenter;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edmark.loyaltyuser.constant.Common.API_SIGN_UP_SUB_ACC;
import static com.edmark.loyaltyuser.constant.Common.API_UPGRADE_PACKAGE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_SWITCH;
import static com.edmark.loyaltyuser.constant.Common.STATUS_CODE_ACTIVE;
import static com.edmark.loyaltyuser.constant.Common.STATUS_CODE_INACTIVE;
import static com.edmark.loyaltyuser.util.AppUtility.capitalize;
import static com.edmark.loyaltyuser.util.AppUtility.getCardDrawable;

/*
 * Created by DEV-Michael-ED2E on 04/12/2018.
 */

@SuppressWarnings("unchecked")
public class AccountActivity extends BaseActivity implements AccountContract.View {
    public static final String TAG = AccountActivity.class.getSimpleName();

    private AccountContract.Presenter presenter;

    @BindView(R.id.activity_account_textview_eda)
    TextView textView_eda;
    @BindView(R.id.activity_account_textview_package_type)
    TextView textView_package_type;
    @BindView(R.id.activity_account_imageview_card)
    ImageView imageView_card;
    @BindView(R.id.activity_account_textview_country)
    TextView textView_country;
    @BindView(R.id.activity_account_textview_total_account)
    TextView textView_total_account;
    @BindView(R.id.activity_account_recyclerview_card_list)
    RecyclerView recyclerView_card_list;
    @BindView(R.id.activity_account_button_upgrade)
    Button button_upgrade;
    @BindView(R.id.activity_account_button_switch)
    Button button_switch;
    @BindView(R.id.activity_account_toolbar_add)
    ImageView imageView_add;

    private ArrayList<CardType> datalist = new ArrayList<>();
    private BaseRecyclerViewAdapter adapter;
    AppPreference preference;
    private CardType currentCard;
    private int currentSelected = -1;
    private Dialog dialog;
    private static boolean isactive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);

        presenter = new AccountPresenter(this);
        preference = AppPreference.getInstance(this);
        setupToolBar(R.id.activity_account_toolbar);
        initGUI();
        presenter.loadAccount();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    @Override
    protected void onDialogDismiss(int requestCode) {
        super.onDialogDismiss(requestCode);

        if (requestCode == REQUEST_CODE_SWITCH) {
            for (CardType card : datalist) {
                if (card.isStatus().equals(STATUS_CODE_ACTIVE)) {
                    Log.d(TAG, new Gson().toJson(card, CardType.class));
                    preference.putString(KEY_CUSID, String.valueOf(card.getCustid()));
                    preference.putString(KEY_EDCOIN_BIND_KEY, "");
                    finish();
                }
            }
        } else {
            presenter.loadAccount();
        }
    }

    private void initGUI() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new BaseRecyclerViewAdapter(datalist, R.layout.child_account_selection, BR.CardType, (v, item, position) ->
        {
            currentSelected = position;
            for (int i = 0; i < datalist.size(); i++) {
                String package_type = datalist.get(i).getPackage_type();
                String eda = datalist.get(i).getEda();
                String name = datalist.get(i).getName();
                int custid = datalist.get(i).getCustid();
                String isActive = STATUS_CODE_INACTIVE;
                if (custid == datalist.get(position).getCustid()) {
                    isActive = STATUS_CODE_ACTIVE;
                }
                CardType cardType = new CardType(package_type, eda, name, custid, isActive);
                datalist.set(i, cardType);
            }
            adapter.animateTo(datalist);
            recyclerView_card_list.scrollToPosition(position);
            currentCard = datalist.get(position);
        });
        recyclerView_card_list.setLayoutManager(linearLayoutManager);
        recyclerView_card_list.setAdapter(adapter);

        button_switch.setOnClickListener(v ->
        {
            if (currentSelected != -1)
                for (CardType card : datalist) {
                    if (card.isStatus().equals(STATUS_CODE_ACTIVE)) {
                        Log.d(TAG, new Gson().toJson(card, CardType.class));
                        String custId = preference.getString(KEY_CUSID);
                        if (card.getCustid() == Integer.valueOf(custId)) {
                            Toast.makeText(getContext(), getResources().getString(R.string.error_switch_current), Toast.LENGTH_LONG).show();
                        } else {
                            showDialog(REQUEST_CODE_SWITCH, getResources().getString(R.string.dialog_confirm_change), false);
                        }
                    }
                }
            else
                showToast(getResources().getString(R.string.error_invalid_card));
        });

        button_upgrade.setOnClickListener(v ->
        {
            if (currentSelected != -1)
                initInputDialog(API_UPGRADE_PACKAGE);
            else
                showToast(getResources().getString(R.string.error_invalid_card));
        });
        imageView_add.setOnClickListener(v -> initInputDialog(API_SIGN_UP_SUB_ACC));
    }

    private void showCurrent(ArrayList<CardType> datalist) {
        String custId = preference.getString(KEY_CUSID);
        for (CardType card : datalist) {
            if (card.getCustid() == Integer.valueOf(custId)) {
                textView_eda.setText(card.getEda());
                textView_package_type.setText(capitalize(card.getPackage_type()));
//                textView_country.setText(card.getEda());
                currentCard = card;
                Glide.with(getContext())
                        .load(getCardDrawable(card.getPackage_type()))
                        .into(imageView_card);
                break;
            }
        }
    }


    public void initInputDialog(String API) {
        dialog = new Dialog(getContext(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input_2);

        Button button_ok = dialog.findViewById(R.id.dialog_input_2_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_2_button_cancel);
        EditText editText_card_number = dialog.findViewById(R.id.dialog_input_2_edittext);
        TextView textView_card_number_title = dialog.findViewById(R.id.dialog_input_2_textview_message);
        EditText editText_activation = dialog.findViewById(R.id.dialog_input_2_edittext_2);
        TextView textView_activation_title = dialog.findViewById(R.id.dialog_input_2_textview_message_2);

        String title = getResources().getString(R.string.dialog_enter_card_number);
        if (API.equals(API_UPGRADE_PACKAGE)) {
            title = getResources().getString(R.string.dialog_enter_card_number_upgrade);
        }

        textView_card_number_title.setText(title);
        editText_card_number.setInputType(InputType.TYPE_CLASS_TEXT);
        editText_card_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});

        textView_activation_title.setText(getResources().getString(R.string.dialog_enter_activation_code));
        editText_activation.setInputType(InputType.TYPE_CLASS_TEXT);
        editText_activation.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});

        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_dismiss.setText(getResources().getString(R.string.button_abort));
        button_ok.setOnClickListener(v ->
                {
                    if (currentCard != null)
                        presenter.loadAPI(String.valueOf(currentCard.getCustid()), API, editText_card_number.getText().toString(), editText_activation.getText().toString());
                    else
                        showToast(getResources().getString(R.string.error_account));
                }
        );
        button_dismiss.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    @Override
    public void onLoadAccountSuccess(ArrayList<CardType> cardlist) {
        if (isactive) {
            Log.d(TAG, "onLoadAccountSuccess:" + datalist.size());
            datalist = cardlist;
            adapter.notifyDataSetChanged();
            adapter.animateTo(datalist);
            textView_total_account.setText(getResources().getString(R.string.label_total_account, String.valueOf(datalist.size())));
            showCurrent(datalist);
            //preference.putString(KEY_EDCOIN_BIND_KEY, "");
        }
    }

    @Override
    public void onDialogDismiss() {
        if (dialog != null) dialog.dismiss();
    }

    public void onLoadAccountFailed() {
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        isactive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        isactive = false;
    }
}
