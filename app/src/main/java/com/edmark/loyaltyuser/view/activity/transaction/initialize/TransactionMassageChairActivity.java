package com.edmark.loyaltyuser.view.activity.transaction.initialize;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CustomerProfile;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.ui.slideview.SlideView;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_GET_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.API_MASSAGE_CHAIR_STATUS;
import static com.edmark.loyaltyuser.constant.Common.API_MASSAGE_CHAIR_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.API_MERCHANT_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.CHAIR_ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.CHAIR_ERROR_CODE_0;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA;
import static com.edmark.loyaltyuser.constant.Common.KEY_EMAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_LOYALTY_EARNED;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANS_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANS_DEVICEID;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RESPONSE_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESSFUL;
import static com.edmark.loyaltyuser.constant.Common.TRANSACTION_ID;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.playBeep;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


/*
 * Created by DEV-Michael-ED2E on 04/10/2018.
 */
public class TransactionMassageChairActivity extends BaseActivity {
    private static final String TAG = TransactionMassageChairActivity.class.getSimpleName();


    @BindView(R.id.activity_massage_chair_button_slide_to_pay)
    SlideView button_slide;
    @BindView(R.id.activity_massage_chair_button_ok)
    Button button_proceed;
    @BindView(R.id.activity_massage_chair_button_cancel)
    Button button_abort;
    @BindView(R.id.activity_massage_chair_button_choose_another)
    Button button_choose_another;
    @BindView(R.id.activity_massage_chair_textview_name)
    TextView textView_name;
    @BindView(R.id.activity_massage_chair_textview_qr_code)
    TextView textView_qr;
    @BindView(R.id.activity_massage_chair_textview_available)
    TextView textView_available;
    @BindView(R.id.activity_massage_chair_textview_not_available)
    TextView textView_not_available;
    @BindView(R.id.activity_massage_chair_layout_profile)
    LinearLayout layout_profile;
    @BindView(R.id.activity_massage_chair_layout_transaction_field)
    LinearLayout layout_transaction_field;
    @BindView(R.id.activity_massage_chair_radio_button_6_mins)
    RadioButton radioButton_6mins;
    @BindView(R.id.activity_massage_chair_radio_button_18_mins)
    RadioButton radioButton_18mins;
    @BindView(R.id.activity_massage_chair_radio_button_40_mins)
    RadioButton radioButton_40mins;

    private static String amount_20 = "20";
    private static String amount_50 = "50";
    private static String amount_100 = "100";
    private static int getTime_20 = 360;
    private static int getTime_50 = 1080;
    private static int getTime_100 = 2400;
    private String amount = "0";
    private int time = 0;
    private String deviceID;
    private String email;
    private ApiInterface apiService;
    private AppPreference preference;
    private String merchantid;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_massage_chair);
        ButterKnife.bind(this);
        Log.d(TAG,"onCreate");

        setupToolBar(R.id.activity_massage_chair_toolbar);
        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        initLoadType(savedInstanceState);
        getMassageChairStatus();
        initGUI();
    }

    private void initGUI() {
        textView_qr.setText(getResources().getString(R.string.label_massage_chair_qr, deviceID));

        button_slide.setOnFinishListener(() -> {

                    if (radioButton_6mins.isChecked()) {
                        amount = amount_20;
                        time = getTime_20;
                    }

                    if (radioButton_18mins.isChecked()) {
                        amount = amount_50;
                        time = getTime_50;
                    }

                    if (radioButton_40mins.isChecked()) {
                        amount = amount_100;
                        time = getTime_100;
                    }

                    if (!amount.equals("0")) {
                        initPinDialog();
                    } else {
                        button_slide.reset();
                        showToast(getResources().getString(R.string.error_invalid_amount));
                    }
                }
        );

        button_proceed.setOnClickListener(v ->
        {
            layout_profile.setVisibility(View.GONE);
            layout_transaction_field.setVisibility(View.VISIBLE);
            button_proceed.setVisibility(View.GONE);
            button_slide.setVisibility(View.VISIBLE);
        });
        button_abort.setOnClickListener(v -> finish());
        button_choose_another.setOnClickListener(v -> finish());
        radioButton_6mins.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                radioButton_18mins.setChecked(false);
                radioButton_40mins.setChecked(false);
            }
        });
        radioButton_18mins.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                radioButton_6mins.setChecked(false);
                radioButton_40mins.setChecked(false);
            }
        });
        radioButton_40mins.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                radioButton_6mins.setChecked(false);
                radioButton_18mins.setChecked(false);
            }
        });
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initLoadType(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchantid = null;
                deviceID = null;
            } else {
                merchantid = extras.getString(KEY_TRANS_CUSTID);
                deviceID = extras.getString(KEY_TRANS_DEVICEID);
            }
        } else {
            merchantid = (String) savedInstanceState.getSerializable(KEY_TRANS_CUSTID);
            deviceID = (String) savedInstanceState.getSerializable(KEY_TRANS_DEVICEID);
        }
    }

    private void isChairAvailable(boolean isAvailable) {
        if (!isAvailable) {
            button_choose_another.setVisibility(View.VISIBLE);
            textView_not_available.setVisibility(View.VISIBLE);
            button_proceed.setVisibility(View.GONE);
            textView_available.setVisibility(View.GONE);
        } else {
            button_choose_another.setVisibility(View.GONE);
            textView_not_available.setVisibility(View.GONE);
            button_proceed.setVisibility(View.VISIBLE);
            textView_available.setVisibility(View.VISIBLE);
        }
    }

    private void initLoadAccountInfo() {
        showProgress();
        Call<ResponseBody> call = apiService.mainCustInfo(API_MERCHANT_PROFILE, merchantid, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    isChairAvailable(false);
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfile info = gson.fromJson(json, CustomerProfile.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());
//                        eda = info.getEda();
                        email = info.getEmail();
                        textView_name.setText(info.getName());

//                        progressbar_profile.setVisibility(View.VISIBLE);
//                        GlideApp.with(Objects.requireNonNull(getContext()))
//                                .load(info.getImage_url())
//                                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                                .onlyRetrieveFromCache(false)
//                                .placeholder(R.drawable.ic_shimmer_user)
//                                .error(R.drawable.ic_default_user)
//                                .listener(new RequestListener<Drawable>() {
//
//                                    @Override
//                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                        progressbar_profile.setVisibility(View.GONE);
//                                        return false;
//                                    }
//
//                                    @Override
//                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                        progressbar_profile.setVisibility(View.GONE);
//                                        return false;
//                                    }
//                                })
//                                .into(imageView_profile);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("initLoadAccountInfo onFailure:");
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    isChairAvailable(false);
                    //finish();
                    return;
                }
                t.printStackTrace();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
                isChairAvailable(false);
                //finish();
            }
        });
    }

    private void getMassageChairStatus() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.massageChairStatus(
                API_MASSAGE_CHAIR_STATUS,
                custId,
                deviceID.substring(13),
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);
                    int chair_error_code = (int) obj.get(CHAIR_ERRORCODE);

                    if (status.equals(SUCCESSFUL) && chair_error_code == CHAIR_ERROR_CODE_0) {
                        isChairAvailable(true);
                        initLoadAccountInfo();
                    } else {
                        hideProgress();
                        isChairAvailable(false);
                    }
                } catch (JSONException | IOException e) {
                    isChairAvailable(false);
                    e.printStackTrace();
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    isChairAvailable(false);
                    return;
                }

                Timber.d("getMassageChairStatus onFailure:");
                showToast(getResources().getString(R.string.error_connection));
                hideProgress();
                isChairAvailable(false);
                t.printStackTrace();
            }
        });
    }

    private void initTransaction(String transactionid) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.massageTransaction(
                API_MASSAGE_CHAIR_TRANSACTION,
                custId,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()),
                merchantid,
                transactionid,
                amount,
                deviceID.substring(13),
                String.valueOf(time));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        playBeep(getContext(), "transaction.mp3");
                        Intent intent = new Intent(getContext(), TransactionSuccessActivity.class);
                        intent.putExtra(KEY_MERCHANT, textView_name.getText().toString());
                        double trans_amount = Double.valueOf(amount);
                        float loyalty_earned = (float) (trans_amount / 100d);
                        String timeMins;
                        if (amount.equals(amount_20)) {
                            timeMins = getResources().getString(R.string.label_massage_chair_6_mins);
                        } else if (amount.equals(amount_50)) {
                            timeMins = getResources().getString(R.string.label_massage_chair_18_mins);
                        } else {
                            timeMins = getResources().getString(R.string.label_massage_chair_40_mins);
                        }
                        intent.putExtra(KEY_TRANSACTION_ID, transactionid);
                        intent.putExtra(KEY_TRANSACTION_AMOUNT, String.valueOf(trans_amount));
                        intent.putExtra(KEY_LOYALTY_EARNED, String.valueOf(loyalty_earned));
                        intent.putExtra(KEY_DESCRIPTION, getResources().getString(R.string.label_massage_chair_description, deviceID.substring(13), timeMins));

                        intent.putExtra(KEY_MERCHANT_ID, merchantid);
                        intent.putExtra(KEY_EDA, deviceID.substring(0,13));
                        intent.putExtra(KEY_EMAIL, email);
                        startActivity(intent);
                        hideProgress();
                        finish();
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                showToast(getResources().getString(R.string.error_connection));
                hideProgress();
                Timber.d("initTransaction onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void initPinDialog() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                validateTransaction(editText_pin.getText().toString(), amount);
            }
        });
        button_dismiss.setOnClickListener(v ->
        {
            button_slide.reset();
            dialog.dismiss();
        });

        dialog.setOnDismissListener(dialog -> {
            button_slide.reset();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void getTransactionID() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(
                API_GET_TRANSACTION_ID,
                custId,
                String.valueOf(unixTimeStamp()),
                getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {

                        String transactionid = (String) obj.get(TRANSACTION_ID);
                        initTransaction(transactionid);

                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                showToast(getResources().getString(R.string.error_connection));
                hideProgress();
                Timber.d("getTransactionID onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void validateTransaction(String pin, String amount) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.validateTransaction(
                API_VALIDATE_TRANSACTION,
                custId,
                merchantid,
                getHash(pin),
                amount,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
//                        getTransactionID();
                        String transactionid = (String) obj.get(RESPONSE_TRANSACTION_ID);
                        initTransaction(transactionid);
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
                button_slide.reset();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                showToast(getResources().getString(R.string.error_connection));
                hideProgress();
                Timber.d("validateTransaction onFailure:");
                t.printStackTrace();
            }
        });
    }
}
