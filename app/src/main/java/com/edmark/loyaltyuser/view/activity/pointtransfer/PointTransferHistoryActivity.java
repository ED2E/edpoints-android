package com.edmark.loyaltyuser.view.activity.pointtransfer;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewSectionedAdapter;
import com.edmark.loyaltyuser.adapter.PointTransferAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.PointTransfer;
import com.edmark.loyaltyuser.model.TransactionHistory;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_TRANSACTION_HISTORY;
import static com.edmark.loyaltyuser.constant.Common.CREATED_DATE;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.ED2E_ID;
import static com.edmark.loyaltyuser.constant.Common.EDP_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.LOGS;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TOTAL_EDP_TRANSFERRED;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_yyyyMMdd;
import static com.edmark.loyaltyuser.util.AppUtility.getDate;
import static com.edmark.loyaltyuser.util.AppUtility.getDateSection;
import static com.edmark.loyaltyuser.util.AppUtility.getFormattedDate;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getTimeinMilli;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/18/2018.
 */
@SuppressWarnings("unchecked")
public class PointTransferHistoryActivity extends BaseActivity {
    public static final String TAG = PointTransferHistoryActivity.class.getSimpleName();


    @BindView(R.id.activity_point_transfer_history_textview_date_start)
    TextView textView_date_from;
    @BindView(R.id.activity_point_transfer_history_textview_date_end)
    TextView textView_date_to;
    @BindView(R.id.activity_point_transfer_history_button_search)
    ImageView button_search;
    @BindView(R.id.activity_point_transfer_history_textview_result_count)
    TextView textView_result_count;
    @BindView(R.id.activity_point_transfer_history_textview_total_point)
    TextView textView_total_point;
    @BindView(R.id.activity_point_transfer_history_textview_message)
    TextView textView_message;
    @BindView(R.id.activity_point_transfer_history_recyclerview)
    RecyclerView recyclerview;
//    @BindView(R.id.activity_point_transfer_history_button_calendar)
//    ImageView button_calendar;

    private ApiInterface apiInterface;
    private AppPreference preference;

    private List<PointTransfer> modelList;

    static RecyclerView recyclerView;
    PointTransferAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_transfer_history);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);

        setupToolBar(R.id.activity_point_transfer_history_toolbar);
        initGUI();

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGUI() {

        //-- initialize recycler view, layout, and adapter
        recyclerView = findViewById(R.id.activity_point_transfer_history_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new PointTransferAdapter(getApplicationContext(), modelList);
        recyclerView.setAdapter(recyclerAdapter);

        // get current date
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat destDf = new SimpleDateFormat("MM-dd-yyyy");
        // format the date into another format
        String dateStr = destDf.format(date);

        textView_date_from.setText(dateStr);
        textView_date_to.setText(dateStr);

        button_search.setOnClickListener(v -> {
            initAPIGetTransferLogs(textView_date_from.getText().toString().trim(), textView_date_to.getText().toString().trim());
        });

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,#00");

        textView_date_from.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                textView_date_from.setText(new StringBuilder().append(formatter.format(month + 1)).append("-")
                        .append(formatter.format(dayOfMonth)).append("-").append(year));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        textView_date_to.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                textView_date_to.setText(new StringBuilder().append(formatter.format(month + 1)).append("-")
                        .append(formatter.format(dayOfMonth)).append("-").append(year));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

//        textView_date_from.setText(getFormattedDate(new Date(), DATE_FORMAT));
//        textView_date_to.setText(getFormattedDate(new Date(), DATE_FORMAT));

        initAPIGetTransferLogs(textView_date_from.getText().toString().trim(), textView_date_to.getText().toString().trim());
    }

    private void initAPIGetTransferLogs(String date_from, String date_to) {
        Timber.d(TAG,"initAPIGetTransferLogs() triggered");
        showProgress();

        String custId = preference.getString(KEY_CUSID);

        Call<ResponseBody> call = apiInterface.getTransferLogs(
                custId,
                formatDate_yyyyMMdd(date_from),
                formatDate_yyyyMMdd(date_to),
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        Log.i(TAG,
                "Data: " +
                        custId + " | " +
                        date_from + " | " +
                        date_to
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                hideProgress();
                Log.i(TAG, "onResponse: " + response.toString());
                Log.i(TAG,"Raw Url:" + response.raw().request().url());

                if(response.body()==null)
                {
                    //progressDialog.dismiss();
                    hideProgress();
                    showToast(getResources().getString(R.string.error_connection_with_message, response.errorBody()));
                    return;
                }

                try{
                    hideProgress();
                    String responseString = response.body().string();
                    Log.i(TAG, "Response Body: " + responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);
                    String message;
                    JSONArray obj_message;

                    if (status.equals(SUCCESS)) {

                        //showToast(responseString);

                        JSONObject obj_data = obj.getJSONObject(DATA);
                        int total_edp_transferred = obj_data.getInt(TOTAL_EDP_TRANSFERRED);
                        JSONArray array_logs = obj_data.getJSONArray(LOGS);

                        textView_result_count.setText(String.valueOf(array_logs.length()));
                        textView_total_point.setText(String.valueOf(total_edp_transferred));

                        if (array_logs.length() == 0) {
                            textView_message.setText(getResources().getString(R.string.label_no_result));
                            textView_message.setVisibility(View.VISIBLE);
                        } else {
                            textView_message.setVisibility(View.GONE);
                        }

                        modelList = new ArrayList<>();
                        for(int i = 0; i < array_logs.length(); i++) {
                            JSONObject object = array_logs.getJSONObject(i);
                            String id = (String) object.get(ID);
                            String ed2e_id = (String) object.get(ED2E_ID);
                            String edp_amount = (String) object.get(EDP_AMOUNT);
                            String status1 = (String) object.get(STATUS);
                            String created_date = (String) object.get(CREATED_DATE);

                            PointTransfer list = new PointTransfer(
                                    id,
                                    ed2e_id,
                                    edp_amount,
                                    status1,
                                    created_date
                            );

                            modelList.add(list);
                        }

                        recyclerAdapter.setModelList(modelList);

                        //showToast("Count: " + array_logs.length());


                    } else if (status.equals(FAIL)) {
                        showToast("FAIL");

                        hideProgress();
                        try{
                            message = (String) obj.get(MESSAGE);
                            showToast(message);

                        } catch(JSONException | IllegalArgumentException | ClassCastException s) {
                            obj_message = obj.getJSONArray(MESSAGE);
                            message =  obj_message.toString().replace("[", "").replace("\"", "").replace("]", "").trim();
                            //showToast_short("Message is in Array: " + message);
                            String[] array_message = message.split(",");
                            for (String string: array_message) {
                                Log.i(TAG, "Message Array: " + string);
                                showToast(string);
                                break;
                            }
                        }

                    } else {
                        hideProgress();
                        showToast(getResources().getString(R.string.error_something_went_wrong));
                    }


                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    //progressDialog.dismiss();
                    hideProgress();
                    showToast(getResources().getString(R.string.error_connection_with_message, e.getMessage()));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    hideProgress();
                    showToast(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
                    //finish();
                    return;
                }

                hideProgress();
                Log.i(TAG, "onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
                //finish();
            }
        });

    }

    /*public void setFromDate(int year, int month, int day) {
        textView_date_from.setText(new StringBuilder().append(month).append("-").append(day).append("-").append(year));
    }

    public void setToDate(int year, int month, int day) {
        textView_date_to.setText(new StringBuilder().append(month).append("-").append(day).append("-").append(year));

        if ((textView_date_from.getText().length() != 0 && textView_date_to.getText().length() != 0)
                || (!textView_date_from.getText().toString().isEmpty() && !textView_date_to.getText().toString().isEmpty())
                || (!textView_date_from.getText().toString().equals("") && !textView_date_to.getText().toString().equals(""))) {

            //initAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_date), Toast.LENGTH_SHORT).show();
        }
    }

    private void initDatePicker(boolean fromDate, Calendar calendar, Calendar calendarMin) {
        if (calendar == null)
            calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {
            if (fromDate) {
                setFromDate(year, month + 1, dayOfMonth);

                Calendar cal = Calendar.getInstance();
                Calendar calMin = Calendar.getInstance();
                try {
                    calMin.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                    calMin.add(Calendar.DATE, 1);  // number of days to add
                    Timber.d("cal" + calMin.getTime());
                    cal.setTime(getFormattedDate(textView_date_to.getText().toString(), DATE_FORMAT));// all done
                    Timber.d("cal" + cal.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                initDatePicker(false, cal, calMin);
            } else {
                setToDate(year, month + 1, dayOfMonth);
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        if (!fromDate) {
            Date newDate = calendarMin.getTime();
            datePickerDialog.getDatePicker().setMinDate(newDate.getTime() - (newDate.getTime() % (24 * 60 * 60 * 1000)));
            datePickerDialog.setTitle(R.string.dialog_date_to);
        }
        else
        {
            datePickerDialog.setTitle(R.string.dialog_date_from);
        }
        datePickerDialog.show();
    }*/

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
