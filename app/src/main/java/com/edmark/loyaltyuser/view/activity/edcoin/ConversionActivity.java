package com.edmark.loyaltyuser.view.activity.edcoin;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.ui.slideview.SlideView;
import com.edmark.loyaltyuser.util.AppPreference;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_PIN_COIN;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDC_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDC_VALUE;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_VALUE;
import static com.edmark.loyaltyuser.constant.Common.KEY_RATE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RATE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.WALLET_AVAILABLE_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.WALLET_BALANCE;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


public class ConversionActivity extends BaseActivity {

    public static final String TAG = ConversionActivity.class.getSimpleName();

    @BindView(R.id.activity_conversion_imageView_refresh)
    ImageView imageview_refresh;
    @BindView(R.id.activity_conversion_textView_company_code)
    TextView textview_company_code;
    /*@BindView(R.id.activity_conversion_textView_current_price_value)
    TextView textview_current_price_value;*/
    @BindView(R.id.activity_conversion_textView_current_conversion_rate_value)
    TextView textview_current_conversion_rate_value;
    @BindView(R.id.activity_conversion_editText_amount_input)
    EditText editText_amount_input;
    @BindView(R.id.activity_conversion_textView_available_edc_balance)
    TextView textView_available_edc_balance;
    @BindView(R.id.activity_conversion_editText_amount_converted)
    EditText editText_amount_converted;
    @BindView(R.id.activity_conversion_button_slide_to_convert)
    SlideView button_slide;

    private ApiInterface apiService;
    private ApiInterface apiInterface;
    private AppPreference preference;

    private String rate;
    private double rateToDouble;
    private double rateToLong;
    private Double toEDP;
    private String convertedValue_toString;

    private String countryCode = "";
    private String cust_id;
    private String eda_number;
    private String bind_key = "";

    private boolean isTransact = false;
    private String edc_balance = "";
    private String available = "";

    private String selectedServer = "";
    private String converted_value = "";

    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        cust_id = preference.getString(KEY_CUSID);

        setupToolBar(R.id.activity_conversion_toolbar);
        initData(savedInstanceState);
        //initEDCBalance(savedInstanceState);
        initAvailableEDCBalance();
        initCompanyCode();
        /* for code w/out EDA Checking */
        eda_number = preference.getString(KEY_EDA, "");
        /* for code w/ EDA Checking */
        //initEDANumber();
        initCheckCoinRateAPI();
        initGUI();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        loadEDCoinActivity();
    }

    private void initData(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            bind_key = "";
        } else {
            bind_key = extras.getString(KEY_BIND_KEY);
        }
    }

    /*private void initEDCBalance(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            edc_balance = "0.00";
        } else {
            edc_balance = extras.getString(KEY_EDC_BALANCE);
        }

        showToast("edc balance: " + edc_balance);
    }*/

    private void initAvailableEDCBalance() {
        showProgress();

        initCompanyCode();

        String bind_key = preference.getString(KEY_EDCOIN_BIND_KEY);

        //showToast("Selected Server: " + selectedServer);

        apiInterface = ApiClientEDCoin.getClient(selectedServer).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.retrieveWalletBalance(
                bind_key
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    loadEDCoinActivity();
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();
                    //showToast(responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);


                    if (status.equals(SUCCESS)) {
                        JSONObject data = obj.getJSONObject("data");
                        edc_balance = (String) data.get(WALLET_BALANCE);
                        available = (String) data.get(WALLET_AVAILABLE_BALANCE);

                        textView_available_edc_balance.setText(getResources().getString(R.string.label_edcoin_available_edc_balance, available));
                    } else if (status.equals(FAIL)) {
                        String message = (String) obj.get(MESSAGE);
                        showToast(message);
                        loadEDCoinActivity();
                    } else {
                        showToast(getResources().getString(R.string.error_try_again));
                        loadEDCoinActivity();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    showToast(e.getMessage());
                    loadEDCoinActivity();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //showToast("Failed Response");

                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){

                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    loadEDCoinActivity();
                    return;
                }

                hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                loadEDCoinActivity();
            }
        });
    }

    private void initCompanyCode(){
        countryCode = preference.getString(KEY_COUNTRY_CODE);

        String[] sg_server_array = getResources().getStringArray(R.array.sg_server_array);
        String[] ams_server_array = getResources().getStringArray(R.array.ams_server_array);

        for (String i:sg_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "SG";
            }
        }
        for (String i:ams_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "AMS";
            }
        }
    }

    private void initEDANumber(){
        showProgress();

        initCompanyCode();

        //showToast("Selected Server: " + selectedServer);

        String companyCode = preference.getString(KEY_COUNTRY_CODE);

        apiInterface = ApiClientEDCoin.getClient(selectedServer).create(ApiInterface.class);
        //Check EDCoin to EDPoint
        Call<ResponseBody> call = apiInterface.checkEDCoinToEDPoint(
                cust_id,
                companyCode,
                0.00,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                //Log.i("SUCCESS", "onResponse: " + response.toString());
                //Log.i("SUCCESS","Raw Url:" + response.raw().request().url());
                Timber.i("onResponse: " + response.toString());
                Timber.i("Raw Url: " + response.raw().request().url());
                progressDialog.dismiss();

                //showToast("onResponse: Completed!");

                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    loadEDCoinActivity();
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);


                    if (status.equals(SUCCESS)) {

                        String json = /*"{\"data\":" +*/ response.body().string();
                        //showToast(json);
                        //Log.i("SUCCESS TRY", "JSON: " + json);
                        Timber.i("JSON: " + json);

                        eda_number = (String) obj.get(EDA);
                        //showToast("EDA: " + eda_number);

                        progressDialog.dismiss();
                    } else if (status.equals(FAIL)) {
                        String message = (String) obj.get(MESSAGE);
                        showToast(message);
                        loadEDCoinActivity();
                    } else {
                        showToast(getResources().getString(R.string.error_try_again));
                        loadEDCoinActivity();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    showToast(e.getMessage());
                    loadEDCoinActivity();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Log.i("ERROR", "onFailure: " + t.getMessage());
                Timber.i("onFailure: " + t.getMessage());
                //showToast("onFailure: " + t.getMessage());
                progressDialog.dismiss();
                loadEDCoinActivity();
            }
        });

        progressDialog.dismiss();
    }

    private void initGUI() {
        initUpdateRateFunction();

        textview_company_code.setText(countryCode);
        //textview_current_price_value.setText(getResources().getString(R.string.label_current_price_value, "0", "0"));
        textview_current_conversion_rate_value.setText(getResources().getString(R.string.label_current_conversion_rate_value,"0"));
        textView_available_edc_balance.setText(getResources().getString(R.string.label_edcoin_available_edc_balance, edc_balance));

        imageview_refresh.setOnClickListener(v -> {
            initCheckCoinRateAPI();
        });

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
        formatter.applyPattern("###,##0.00");
        formatter.setGroupingSize(3);

        button_slide.setOnFinishListener(() -> {
            if(editText_amount_input.getText().toString().trim().isEmpty()){
                editText_amount_input.setError("Please input a valid amount.\n(Must be at least 0.01)");
                button_slide.reset();
            } else{
                boolean isDoubleValid = false;
                if(editText_amount_input.getText().toString().trim().equalsIgnoreCase(".")){
                    isDoubleValid = false;
                } else {
                    try{
                        Double input = Double.parseDouble(editText_amount_input.getText().toString().trim().replace(",",""));
                        if(input == 0) {
                            isDoubleValid = false;
                        } else if(input < 0.01) {
                            isDoubleValid = false;
                        } else {
                            isDoubleValid = true;
                        }
                    } catch (NumberFormatException e) {
                        isDoubleValid = false;
                    }
                }

                if(isDoubleValid) {
                    button_slide.reset();
                    initPinDialog();
                } else {
                    editText_amount_input.setError("Please input a valid amount.\n(Must be at least 0.01)");
                    editText_amount_input.requestFocus();
                    button_slide.reset();
                }

                //initConfirmationDialog();
                //initConfirmationScreeen();
            }
        });
    }

    private void initConfirmationScreeen() {

        String edp_value = editText_amount_input.getText().toString().trim().replace(",","");
        DecimalFormat formatter = new DecimalFormat("###,###.##");
        //edp_value = String.valueOf(formatter.format(Double.parseDouble(edp_value)));
        edp_value = String.valueOf(amount_input);

        String edc_value = /*editText_amount_converted.getText().toString().trim();*/ String.valueOf(amount_input * toEDP);

        //showToast("EDC: " + edc_value + " | EDP: " + edp_value);
        button_slide.reset();

        Intent intent = new Intent(getContext(), ConversionConfirmationActivity.class);

        intent.putExtra(KEY_EDC_VALUE, edc_value);
        intent.putExtra(KEY_EDP_VALUE, edp_value);
        intent.putExtra(KEY_CUSTID, cust_id);
        intent.putExtra(KEY_RATE, rate);
        intent.putExtra(KEY_BIND_KEY, bind_key);
        intent.putExtra(KEY_EDC_BALANCE, available);

        startActivity(intent);
        finish();
    }

    private void initCheckCoinRateAPI() {
        showProgress();

        initCompanyCode();

        //showToast("Selected Server: " + selectedServer);

        apiInterface = ApiClientEDCoin.getClient(selectedServer).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.checkCoinRate(
                cust_id,
                countryCode,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    loadEDCoinActivity();
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);
                    String edp_value_formatted_toString = null;

                    if (status.equals(SUCCESS)) {
                        String rate_inString = (String) obj.get(RATE);
                        rate = rate_inString;

                        try {
                            //parse (rate_inString) to double
                            rateToDouble = Double.valueOf(rate_inString.replace(",", "").trim());
                            Timber.i("rateToDouble: " + rateToDouble);
                            toEDP = 1 / rateToDouble;
                            Timber.i("toEDP: " + toEDP);

                            //format decimal value of (toEDP) double with US locale
                            //DecimalFormat formatter = new DecimalFormat("###,##0.00");
                            DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
                            formatter.applyPattern("###,##0.00");
                            Double formatter_toDouble = Double.parseDouble(formatter.format(toEDP));

                            //format number value to US locale
                            //NumberFormat nf = NumberFormat.getInstance(new Locale("en","US"));
                            edp_value_formatted_toString = String.valueOf(formatter_toDouble);
                            Timber.i("edp_value_formatted_toString: " + edp_value_formatted_toString);

                            //update value of (rateToDouble) to (edp_value_formatted_toString) and parse it to double
                            rateToDouble = Double.valueOf(edp_value_formatted_toString);
                            Timber.i("rateToDouble: " + rateToDouble);
                        }catch(NumberFormatException e) {
                            showToast(getResources().getString(R.string.error_try_again) + "\n(" + e.getMessage() + ")");
                        }

                        //set edc value to textview
                        textview_current_conversion_rate_value.setText(getResources().getString(R.string.label_current_conversion_rate_value, String.valueOf(rateToDouble)));
                    } else if (status.equals(FAIL)) {
                        String message = (String) obj.get(MESSAGE);
                        showToast(message);
                        loadEDCoinActivity();
                    } else {
                        showToast(getResources().getString(R.string.error_try_again));
                        loadEDCoinActivity();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    showToast(e.getMessage());
                    loadEDCoinActivity();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    loadEDCoinActivity();
                    return;
                }

                hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                loadEDCoinActivity();
            }
        });
    }

    private void initPinDialog() {
        Timber.d("initPinDialog");
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                if(!isTransact) {

                    String pin = editText_pin.getText().toString();

                    initValidatePinAPI(pin);

                    button_slide.reset();
                    dialog.dismiss();

                }
            }
        });
        button_dismiss.setOnClickListener(v ->
        {
            button_slide.reset();
            dialog.dismiss();
        });

        dialog.setOnDismissListener(dialog -> {
            button_slide.reset();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void initValidatePinAPI(String pin) {
        Timber.d("validatePin");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call2 = apiService.validatePinCoin(
                API_VALIDATE_PIN_COIN,
                custId,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()),
                getHash(pin));
        call2.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        //initConvertEDCoinAPI();
                        //initWarningDialog(edc_balance);
                        //showToast("JSON: " + json);
                        hideProgress();
                        initConfirmationScreeen();
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
                button_slide.reset();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    Double amount_input;

    private void initUpdateRateFunction() {
        Observable<CharSequence> changeObservableSearch = RxTextView.textChanges(editText_amount_input);

        disposables.add(changeObservableSearch.map(inputText -> (inputText.length() == 0))
                .subscribe(isValid ->
                {
                    if(!isValid)
                    {
                        if(editText_amount_input.getText().toString().trim().equalsIgnoreCase(".")){
                            amount_input = Double.valueOf("0.00");
                        } else {
                            amount_input = Double.valueOf(editText_amount_input.getText().toString().replace(",", "").trim());
                        }

                        convertValue(amount_input);
                    }
                    else
                    {
                        amount_input = 0.00;
                        convertValue(amount_input);
                    }
                }));
    }

    public void convertValue(Double amount_input){
        if(editText_amount_input.getText().toString().equalsIgnoreCase("")){
            editText_amount_converted.setText("");
        } else {
            DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
            formatter.applyPattern("###,##0.000000000000000000");

            converted_value = String.valueOf(formatter.format(amount_input * toEDP));
            Timber.d("toEDP: " + toEDP);
            Timber.d("converted_value: " + converted_value);

            editText_amount_converted.setText(converted_value);
        }
    }

    public void loadEDCoinActivity(){
        Intent intent = new Intent(getContext(), EDCoinActivity.class);
        startActivity(intent);

        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                loadEDCoinActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
