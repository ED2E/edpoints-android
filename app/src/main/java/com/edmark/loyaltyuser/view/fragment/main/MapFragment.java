package com.edmark.loyaltyuser.view.fragment.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.InfoWindowGoogleMapAdapter;
import com.edmark.loyaltyuser.model.MainMessageEvent;
import com.edmark.loyaltyuser.model.Merchant;
import com.edmark.loyaltyuser.model.MerchantMessageEvent;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static android.content.Context.LOCATION_SERVICE;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_MAP;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_MAP_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.LOCATION_NOT_AVAILABLE;
import static com.edmark.loyaltyuser.constant.Common.LOCATION_PERMISSION_REQUEST_CODE;

/*
 * Created by DEV-Michael-ED2E on 04/04/2018.
 */
@SuppressWarnings("unchecked")
public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener {
    private static final String TAG = MapFragment.class.getSimpleName();

    @BindView(R.id.fragment_map_editText_search)
    EditText editext_search;
    @BindView(R.id.fragment_map_textview_label)
    TextView textView_label;
    @BindView(R.id.fragment_map_progress)
    ProgressBar progressBar;
    private Unbinder unbinder;
    private SupportMapFragment mapFragment;
    private ArrayList<Marker> markers = new ArrayList<>();
    private GoogleMap map;
    private ArrayList<Merchant> merchants = new ArrayList<>();
    private ArrayList<Merchant> filteredMerchants = new ArrayList<>();
    private CompositeDisposable disposables;
    private boolean isBuffered = false;
    private CountDownTimer countDownTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, view);
        Log.d(TAG, "onCreateView");

        if (mapFragment == null) {
            Timber.d("mapFragment create");
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(this);

        }
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_map_framelayout, mapFragment).commit();
        initGUI();
        return view;
    }

    private void initGUI() {

        Timber.d("initGUI");
        editext_search.setText("");
        filteredMerchants = new ArrayList<>();
        if (map != null) map.clear();
        Observable<CharSequence> changeObservableSearch = RxTextView.textChanges(editext_search);
        disposables = new CompositeDisposable();
        disposables.add(changeObservableSearch.map(inputText -> (inputText.length() == 0))
                .subscribe(isValid ->
                {
                    Timber.d("isValid:" + isValid);
                    if (!isValid) {
                        textView_label.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                        refreshTimer();
//                        filteredMerchants.clear();
//                        for (int i = 0; i < merchants.size(); i++) {
//                            Gson gson = new Gson();
//                            Merchant merchant = gson.fromJson(gson.toJson(merchants.get(i)), Merchant.class);
//                            if (merchant.getMerchant_name().toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
//                                filteredMerchants.add(merchant);
//                            }
//
//                            if (merchant.getAddress() != null) {
//                                if (merchant.getAddress().toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
//                                    filteredMerchants.add(merchant);
//                                }
//                            }
//                        }
//                        if (map != null)
//                            initMapData(map, filteredMerchants, true);
                    } else {
                        cancelTimer();
                        progressBar.setVisibility(View.GONE);
                        textView_label.setVisibility(View.VISIBLE);
                        filteredMerchants.clear();
                        filteredMerchants.addAll(merchants);

                        if (map != null)
                            initMapData(map, filteredMerchants, false);
                    }
                }));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Timber.d("onMapReady");
        map = googleMap;
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_map_framelayout, mapFragment).commit();

        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        }

        if (isBuffered) {
            initMapData(map, filteredMerchants, false);
            isBuffered = false;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView");
        unbinder.unbind();
        // Using clear will clear all, but can accept new disposable
        disposables.clear();
        // Using dispose will clear all and set isDisposed = true, so it will not accept any new disposable
        disposables.dispose();
    }

    private CameraUpdate initMapUpdate() {
        Timber.d("CameraUpdate()");
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker markerItem : markers) {
            builder.include(markerItem.getPosition());
        }
        LatLngBounds bounds;
        if (markers.size() != 0) {
            bounds = builder.build();
        } else {
            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MAP, LOCATION_NOT_AVAILABLE));
            bounds = new LatLngBounds(new LatLng(0, 0), new LatLng(0, 0));
        }
        int padding = 100; // offset from edges of the map in pixels
        return CameraUpdateFactory.newLatLngBounds(bounds, padding);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MainMessageEvent event) {
        filteredMerchants.clear();
        Timber.d("onMessageEvent:" + event.getMessage());
        switch (event.getMessage()) {
            case EVENT_VIEW_MAP: {
                Timber.d("map:" + map);
                Gson gson = new Gson();
                merchants = gson.fromJson(event.getData(), ArrayList.class);
                filteredMerchants.addAll(merchants);
                if (filteredMerchants.size() != 0) {
                    if (map != null) {
                        Timber.d("onMessageEvent:" + event.getData());
                        initMapData(map, filteredMerchants, false);
                        isBuffered = false;
                    } else {
                        isBuffered = true;
                    }
                }
                break;
            }
            case EVENT_VIEW_MAP_MERCHANT: {
                Timber.d("map:" + map);
                Gson gson = new Gson();
                merchants = gson.fromJson(event.getData(), ArrayList.class);
                filteredMerchants.addAll(merchants);
                if (filteredMerchants.size() != 0) {
                    if (map != null) {
                        Timber.d("onMessageEvent:" + event.getData());
                        initMapData(map, filteredMerchants, true);
                        isBuffered = false;
                    } else {
                        isBuffered = true;
                    }
                }
                break;
            }
        }
    }

    private void initMapData(GoogleMap googleMap, ArrayList<Merchant> data, boolean isSearch) {
        Timber.d("initMapData:" + data);
        googleMap.clear();

        if (data.size() == 0) {
            return;
        }

        markers = new ArrayList<>();
        BitmapDescriptor iconRes = BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker);
        Gson gson = new Gson();
//        new Thread(() -> {
        for (int i = 0; i < data.size(); i++) {
            Merchant merchant = gson.fromJson(gson.toJson(data.get(i)), Merchant.class);

            /*ORIGINAL AND WORKING WITH THE LATLONG VALUE*/ // no need to use thread for this
            double lat;
            double longi;
            Timber.d("initMapData:" + merchant.getMerchant_name() + " " + merchant.getLatitude() + " " + merchant.getLongitude());
            if (!merchant.getLatitude().equals("") && !merchant.getLongitude().equals("")) {
                lat = Double.valueOf(merchant.getLatitude());
                longi = Double.valueOf(merchant.getLongitude());
                LatLng latLng = new LatLng(lat, longi);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng).title(merchant.getMerchant_name()).snippet(merchant.getAddress());
                InfoWindowGoogleMapAdapter adapter = new InfoWindowGoogleMapAdapter(getContext());
                googleMap.setInfoWindowAdapter(adapter);
                Marker marker = googleMap.addMarker(markerOptions);
                marker.setTag(merchant);
                marker.setIcon(iconRes); //ORIGINAL
                markers.add(marker);
                Timber.d("initMapData:" + markers.size());

                Timber.d("checkSelfPermission show my current ACCESS_FINE_LOCATION:" + ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_FINE_LOCATION));
                Timber.d("checkSelfPermission show my current ACCESS_COARSE_LOCATION:" + ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_COARSE_LOCATION));

                if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    googleMap.setMyLocationEnabled(true);

                googleMap.setOnInfoWindowClickListener(markerInfo -> {
                    Merchant merchant_detail = (Merchant) markerInfo.getTag();
                    if (merchant_detail != null) {
                        EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MERCHANT, gson.toJson(merchant_detail)));
                    }
                });

                if (isSearch) {
                    googleMap.animateCamera(initMapUpdate());
                }

            }
        }
        if (!isSearch) {

            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                // Getting LocationManager object from System Service LOCATION_SERVICE
                LocationManager locationManager = (LocationManager) Objects.requireNonNull(getContext()).getSystemService(LOCATION_SERVICE);

                Timber.d("locationManager:" + locationManager);
                // Creating a criteria object to retrieve provider
//            Criteria criteria = new Criteria();

                // Getting the name of the best provider
                String provider = "";
                if (locationManager != null) {
//                provider = locationManager.getBestProvider(criteria, true);
                    provider = LocationManager.NETWORK_PROVIDER;
                }
                Timber.d("locationManager provider:" + provider);

                // Getting Current Location
                Location location = null;
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(provider);
                    Timber.d("locationManager location1:" + location);
                }
                Timber.d("locationManager location2:" + location);

                if (location != null) {
                    // Getting latitude of the current location
                    double latitude = location.getLatitude();

                    // Getting longitude of the current location
                    double longitude = location.getLongitude();

                    // Creating a LatLng object for the current location
                    LatLng currentLoc = new LatLng(latitude, longitude);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLoc, 15));
                } else {
                    EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MAP, LOCATION_NOT_AVAILABLE));
                    googleMap.animateCamera(initMapUpdate());
                }
            }
        }
//        }).start();
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.d("onStart()");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        Timber.d("onStop()");
        EventBus.getDefault().unregister(this);
        cancelTimer();
        super.onStop();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    //start timer function
    void startTimer() {
        countDownTimer = new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                progressBar.setVisibility(View.GONE);
                filteredMerchants.clear();
                for (int i = 0; i < merchants.size(); i++) {
                    Gson gson = new Gson();
                    Merchant merchant = gson.fromJson(gson.toJson(merchants.get(i)), Merchant.class);
                    if (merchant.getMerchant_name().toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
                        filteredMerchants.add(merchant);
                    }

                    if (merchant.getAddress() != null) {
                        if (merchant.getAddress().toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
                            filteredMerchants.add(merchant);
                        }
                    }
                }
                if (map != null)
                    initMapData(map, filteredMerchants, true);
            }
        };
        countDownTimer.start();
    }


    //cancel timer
    void cancelTimer() {
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    private void refreshTimer() {
        cancelTimer();
        startTimer();
    }
}