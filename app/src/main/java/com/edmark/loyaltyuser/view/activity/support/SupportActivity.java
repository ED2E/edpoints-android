package com.edmark.loyaltyuser.view.activity.support;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.BuildConfig;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.SupportItem;
import com.edmark.loyaltyuser.util.AppUtility;
import com.edmark.loyaltyuser.view.activity.main.WebviewActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.DEFTYPE_ARRAY;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.SUPPORT_ARRAY_HEADER;
import static com.edmark.loyaltyuser.constant.Common.URL_ABOUT_EDPOINTS;
import static com.edmark.loyaltyuser.constant.Common.URL_FAIL;
import static com.edmark.loyaltyuser.constant.Common.URL_FAQ;
import static com.edmark.loyaltyuser.constant.Common.URL_HEADER;
import static com.edmark.loyaltyuser.constant.Common.URL_MAPS;

/*
 * Created by DEV-Michael-ED2E on 04/12/2018.
 */
@SuppressWarnings("unchecked")
public class SupportActivity extends BaseActivity
{
    public static final String TAG = SupportActivity.class.getSimpleName();

    @BindView(R.id.activity_support_recyclerview_option)RecyclerView recyclerView_option;
    @BindView(R.id.activity_support_toolbar_title)TextView textView_tootbar_title;
    @BindView(R.id.activity_support_webView)WebView webView;

    private ArrayList<SupportItem> supportList = new ArrayList<>();
    private BaseRecyclerViewAdapter adapter;
    private boolean mPageFinished = false;
    private List<String> support_labels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        ButterKnife.bind(this);

        initToolBar();
        initGui();
        initData(getCountry(savedInstanceState).toLowerCase());
    }

    private void initToolBar()
    {
        setupToolBar(R.id.activity_support_toolbar);
        textView_tootbar_title.setText(getResources().getString(R.string.title_support));
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        if(webView.isShown())
        {
            webView.setVisibility(View.GONE);
            mPageFinished = false;
            textView_tootbar_title.setText(getResources().getString(R.string.title_support));
        }
        else {
            finish();
        }
    }

    private String getCountry(Bundle savedInstanceState) {
        String country_code;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                country_code = null;
            } else {
                country_code = extras.getString(KEY_COUNTRY_CODE);
            }
        } else {
            country_code = (String) savedInstanceState.getSerializable(KEY_COUNTRY_CODE);
        }
        return country_code;
    }

    private void initData(String countryCode)
    {
        try
        {
            int arrayResId = this.getResources().getIdentifier(SUPPORT_ARRAY_HEADER+countryCode, DEFTYPE_ARRAY, this.getPackageName());
            support_labels = Arrays.asList(getResources().getStringArray(arrayResId));
        }catch (Exception e)
        {
            int arrayResId = this.getResources().getIdentifier(SUPPORT_ARRAY_HEADER, DEFTYPE_ARRAY, this.getPackageName());
            support_labels = Arrays.asList(getResources().getStringArray(arrayResId));
            e.printStackTrace();
            Timber.d(support_labels.toString());
        }

        for (String label: support_labels)
        {
            int drawable;
            if(label.equals(support_labels.get(0)))
            {
                drawable = R.drawable.ic_contact;
            }
            else if(label.equals(support_labels.get(1)))
            {
                drawable = R.drawable.ic_mail;
            }
            else if(label.equals(support_labels.get(2)))
            {
                drawable = R.drawable.ic_globe;
            }
            else if(label.equals(support_labels.get(3)))
            {
                drawable = R.drawable.ic_skyscraper;
            }
            else if(label.equals(support_labels.get(4)))
            {
                drawable = R.drawable.ic_question_mark;
            }
            else if(label.equals(support_labels.get(5)))
            {
                drawable = R.drawable.ic_about;
            }
            else
            {
                drawable = R.drawable.ic_vs;
                label = label+" "+BuildConfig.VERSION_NAME;
            }
            SupportItem item = new SupportItem(label,drawable);
            supportList.add(item);
        }
        adapter.animateTo(supportList);
    }

    private void initGui() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new BaseRecyclerViewAdapter(supportList,R.layout.child_support, BR.SupportItem, (v, item, position) -> onclickItem(supportList.get(position).getDisplay_msg()));
        recyclerView_option.setLayoutManager(linearLayoutManager);
        recyclerView_option.setAdapter(adapter);
    }


    /**
     * Send Email
     */
    private void openEmailMethod(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email.trim(), null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, "Send email"));
    }

    /**
     * Call dialpad
     */
    public void phoneNumberClicked(String number) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(number.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    private void onclickItem(String value) {

        if(value.equals(support_labels.get(0)))
        {
            phoneNumberClicked(value);
        }
        else if(value.equals(support_labels.get(1)))
        {
            openEmailMethod(value);
        }
        else if(value.equals(support_labels.get(2)))
        {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(URL_HEADER + value.trim()));
            startActivity(i);
        }
        else if(value.equals(support_labels.get(3)))
        {
            String geoUri = URL_MAPS + value;
            Timber.d(TAG,"geoUri:"+geoUri);
            Uri geoDat = Uri.parse(geoUri);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(geoDat);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        else if(value.equals(support_labels.get(4)))
        {
            textView_tootbar_title.setText(getResources().getString(R.string.title_faq));
            setWebView(URL_FAQ);
        }
        else if(value.equals(support_labels.get(5)))
        {
            textView_tootbar_title.setText(getResources().getString(R.string.title_about_us));
            setWebView(URL_ABOUT_EDPOINTS);
        }
        else
        {
            Intent intent = new Intent(SupportActivity.this, SupportGameActivity.class);
            startActivity(intent);
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView(String URL) {
        Timber.e(TAG, "setWebView");
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setOnLongClickListener(v -> true);
        webView.loadUrl(URL);
        webView.setWebViewClient(new CustomWebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
    }

    public class CustomWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(final WebView view, int errorCode,
                                    String description, final String failingUrl) {
            Timber.e("errorCode", String.valueOf(errorCode));
            final View noInternet = View.inflate(SupportActivity.this, R.layout.view_no_internet, null);
            noInternet.findViewById(R.id.refresh_button_new).setOnClickListener(v -> {
                mPageFinished = false;
                view.loadUrl(failingUrl);
                view.removeView(noInternet);
            });
            if (view.findViewById(noInternet.getId()) == null) {
                view.loadUrl(URL_FAIL);
                view.addView(noInternet);
            }
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//            AppUtility.sslMessage(handler, FaqActivity.this);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgress();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgress();
            mPageFinished = true;

        }

        /**
         * Redirecting user to another wenview.
         * mPageFinished will stop the redirection till first time page loads as server is redirecting pages.
         *
         * @param view - the webView
         * @param url  - url to be redirected
         * @return - status
         */
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (mPageFinished) {                     // Flag to stop redirect for the 1st time
                Intent in = new Intent(SupportActivity.this, WebviewActivity.class);
                in.putExtra(KEY_URL, url);
                in.putExtra(KEY_TITLE, AppUtility.getUrlTitle(url));
                startActivity(in);
                return true;
//
            }
            return false;
            //view.loadUrl(url);
        }

        /**
         * Redirecting user to another wenview.
         * mPageFinished will stop the redirection till first time page loads as server is redirecting pages.
         *
         * @param view    - the webView
         * @param request - page to be redirected
         * @return - status
         */
        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (mPageFinished) {                                  // Flag to stop redirect for the 1st time
                Intent in = new Intent(SupportActivity.this, WebviewActivity.class);
                in.putExtra(KEY_URL, request.getUrl().toString());
                in.putExtra(KEY_TITLE, AppUtility.getUrlTitle(request.getUrl().toString()));
                startActivity(in);
                return true;
            }
            return false;
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if(webView.isShown())
                {
                    webView.setVisibility(View.GONE);
                    mPageFinished = false;
                    textView_tootbar_title.setText(getResources().getString(R.string.title_support));
                }
                else {
                    finish();
                }
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
