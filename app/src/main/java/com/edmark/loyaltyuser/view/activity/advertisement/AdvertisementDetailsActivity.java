package com.edmark.loyaltyuser.view.activity.advertisement;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Ads;
import com.edmark.loyaltyuser.util.AppUtility;
import com.edmark.loyaltyuser.view.activity.main.WebviewActivity;
import com.google.gson.Gson;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.KEY_ADS_DETAILS;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.URL_FAIL;
import static com.edmark.loyaltyuser.constant.Common.URL_HEADER;

/*
 * Created by DEV-Michael-ED2E on 04/27/2018.
 */
@SuppressWarnings("unchecked")
public class AdvertisementDetailsActivity extends BaseActivity {
    public static final String TAG = AdvertisementDetailsActivity.class.getSimpleName();

    @BindView(R.id.activity_advertisement_detail_textview_name)
    TextView textView_name;
    @BindView(R.id.activity_advertisement_detail_textview_content)
    TextView textView_content;
    @BindView(R.id.activity_advertisement_detail_webView)
    WebView webView;
    @BindView(R.id.activity_advertisement_detail_imageview_featuredimage)
    ImageView imageView_featuredimage;

    private String merchant_code;
    private boolean mPageFinished = false;
    private Ads adData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement_detail);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_advertisement_detail_toolbar);
        merchant_code = initData(savedInstanceState);
        initGui();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private String initData(Bundle savedInstanceState) {
        Gson gson = new Gson();
        String merchant_code;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchant_code = null;
                adData = null;
            } else {
                merchant_code = extras.getString(KEY_MERCHANT_CODE);
                adData = gson.fromJson(extras.getString(KEY_ADS_DETAILS), Ads.class);
            }
        } else {
            merchant_code = (String) savedInstanceState.getSerializable(KEY_MERCHANT_CODE);
            adData = gson.fromJson((String) savedInstanceState.getSerializable(KEY_ADS_DETAILS), Ads.class);
        }
        return merchant_code;
    }

    private void initGui() {
        if (adData != null) {
            Glide.with(getContext()).load(adData.getImage_url()).into(imageView_featuredimage);

            textView_name.setText(adData.getAds_title());
//            textView_content.setText(Html.fromHtml(adData.getContent()));
            String url;
            if (Objects.requireNonNull(adData.getWebsite_url()).contains(URL_HEADER))
                url = adData.getWebsite_url();
            else
                url = URL_HEADER + adData.getWebsite_url();

//            setWebView(url);
//            setWebView(adData.getContent());
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadDataWithBaseURL("", adData.getContent(), "text/html", "UTF-8", "");
            webView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_merchant_details, menu);//Menu Resource, Menu
        MenuItem item = menu.findItem(R.id.action_location);
        if (merchant_code != null) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:
                Intent returnIntent = new Intent();
                returnIntent.putExtra(KEY_MERCHANT_CODE, merchant_code);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView(String URL) {
        Timber.e(TAG, "setWebView");
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setOnLongClickListener(v -> true);
//        webView.loadUrl(URL);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("", URL, "text/html", "UTF-8", "");
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setWebViewClient(new CustomWebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
    }

    public class CustomWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(final WebView view, int errorCode,
                                    String description, final String failingUrl) {
            Timber.e("errorCode", String.valueOf(errorCode));
            final View noInternet = View.inflate(getContext(), R.layout.view_no_internet, null);
            noInternet.findViewById(R.id.refresh_button_new).setOnClickListener(v -> {
                mPageFinished = false;
                view.loadUrl(failingUrl);
                view.removeView(noInternet);
            });
            if (view.findViewById(noInternet.getId()) == null) {
                view.loadUrl(URL_FAIL);
                view.addView(noInternet);
            }
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//            AppUtility.sslMessage(handler, FaqActivity.this);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgress();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgress();
            mPageFinished = true;

        }

        /**
         * Redirecting user to another wenview.
         * mPageFinished will stop the redirection till first time page loads as server is redirecting pages.
         *
         * @param view - the webView
         * @param url  - url to be redirected
         * @return - status
         */
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (mPageFinished) {                     // Flag to stop redirect for the 1st time
                Intent in = new Intent(getContext(), WebviewActivity.class);
                in.putExtra(KEY_URL, url);
                in.putExtra(KEY_TITLE, AppUtility.getUrlTitle(url));
                startActivity(in);
                return true;
//
            }
            return false;
            //view.loadUrl(url);
        }

        /**
         * Redirecting user to another wenview.
         * mPageFinished will stop the redirection till first time page loads as server is redirecting pages.
         *
         * @param view    - the webView
         * @param request - page to be redirected
         * @return - status
         */
        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (mPageFinished) {                                  // Flag to stop redirect for the 1st time
                Intent in = new Intent(getContext(), WebviewActivity.class);
                in.putExtra(KEY_URL, request.getUrl().toString());
                in.putExtra(KEY_TITLE, AppUtility.getUrlTitle(request.getUrl().toString()));
                startActivity(in);
                return true;
            }
            return false;
        }
    }
}
