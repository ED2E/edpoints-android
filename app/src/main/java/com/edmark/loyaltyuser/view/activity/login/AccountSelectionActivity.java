package com.edmark.loyaltyuser.view.activity.login;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.widget.TextView;


import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CardType;
import com.edmark.loyaltyuser.presenter.login.AccountSelectionContract;
import com.edmark.loyaltyuser.presenter.login.AccountSelectionPresenter;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.main.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edmark.loyaltyuser.constant.Common.DISTRIBUTOR;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;

/*
 * Created by DEV-Michael-ED2E on 04/02/2018.
 */

@SuppressWarnings("unchecked")
public class AccountSelectionActivity extends BaseActivity implements AccountSelectionContract.View{
    public static final String TAG = AccountSelectionActivity.class.getSimpleName();

    private AccountSelectionContract.Presenter presenter;

    @BindView(R.id.activity_account_selection_textview_total_account)TextView textView_total_account;
    @BindView(R.id.activity_account_selection_recyclerview_card_list)RecyclerView recyclerView_card_list;

    private ArrayList<CardType> datalist = new ArrayList<>();
    private BaseRecyclerViewAdapter adapter;

    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountselection);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        presenter = new AccountSelectionPresenter(this);
        setupToolBar(R.id.activity_account_selection_toolbar);
        presenter.accounts();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void initGUI() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new BaseRecyclerViewAdapter(datalist,R.layout.child_account_selection, BR.CardType, (v, item, position) ->
        {
            Intent intent = new Intent(getContext(), MainActivity.class);
            presenter.saveCustID(String.valueOf(datalist.get(position).getCustid()));
            startActivity(intent);
            finish();
        });
        recyclerView_card_list.setLayoutManager(linearLayoutManager);
        recyclerView_card_list.setAdapter(adapter);
    }

    @Override
    public void onLoadSuccess(ArrayList<CardType> data) {
        datalist.clear();
        datalist.addAll(data);

        initGUI();
        adapter.notifyDataSetChanged();
        adapter.animateTo(datalist);
        textView_total_account.setText(getResources().getString(R.string.label_total_account,String.valueOf(datalist.size())));

        preference.putString(KEY_EDCOIN_BIND_KEY, "");
    }

    @Override
    public void onLoadFailed() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
