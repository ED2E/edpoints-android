package com.edmark.loyaltyuser.view.activity.support;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.WheelItem;
import com.edmark.loyaltyuser.ui.wheelview.LuckyWheelView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SupportGameActivity extends BaseActivity {

    @BindView(R.id.activity_support_game_wheelview)LuckyWheelView luckyWheelView;
    @BindView(R.id.activity_support_button_play)Button buttonPlay;

    List<WheelItem> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_game);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_support_game_toolbar);
        int color_1 = 0xff95c2ff;
        int color_2 = 0xff5e92f3;
        int color_3 = 0xff1565c0;

        WheelItem luckyItem1 = new WheelItem();
        luckyItem1.text = "100";
//        luckyItem1.icon = R.drawable.ic_home;
        luckyItem1.color = color_1;
        data.add(luckyItem1);

        WheelItem luckyItem2 = new WheelItem();
        luckyItem2.text = "200";
//        luckyItem2.icon = R.drawable.ic_ledger;
        luckyItem2.color = color_2;
        data.add(luckyItem2);

        WheelItem luckyItem10 = new WheelItem();
        luckyItem10.text = "1000";
//        luckyItem10.icon = R.mipmap.ic_launcher;
        luckyItem10.color = color_3;
        data.add(luckyItem10);

        WheelItem luckyItem3 = new WheelItem();
        luckyItem3.text = "300";
//        luckyItem3.icon = R.drawable.ic_address;
        luckyItem3.color = color_1;
        data.add(luckyItem3);

        //////////////////
        WheelItem luckyItem4 = new WheelItem();
        luckyItem4.text = "400";
//        luckyItem4.icon = R.drawable.ic_advertisement;
        luckyItem4.color = color_2;
        data.add(luckyItem4);

        WheelItem luckyItem5 = new WheelItem();
        luckyItem5.text = "500";
//        luckyItem5.icon = R.drawable.ic_key_in;
        luckyItem5.color = color_3;
        data.add(luckyItem5);

        WheelItem luckyItem11 = new WheelItem();
        luckyItem11.text = "2000";
//        luckyItem11.icon = R.mipmap.ic_launcher;
        luckyItem11.color = color_1;
        data.add(luckyItem11);

        WheelItem luckyItem6 = new WheelItem();
        luckyItem6.text = "600";
//        luckyItem6.icon = R.drawable.ic_scan;
        luckyItem6.color = color_2;
        data.add(luckyItem6);
        //////////////////

        //////////////////////
        WheelItem luckyItem7 = new WheelItem();
        luckyItem7.text = "700";
//        luckyItem7.icon = R.drawable.ic_about;
        luckyItem7.color = color_3;
        data.add(luckyItem7);

        WheelItem luckyItem8 = new WheelItem();
        luckyItem8.text = "800";
//        luckyItem8.icon = R.drawable.ic_card;
        luckyItem8.color = color_1;
        data.add(luckyItem8);


        WheelItem luckyItem9 = new WheelItem();
        luckyItem9.text = "900";
//        luckyItem9.icon = R.drawable.ic_calendar_50;
        luckyItem9.color = color_2;
        data.add(luckyItem9);
        ////////////////////////

        WheelItem luckyItem12 = new WheelItem();
        luckyItem12.text = "3000";
//        luckyItem12.icon = R.mipmap.ic_launcher;
        luckyItem12.color = color_3;
        data.add(luckyItem12);

        /////////////////////

        luckyWheelView.setData(data);
        luckyWheelView.setRound(getRandomRound());
        luckyWheelView.setLuckyWheelBackgrouldColor(0xff010066);
        luckyWheelView.setLuckyWheelTextColor(0xffffffff);

        /*luckyWheelView.setLuckyWheelBackgrouldColor(0xff0000ff);
        luckyWheelView.setLuckyWheelTextColor(0xffcc0000);
        luckyWheelView.setLuckyWheelCenterImage(getResources().getDrawable(R.drawable.icon));
        luckyWheelView.setLuckyWheelCursorImage(R.drawable.ic_cursor);*/

        buttonPlay.setOnClickListener(v ->
        {
            int index = getRandomIndex();
            luckyWheelView.startLuckyWheelWithTargetIndex(index);
        });

        luckyWheelView.setLuckyRoundItemSelectedListener(index ->

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.label_congratulations, data.get(index==0 ?index:index-1).text), Toast.LENGTH_SHORT).show());
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private int getRandomIndex() {
        Random rand = new Random();
        return rand.nextInt(data.size() - 1);
    }

    private int getRandomRound() {
        Random rand = new Random();
        return rand.nextInt(10) ;
    }
}
