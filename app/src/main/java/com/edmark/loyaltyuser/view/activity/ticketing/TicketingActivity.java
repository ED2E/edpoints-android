package com.edmark.loyaltyuser.view.activity.ticketing;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.BuildConfig;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Ads;
import com.edmark.loyaltyuser.model.Ticketing;
import com.edmark.loyaltyuser.presenter.advertisement.AdvertisementPresenter;
import com.edmark.loyaltyuser.presenter.advertisement.AdvertsimentContract;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.advertisement.AdvertisementDetailsActivity;
import com.edmark.loyaltyuser.view.activity.main.OnboardingActivity;
import com.edmark.loyaltyuser.view.activity.splash.SplashActivity;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_ADS_LIST;
import static com.edmark.loyaltyuser.constant.Common.COMPANY_CODE;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.DEVICE_TYPE;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_APP_VER;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_TICKETING;
import static com.edmark.loyaltyuser.constant.Common.KEY_ADS_DETAILS;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_UPDATE;
import static com.edmark.loyaltyuser.constant.Common.SPLASH_SCREEN_TIME;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/18/2018.
 */
@SuppressWarnings("unchecked")
public class TicketingActivity extends BaseActivity implements AdvertsimentContract.View {
    public static final String TAG = TicketingActivity.class.getSimpleName();

    @BindView(R.id.activity_ticketing_recyclerview_merchant)
    RecyclerView recyclerView_option;
    @BindView(R.id.activity_ticketing_toolbar_title)
    TextView textView_tootbar_title;
    @BindView(R.id.activity_ticketing_textview_message)
    TextView textView_message;

    private ArrayList<Ticketing> adsList = new ArrayList<>();
    private BaseRecyclerViewAdapter adapter;
    private AdvertsimentContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticketing);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);

        presenter = new AdvertisementPresenter(this);
        setupToolBar(R.id.activity_ticketing_toolbar);
        initGUI();

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGUI() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        Gson gson = new Gson();
        adapter = new BaseRecyclerViewAdapter(adsList, R.layout.child_ticketing, BR.Ticketing, (v, item, position) ->
        {
            Intent intent = new Intent(getContext(), TicketingDetailsActivity.class);
            intent.putExtra(KEY_ADS_DETAILS, gson.toJson(adsList.get(position)));
            startActivity(intent);
        });
        recyclerView_option.setLayoutManager(linearLayoutManager);
        recyclerView_option.setAdapter(adapter);

        AppPreference preference = AppPreference.getInstance(getContext());
        String country_code = preference.getString(KEY_COUNTRY_CODE);
//        getAdvertisement(country_code);
//        presenter.getAdvertisement(country_code);

        initData();

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onLoadSuccess(ArrayList<Ads> adsList) {
//        this.adsList.clear();
//        this.adsList.addAll(adsList);
//        adapter.notifyDataSetChanged();
//        adapter.animateTo(this.adsList);
//        if (adsList.size() != 0)
//            textView_message.setVisibility(View.GONE);
//        else
//            textView_message.setVisibility(View.VISIBLE);
    }

    private void initData() {
        showProgress();
        AppPreference preference = AppPreference.getInstance(getContext());
        String country_code = preference.getString(KEY_COUNTRY_CODE);
        adsList.clear();
        ArrayList<Ticketing> datalist = new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
//        // Create a reference to the cities collection
//        CollectionReference citiesRef = db.collection(FIREBASE_KEY_TICKETING);
//        // Create a query against the collection.
//        Query query = citiesRef.whereEqualTo(COMPANY_CODE, country_code);
        db.collection(FIREBASE_KEY_TICKETING)
                .whereEqualTo(COMPANY_CODE, country_code)
                .get()
                .addOnCompleteListener(task -> {

                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            Log.d(TAG, document.getId() + " => " + document.getData());
                        }
                        //direct document
                        for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                            Ticketing ticketing = doc.toObject(Ticketing.class);
                            datalist.add(ticketing);
                        }

                        adsList.addAll(datalist);
                        adapter.notifyDataSetChanged();
                        adapter.animateTo(adsList);
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                        hideProgress();
                        showToast(getResources().getString(R.string.error_connection));
                    }
                    if (adsList.size() != 0)
                        textView_message.setVisibility(View.GONE);
                    else
                        textView_message.setVisibility(View.VISIBLE);
                    hideProgress();
                });
    }
}
