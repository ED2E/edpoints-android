package com.edmark.loyaltyuser.view.activity.transaction.initialize;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CustomerProfile;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImageContract;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImagePresenter;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.ui.imageview.CircleImageView;
import com.edmark.loyaltyuser.ui.slideview.SlideView;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.ThousandTextWatcher;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.SocketTimeoutException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_MERCHANT_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.API_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.API_UPDATE_TRANSACTION_CSB;
import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_TRANSACTION_CSB;
import static com.edmark.loyaltyuser.constant.Common.CSB;
import static com.edmark.loyaltyuser.constant.Common.CSB_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.DOCNO;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA;
import static com.edmark.loyaltyuser.constant.Common.KEY_EMAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_LOYALTY_EARNED;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_SCAN_PROCESS;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANS_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.MERCHANTID;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RESPONSE_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.playBeep;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


/*
 * Created by DEV-Michael-ED2E on 04/10/2018.
 */
public class TransactionActivity extends BaseActivity implements LoadImageContract {
    private static final String TAG = TransactionActivity.class.getSimpleName();


    @BindView(R.id.activity_transaction_button_slide_to_pay)
    SlideView button_slide;
    @BindView(R.id.activity_transaction_button_ok)
    Button button_proceed;
    @BindView(R.id.activity_transaction_button_cancel)
    Button button_abort;
    @BindView(R.id.activity_transaction_imageview_profile)
    CircleImageView imageView_profile;
    @BindView(R.id.activity_transaction_textview_name)
    TextView textView_name;
    @BindView(R.id.activity_transaction_progress)
    ProgressBar progressbar_profile;
    @BindView(R.id.activity_transaction_layout_profile)
    LinearLayout layout_profile;
    @BindView(R.id.activity_transaction_layout_transaction_field)
    LinearLayout layout_transaction_field;
    @BindView(R.id.activity_transaction_editText_amount)
    EditText editText_amount;
    @BindView(R.id.activity_transaction_editText_description)
    EditText editText_description;

    private ApiInterface apiService;
    private AppPreference preference;
    private String merchantid;
    private String eda;
    private String email;
    private Dialog dialog;
    private String amount;
    private String description;
    private String scan_process;
    private boolean isTransact = false;
    private LoadImagePresenter imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        ButterKnife.bind(this);

        imageLoader = new LoadImagePresenter(this);
        setupToolBar(R.id.activity_transaction_toolbar);
        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        initLoadType(savedInstanceState);
        initLoadAccountInfo();
        initGUI();
    }

    private void initGUI() {
        Log.d(TAG,"initGUI");
//        editText_amount.Currency = Currency.NONE;
//        editText_amount.Spacing = false;
//        editText_amount.Decimals = false;

        editText_amount.addTextChangedListener(new ThousandTextWatcher(editText_amount));

        button_slide.setOnFinishListener(() -> {
                    if (!editText_amount.getText().toString().equals("")) {
                        initPinDialog();
                    } else {
                        button_slide.reset();
                        showToast(getResources().getString(R.string.error_invalid_amount));
                    }
                }
        );

        button_proceed.setOnClickListener(v ->
        {
            layout_profile.setVisibility(View.GONE);
            layout_transaction_field.setVisibility(View.VISIBLE);
            button_proceed.setVisibility(View.GONE);
            button_slide.setVisibility(View.VISIBLE);
        });
        button_abort.setOnClickListener(v -> finish());

        if(amount!=null)
        {
            if(!amount.equals(""))
            {
                editText_amount.setText(amount);
                editText_amount.setEnabled(false);
                editText_description.setText(description);
                editText_description.setEnabled(false);
            }
        }
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initLoadType(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchantid = null;
                amount = null;
                description = null;
                scan_process = null;
            } else {
                merchantid = extras.getString(KEY_TRANS_CUSTID);
                amount = extras.getString(KEY_TRANSACTION_AMOUNT);
                description = extras.getString(KEY_DESCRIPTION);
                scan_process = extras.getString(KEY_SCAN_PROCESS);

            }
        } else {
            merchantid = (String) savedInstanceState.getSerializable(KEY_TRANS_CUSTID);
            amount = (String) savedInstanceState.getSerializable(KEY_TRANSACTION_AMOUNT);
            description = (String) savedInstanceState.getSerializable(KEY_DESCRIPTION);
            scan_process = (String) savedInstanceState.getSerializable(KEY_SCAN_PROCESS);
        }
    }

    private void initLoadAccountInfo() {
        showProgress();
        Call<ResponseBody> call = apiService.mainCustInfo(API_MERCHANT_PROFILE, merchantid, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfile info = gson.fromJson(json, CustomerProfile.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());
                        eda = info.getEda();
                        email = info.getEmail();
                        textView_name.setText(info.getName());

                        progressbar_profile.setVisibility(View.VISIBLE);
                        imageLoader.loadImage(getContext(), TAG, 0, info.getImage_url(), imageView_profile, R.drawable.ic_default_user, true);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo();
                hideProgress();
            }
        });
    }

    private void initTransaction(String transactionid, String description) {
        Timber.d("initTransaction");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.transaction(
                API_TRANSACTION,
                custId,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()),
                merchantid,
                transactionid,
                editText_amount.getText().toString().replace(",", ""),
                description);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));

                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        isTransact = false;
                        playBeep(getContext(),"transaction.mp3");
                        Intent intent = new Intent(TransactionActivity.this, TransactionSuccessActivity.class);
                        intent.putExtra(KEY_MERCHANT, textView_name.getText().toString());
                        double trans_amount = Double.valueOf(editText_amount.getText().toString().replace(",", "").trim());
                        float loyalty_earned = (float) (trans_amount / 100d);
                        intent.putExtra(KEY_TRANSACTION_ID, transactionid);
                        intent.putExtra(KEY_TRANSACTION_AMOUNT, String.valueOf(trans_amount));
                        intent.putExtra(KEY_LOYALTY_EARNED, String.valueOf(loyalty_earned));
                        intent.putExtra(KEY_DESCRIPTION, description);

                        intent.putExtra(KEY_MERCHANT_ID, merchantid);
                        intent.putExtra(KEY_EDA, eda);
                        intent.putExtra(KEY_EMAIL, email);
                        startActivity(intent);
                        hideProgress();
                        finish();
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();

                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    private void initPinDialog() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                if(!isTransact) {
                    isTransact = true;
                    String pin = editText_pin.getText().toString();
                    String amount = editText_amount.getText().toString().replace(",", "");
                    String description = editText_description.getText().toString();
                    validateTransaction(pin, amount, description);
                }
            }
        });
        button_dismiss.setOnClickListener(v ->
        {
            button_slide.reset();
            dialog.dismiss();
        });

        dialog.setOnDismissListener(dialog -> {
            button_slide.reset();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }



//    private void getTransactionID() {
//        showProgress();
//        String custId = preference.getString(KEY_CUSID);
//        Call<ResponseBody> call = apiService.mainCustInfo(
//                API_GET_TRANSACTION_ID,
//                custId,
//                String.valueOf(unixTimeStamp()),
//                getVCKeyED2E());
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                Timber.d("url():" + response.raw().request().url());
//                if(response.body()==null)
//                {
//                    isTransact = false;
//                    showToast(getResources().getString(R.string.error_connection));
//                    hideProgress();
//                    return;
//                }
//                try {
//                    String json = response.body().string();
//                    Timber.d("onResponse:" + response.code() + " " + json);
//                    JSONObject obj = new JSONObject(json);
//
//                    String status = (String) obj.get(STATUS);
//
//                    if (status.equals(SUCCESS)) {
//
//                        String transactionid = (String) obj.get(TRANSACTION_ID);
//                        initTransaction(transactionid,description);
//
//                    } else {
//                        isTransact = false;
//                        hideProgress();
//                        String message = (String) obj.get(MESSAGE);
//                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
//                    }
//                } catch (IOException | JSONException e) {
//                    isTransact = false;
//                    e.printStackTrace();
//                    hideProgress();
//                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                isTransact = false;
//                if(t instanceof SocketTimeoutException){
//                    showToast(getResources().getString(R.string.error_connection));
//                    hideProgress();
//                    return;
//                }
//
//                Timber.d("onFailure:");
//                t.printStackTrace();
//                initLoadAccountInfo();
//                hideProgress();
//            }
//        });
//    }

    private void validateTransaction(String pin, String amount, String description) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.validateTransaction(
                API_VALIDATE_TRANSACTION,
                custId,
                merchantid,
                getHash(pin),
                amount,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
//                        getTransactionID();
                        String transactionid = (String) obj.get(RESPONSE_TRANSACTION_ID);

                        //Log.e("CHECK", scan_process + "RESPONSE: " + response.toString());

                        //if(scan_process.equals("transaction")){
                        if(scan_process == null) {
                            //Transaction Process
                            initTransaction(transactionid,description);
                        } else {
                            //Cash Bill Process
                            initTransactionCSB(custId,description);
                        }
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
                button_slide.reset();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo();
                hideProgress();
            }
        });
    }

    @Override
    public void onLoadfailed(int requestCode) {
        if (progressbar_profile != null)
            progressbar_profile.setVisibility(View.GONE);
    }

    @Override
    public void onResourceready(int requestCode) {
        if (progressbar_profile != null)
            progressbar_profile.setVisibility(View.GONE);
    }

    private void initTransactionCSB(String custid, String docno){
        Timber.d("initTransactionCSB");
        showProgress();
        Call<ResponseBody> call = apiService.cashBillTrans(
                API_UPDATE_TRANSACTION_CSB,
                custid,
                docno,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp())
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("Raw Url:" + response.raw().request().url());

//                Log.i("tagconvertstr", "["+response+"]");
//                Log.i("tagconvertstr", "["+response.body()+"]");
//                Log.i("tagconvertstr", "["+response.body().toString()+"]");
//                Log.i("tagconvertstr", "["+response.message()+"]");

                if (response.body() == null) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body().string());
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);

                    if (status.equals("Success")) {
                        isTransact = false;
                        playBeep(getContext(),"transaction.mp3");
                        Intent intent = new Intent(TransactionActivity.this, TransactionSuccessActivity.class);
                        intent.putExtra(KEY_MERCHANT, textView_name.getText().toString());
                        double trans_amount = Double.valueOf(editText_amount.getText().toString().replace(",", "").trim());
                        float loyalty_earned = (float) (trans_amount / 100d);
                        String transactionID = (String) obj.get(CSB);

                        intent.putExtra(KEY_TRANSACTION_ID, transactionID);
                        intent.putExtra(KEY_TRANSACTION_AMOUNT, String.valueOf(trans_amount));
                        intent.putExtra(KEY_LOYALTY_EARNED, String.valueOf(loyalty_earned));
                        intent.putExtra(KEY_DESCRIPTION, description);
                        intent.putExtra(KEY_MERCHANT_ID, merchantid);
                        intent.putExtra(KEY_EDA, eda);
                        intent.putExtra(KEY_EMAIL, email);
                        startActivity(intent);
                        hideProgress();
                        finish();
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        hideProgress();

                        Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        if (dialog.getWindow() != null)
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.dialog_main);

                        TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);
                        Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
                        Button button_cancel = dialog.findViewById(R.id.dialog_main_button_cancel);

                        textView_message.setText(message);
                        button_ok.setText(getResources().getString(R.string.button_ok));

                        button_cancel.setVisibility(View.GONE);

                        button_ok.setOnClickListener(v -> {
                            dialog.dismiss();
                            finish();
                        });
                        dialog.show();

                        //showDialog(0, message,true);
                        //Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();

                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));
                    Log.i("TEST", "e.printStackTrace(): " + writer.toString());
                    Log.i("TEST", " e.getMessage(): " + e.getMessage());


                    hideProgress();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d(TAG, "onFailure:");
                t.printStackTrace();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }
}
