package com.edmark.loyaltyuser.view.activity.utility;


import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.UtilityAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Utility;
import com.edmark.loyaltyuser.rest.ApiClientExpressPay;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.sha256;

public class UtilityActivity extends BaseActivity {

    public static final String TAG = UtilityActivity.class.getSimpleName();

    @BindView(R.id.activity_utility_toolbar_title)
    TextView textView_tootbar_title;
    @BindView(R.id.activity_utility_textview_message)
    TextView textView_message;
    @BindView(R.id.activity_utility_textview_label)
    TextView textView_label;
    @BindView(R.id.activity_utility_editText_search)
    TextView editext_search;

    private String biller_id, biller_name;

    private ApiInterface apiInterface;
    private List<Utility> modelList;
    private List<Utility> modelList2;

    static RecyclerView recyclerView;
    UtilityAdapter recyclerAdapter;

    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_utility_toolbar);

        //initBillerInfo(savedInstanceState);
        initGui();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    /*private void initBillerInfo(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            biller_id = "BILLER ID";
            biller_name = "BILLER NAME";
        } else {
            biller_id = extras.getString(KEY_UTILITY_BILLERID);
            biller_name = extras.getString(KEY_UTILITY_BILLERNAME);
        }

        showToast("Biller ID: " + biller_id + "\n" + "Biller Name: " + biller_name);
    }*/

    private void initGui() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        //-- initialize recycler view, layout, and adapter
        recyclerView = (RecyclerView)findViewById(R.id.activity_utility_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new UtilityAdapter(this,modelList);
        recyclerView.setAdapter(recyclerAdapter);

        Log.e("CHECK", "1");
        initAPI();
    }

    private void initAPI() {
        showProgress();

        apiInterface = ApiClientExpressPay.getClient().create(ApiInterface.class);
        Call<List<Utility>> call = apiInterface.retrieveBillers(getHash(sha256("expresspayapikey")));

        Log.e("CHECK", "2");

        call.enqueue((new Callback<List<Utility>>() {
            @Override
            public void onResponse(Call<List<Utility>> call, Response<List<Utility>> response) {
                Log.e("SUCCESS", "onResponse: " + response.toString());

                Log.d("TAG","Response = "+ modelList);

                Timber.d("url():" + response.raw().request().url());

                if(response.body()==null)
                {
                    textView_message.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    finish();
                    return;
                }

                if(response.isSuccessful()) {
                    hideProgress();

                    Log.i("SUCCESS PT 2", "Successful Response: " + response.body().toString());

                    //-- place response body inside array list of model class
                    modelList = new ArrayList<>(response.body());
                    modelList2 = new ArrayList<>(response.body());
                    String billerId, billerName;

                    //-- test to preview data response size after being placed in array list
                    Log.i("SUCCESS PT 3", "Model Class: " + modelList.size());

                    //-- test to preview data response stored in array list of model class
                    for(int i = 0; i < modelList.size(); i++){
                        billerId = String.valueOf(modelList.get(i).getBillerID());
                        billerName = String.valueOf(modelList.get(i).getBillerName());

                        Log.i("SUCCESS PT 4", "========================\n" +
                                "\n" + billerId +
                                "\n" + billerName);
                    }
                    if (modelList.size() != 0)
                        textView_message.setVisibility(View.GONE);
                    else
                        textView_message.setVisibility(View.VISIBLE);

                    recyclerAdapter.setModelList(modelList);
                    initSearchFunction(modelList2);
                }
            }

            @Override
            public void onFailure(Call<List<Utility>> call, Throwable t) {
                if(t instanceof SocketTimeoutException||t instanceof ConnectException){
                    textView_message.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    finish();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                finish();
            }
        }));
    }

    /*@BindView(R.id.activity_utility_recyclerview)
    RecyclerView recyclerView;
    private BaseRecyclerViewAdapter adapter;
    private BaseRecyclerViewSectionedAdapter mSectionedAdapter;

    @BindView(R.id.activity_utility_toolbar_title)
    TextView textView_tootbar_title;

    private ApiInterface apiService;
    private AppPreference preference;
    private String account_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiService = ApiClientExpressPay.getClient().create(ApiInterface.class);

        setupToolBar(R.id.activity_utility_toolbar);
        initGui();
    }

    private void initGui() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        initAPI();
    }

    private void initAPI() {
        showProgress();

        *//*
        Call<ResponseBody> call = apiService.transactions(API_TRANSACTION_HISTORY, custId, account_type, dateFrom, dateTo, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    textView_message.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = response.body().string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            String message = (String) obj.get(MESSAGE);
                            Timber.d("message:" + message);
                            setNoResult();
                            hideProgress();
                        } catch (JSONException e) {

                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            ArrayList<TransactionHistory> data = new ArrayList<>();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Gson gson = new Gson();
                                TransactionHistory bonus = gson.fromJson(jsonobject.toString(), TransactionHistory.class);
                                bonus.setType(account_type);
                                Timber.d("Data:" + gson.toJson(bonus));
                                data.add(bonus);
                            }

                            setData(data);

                        }
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    hideProgress();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    textView_message.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
            }
        });

        *//*

        //========================================================================================


        Log.e("CHECK", "1");

        Call<ResponseBody> call = apiService.getBiller(getHash(sha256("expresspayapikey")));

        Log.e("CHECK", "2");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgress();
                Log.e("SUCCESS", "onResponse: " + response.toString());

                if(response.isSuccessful()) {
                    Log.i("SUCCESS PT 2", "Successful Response: " + response.body().toString());

                    try {
                        String json = response.body().string();
                        Log.d("SUCCESS", "JSON: " + json);

                        JSONArray array = new JSONArray(json);
                        //JSONObject object = array.getJSONObject("");


                    } catch (JSONException e) {
                        Log.e("SUCCESS CATCH", "JSONException: " + e.getMessage());
                    } catch (IOException e) {
                        Log.e("SUCCESS CATCH", "IOException: " + e.getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Log.e("ERROR", "onFailure: " + t.getMessage());
                if(t instanceof SocketTimeoutException){
                    Toast.makeText(UtilityActivity.this, "Socket Time out. Please try again. " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
                Toast.makeText(UtilityActivity.this, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }*/

    private void initSearchFunction(/*JSONArray downline_list*/List<Utility> modelList2) {
        Observable<CharSequence> changeObservableSearch = RxTextView.textChanges(editext_search);

        disposables.add(changeObservableSearch.map(inputText -> (inputText.length() == 0))
                .subscribe(isValid ->
                {
                    if(!isValid)
                    {
                        //showToast("Searching...");
                        textView_label.setVisibility(View.GONE);



                        modelList.clear();
                        recyclerAdapter.notifyDataSetChanged();

                        /*showToast("1 "+modelList.size());
                        showToast("2 "+modelList2.size());*/

                        for(int i = 0; i < modelList2.size(); i++){
                            /*billerId = String.valueOf(modelList.get(i).getBillerID());
                            billerName = String.valueOf(modelList.get(i).getBillerName());

                            Log.i("SUCCESS PT 4", "========================\n" +
                                    "\n" + billerId +
                                    "\n" + billerName);*/

                            //JSONObject downline_object = downline_list.getJSONObject(i);

                            if (modelList2.size() == 0) {
                                textView_message.setText(getResources().getString(R.string.label_no_donwline_available));
                                textView_message.setVisibility(View.VISIBLE);
                            }
                            else {
                                if (modelList2.get(i).getBillerName().toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
                                    Utility list = new Utility(
                                            modelList2.get(i).getBillerID(),
                                            modelList2.get(i).getBillerName()
                                    );
                                    recyclerAdapter.notifyDataSetChanged();

                                    modelList.add(list);
                                    recyclerAdapter.setModelList(modelList);
                                }
                            }
                        }

                        if (modelList.size() == 0) {
                            textView_message.setText(getResources().getString(R.string.label_no_result));
                            textView_message.setVisibility(View.VISIBLE);
                        }
                        else {
                            textView_message.setVisibility(View.GONE);
                        }

                        //recyclerAdapter.setModelList(response.body());

                        /*for(int i = 0; i < downline_list.length(); i++){
                            JSONObject downline_object = downline_list.getJSONObject(i);

                            if (downline_list.length() == 0) {
                                textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                                textview_family_tree.setVisibility(View.VISIBLE);
                            }
                            else {
                                if (downline_object.getString(DOWNLINE_CUSTNAME).toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
                                    FamilyTree list = new FamilyTree(
                                            downline_object.getString(DOWNLINE_TOTALDONWLINES),
                                            downline_object.getString(DOWNLINE_CUSTID),
                                            downline_object.getString(DOWNLINE_CUSTSERIALNO),
                                            downline_object.getString(DOWNLINE_CUSTEDA),
                                            downline_object.getString(DOWNLINE_CUSTNAME),
                                            downline_object.getString(DOWNLINE_CUSTTYPE),
                                            downline_object.getString(DOWNLINE_CUSTJOINDATE)
                                    );
                                    recyclerAdapter.notifyDataSetChanged();

                                    modelList.add(list);
                                    recyclerAdapter.setModelList(modelList, counter);
                                }
                            }
                        }

                        //showToast("Model Size: " + modelList.size());

                        if(modelList.size() == 0) {
                            textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                            textview_family_tree.setVisibility(View.VISIBLE);
                        } else {
                            textview_family_tree.setVisibility(View.GONE);
                        }*/
                    }
                    else
                    {
                        //showToast("Search stopped!");
                        textView_label.setVisibility(View.VISIBLE);

                        modelList.clear();
                        recyclerAdapter.notifyDataSetChanged();

                        /*showToast("1 "+modelList.size());
                        showToast("2 "+modelList2.size());*/

                        for(int i = 0; i < modelList2.size(); i++){
                            /*billerId = String.valueOf(modelList.get(i).getBillerID());
                            billerName = String.valueOf(modelList.get(i).getBillerName());

                            Log.i("SUCCESS PT 4", "========================\n" +
                                    "\n" + billerId +
                                    "\n" + billerName);*/

                            //JSONObject downline_object = downline_list.getJSONObject(i);

                            if (modelList2.size() == 0) {
                                textView_message.setText(getResources().getString(R.string.label_no_donwline_available));
                                textView_message.setVisibility(View.VISIBLE);
                            }
                            else {
                                Utility list = new Utility(
                                        modelList2.get(i).getBillerID(),
                                        modelList2.get(i).getBillerName()
                                );
                                recyclerAdapter.notifyDataSetChanged();

                                modelList.add(list);
                                recyclerAdapter.setModelList(modelList);
                            }
                        }

                        if (modelList.size() == 0) {
                            textView_message.setText(getResources().getString(R.string.label_no_result));
                            textView_message.setVisibility(View.VISIBLE);
                        }
                        else {
                            textView_message.setVisibility(View.GONE);
                        }

                        /*modelList.clear();
                        recyclerAdapter.notifyDataSetChanged();

                        for(int i = 0; i < downline_list.length(); i++){
                            JSONObject downline_object = downline_list.getJSONObject(i);

                            if (downline_list.length() != 0) {
                                textview_family_tree.setVisibility(View.GONE);

                                FamilyTree list = new FamilyTree(
                                        downline_object.getString(DOWNLINE_TOTALDONWLINES),
                                        downline_object.getString(DOWNLINE_CUSTID),
                                        downline_object.getString(DOWNLINE_CUSTSERIALNO),
                                        downline_object.getString(DOWNLINE_CUSTEDA),
                                        downline_object.getString(DOWNLINE_CUSTNAME),
                                        downline_object.getString(DOWNLINE_CUSTTYPE),
                                        downline_object.getString(DOWNLINE_CUSTJOINDATE)
                                );
                                recyclerAdapter.notifyDataSetChanged();

                                modelList.add(list);
                                recyclerAdapter.setModelList(modelList, counter);
                            }
                            else {
                                textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                                textview_family_tree.setVisibility(View.VISIBLE);
                            }
                        }

                        if(modelList.size() == 0) {
                            textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                            textview_family_tree.setVisibility(View.VISIBLE);
                        } else {
                            textview_family_tree.setVisibility(View.GONE);
                        }*/
                    }
                }));
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    /*public static String getHash(String str) {
        MessageDigest digest = null;
        byte[] input = null;

        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            input = digest.digest(str.getBytes("UTF-8"));

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return convertToHex(input);
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }*/

}
