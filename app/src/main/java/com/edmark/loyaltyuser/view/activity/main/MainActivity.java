package com.edmark.loyaltyuser.view.activity.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTabHost;

import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CardType;
import com.edmark.loyaltyuser.model.CustomerInfo;
import com.edmark.loyaltyuser.model.CustomerProfile;
import com.edmark.loyaltyuser.model.FirebaseToken;
import com.edmark.loyaltyuser.model.HomeMessageEvent;
import com.edmark.loyaltyuser.model.MainMessageEvent;
import com.edmark.loyaltyuser.model.Merchant;
import com.edmark.loyaltyuser.model.MerchantMessageEvent;
import com.edmark.loyaltyuser.model.sugardb.FirebasePendingNotif;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientDemo;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.PermissionUtils;
import com.edmark.loyaltyuser.view.activity.account.AccountActivity;
import com.edmark.loyaltyuser.view.activity.advertisement.AdvertisementActivity;
import com.edmark.loyaltyuser.view.activity.edcoin.ConversionActivity;
import com.edmark.loyaltyuser.view.activity.edcoin.EDCoinActivity;
import com.edmark.loyaltyuser.view.activity.familytree.FamilyTreeActivity;
import com.edmark.loyaltyuser.view.activity.ledger.LedgerActivity;
import com.edmark.loyaltyuser.view.activity.login.LoginActivity;
import com.edmark.loyaltyuser.view.activity.merchant.MerchantDetailsActivity;
import com.edmark.loyaltyuser.view.activity.merchant.MerchantListActivity;
import com.edmark.loyaltyuser.view.activity.pointtransfer.PointTransferActivity;
import com.edmark.loyaltyuser.view.activity.profile.ProfileActivity;
import com.edmark.loyaltyuser.view.activity.statistic.StatisticalAnalysisActivity;
import com.edmark.loyaltyuser.view.activity.support.SupportActivity;
import com.edmark.loyaltyuser.view.activity.ticketing.TicketingActivity;
import com.edmark.loyaltyuser.view.activity.transaction.history.TransactionHistoryActivity;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionActivity;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionMassageChairActivity;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionWebviewActivity;
import com.edmark.loyaltyuser.view.activity.transaction.qrcode.ScanQRActivity;
import com.edmark.loyaltyuser.view.activity.transaction.qrcode.ShowQRActivity;
import com.edmark.loyaltyuser.view.activity.utility.TransactionUtilityActivity;
import com.edmark.loyaltyuser.view.activity.utility.UtilityActivity;
import com.edmark.loyaltyuser.view.fragment.main.HomeFragment;
import com.edmark.loyaltyuser.view.fragment.main.MapFragment;
import com.edmark.loyaltyuser.view.fragment.main.MerchantFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.orm.SugarDb;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.*;
import static com.edmark.loyaltyuser.service.Nofication.showBigNotification;
import static com.edmark.loyaltyuser.service.Nofication.showNotification;
import static com.edmark.loyaltyuser.util.AppUtility.androidId;
import static com.edmark.loyaltyuser.util.AppUtility.getBitmapFromURL;
import static com.edmark.loyaltyuser.util.AppUtility.getDeviceName;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getOsVersion;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.setHintFont;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;
import static com.edmark.loyaltyuser.util.SugarDBUtils.createTable;

/*
 * Created by DEV-Michael-ED2E on 04/02/2018.
 */

public class MainActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    public static final String TAG = MainActivity.class.getSimpleName();

    private ApiInterface apiService;
    private String account_type = "";
    private String custId;
    private AppPreference preference;
    private FragmentTabHost mTabHost;
    private String merchant_code = "";

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private boolean gpsDialogisShown = false;
    private boolean pinDialogisShown = false;
    private ArrayList<CardType> datalist = new ArrayList<>();
    private int index = 1;
    private boolean isWriting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);

        initGUI();
        custId = preference.getString(KEY_CUSID);
        account_type = preference.getString(KEY_ACCOUNT_TYPE);
    }

    private void checkFirebaseToken(String custId, CustomerInfo info, String deviceid, String devicename, String deviceos) {
        Timber.d("checkFirebaseToken:");
        ArrayList<String> token = new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(FIREBASE_KEY_TOKEN)
                .whereEqualTo(CUSTID, custId)
                .whereEqualTo(DEVICE_ID, deviceid)
                .whereEqualTo(DEVICE_NAME, devicename)
                .whereEqualTo(DEVICE_OS_VER, deviceos)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        //direct document
                        for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                            if (doc.get(CUSTID) != null) {
                                Timber.d("FirebaseToken:" + doc.getString(CUSTID) + " => " + doc.getString(EDA));
                            }
                        }

                        if (task.getResult() != null) {
                            List<FirebaseToken> tokens = task.getResult().toObjects(FirebaseToken.class);
                            for (FirebaseToken item : tokens) {
                                token.add(item.getToken());
                                Timber.d("FirebaseToken:" + item.getDeviceid() + " => " + item.getToken());
                            }
                        }

                        if (token.size() == 0) {
                            if(!isWriting) {
                                isWriting = true;
                                initWritetoFirebaseDB(custId, info.getEda(), info.getEmail(), account_type, info.getCompany_code());
                            }
                        } else {
                            String notificationToken = FirebaseInstanceId.getInstance().getToken();
                            Timber.d("notificationToken:" + notificationToken);
                            for (DocumentSnapshot document : task.getResult()) {
                                Timber.d(document.getId() + " => " + document.getData());
                                DocumentReference contact = db.collection(FIREBASE_KEY_TOKEN).document(document.getId());
                                contact.update(DEVICE_TOKEN, notificationToken).addOnSuccessListener(aVoid -> Timber.d("Token updated successfully"));
                            }
                        }
                        Timber.d("notificationToken:" + token);
                    } else {
                        Timber.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    private void initWritetoFirebaseDB(String custid, String eda, String email, String type, String country) {
        Timber.d("initWritetoFirebaseDB:");
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        String notificationToken = FirebaseInstanceId.getInstance().getToken();
        FirebaseToken token = new FirebaseToken(
                notificationToken,
                custid,
                eda,
                email,
                androidId(getContext()),
                getOsVersion(getContext()),
                String.valueOf(unixTimeStamp()),
                getDeviceName(getContext()),
                type,
                country);
        db.collection(FIREBASE_KEY_TOKEN)
                .document()
                .set(token)
                .addOnSuccessListener(aVoid ->
                {
                    isWriting = false;
                    Log.d(TAG, "DocumentSnapshot successfully written!");

                })
                .addOnFailureListener(e ->
                {
                    isWriting = false;
                    Log.w(TAG, "Error writing document", e);
                });
    }

    private void initGUI() {

        if (preference != null) {
            custId = preference.getString(KEY_CUSID);
            account_type = preference.getString(KEY_ACCOUNT_TYPE);
        }

        mTabHost = findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        this.setNewTab(mTabHost, TAG_FRAGMENT_HOME + ":" + account_type, R.string.label_home, R.drawable.ic_home, HomeFragment.class);
        this.setNewTab(mTabHost, TAG_FRAGMENT_MERCHANT, R.string.label_merchant, R.drawable.ic_merchant, MerchantFragment.class);
        this.setNewTab(mTabHost, TAG_FRAGMENT_LOCATION, R.string.label_location, R.drawable.ic_location, MapFragment.class);

        //home fragment
        initHomePage();

        mTabHost.setOnTabChangedListener(tabId -> {
            Timber.d("onTabChanged:" + tabId);
            if (tabId.equals(TAG_FRAGMENT_HOME + ":" + account_type)) {
                initHomePage();

            } else if (tabId.equals(TAG_FRAGMENT_MERCHANT)) {

                EventBus.getDefault().post(new MainMessageEvent("testing", null));
            } else if (tabId.equals(TAG_FRAGMENT_LOCATION)) {

                initMapData();
            }
        });
        enableMyLocationIfPermitted(null);

        initNotifChecker();
    }

    private void initNotifChecker() {
        Gson gson = new Gson();
        try {
            List<FirebasePendingNotif> notifs = FirebasePendingNotif.listAll(FirebasePendingNotif.class);
            Timber.d("pendingNotifs:" + notifs);
            for (FirebasePendingNotif notification : notifs) {
                Timber.d("pendingNotifs:" + notification.getData());
                Map pendingNotif = gson.fromJson(notification.getData(), Map.class);
                Timber.d("pendingNotif:" + pendingNotif);
                String notifCustid = (String) pendingNotif.get(FIREBASE_KEY_CUSTID);
                if (custId.equals(notifCustid)) {
                    Thread thread = new Thread(() -> {
                        Timber.d("custId:" + custId + " : " + notifCustid);
                        String id = "";
                        String imageUrl = "";
                        String url = "";
                        String message = "";
                        String notitype = "";
                        String title = getString(R.string.app_name);
                        String iconUrl = "";
                        if (pendingNotif.get(FIREBASE_KEY_ID) != null) {
                            id = (String) pendingNotif.get(FIREBASE_KEY_ID);
                        }
                        if (pendingNotif.get(FIREBASE_KEY_URL) != null) {
                            url = (String) pendingNotif.get(FIREBASE_KEY_URL);
                        }
                        if (pendingNotif.get(FIREBASE_KEY_MESSAGE) != null) {

                            message = (String) pendingNotif.get(FIREBASE_KEY_MESSAGE);
                        }
                        if (pendingNotif.get(FIREBASE_KEY_NOTIFTYPE) != null) {
                            notitype = (String) pendingNotif.get(FIREBASE_KEY_NOTIFTYPE);
                        }
                        if (pendingNotif.get(FIREBASE_KEY_PICTURE) != null) {
                            imageUrl = (String) pendingNotif.get(FIREBASE_KEY_PICTURE);
                        }
                        if (pendingNotif.get(FIREBASE_KEY_TITLE) != null) {
                            title = (String) pendingNotif.get(FIREBASE_KEY_TITLE);
                        }
                        if (pendingNotif.get(FIREBASE_KEY_ICON) != null) {
                            iconUrl = (String) pendingNotif.get(FIREBASE_KEY_ICON);
                        }
                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                        if (iconUrl != null && iconUrl.length() > 4 && Patterns.WEB_URL.matcher(iconUrl).matches()) {
                            icon = getBitmapFromURL(iconUrl);
                            if (icon != null) {
                                icon = Bitmap.createScaledBitmap(icon, 192, 192, true);
                            }
                        }
                        if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {
                            Bitmap bitmap = getBitmapFromURL(imageUrl);
                            if (bitmap != null) {
                                showBigNotification(getContext(), id, notitype, url, bitmap, icon, title, message);
                            } else {
                                showNotification(getContext(), id, notitype, url, icon, title, message);
                            }
                        } else {
                            showNotification(getContext(), id, notitype, url, icon, title, message);
                        }
                    });

                    thread.start();
                    notification.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            createTable(FirebasePendingNotif.class, new SugarDb(this).getDB());
        }
    }

    private void setNewTab(FragmentTabHost tabHost, String tag, int title, int icon, Class<?> fragment) {
        TabHost.TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getTabIndicator(tabHost.getContext(), title, icon)); // new function to inject our own tab layout
        tabHost.addTab(tabSpec, fragment, null);
    }

    private View getTabIndicator(Context context, int title, int icon) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        ImageView iv = view.findViewById(R.id.tab_layout_imageview_icon);
        iv.setImageResource(icon);
        TextView tv = view.findViewById(R.id.tab_layout_textview_title);
        tv.setText(title);
        return view;
    }

    private void initLoadAccountInfo() {
        Log.d(TAG, "initLoadAccountInfo()");
        Call<ResponseBody> call = apiService.mainCustInfo(API_CUSTINFO, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerInfo info = gson.fromJson(json, CustomerInfo.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda() + " " + info.getCompany_code());
                        preference.putString(KEY_COUNTRY_CODE, info.getCompany_code());
                        //For subcribing depends on the company code
                        FirebaseMessaging.getInstance().subscribeToTopic(FIREBASE_TOPIC + info.getCompany_code());
                        EventBus.getDefault().post(new MainMessageEvent(EVENT_SET_INFO, json));
                        checkFirebaseToken(custId, info, androidId(getContext()), getDeviceName(getContext()), getOsVersion(getContext()));
                        initLoadAccountInfo(account_type);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo();
            }
        });
    }

    private void initLoadAccountInfo(String account_type) {
        String custId = preference.getString(KEY_CUSID);
        String page;
        if (account_type.equals(MERCHANT))
            page = API_MERCHANT_PROFILE;
        else
            page = API_CUST_PROFILE;

        Call<ResponseBody> call = apiService.mainCustInfo(page, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfile info = gson.fromJson(json, CustomerProfile.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());
                        String imageurl = info.getImage_url();
                        EventBus.getDefault().post(new MainMessageEvent(EVENT_SET_PROFILE_IMAGE, imageurl));
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Timber.e("initLoadAccountInfo error:" + message + " " + errorCode);
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void initLoadEdPoints() {
        Call<ResponseBody> call = apiService.mainCustInfo(API_CUSTPOINT, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        String point = obj.getString(POINT);
                        String name = obj.getString(NAME);
                        preference.putString(KEY_NAME, name);
                        EventBus.getDefault().post(new MainMessageEvent(EVENT_SET_ED_POINT, point));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadEdPoints();
            }
        });
    }

    private void initLoadLoyaltyPoints() {
        Call<ResponseBody> call = apiService.mainCustInfo(API_LOYALTYPOINT, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        String point = obj.getString(POINT);
                        EventBus.getDefault().post(new MainMessageEvent(EVENT_SET_LOYALTY_POINT, point));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadLoyaltyPoints();
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                showDialog(REQUEST_CODE_EXIT, getResources().getString(R.string.dialog_exit), false);

                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }


    @Override
    protected void onDialogDismiss(int requestCode) {
        super.onDialogDismiss(requestCode);

        if (requestCode == REQUEST_CODE_EXIT) {
            finish();
        } else if (requestCode == REQUEST_CODE_LOGOUT) {

            preference.putBoolean(KEY_REMEMBERED, false);
            preference.putString(KEY_ACCOUNT_TYPE, "");
            preference.putString(KEY_CUSID, "");
            preference.putString(KEY_COUNTRY_CODE, "");
            preference.putString(KEY_EDCOIN_BIND_KEY, "");

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if (requestCode == REQUEST_CODE_OPEN_GPS) {
            initMapData();
        } else if (requestCode == REQUEST_CODE_PIN_STATUS) {
            Timber.d("REQUEST_CODE_PIN_STATUS");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(HomeMessageEvent event) {
        switch (event.getMessage()) {
            case TAG_HOME_ACCOUNT:
                initAccount();
                break;
            case TAG_HOME_ADVERTISEMENT:
                initAdvertisement();
                break;
            case TAG_HOME_KEY_IN:
                initKeyIn();
                break;
            case TAG_HOME_LEDGER:
                initLedger();
                break;
            case TAG_HOME_LOG_OUT:
                logout();
                break;
            case TAG_HOME_PROFILE: {
                initProfile();
                break;
            }
            case TAG_HOME_SCAN:
                initScan();
                break;
            case TAG_HOME_SHOW_QR: {
                initShowQR();
                break;
            }
            case TAG_HOME_SUPPORT:
                initSupport();
                break;
            case TAG_HOME_TRANSACTION:
                initTransactionHistory();
                break;
            case TAG_HOME_STATISTICAL_ANALYSIS:
                initStatisticalAnalysis();
                break;
            case TAG_HOME_UTILITIES:
                initUtilities();
                break;
            case TAG_HOME_RELOAD: {
                initReload();
                break;
            }
            case TAG_HOME_TICKETING: {
                initTicketing();
                break;
            }
            //TODO: remove code for country
            case TAG_HOME_COUNTRY: {
                initCountry();
                break;
            }
            case TAG_HOME_FAMILY_TREE: {
                initFamilyTree();
                break;
            }
            case TAG_HOME_EDCOIN: {
                initEDCoin();
                break;
            }
            case TAG_HOME_POINT_TRANSFER: {
                initPointTransfer();
                break;
            }
            case TAG_HOME_MASSAGE_CHAIR: {
                Timber.d("initMassageChairController");
                initMassageChairController();
                break;
            }
            case TAG_HOME_MASSAGE_CHAIR_USAGE: {
                Timber.d("TAG_HOME_MASSAGE_CHAIR_USAGE");
                getMassageUsage();
                break;
            }
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MerchantMessageEvent event) {
        Timber.d("onMessageEvent:" + event.getData());
        switch (event.getMessage()) {
            case TAG_MERCHANT_OFFLINE:
                initMerchantList(API_OFFLINE_MERCHANT);
//                initMerchants(API_OFFLINE_MERCHANT);
                break;
            case TAG_MERCHANT_ONLINE:
                initMerchantList(API_ONLINE_MERCHANT);
//                initMerchants(API_ONLINE_MERCHANT);
                break;
            case EVENT_VIEW_MERCHANT:
                initViewMerchantDetail(event.getData());
                break;
            case EVENT_VIEW_ONLINE_MERCHANT:
                if (event.getData() != null) {
                    openOnlineMerchantContent(event.getData());
                } else {
                    showToast(getResources().getString(R.string.label_toast_coming_soon));
                }
                break;
            case EVENT_VIEW_MERCHANT_LIST:
                initMerchantListCategory(event.getData(), API_OFFLINE_MERCHANT);
                break;
            case EVENT_VIEW_ONLINE_MERCHANT_LIST:
                initMerchantListCategory(event.getData(), API_ONLINE_MERCHANT);
                break;
            case EVENT_VIEW_MAP:
                if (event.getData() != null) {
                    if (event.getData().equals(LOCATION_NOT_AVAILABLE)) {
                        initGoogleLocation();
                    }
                }
                break;
        }
    }

    private void logout() {
        showDialog(REQUEST_CODE_LOGOUT, getResources().getString(R.string.dialog_logout), false);
    }

    private void initAccount() {
        Intent intent = new Intent(MainActivity.this, AccountActivity.class);
        startActivity(intent);
    }

    private void initScan() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{PermissionUtils.PER_CAMERA};
            PermissionUtils.RequestPermissions(MainActivity.this, REQUEST_PERMISSION_SETTING, permissions);
        } else {
            //You already have the permission, just go ahead.
            Intent intent = new Intent(MainActivity.this, ScanQRActivity.class);
            startActivity(intent);
        }

    }

    private void initKeyIn() {
        Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

//        edittext.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        editText_pin.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        editText_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(17)});

        textView_message.setText(getResources().getString(R.string.dialog_insert_merchant_id));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {

            // if input is ED2E Number -> initialize Transaction
            if (editText_pin.getText().toString().length() <= 13) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ResponseBody> call = apiService.getCustCode(API_CUSTCODE, editText_pin.getText().toString(), String.valueOf(unixTimeStamp()), getVCKeyED2E());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Timber.d("Raw Url:" + response.raw().request().url());
                        if(response.body()==null) {
                            showToast(getResources().getString(R.string.error_connection));
                            hideProgress();
                            return;
                        }
                        try {
                            String json = Objects.requireNonNull(response.body()).string();
                            Timber.d("onResponse:" + response.code() + " " + json);
                            JSONObject obj = new JSONObject(json);
                            String status = (String) obj.get(STATUS);
                            if (status.equals(SUCCESS)) {
                                String type = (String) obj.get(TYPE);
                                if (type.equals(MERCHANT)) {
                                    String custid = (String) obj.get(CUSTID);
                                    Intent intent = new Intent(MainActivity.this, TransactionActivity.class);
                                    intent.putExtra(KEY_TRANS_CUSTID, custid);
                                    startActivity(intent);
                                } else {
                                    Timber.d("Distributor QR");
                                    Toast.makeText(getContext(), getResources().getString(R.string.label_error_transaction), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                String message = (String) obj.get(MESSAGE);
                                String errorCode = (String) obj.get(ERRORCODE);
                                Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                            }
                            hideProgress();
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            hideProgress();
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if(t instanceof SocketTimeoutException){
                            showToast(getResources().getString(R.string.error_connection));
                            hideProgress();
                            return;
                        }

                        Timber.d("onFailure:");
                        t.printStackTrace();
                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                        hideProgress();
                    }
                });

            // if input is Massage Chair code -> initialize Massage Chair
            } else {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ResponseBody> call = apiService.getCustCode(API_CUSTCODE, editText_pin.getText().toString().substring(0, 13), String.valueOf(unixTimeStamp()), getVCKeyED2E());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Timber.d("Raw Url:" + response.raw().request().url());
                        if(response.body()==null)
                        {
                            showToast(getResources().getString(R.string.error_connection));
                            hideProgress();
                            return;
                        }
                        try {
                            String json = Objects.requireNonNull(response.body()).string();
                            Timber.d("onResponse:" + response.code() + " " + json);
                            JSONObject obj = new JSONObject(json);
                            String status = (String) obj.get(STATUS);
                            if (status.equals(SUCCESS)) {
                                String type = (String) obj.get(TYPE);
                                if (type.equals(MERCHANT)) {
                                    String custid = (String) obj.get(CUSTID);
                                    Intent intent = new Intent(getContext(), TransactionMassageChairActivity.class);
                                    intent.putExtra(KEY_TRANS_CUSTID, custid);
                                    intent.putExtra(KEY_TRANS_DEVICEID, editText_pin.getText().toString());
                                    startActivity(intent);
                                } else {
                                    Timber.d("Distributor QR");
                                    Toast.makeText(getContext(), getResources().getString(R.string.label_error_transaction), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                String message = (String) obj.get(MESSAGE);
                                String errorCode = (String) obj.get(ERRORCODE);
                                Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                            }
                            hideProgress();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideProgress();
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if(t instanceof SocketTimeoutException){
                            showToast(getResources().getString(R.string.error_connection));
                            hideProgress();
                            return;
                        }

                        Timber.d("onFailure:");
                        t.printStackTrace();
                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                        hideProgress();
                    }
                });
            }
        });
        button_dismiss.setOnClickListener(v ->
                dialog.dismiss());
        dialog.show();
    }

    private void initSupport() {
        Intent intent = new Intent(MainActivity.this, SupportActivity.class);
        String company_code = preference.getString(KEY_COUNTRY_CODE);
        intent.putExtra(KEY_COUNTRY_CODE, company_code);
        startActivity(intent);
    }

    private void initLedger() {
        Intent intent = new Intent(MainActivity.this, LedgerActivity.class);
        startActivity(intent);
    }

    private void initProfile() {
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        intent.putExtra(KEY_ACCOUNT_TYPE, account_type);
        startActivity(intent);
    }

    private void initTransactionHistory() {
        Intent intent = new Intent(MainActivity.this, TransactionHistoryActivity.class);
        intent.putExtra(KEY_ACCOUNT_TYPE, account_type);
        startActivity(intent);
    }

    private void initAdvertisement() {
        Intent intent = new Intent(MainActivity.this, AdvertisementActivity.class);
        startActivity(intent);
    }

    private void initShowQR() {
        Intent intent = new Intent(MainActivity.this, ShowQRActivity.class);
        startActivity(intent);
    }

    private void initMerchantList(String api) {
        showProgress();
        Call<ResponseBody> call = apiService.getmerchant(api, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                    Gson gson = new Gson();
                    ArrayList<Merchant> merchants = new ArrayList<>();
                    for (int i = 0; i < jsonarrayMain.length(); i++) {
                        JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                        Merchant merchant = gson.fromJson(jsonobject.toString(), Merchant.class);
                        Timber.d("merchant list:" + gson.toJson(merchant));
                        merchants.add(merchant);
                    }
                    EventBus.getDefault().post(new MainMessageEvent(api, gson.toJson(merchants)));

                } catch (IOException e) {
                    e.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection)/* + " (" + e.getMessage() + ")"*/);
                    //Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection)/* + " (" + t.getMessage() + ")"*/);
                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }

    private void initStatisticalAnalysis() {
        Intent intent = new Intent(MainActivity.this, StatisticalAnalysisActivity.class);
        startActivity(intent);
    }

    private void initUtilities() {
        /*Intent intent = new Intent(getContext(), TransactionUtilityActivity.class);
        intent.putExtra(KEY_UTILITY_BILLERID, "120");
        intent.putExtra(KEY_UTILITY_BILLERNAME, "CIGNAL");
        startActivity(intent);*/

        String countryCode = preference.getString(KEY_COUNTRY_CODE);

        if(countryCode.equalsIgnoreCase("PH")){
            Intent intent = new Intent(getContext(), ComingSoonActivity.class);
            //Intent intent = new Intent(getContext(), UtilityActivity.class);
            /*intent.putExtra(KEY_UTILITY_BILLERID, "120");
            intent.putExtra(KEY_UTILITY_BILLERNAME, "CIGNAL");*/
            startActivity(intent);
        } else{
            Intent intent = new Intent(getContext(), ComingSoonActivity.class);
            startActivity(intent);
        }
    }

    private void initReload() {
        Intent intent = new Intent(MainActivity.this, ComingSoonActivity.class);
        startActivity(intent);


        /*Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_key_amount = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_key_amount.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText_key_amount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(17)});

        textView_message.setText(getResources().getString(R.string.dialog_key_amount));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {

            if (editText_key_amount.getText().toString().length() == 0 ) {
                showToast(getResources().getString(R.string.error_invalid_amount));
            } else {
                try {
                    Integer.parseInt(editText_key_amount.getText().toString());
                    dialog.dismiss();

                    initConfirmationDialog(editText_key_amount.getText().toString());
                } catch (Exception e) {
                    showToast(getResources().getString(R.string.error_invalid_amount));
                }
            }
        });
        button_dismiss.setOnClickListener(v ->
                dialog.dismiss());
        dialog.show();*/
    }

    private void initTicketing() {
        Intent intent = new Intent(getContext(), TicketingActivity.class);
        startActivity(intent);
    }

    //TODO: remove code for country
    private void initCountry() {
        Intent intent = new Intent(getContext(), ComingSoonActivity.class);
        startActivity(intent);
    }

    private void initFamilyTree() {
        //Intent intent = new Intent(getContext(), ComingSoonActivity.class);
        Intent intent = new Intent(getContext(), FamilyTreeActivity.class);
        intent.putExtra(KEY_FAMILY_TREE_CUSTID, /*"849049"*/ /*"1013713"*/ preference.getString(KEY_CUSID));
        intent.putExtra(KEY_FAMILY_TREE_COUNT, 0);
        startActivity(intent);
    }

    private void initEDCoin() {
        Intent intent = new Intent(getContext(), EDCoinActivity.class);
        startActivity(intent);
    }

    private void initPointTransfer() {
        Intent intent = new Intent(getContext(), PointTransferActivity.class);
        startActivity(intent);
    }

    private void initViewMerchantDetail(String merchantcode) {
        Intent intent = new Intent(MainActivity.this, MerchantDetailsActivity.class);
        intent.putExtra(KEY_MERCHANT_CODE, merchantcode);
        startActivityForResult(intent, REQUEST_CODE_MERCHANT_DETAILS);
    }

    private void enableMyLocationIfPermitted(GoogleMap map) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        } else if (map != null) {
            map.setMyLocationEnabled(true);
        }
    }

    private void initMassageChairController() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(
                API_MASSAGE_CHAIR_USAGE,
                custId,
                String.valueOf(unixTimeStamp()),
                getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        hideProgress();
                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            Timber.d("jsonarrayMain:" + jsonarrayMain.get(jsonarrayMain.length() - 1).toString());
                            Intent in = new Intent(getContext(), TransactionWebviewActivity.class);
                            in.putExtra(KEY_URL, jsonarrayMain.get(jsonarrayMain.length() - 1).toString());
                            in.putExtra(KEY_TITLE, getResources().getString(R.string.label_massage_chair_merchant));
                            startActivity(in);
                        } catch (JSONException e) {
                            Timber.d("DeviceID is not active");
                            String message = (String) obj.get(MESSAGE);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("validateTransaction onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    private void initMapData() {
        showProgress();
        Call<ResponseBody> call = apiService.getmerchant(API_OFFLINE_MERCHANT, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = response.body().string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    Log.d(TAG,"onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                    Gson gson = new Gson();
                    ArrayList<Merchant> merchants = new ArrayList<>();
                    for (int i = 0; i < jsonarrayMain.length(); i++) {
                        JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                        Merchant merchant = gson.fromJson(jsonobject.toString(), Merchant.class);
                        Timber.d("merchant_code:" + merchant_code);
                        if (merchant_code.equals("")) {
                            Timber.d("merchant:" + gson.toJson(merchant));
                            merchants.add(merchant);
                        } else {
                            if (merchant_code.equals(merchant.getMerchant_code())) {
                                Timber.d("merchant:" + gson.toJson(merchant));
                                merchants.add(merchant);
                            }
                        }
                    }


                    Timber.d("merchantList:" + gson.toJson(merchants));
                    if (merchant_code.equals(""))
                        EventBus.getDefault().post(new MainMessageEvent(EVENT_VIEW_MAP, gson.toJson(merchants)));
                    else
                        EventBus.getDefault().post(new MainMessageEvent(EVENT_VIEW_MAP_MERCHANT, gson.toJson(merchants)));
                    merchant_code = "";

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection) /*+ " (" + e.getMessage() + ")"*/);
                    //Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    merchant_code = "";
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection)/* + " (" + t.getMessage() + ")"*/);
                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
                merchant_code = "";
            }
        });
    }

    private void openOnlineMerchantContent(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void initMerchantListCategory(String category, String api) {
        Intent intent = new Intent(MainActivity.this, MerchantListActivity.class);
        intent.putExtra(KEY_CATEGORY, category);
        intent.putExtra(KEY_API, api);
        startActivity(intent);
    }

    private void checkPinStatus() {
        Call<ResponseBody> call = apiService.mainCustInfo(API_CHECK_PIN_STATUS, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        String email = (String) obj.get(PIN);

                        if (!account_type.equals(MERCHANT)) {
                            if (email.isEmpty() || email.equals("")) {
//                            showDialog(REQUEST_CODE_PIN_STATUS, getResources().getString(R.string.dialog_pin_status), true);
                                initInputDialog();
                            }
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        showToast(getResources().getString(R.string.label_error_code, message, errorCode));
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    showToast(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                Timber.d("onFailure:");
                t.printStackTrace();
                checkPinStatus();
            }
        });
    }


    private void getMassageUsage() {
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(
                API_MASSAGE_CHAIR_USAGE,
                custId,
                String.valueOf(unixTimeStamp()),
                getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = response.body().string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        //TODO add webview
                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            Timber.d("jsonarrayMain:" + jsonarrayMain.get(0).toString());
                            int timer = 0;
                            int prevtimer = 0;
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                Timber.d("jsonarrayMain:" + jsonarrayMain.get(i).toString());
                                Timber.d("jsonarrayMain:" + jsonarrayMain.get(i).toString());
                                int startIndex = jsonarrayMain.get(i).toString().lastIndexOf("remain_in_sec=") + 14;
                                int endIndex = jsonarrayMain.get(i).toString().lastIndexOf("&w_t=");

                                timer = Integer.valueOf(jsonarrayMain.get(i).toString().substring(startIndex, endIndex));
                                if (prevtimer < timer) {
                                    prevtimer = timer;
                                }
                            }

                            //TODO check if needed
                            Timber.d("CountDownTimer: " + prevtimer);
                            new CountDownTimer(prevtimer * 1000, 1000) {

                                public void onTick(long millisUntilFinished) {

                                    Timber.d("seconds remaining: " + millisUntilFinished / 1000);
//                                    mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {
                                    Timber.d("CountDownTimer onFinish");
//                                    mTextField.setText("done!");
                                    getMassageUsage();
                                }

                            }.start();

                            EventBus.getDefault().post(new MainMessageEvent(EVENT_SET_MASSAGE_CHAIR, String.valueOf(prevtimer)));
//                            Intent in = new Intent(getContext(), WebviewActivity.class);
//                            in.putExtra(KEY_URL, jsonarrayMain.get(0).toString());
//                            in.putExtra(KEY_TITLE, getResources().getString(R.string.label_massage_chair_merchant));
//                            startActivity(in);
                        } catch (JSONException e) {
                            Timber.d("No active device");
                            String message = (String) obj.get(MESSAGE);
                            EventBus.getDefault().post(new MainMessageEvent(EVENT_SET_MASSAGE_CHAIR, null));
                        }
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("validateTransaction onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    public void initInputDialog() {
        if (!pinDialogisShown) {
            pinDialogisShown = true;
            Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (dialog.getWindow() != null)
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_input_2);
            dialog.setCancelable(false);

            Button button_ok = dialog.findViewById(R.id.dialog_input_2_button_ok);
            Button button_dismiss = dialog.findViewById(R.id.dialog_input_2_button_cancel);
//        EditText editText_old_pass_pin = dialog.findViewById(R.id.dialog_input_3_edittext);
//        TextView textView_old_pass_pin_title = dialog.findViewById(R.id.dialog_input_3_textview_message);
            EditText editText_new_pass_pin = dialog.findViewById(R.id.dialog_input_2_edittext);
            TextView textView_new_pass_pin_title = dialog.findViewById(R.id.dialog_input_2_textview_message);
            EditText editText_confirm_pass_pin = dialog.findViewById(R.id.dialog_input_2_edittext_2);
            TextView textView_confirm_pass_pin_title = dialog.findViewById(R.id.dialog_input_2_textview_message_2);

            String title;

            title = getResources().getString(R.string.dialog_pin);

            editText_new_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_enter_pin),FONT_CALIBRI));
            editText_confirm_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_confirm_pin),FONT_CALIBRI));
            editText_new_pass_pin.setTypeface(TypefaceUtils.load(getAssets(),FONT_CALIBRI));
            editText_confirm_pass_pin.setTypeface(TypefaceUtils.load(getAssets(),FONT_CALIBRI));
            editText_new_pass_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            editText_confirm_pass_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            editText_new_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            editText_confirm_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            String label = getResources().getString(R.string.dialog_pin_status) + "\n\n" + getResources().getString(R.string.dialog_new, title);
            textView_new_pass_pin_title.setText(label);
            textView_confirm_pass_pin_title.setText(getResources().getString(R.string.dialog_re_type, title));

            button_ok.setText(getResources().getString(R.string.button_proceed));
            button_dismiss.setText(getResources().getString(R.string.button_cancel));
            button_ok.setOnClickListener(v ->
            {
                String newpass = editText_new_pass_pin.getText().toString();
                String confirmpass = editText_confirm_pass_pin.getText().toString();
                if (newpass.length() < 4 || confirmpass.length() < 4) {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
                } else {
                    if (newpass.equals(confirmpass)) {
                        String custId = preference.getString(KEY_CUSID);
//                    initChangePin(custId,confirmpass);
                        initLoadAccounts(dialog, custId, confirmpass);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin_mismatch), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            button_dismiss.setOnClickListener(v -> dialog.dismiss());
            button_dismiss.setVisibility(View.GONE);
            dialog.show();
        }
    }

    private void initLoadAccounts(Dialog dialog, String custId, String newpass) {
        showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.mainCustInfo(API_ACCOUNT_SELECT, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("response.raw().request().url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(MAIN_ACCOUNT)));
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                String package_type = jsonobject.getString(PACKAGE_TYPE);
                                String eda = jsonobject.getString(EDA);
                                String name = jsonobject.getString(NAME);
                                int custid = jsonobject.getInt(CUSTID);
                                CardType cardType = new CardType(package_type, eda, name, custid, STATUS_CODE_INACTIVE);
                                datalist.add(cardType);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            JSONArray jsonarraySub = new JSONArray(String.valueOf(obj.get(SUB_ACCOUNT)));
                            for (int i = 0; i < jsonarraySub.length(); i++) {
                                JSONObject jsonobject = jsonarraySub.getJSONObject(i);
                                String package_type = jsonobject.getString(PACKAGE_TYPE);
                                String eda = jsonobject.getString(EDA);
                                String name = jsonobject.getString(NAME);
                                int custid = jsonobject.getInt(CUSTID);
                                CardType cardType = new CardType(package_type, eda, name, custid, STATUS_CODE_INACTIVE);
                                datalist.add(cardType);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Timber.d("datalist:" + datalist.size());
                        initChangePin(String.valueOf(datalist.get(0).getCustid()), newpass);
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        showToast(getResources().getString(R.string.label_error_code, message, errorCode));
                    }

                    dialog.dismiss();

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    showToast(e.getMessage());
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(t.getMessage());
                hideProgress();
            }
        });
    }

    private void initChangePin(String custId, String pin) {
        showProgress();
        Call<ResponseBody> call = apiService.insert_pin(API_INSERT_PIN, custId, getHash(pin), String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);
                    String message = (String) obj.get(MESSAGE);
                    if (status.equals(SUCCESS)) {
                        Timber.d("onResponse:" + (datalist.size() - 1));
                        if ((datalist.size()) != index) {
                            initChangePin(String.valueOf(datalist.get(index).getCustid()), pin);
                            index++;
                        } else {
                            index = 1;
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume:");

        if (preference != null)
            custId = preference.getString(KEY_CUSID);

        if (mTabHost != null) {
            int current = mTabHost.getCurrentTab();
            Timber.d("current:" + current);
            if (current == 0) {
                initHomePage();
            }
        }
    }

    private void initHomePage() {
        pinDialogisShown = false;
        initLoadAccountInfo();
        initLoadEdPoints();
        initLoadLoyaltyPoints();
        checkPinStatus();
        getMassageUsage();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Timber.d("onActivityResult:" + resultCode);

        if (requestCode == REQUEST_CODE_MERCHANT_DETAILS) {
            if (resultCode == Activity.RESULT_OK) {
                Timber.d("onActivityResult merchant_code:" + data.getStringExtra(KEY_MERCHANT_CODE));
                merchant_code = data.getStringExtra(KEY_MERCHANT_CODE);
                Timber.d("onActivityResult merchant_code:" + merchant_code);
                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    mTabHost.setCurrentTab(2);
                }, 100);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Timber.d("onActivityResult:" + resultCode);
            }
        } else if (requestCode == REQUEST_CODE_GOOGLE_LOCATION) {
            Timber.d("onActivityResult:" + requestCode);
            gpsDialogisShown = false;
            if (resultCode == RESULT_OK) {
                initMapData();
            }
        } else if (requestCode == REQUEST_PERMISSION_SETTING) {
            Intent intent = new Intent(MainActivity.this, ScanQRActivity.class);
            startActivity(intent);
        } else {
            Timber.d("onActivityResult:" + requestCode);
        }
    }//onActivityResult

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Timber.d("onRequestPermissionsResult:" + requestCode);
        switch (requestCode) {
            case REQUEST_PERMISSION_SETTING: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MainActivity.this, ScanQRActivity.class);
                    startActivity(intent);
                }
            }
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initMapData();
                }
            }

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        initGoogleLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Timber.d("LocationSettingsStates SUCCESS");
                // All location settings are satisfied. The client can initialize location
                // requests here.
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Timber.d("LocationSettingsStates RESOLUTION_REQUIRED");
                // Location settings are not satisfied. But could be fixed by showing the user
                // a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    if (!gpsDialogisShown) {
                        status.startResolutionForResult(MainActivity.this, REQUEST_CODE_GOOGLE_LOCATION);
                        gpsDialogisShown = true;
                    }
                } catch (IntentSender.SendIntentException e) {
                    // Ignore the error.
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Timber.d("LocationSettingsStates SETTINGS_CHANGE_UNAVAILABLE");
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
                break;
        }
    }

    private void initGoogleLocation() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(this);
    }

    private void initConfirmationDialog(String key_amount) {
        Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_main);

        Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_main_button_cancel);
        TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);

        textView_message.setText(getResources().getString(R.string.dialog_key_amount_confirmation, key_amount));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            showToast("TODO: Reload function");
        });
        button_dismiss.setOnClickListener(v ->
                dialog.dismiss());
        dialog.show();
    }
}
