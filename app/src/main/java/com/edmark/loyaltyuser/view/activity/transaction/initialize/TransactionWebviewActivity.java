package com.edmark.loyaltyuser.view.activity.transaction.initialize;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.HomeMessageEvent;
import com.edmark.loyaltyuser.model.MainMessageEvent;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.AppUtility;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_MASSAGE_CHAIR_USAGE;
import static com.edmark.loyaltyuser.constant.Common.CONSENT_CLAUSE;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_MASSAGE_CHAIR;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.KEY_WEBVIEW_ACTION;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.PRIVACY_POLICY;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_MASSAGE_CHAIR;
import static com.edmark.loyaltyuser.constant.Common.TAG_HOME_MASSAGE_CHAIR_USAGE;
import static com.edmark.loyaltyuser.constant.Common.TERMS_CONDITIONS;
import static com.edmark.loyaltyuser.rest.ApiClient.BASE_URL;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


/*
 * Created by DEV-Michael-ED2E on 04/24/2018.
 */
public class TransactionWebviewActivity extends BaseActivity {
    private String TAG = TransactionWebviewActivity.class.getSimpleName();

    @BindView(R.id.activity_transaction_webview) WebView webView;
    @BindView(R.id.activity_transaction_webview_Progressbar) ProgressBar progressbar_loading;
    @BindView(R.id.activity_transaction_webview_toolbar_title) TextView textView_tootbar_title;
    private String mUrl;
    private String mToolTitle = "";
    private boolean isGetPasswordUrl = false;            // Flags to handle when to inflate alert dialog
    private View noInternet;

    private ApiInterface apiService;
    private AppPreference preference;
    private ArrayList<String> devices = new ArrayList<>();

    @SuppressLint("TimberArgCount")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_webview);
        ButterKnife.bind(this);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        preference = AppPreference.getInstance(this);
        //TODO clean this class
        if (getIntent() != null) {
            mUrl = getIntent().getStringExtra(KEY_URL);
            mToolTitle = getIntent().getStringExtra(KEY_TITLE);
            if(mUrl==null)
            {
                Uri data = getIntent().getData();
                if (data != null) {
                    Timber.i("LOG", "FULL URI: " + data.toString());
                    mUrl = data.toString();
                    switch (mUrl) {
                        case PRIVACY_POLICY:
                            mToolTitle = getResources().getString(R.string.label_privacy_policy);
                            break;
                        case TERMS_CONDITIONS:
                            mToolTitle = getResources().getString(R.string.label_terms_condition);
                            break;
                        case CONSENT_CLAUSE:
                            mToolTitle = getResources().getString(R.string.label_consent_clause);
                            break;
                    }
                    mUrl = mUrl.replace(KEY_WEBVIEW_ACTION,"");
                }
            }
            Timber.v("Url is ", mToolTitle +" "+ mUrl);
        }
        initGUI();
        getUsage(false, -1);
    }

    private void initGUI() {
        initToolBar();
        setWebView();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        if (webView.canGoBack()) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            webView.goBack();
            Timber.v(TAG, webView.getUrl());
        } else {
            finish();
        }
    }

    private void initToolBar()
    {
        Toolbar toolbar = setupToolBar(R.id.activity_transaction_webview_toolbar);
        textView_tootbar_title.setText(mToolTitle);
        Drawable drawable = getResources().getDrawable(R.drawable.ic_mc);
        toolbar.setOverflowIcon(drawable);
    }

    private static final int MY_MENU_1 = Menu.FIRST;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

//        menu.add(0, MY_MENU_1, 0, "Hello").setShortcut('3', 'c');
//        menu.add(0, MY_MENU_1+1, 0, "About").setShortcut('4', 's');
//        menu.add(0, MY_MENU_1+2, 0, "Help").setShortcut('5', 'z');

        for (int i = 0; i < devices.size(); i++) {
            int startIndex = devices.get(i).lastIndexOf("e_id=")+5;
            int endIndex = devices.get(i).lastIndexOf("&remain_in_sec=");
            String device_id = getResources().getString(R.string.label_massage_chair_id,devices.get(i).substring(startIndex,endIndex));
            Timber.d("device_id:" + device_id);
            menu.add(0, MY_MENU_1+i, 0, device_id);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        for (int i = 0; i < devices.size(); i++) {
//            if(item.getItemId()==MY_MENU_1+i)
//            {
//                mUrl = devices.get(i);
//                setWebView();
//                return true;
//            }
//        }

        getUsage(true, item.getItemId());

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView() {
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        String ua = webView.getSettings().getUserAgentString();
        if (!ua.contains("Mobile")) {
            ua = new StringBuffer(ua).insert(ua.lastIndexOf(" "), " Mobile")
                    .toString();
            webView.getSettings().setUserAgentString(ua);
        }
        webView.setOnLongClickListener(v -> true);

        webView.loadUrl(mUrl);
        webView.setWebViewClient(new CustomWebViewClient());
        webView.setWebChromeClient(new MyWebChromeClient());
    }

    public class CustomWebViewClient extends WebViewClient {
        @SuppressLint("TimberArgCount")
        @Override
        public void onReceivedError(final WebView view, int errorCode,
                                    String description, final String failingUrl) {
            Timber.e("errorCode", String.valueOf(errorCode));
            noInternet = View.inflate(TransactionWebviewActivity.this, R.layout.view_no_internet, null);
            noInternet.findViewById(R.id.refresh_button_new).setOnClickListener(v -> {
                view.loadUrl(failingUrl);
                view.removeView(noInternet);
            });
            if (view.findViewById(noInternet.getId()) == null) {
                view.loadUrl("file:///android_asset/fail.html");
                view.addView(noInternet);
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Timber.d(TAG,"onPageStarted:"+url);

            progressbar_loading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressbar_loading.setVisibility(View.GONE);
            if(!AppUtility.getUrlTitle(webView.getUrl()).equals(""))
            {
                textView_tootbar_title.setText(AppUtility.getUrlTitle(webView.getUrl()));
            }
            Timber.d(TAG,"onPageFinished:"+webView.getUrl());
            Timber.d(TAG,"onPageFinished:"+AppUtility.getUrlTitle(webView.getUrl()));
            if (isGetPasswordUrl) {
                isGetPasswordUrl = false;
                initPinDialog();
            }
            if (webView.getUrl().contains(BASE_URL + "e-commerce/index.php/checkout/onepage/success")) {
                textView_tootbar_title.setText(getString(R.string.reward_order_success));
            }
        }

        /**
         * Any changes done in this method need to be done for Android N method as well just below to this
         * This will work for Below Android 7.0 (Nougat)
         *
         * @param view - the webView
         * @param url  - url to be redirected
         * @return - status
         */
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return true;
        }
    }

    public class MyWebChromeClient extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result)
        {
            Timber.d("onJsAlert:" + result + " " + message);
            final JsResult finalRes = result;
            try {
                new AlertDialog.Builder(view.getContext())
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, (dialog, which) ->
                        {
                            getUsage(false, -2);
                            finalRes.confirm();
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            } catch (Exception e)
            {
                EventBus.getDefault().post(new HomeMessageEvent(TAG_HOME_MASSAGE_CHAIR_USAGE, null));
            }
            return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        webView.removeView(noInternet);

        if (webView.canGoBack()) {
            webView.goBack();
            Timber.v(TAG, webView.getUrl());
        } else {
            super.onBackPressed();
        }
    }
    private void initPinDialog() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Should check the API", Toast.LENGTH_SHORT).show();
                //TODO must check the pin but this is bypass
            }
        });
        button_dismiss.setOnClickListener(v ->
                dialog.dismiss());
        dialog.show();
    }

    private void getUsage(boolean isMenu,int itemId) {
        Timber.d("getUsage()");
        devices.clear();
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(
                API_MASSAGE_CHAIR_USAGE,
                custId,
                String.valueOf(unixTimeStamp()),
                getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                Timber.d("jsonarrayMain:" + jsonarrayMain.get(i).toString());
                                int startIndex = jsonarrayMain.get(i).toString().lastIndexOf("remain_in_sec=")+14;
                                int endIndex = jsonarrayMain.get(i).toString().lastIndexOf("&w_t=");
                                devices.add(jsonarrayMain.get(i).toString());
                            }
                            invalidateOptionsMenu();
                            if(itemId==-2)
                            {
                                mUrl = devices.get(devices.size()-1);
                                setWebView();
                            }

                            if(isMenu)
                            {
                                for (int i = 0; i < devices.size(); i++) {
                                    if(itemId==MY_MENU_1+i)
                                    {
                                        mUrl = devices.get(i);
                                        setWebView();
                                    }
                                }
                            }
                            hideProgress();
                        } catch (JSONException e) {
                            Timber.d("No active Device");
                            hideProgress();
                            String message = (String) obj.get(MESSAGE);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Timber.d("validateTransaction onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }
}
