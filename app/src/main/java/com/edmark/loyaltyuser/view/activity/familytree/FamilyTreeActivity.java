package com.edmark.loyaltyuser.view.activity.familytree;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.FamilyTreeAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.FamilyTree;
import com.edmark.loyaltyuser.model.Merchant;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientFamilyTree;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import static com.edmark.loyaltyuser.constant.Common.DOWNLINE;
import static com.edmark.loyaltyuser.constant.Common.DOWNLINE_CUSTEDA;
import static com.edmark.loyaltyuser.constant.Common.DOWNLINE_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.DOWNLINE_CUSTJOINDATE;
import static com.edmark.loyaltyuser.constant.Common.DOWNLINE_CUSTNAME;
import static com.edmark.loyaltyuser.constant.Common.DOWNLINE_CUSTSERIALNO;
import static com.edmark.loyaltyuser.constant.Common.DOWNLINE_CUSTTYPE;
import static com.edmark.loyaltyuser.constant.Common.DOWNLINE_TOTALDONWLINES;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_FAMILY_TREE_COUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_FAMILY_TREE_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TOTAL_DOWNLINE;
import static com.edmark.loyaltyuser.constant.Common.CUSTSERIALNO;
import static com.edmark.loyaltyuser.constant.Common.CUSTNAME;
import static com.edmark.loyaltyuser.constant.Common.CUSTTYPE;
import static com.edmark.loyaltyuser.constant.Common.CUSTJOINDATE;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class FamilyTreeActivity extends BaseActivity {

    public static final String TAG = FamilyTreeActivity.class.getSimpleName();

    @BindView(R.id.activity_family_tree_toolbar_title)
    TextView textview_toolbar_title;
    @BindView(R.id.activity_family_tree_textview_message)
    TextView textview_family_tree;

    @BindView(R.id.activity_family_tree_textview_direct_downline)
    TextView textview_direct_downline;
    @BindView(R.id.activity_family_tree_textview_total_downline)
    TextView textview_total_downline;
    @BindView(R.id.activity_family_tree_textview_name)
    TextView textview_name;
    @BindView(R.id.activity_family_tree_textview_code)
    TextView textview_code;
    @BindView(R.id.activity_family_tree_textview_card_type)
    TextView textview_card_type;
    @BindView(R.id.activity_family_tree_textview_date_started)
    TextView textview_date_started;

    @BindView(R.id.activity_family_tree_editText_search)
    EditText editext_search;
    @BindView(R.id.activity_family_tree_textview_label)
    TextView textView_label;

    private AppPreference preference;
    private String account_type;

    private ApiInterface apiInterface;

    private List<FamilyTree> modelList;
    private List<FamilyTree> modelListL2;
    private ArrayList<Merchant> searchdatalist = new ArrayList<>();

    static RecyclerView recyclerView;
    FamilyTreeAdapter recyclerAdapter;

    private String custID;
    private int counter;
    private int totalDownlineCounter = 0;

    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_tree);
        ButterKnife.bind(this);

        //LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("custom-message"));

        preference = AppPreference.getInstance(this);
        account_type = preference.getString(KEY_ACCOUNT_TYPE);

        setupToolBar(R.id.activity_family_tree_toolbar);

        initCustInfo(savedInstanceState);
        initGui();

    }

    private void initCustInfo(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            custID = preference.getString(KEY_CUSID);
            counter = 0;
        } else {
            custID = extras.getString(KEY_FAMILY_TREE_CUSTID);
            counter = extras.getInt(KEY_FAMILY_TREE_COUNT);
        }

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGui() {
        //editext_search.getText().clear();

        counter++;
        textview_toolbar_title.setText(getResources().getString(R.string.title_family_tree, "" + counter));

        textview_direct_downline.setText(getResources().getString(R.string.label_direct_downline, "0"));
        textview_total_downline.setText(getResources().getString(R.string.label_total_downline, "0"));
        textview_date_started.setText(getResources().getString(R.string.label_since, "(Date)"));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        //-- initialize recycler view, layout, and adapter
        recyclerView = findViewById(R.id.activity_family_tree_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new FamilyTreeAdapter(this,modelList);
        recyclerView.setAdapter(recyclerAdapter);

        initAPI();
    }

    private void initAPI() {
        showProgress();
        String page = "Solucis:apiCustTreeV2";
        //String custId = preference.getString(KEY_CUSID);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<okhttp3.ResponseBody> call = apiInterface.getFamilyTree(
                /*"Solucis:apiCustTreeV2",
                "849049",
                "e25cbcc03245af9bb80b27dd910156fe",
                "1464947872"*/
                page,
                custID,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp())
        );

        call.enqueue((new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
                //Toast.makeText(FamilyTreeActivity.this, "Successful!", Toast.LENGTH_SHORT).show();

                /*Log.i("SUCCESS", "Pass 3.1: " + response.raw().request().url());
                Log.i("SUCCESS", "Pass 3.2: " + response);
                Log.i("SUCCESS", "Pass 3.3: " + response.message());
                Log.i("SUCCESS", "Pass 3.4: " + response.body());
                //Log.i("SUCCESS", "Pass 3.5: " + response.body().string());
                Log.i("SUCCESS", "Pass 3.6: " + response.errorBody());*/

                if(response.body()==null)
                {
                    textview_family_tree.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    finish();
                    return;
                }

                try{
                    hideProgress();
                    String responseString = response.body().string();
                    //Log.i("SUCCESS", "Pass 3.5: " + responseString);
                    //textview_family_tree.setText(responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {

                        JSONArray downline_list = obj.getJSONArray(DOWNLINE);

                        String downlineCount;

                        try{ //if downline_list has values - set downlineCount to length of downline_list

                            downlineCount = String.valueOf(downline_list.length());

                            modelList = new ArrayList<>();

                            if(downline_list.length() == 0) {
                                textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                                textview_family_tree.setVisibility(View.VISIBLE);
                            } else {
                                textview_family_tree.setVisibility(View.GONE);
                            }

                            for(int i = 0; i < downline_list.length(); i++){
                                JSONObject downline_object = downline_list.getJSONObject(i);
                                FamilyTree list = new FamilyTree(
                                        downline_object.getString(DOWNLINE_TOTALDONWLINES),
                                        downline_object.getString(DOWNLINE_CUSTID),
                                        downline_object.getString(DOWNLINE_CUSTSERIALNO),
                                        downline_object.getString(DOWNLINE_CUSTEDA),
                                        downline_object.getString(DOWNLINE_CUSTNAME),
                                        downline_object.getString(DOWNLINE_CUSTTYPE),
                                        downline_object.getString(DOWNLINE_CUSTJOINDATE)
                                );

                                modelList.add(list);

                                //Log.i("Pass", "TRY - Model Placed: " + modelList.toString());
                            }
                            recyclerAdapter.setModelList(modelList, counter);

                        } catch (JSONException e) {
                            //Log.i("Pass", "CATCHED");
                            downlineCount = "0";

                            textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                            textview_family_tree.setVisibility(View.VISIBLE);
                        }

                        totalDownlineCounter += Integer.parseInt(downlineCount);
                        //showToast("totalDownlineCounter: " + totalDownlineCounter);

                        //countTotalDownlineL2 -> START
                        countTotalDownlineL2(downline_list, page);
                        //countTotalDownlineL2 -> END

                        //initialize family tree header data -> START
                        //textview_total_downline.setText(getResources().getString(R.string.label_total_downline, "" + downlineCount));
                        textview_direct_downline.setText(getResources().getString(R.string.label_direct_downline,  obj.getString(TOTAL_DOWNLINE)));
                        textview_name.setText(getResources().getString(R.string.label_upline, obj.getString(CUSTNAME).toUpperCase()));
                        textview_code.setText(obj.getString(CUSTSERIALNO));
                        textview_card_type.setText(obj.getString(CUSTTYPE));
                        textview_date_started.setText(getResources().getString(R.string.label_since, obj.get(CUSTJOINDATE)));
                        //initialize family tree header data -> END

                        //Search Text Field -> START
                        initSearchFunction(downline_list);
                        //Search Text Field -> END

                    } else {
                        showToast(getResources().getString(R.string.error_invalid_account));
                        finish();
                    }
                } catch (IOException | JSONException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    textview_family_tree.setText(getResources().getString(R.string.error_connection_2));
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    finish();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                finish();
            }
        }));
    }

    private void initSearchFunction(JSONArray downline_list) {
        Observable<CharSequence> changeObservableSearch = RxTextView.textChanges(editext_search);

        disposables.add(changeObservableSearch.map(inputText -> (inputText.length() == 0))
                .subscribe(isValid ->
                {
                    if(!isValid)
                    {
                        //showToast("Searching...");
                        textView_label.setVisibility(View.GONE);

                        modelList.clear();
                        recyclerAdapter.notifyDataSetChanged();

                        for(int i = 0; i < downline_list.length(); i++){
                            JSONObject downline_object = downline_list.getJSONObject(i);

                            if (downline_list.length() == 0) {
                                textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                                textview_family_tree.setVisibility(View.VISIBLE);
                            }
                            else {
                                if (downline_object.getString(DOWNLINE_CUSTNAME).toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
                                    FamilyTree list = new FamilyTree(
                                            downline_object.getString(DOWNLINE_TOTALDONWLINES),
                                            downline_object.getString(DOWNLINE_CUSTID),
                                            downline_object.getString(DOWNLINE_CUSTSERIALNO),
                                            downline_object.getString(DOWNLINE_CUSTEDA),
                                            downline_object.getString(DOWNLINE_CUSTNAME),
                                            downline_object.getString(DOWNLINE_CUSTTYPE),
                                            downline_object.getString(DOWNLINE_CUSTJOINDATE)
                                    );
                                    recyclerAdapter.notifyDataSetChanged();

                                    modelList.add(list);
                                    recyclerAdapter.setModelList(modelList, counter);
                                }
                            }
                        }

                        //showToast("Model Size: " + modelList.size());

                        if(modelList.size() == 0) {
                            textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                            textview_family_tree.setVisibility(View.VISIBLE);
                        } else {
                            textview_family_tree.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        //showToast("Search stopped!");
                        textView_label.setVisibility(View.VISIBLE);

                        modelList.clear();
                        recyclerAdapter.notifyDataSetChanged();

                        for(int i = 0; i < downline_list.length(); i++){
                            JSONObject downline_object = downline_list.getJSONObject(i);

                            if (downline_list.length() != 0) {
                                textview_family_tree.setVisibility(View.GONE);

                                FamilyTree list = new FamilyTree(
                                        downline_object.getString(DOWNLINE_TOTALDONWLINES),
                                        downline_object.getString(DOWNLINE_CUSTID),
                                        downline_object.getString(DOWNLINE_CUSTSERIALNO),
                                        downline_object.getString(DOWNLINE_CUSTEDA),
                                        downline_object.getString(DOWNLINE_CUSTNAME),
                                        downline_object.getString(DOWNLINE_CUSTTYPE),
                                        downline_object.getString(DOWNLINE_CUSTJOINDATE)
                                );
                                recyclerAdapter.notifyDataSetChanged();

                                modelList.add(list);
                                recyclerAdapter.setModelList(modelList, counter);
                            }
                            else {
                                textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                                textview_family_tree.setVisibility(View.VISIBLE);
                            }
                        }

                        if(modelList.size() == 0) {
                            textview_family_tree.setText(getResources().getString(R.string.label_no_donwline_available));
                            textview_family_tree.setVisibility(View.VISIBLE);
                        } else {
                            textview_family_tree.setVisibility(View.GONE);
                        }
                    }
                }));
    }

    private void countTotalDownlineL2(JSONArray downline_list, String page) {
        //loop call for every downline of L1
        for(int i = 0; i < downline_list.length(); i++){
            Call<okhttp3.ResponseBody> call2 = apiInterface.getFamilyTree(
                    page,
                    modelList.get(i).getDownline_custid(),
                    getVCKeyED2E(),
                    String.valueOf(unixTimeStamp())
            );

            call2.enqueue(new Callback<okhttp3.ResponseBody>() {
                @Override
                public void onResponse(Call<okhttp3.ResponseBody> call2, Response<okhttp3.ResponseBody> response) {
                    try {
                        //showToast("SUCCESS TRY 2.1");
                        String responseStringL2 = response.body().string();
                        JSONObject objL2 = new JSONObject(responseStringL2);
                        String statusL2 = (String) objL2.get(STATUS);

                        if (statusL2.equals(SUCCESS)) {

                            JSONArray downline_listL2 = objL2.getJSONArray(DOWNLINE);
                            String downlineCountL2;

                            //+

                            try{ //if downline_list has values - set downlineCount to length of downline_list

                                downlineCountL2 = String.valueOf(downline_listL2.length());
                                modelListL2 = new ArrayList<>();

                                for(int i = 0; i < downline_listL2.length(); i++){
                                    JSONObject downline_objectL2 = downline_listL2.getJSONObject(i);
                                    FamilyTree listL2 = new FamilyTree(
                                            downline_objectL2.getString(DOWNLINE_TOTALDONWLINES),
                                            downline_objectL2.getString(DOWNLINE_CUSTID),
                                            downline_objectL2.getString(DOWNLINE_CUSTSERIALNO),
                                            downline_objectL2.getString(DOWNLINE_CUSTEDA),
                                            downline_objectL2.getString(DOWNLINE_CUSTNAME),
                                            downline_objectL2.getString(DOWNLINE_CUSTTYPE),
                                            downline_objectL2.getString(DOWNLINE_CUSTJOINDATE)
                                    );
                                    modelListL2.add(listL2);
                                }

                            } catch (JSONException e) {
                                //Log.i("Pass", "CATCHED");
                                downlineCountL2 = "0";
                            }

                            updateValueTotalDownlineCounter(Integer.parseInt(downlineCountL2));
                            //totalDownlineCounter = Integer.parseInt(downlineCountL2) + totalDownlineCounter;

                            //countTotalDownlineL3 -> START
                            countTotalDownlineL3(downline_listL2, page);
                            //countTotalDownlineL3 -> END

                            textview_total_downline.setText(getResources().getString(R.string.label_total_downline, "" + totalDownlineCounter));
                        } else {
                            showToast(getResources().getString(R.string.error_invalid_account));
                            finish();
                        }
                    } catch (IOException | JSONException e) {
                        hideProgress();
                        e.printStackTrace();
                        Writer writer = new StringWriter();
                        e.printStackTrace(new PrintWriter(writer));
                    }
                }

                @Override
                public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {

                }
            });
        }
        //showToast("totalDownlineCounter: " + totalDownlineCounter);
    }

    private void countTotalDownlineL3(JSONArray downline_listL2, String page){
        //loop call for every downline of L2
        for(int i = 0; i < downline_listL2.length(); i++){
            Call<okhttp3.ResponseBody> call3 = apiInterface.getFamilyTree(
                    page,
                    modelListL2.get(i).getDownline_custid(),
                    getVCKeyED2E(),
                    String.valueOf(unixTimeStamp())
            );

            call3.enqueue(new Callback<okhttp3.ResponseBody>() {
                @Override
                public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
                    try {
                        String responseStringL3 = response.body().string();
                        JSONObject objL3 = new JSONObject(responseStringL3);
                        String statusL3 = (String) objL3.get(STATUS);

                        if (statusL3.equals(SUCCESS)) {
                            JSONArray downline_listL3 = objL3.getJSONArray(DOWNLINE);
                            String downlineCountL3;

                            //int downlinecountL3;
                            try{
                                downlineCountL3 = String.valueOf(downline_listL3.length());
                                //downlinecount = objL3.getInt(TOTAL_DOWNLINE);
                                //totalDownlineCounter = downlinecountL3 + totalDownlineCounter;

                                //for catching purposes only - no other stuff
                                JSONObject downline_objectL3 = downline_listL3.getJSONObject(0);
                            } catch (JSONException e){
                                downlineCountL3 = "0";
                            }
                            updateValueTotalDownlineCounter(Integer.parseInt(downlineCountL3));
                            textview_total_downline.setText(getResources().getString(R.string.label_total_downline, "" + totalDownlineCounter));
                        } else {
                            showToast(getResources().getString(R.string.error_invalid_account));
                            finish();
                        }

                    } catch (IOException | JSONException e) {
                        hideProgress();
                        e.printStackTrace();
                        Writer writer = new StringWriter();
                        e.printStackTrace(new PrintWriter(writer));
                    }
                }

                @Override
                public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {

                }
            });
        }
    }

    private void updateValueTotalDownlineCounter(int i) {
        totalDownlineCounter = i + totalDownlineCounter;
    }
}
