package com.edmark.loyaltyuser.view.fragment.ledger;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewSectionedAdapter;
import com.edmark.loyaltyuser.model.DatePicker;
import com.edmark.loyaltyuser.model.LedgerMessageEvent;
import com.edmark.loyaltyuser.model.SetDateDisplay;
import com.edmark.loyaltyuser.model.Tier;
import com.edmark.loyaltyuser.model.TierData;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.EVENT_INIT_API;
import static com.edmark.loyaltyuser.constant.Common.EVENT_OPEN_DATE_PICKER;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_DATA_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_DATE_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_NO_DATA_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_TIER_ONE;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.getDateSection;
import static com.edmark.loyaltyuser.util.AppUtility.getFormattedDate;

@SuppressWarnings("unchecked")
public class TierOneFragment extends Fragment {
    private static final String TAG = TierOneFragment.class.getSimpleName();


    @BindView(R.id.fragment_tier_one_textview_date_start)
    TextView textView_date_from;
    @BindView(R.id.fragment_tier_one_textview_date_end)
    TextView textView_date_to;
    @BindView(R.id.fragment_tier_one_textview_result_count)
    TextView textView_result_count;
    @BindView(R.id.fragment_tier_one_textview_total_point)
    TextView textView_total_point;
    @BindView(R.id.fragment_tier_one_textview_total_spent)
    TextView textView_total_spent;
    @BindView(R.id.fragment_tier_one_textview_message)
    TextView textView_message;
    @BindView(R.id.fragment_tier_one_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.fragment_tier_one_button_calendar)
    ImageView button_calendar;
    @BindView(R.id.fragment_tier_one_button_search)
    ImageView button_search;
    private BaseRecyclerViewAdapter adapter;
    private BaseRecyclerViewSectionedAdapter mSectionedAdapter;
    private Gson gson;

    public TierOneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tier_one, container, false);
        ButterKnife.bind(this, view);

        gson = new Gson();
        if (getUserVisibleHint()) {
            initGUI();
        }
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Timber.d("setUserVisibleHint():" + isVisibleToUser + " " + getView());
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI();
            }
        }
    }

    private void initGUI() {
        Timber.d("initGUI()");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);
        button_calendar.setOnClickListener(v ->
        {
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                Timber.d("cal" + cal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_OPEN_DATE_PICKER, gson.toJson(new DatePicker(FRAGMENT_TIER_ONE, true, cal, null))));
        });
        button_search.setOnClickListener(v ->
        {
            if ((textView_date_from.getText().length() != 0 && textView_date_to.getText().length() != 0)
                    || (!textView_date_from.getText().toString().isEmpty() && !textView_date_to.getText().toString().isEmpty())
                    || (!textView_date_from.getText().toString().equals("") && !textView_date_to.getText().toString().equals(""))) {
                EventBus.getDefault().post(new LedgerMessageEvent(EVENT_INIT_API, String.valueOf(FRAGMENT_TIER_ONE)));
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_date), Toast.LENGTH_SHORT).show();
            }
        });

        textView_date_to.setOnClickListener(v -> {

            Calendar cal = Calendar.getInstance();
            Calendar calMin = Calendar.getInstance();
            try {
                calMin.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                calMin.add(Calendar.DATE, 1);  // number of days to add
                Timber.d("cal" + calMin.getTime());
                cal.setTime(getFormattedDate(textView_date_to.getText().toString(), DATE_FORMAT));// all done
                Timber.d("cal" + cal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_OPEN_DATE_PICKER, gson.toJson(new DatePicker(FRAGMENT_TIER_ONE, false, cal, calMin))));
        });

        textView_date_from.setOnClickListener(v -> {
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
                Timber.d("cal" + cal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_OPEN_DATE_PICKER, gson.toJson(new DatePicker(FRAGMENT_TIER_ONE, true, cal, null))));
        });


        textView_date_from.setText(getFormattedDate(new Date(), DATE_FORMAT));
        textView_date_to.setText(getFormattedDate(new Date(), DATE_FORMAT));
        EventBus.getDefault().post(new LedgerMessageEvent(EVENT_INIT_API, String.valueOf(FRAGMENT_TIER_ONE)));
    }

    private void setFromDate(int year, int month, int day) {
        textView_date_from.setText(new StringBuilder().append(month).append("-").append(day).append("-").append(year));
    }

    private void setToDate(int year, int month, int day) {
        textView_date_to.setText(new StringBuilder().append(month).append("-").append(day).append("-").append(year));
        EventBus.getDefault().post(new LedgerMessageEvent(EVENT_INIT_API, String.valueOf(FRAGMENT_TIER_ONE)));
    }

    private void initDatePickerTo() {
        Calendar cal = Calendar.getInstance();
        Calendar calMin = Calendar.getInstance();
        try {
            calMin.setTime(getFormattedDate(textView_date_from.getText().toString(), DATE_FORMAT));// all done
            calMin.add(Calendar.DATE, 1);  // number of days to add
            Timber.d("cal" + calMin.getTime());
            cal.setTime(getFormattedDate(textView_date_to.getText().toString(), DATE_FORMAT));// all done
            Timber.d("cal" + cal.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        EventBus.getDefault().post(new LedgerMessageEvent(EVENT_OPEN_DATE_PICKER, gson.toJson(new DatePicker(FRAGMENT_TIER_ONE, false, cal, calMin))));
    }

    private void setNoResult() {
        textView_message.setVisibility(View.VISIBLE);
        textView_result_count.setText(String.valueOf(0));
        textView_total_point.setText(getResources().getString(R.string.label_point, "0.0"));
        textView_total_spent.setText(getResources().getString(R.string.label_point, "0.0"));
        if (adapter != null) {
            adapter.clear();
        }
        if (mSectionedAdapter != null) {
            mSectionedAdapter.notifyDataSetChanged();
        }
    }

    private void setDataNoDate(ArrayList<Tier> data)
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        textView_message.setVisibility(View.GONE);
        adapter = new BaseRecyclerViewAdapter(data, R.layout.child_tier, BR.Tier, (v, item, position) ->
        {
        });
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        double totalPoint = 0;
        double totalSpentPoint = 0;
        for (Tier items : data) {
            totalPoint = totalPoint + Double.valueOf(items.getBonus_amount().replace(",", ""));
            totalSpentPoint = totalSpentPoint + Double.valueOf(items.getSpending_amount().replace(",", ""));
        }

        String totalPointDisplay = getResources().getString(R.string.label_point, decimalOutputFormat(String.valueOf(totalPoint)));
        String totalSpentDisplay = getResources().getString(R.string.label_point, decimalOutputFormat(String.valueOf(totalSpentPoint)));
        textView_total_point.setText(totalPointDisplay);
        textView_result_count.setText(String.valueOf(data.size()));
        textView_total_spent.setText(totalSpentDisplay);

        Collections.sort(data, (o1, o2) -> o1.getPassup_date().compareTo(o2.getPassup_date()));
        adapter.animateTo(data);
    }

    private void setData(ArrayList<Tier> data) {
        textView_message.setVisibility(View.GONE);
        adapter = new BaseRecyclerViewAdapter(data, R.layout.child_tier, BR.Tier, (v, item, position) ->
        {
        });
        //Add your adapter to the sectionAdapter
        mSectionedAdapter = new BaseRecyclerViewSectionedAdapter(Objects.requireNonNull(getActivity()), R.layout.child_header, R.id.child_header_text, adapter);
        //Apply this adapter to the RecyclerView
        recyclerView.setAdapter(mSectionedAdapter);
        //Get all dates for the header/section
        ArrayList<String> dates = new ArrayList<>();
        double totalPoint = 0;
        double totalSpentPoint = 0;
        for (Tier items : data) {
            dates.add(items.getPassup_date());
            totalPoint = totalPoint + Double.valueOf(items.getBonus_amount().replace(",", ""));
            totalSpentPoint = totalSpentPoint + Double.valueOf(items.getSpending_amount().replace(",", ""));
        }

        String totalPointDisplay = getResources().getString(R.string.label_point, decimalOutputFormat(String.valueOf(totalPoint)));
        String totalSpentDisplay = getResources().getString(R.string.label_point, decimalOutputFormat(String.valueOf(totalSpentPoint)));
        textView_total_point.setText(totalPointDisplay);
        textView_result_count.setText(String.valueOf(data.size()));
        textView_total_spent.setText(totalSpentDisplay);

        Collections.sort(data, (o1, o2) -> o1.getPassup_date().compareTo(o2.getPassup_date()));
        adapter.animateTo(data);

        //Setting up the headers/sections
        List<BaseRecyclerViewSectionedAdapter.Section> sections = new ArrayList<>();

        for (String date : getDateSection(dates)) {
//            int occurrences = Collections.frequency(data, new Tier(date));
//            Timber.d("Date:" + date + " occurrences:" + occurrences);
            int position = 0;
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getPassup_date().equals(date)) {
                    position = i;
                    Timber.d("Date:" + date + " position:" + position);
                    break;
                }
            }
            sections.add(new BaseRecyclerViewSectionedAdapter.Section(position, date));
        }

        mSectionedAdapter.setSections(sections.toArray(new BaseRecyclerViewSectionedAdapter.Section[sections.size()]));
        mSectionedAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LedgerMessageEvent event) {
        Timber.d("onMessageEvent:" + event.getMessage() + " " + event.getData());
        switch (event.getMessage()) {
            case EVENT_SET_DATE_DISPLAY:
                SetDateDisplay setDateDisplay = gson.fromJson(event.getData(), SetDateDisplay.class);
                if (setDateDisplay.getPosition() == FRAGMENT_TIER_ONE)
                    if (setDateDisplay.getFromDate()) {
                        setFromDate(setDateDisplay.getYear(), setDateDisplay.getMonth() + 1, setDateDisplay.getDay());
                        initDatePickerTo();
                    } else {
                        setToDate(setDateDisplay.getYear(), setDateDisplay.getMonth() + 1, setDateDisplay.getDay());
                    }
                break;
            case EVENT_SET_DATA_DISPLAY:
                TierData data = gson.fromJson(event.getData(), TierData.class);
                if (data.getPosition() == FRAGMENT_TIER_ONE) {
                    ArrayList<Tier> datalist = data.getData();
//                    setData(datalist);
                    setDataNoDate(datalist);
                    break;
                }
            case EVENT_SET_NO_DATA_DISPLAY:
                    setNoResult();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.d("onStart:");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        Timber.d("onStop:");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}