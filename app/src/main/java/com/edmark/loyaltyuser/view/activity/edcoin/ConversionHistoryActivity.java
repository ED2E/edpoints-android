package com.edmark.loyaltyuser.view.activity.edcoin;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.ConversionHistoryAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.EDCoin;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.CREATED_DATE;
import static com.edmark.loyaltyuser.constant.Common.DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.EDC_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.EDP_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RECORD;
import static com.edmark.loyaltyuser.constant.Common.REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;

public class ConversionHistoryActivity extends BaseActivity {

    public static final String TAG = ConversionHistoryActivity.class.getSimpleName();

    @BindView(R.id.activity_conversion_history_textview_message)
    TextView textView_message;

    private ApiInterface apiInterface;
    private AppPreference preference;

    private ArrayList<EDCoin> modelList;
    static RecyclerView recyclerView;
    ConversionHistoryAdapter recyclerAdapter;

    private int cust_id;
    private String eda_no;
    private String bind_key = "";
    private String countryCode = "";
    private String selectedServer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_history);

        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        cust_id = Integer.parseInt(preference.getString(KEY_CUSID));

        setupToolBar(R.id.activity_conversion_history_toolbar);
        initData(savedInstanceState);
        initCompanyCode();
        //initEDANumber();
        initGUI();
        initAPI();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        Intent intent = new Intent(getContext(), EDCoinActivity.class);
        startActivity(intent);

        finish();
    }

    private void initData(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            //eda_no = "0";
            bind_key = "";
        } else {
            //eda_no = extras.getString(KEY_EDA_NO_VALUE);
            bind_key = extras.getString(KEY_BIND_KEY);
        }
    }

    private void initCompanyCode(){
        countryCode = preference.getString(KEY_COUNTRY_CODE);

        String[] sg_server_array = getResources().getStringArray(R.array.sg_server_array);
        String[] ams_server_array = getResources().getStringArray(R.array.ams_server_array);

        for (String i:sg_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "SG";
            }
        }
        for (String i:ams_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "AMS";
            }
        }
    }

    private void initGUI() {

        //-- initialize recycler view, layout, and adapter
        recyclerView = (RecyclerView)findViewById(R.id.activity_conversion_history_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new ConversionHistoryAdapter(this, modelList);
        recyclerView.setAdapter(recyclerAdapter);

        //textView_message.setText(eda_no);

    }

    private void initAPI() {
        showProgress();

        apiInterface = ApiClientEDCoin.getClient(selectedServer).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getTransactionLogs(
                //eda_no,
                bind_key,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        Timber.d("bind_key: " + bind_key );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                //showToast("Successful Response");
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection) + "\n(Response body is null)");
                    hideProgress();
                    loadEDCoinActivity();
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();
                    //showToast(responseString);
                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);


                    if (status.equals(SUCCESS)) {
                        textView_message.setText(responseString);
                        /*String rate = (String) obj.get(RATE);
                        double rateToDouble = Double.valueOf(rate.replace(",", "").trim());
                        textview_current_conversion_rate_value.setText(getResources().getString(R.string.label_current_conversion_rate_value,String.valueOf(rateToDouble)));*/

                        modelList = new ArrayList<>();

                        JSONArray record_array = obj.getJSONArray(RECORD);

                        for(int i = (record_array.length()-1) ; i >= 0; i--){
                            JSONObject record_object = record_array.getJSONObject(i);
                            EDCoin list = new EDCoin(
                                    record_object.getString(REFERENCE_NO),
                                    record_object.getString(EDC_AMOUNT),
                                    record_object.getString(EDP_AMOUNT),
                                    record_object.getString(DESCRIPTION),
                                    record_object.getString(CREATED_DATE)

                            );

                            modelList.add(list);
                        }

                        if (modelList.size() != 0)
                            textView_message.setVisibility(View.GONE);
                        else
                            textView_message.setVisibility(View.VISIBLE);

                        recyclerAdapter.setModelList(modelList);

                    } else if (status.equals(FAIL)) {

                        hideProgress();
                        String message = (String) obj.get(MESSAGE);

                        if(message.equalsIgnoreCase("No Data Found")){
                            textView_message.setVisibility(View.VISIBLE);
                        } else {
                            textView_message.setVisibility(View.GONE);
                            showToast(message);
                            loadEDCoinActivity();
                        }

                    } else {
                        showToast(getResources().getString(R.string.error_try_again));
                        loadEDCoinActivity();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    showToast(e.getMessage());
                    loadEDCoinActivity();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //showToast("Failed Response");

                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection) + "\n(" + t.getMessage() + ")");
                    hideProgress();
                    loadEDCoinActivity();
                    return;
                }

                hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection) + "\n(" + t.getMessage() + ")");
                loadEDCoinActivity();
            }
        });
    }

    public void loadEDCoinActivity(){
        Intent intent = new Intent(this, EDCoinActivity.class);
        startActivity(intent);

        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Intent intent = new Intent(getContext(), EDCoinActivity.class);
                startActivity(intent);

                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
