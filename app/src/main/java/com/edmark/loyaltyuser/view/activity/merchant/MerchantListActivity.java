package com.edmark.loyaltyuser.view.activity.merchant;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Merchant;
import com.edmark.loyaltyuser.presenter.merchant.MerchantListContract;
import com.edmark.loyaltyuser.presenter.merchant.MerchantListPresenter;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

import static com.edmark.loyaltyuser.constant.Common.KEY_API;
import static com.edmark.loyaltyuser.constant.Common.KEY_CATEGORY;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_CODE;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_MERCHANT_DETAILS;

/*
 * Created by DEV-Michael-ED2E on 04/12/2018.
 */

@SuppressWarnings("unchecked")
public class MerchantListActivity extends BaseActivity implements MerchantListContract.View{
    public static final String TAG = MerchantListActivity.class.getSimpleName();

    @BindView(R.id.activity_merchant_list_recyclerview) RecyclerView recyclerView;
    @BindView(R.id.activity_merchant_list_toolbar_title) TextView textView_title;
    @BindView(R.id.activity_merchant_list_editText_search) EditText editext_search;
    @BindView(R.id.activity_merchant_list_textview_label) TextView textView_label;

    private ArrayList<Merchant> datalist = new ArrayList<>();
    private ArrayList<Merchant> searchdatalist=new ArrayList<>();
    private BaseRecyclerViewAdapter adapter;
    AppPreference preference;
    private Gson gson;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private String company_code;
    private  MerchantListContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_list);
        ButterKnife.bind(this);

        presenter = new MerchantListPresenter(this);
        preference = AppPreference.getInstance(this);
        gson = new Gson();

        initToolBar(savedInstanceState);
        initGUI(getAPI(savedInstanceState));
    }

    private String getCategory(Bundle savedInstanceState) {
        String category;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                category = null;
            } else {
                category = extras.getString(KEY_CATEGORY);
            }
        } else {
            category = (String) savedInstanceState.getSerializable(KEY_CATEGORY);
        }
        return category;
    }

    private String getAPI(Bundle savedInstanceState) {
        String api;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                api = null;
            } else {
                api = extras.getString(KEY_API);
            }
        } else {
            api = (String) savedInstanceState.getSerializable(KEY_API);
        }
        return api;
    }

    private void initToolBar(Bundle savedInstanceState) {
        setupToolBar(R.id.activity_merchant_list_toolbar);
        int startIndex = getCategory(savedInstanceState).lastIndexOf(":");
        textView_title.setText(getCategory(savedInstanceState).substring(0,startIndex));
        company_code = getCategory(savedInstanceState).substring(startIndex+1);
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGUI(String api) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new BaseRecyclerViewAdapter(searchdatalist, R.layout.child_merchant, BR.Merchant, (v, item, position) ->
        {
            Intent intent = new Intent(getContext(), MerchantDetailsActivity.class);
            intent.putExtra(KEY_MERCHANT_CODE, gson.toJson(searchdatalist.get(position)));
            startActivityForResult(intent, REQUEST_CODE_MERCHANT_DETAILS);
        });
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        presenter.getMerchants(api,textView_title.getText().toString(),company_code);

        Observable<CharSequence> changeObservableSearch = RxTextView.textChanges(editext_search);

        disposables.add(changeObservableSearch.map(inputText -> (inputText.length() == 0))
                .subscribe(isValid ->
                {
                    if(!isValid)
                    {
                        textView_label.setVisibility(View.GONE);
                        searchdatalist.clear();
                        adapter.notifyDataSetChanged();
                        adapter.animateTo(searchdatalist);
                        for (int i = 0; i < datalist.size(); i++) {
                            Merchant merchant = datalist.get(i);
                            if (merchant.getMerchant_name().toUpperCase().contains(editext_search.getText().toString().toUpperCase())) {
                                searchdatalist.add(merchant);
                                adapter.notifyDataSetChanged();
                                adapter.animateTo(searchdatalist);
                            }
                        }
                    }
                    else
                    {
                        textView_label.setVisibility(View.VISIBLE);
                        searchdatalist.clear();
                        searchdatalist.addAll(datalist);
                        adapter.notifyDataSetChanged();
                        adapter.animateTo(searchdatalist);
                    }
                }));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Using clear will clear all, but can accept new disposable
        disposables.clear();
        // Using dispose will clear all and set isDisposed = true, so it will not accept any new disposable
        disposables.dispose();
    }

    @Override
    public void onLoadSuccess(@NotNull ArrayList<Merchant> merchantList) {
        datalist.clear();
        datalist.addAll(merchantList);
        Collections.sort(datalist, (o1, o2) -> o1.getMerchant_name().compareTo(o2.getMerchant_name()));
        searchdatalist.clear();
        searchdatalist.addAll(datalist);
        adapter.notifyDataSetChanged();
        adapter.animateTo(searchdatalist);
    }
}
