package com.edmark.loyaltyuser.view.fragment.merchant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.adapter.SpinnerAdapter;
import com.edmark.loyaltyuser.model.Country;
import com.edmark.loyaltyuser.model.MainMessageEvent;
import com.edmark.loyaltyuser.model.Merchant;
import com.edmark.loyaltyuser.model.MerchantMessageEvent;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.AppUtility;
import com.glide.slider.library.svg.GlideApp;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_ONLINE_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_ONLINE_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.TAG_MERCHANT_ONLINE;
import static com.edmark.loyaltyuser.constant.Common.URL_HEADER;

/*
 * Created by DEV-Michael-ED2E on 04/25/2018.
 */

@SuppressWarnings("unchecked")
public class OnlineFragment extends Fragment {
    public static final String TAG = OnlineFragment.class.getSimpleName();


    @BindView(R.id.fragment_merchant_online_layout_coming_soon) LinearLayout layout_coming_soon;
    @BindView(R.id.fragment_merchant_online_imageview) ImageView imageView_coming_soon;
    @BindView(R.id.fragment_merchant_online_recyclerview_merchant) RecyclerView recyclerView;
    /*For category**/
//    @BindView(R.id.fragment_merchant_online_recyclerview_merchant_active_lifestyle) RecyclerView recyclerViewActiveLifeStyle;
//    @BindView(R.id.fragment_merchant_online_recyclerview_merchant_health_beauty) RecyclerView recyclerViewHealthBeauty;
//    @BindView(R.id.fragment_merchant_online_textview_food_more) TextView textView_food;
//    @BindView(R.id.fragment_merchant_online_textview_active_lifestyle_more) TextView textView_active_lifestyle;
//    @BindView(R.id.fragment_merchant_online_textview_health_beauty_more) TextView textView_health_beauty;
    @BindView(R.id.fragment_merchant_online_spinner_country) Spinner spinner;
    private Unbinder unbinder;
    private BaseRecyclerViewAdapter adapter;
    /*For category**/
//    private BaseRecyclerViewAdapter adapterActiveLifeStyle;
//    private BaseRecyclerViewAdapter adapterHealthBeauty;
    private ArrayList<Merchant> datalist = new ArrayList<>();
    private ArrayList<Merchant> filtereddatalist = new ArrayList<>();
    private ArrayList<Merchant> datalistActiveLifeStyle = new ArrayList<>();
    private ArrayList<Merchant> datalistHealthBeauty = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_merchant_online, container, false);
        unbinder = ButterKnife.bind(this, view);
        if(getUserVisibleHint())
        {
            initGUI();
        }
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Timber.d("setUserVisibleHint():"+isVisibleToUser+" "+getView());
        if(getView()!=null) {
            if (isVisibleToUser) {
                initGUI();
            }
        }
    }

    private void initGUI() {
        //For category
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
//        adapter = new BaseRecyclerViewAdapter(datalist,R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
//        {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT,datalist.get(position).getEmail()));
//        });
//        adapterActiveLifeStyle = new BaseRecyclerViewAdapter(datalistActiveLifeStyle,R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
//        {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT,datalist.get(position).getEmail()));
//        });
//        adapterHealthBeauty = new BaseRecyclerViewAdapter(datalistHealthBeauty,R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
//        {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT,datalist.get(position).getEmail()));
//        });
//        recyclerView.setLayoutManager(gridLayoutManager);
//        recyclerView.setAdapter(adapter);
//
//        gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
//        recyclerViewActiveLifeStyle.setLayoutManager(gridLayoutManager);
//        recyclerViewActiveLifeStyle.setAdapter(adapterActiveLifeStyle);
//
//        gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
//        recyclerViewHealthBeauty.setLayoutManager(gridLayoutManager);
//        recyclerViewHealthBeauty.setAdapter(adapterHealthBeauty);
//
//        EventBus.getDefault().post(new MerchantMessageEvent(TAG_MERCHANT_ONLINE,null));
//        textView_food.setOnClickListener(v -> {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MERCHANT_LIST, getResources().getString(R.string.label_merchant_category_food)));
//
//        });
//        textView_active_lifestyle.setOnClickListener(v -> {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MERCHANT_LIST, getResources().getString(R.string.label_merchant_category_active_life_style)));
//
//        });
//        textView_health_beauty.setOnClickListener(v -> {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MERCHANT_LIST, getResources().getString(R.string.label_merchant_category_health_beauty)));
//
//        });



        //For no category
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
        adapter = new BaseRecyclerViewAdapter(filtereddatalist,R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
        {
            String url = filtereddatalist.get(position).getWebsiteurl();
            if(url.equals(""))
            {
                EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT,null));
            }
            else
            {
                if(!url.contains(URL_HEADER))
                    url = URL_HEADER + filtereddatalist.get(position).getWebsiteurl();

                EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT,url));
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
        EventBus.getDefault().post(new MerchantMessageEvent(TAG_MERCHANT_ONLINE,null));

        ArrayList<Country> countries = new ArrayList<>(AppUtility.getCompanyCountry(Objects.requireNonNull(getContext())));
        Collections.sort(countries, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(),R.layout.child_spinner_country,countries);
        spinner.setAdapter(spinnerAdapter);

        AppPreference preference = AppPreference.getInstance(getContext());
        String country_code = preference.getString(KEY_COUNTRY_CODE);
        int position = 0;
        for (int i = 0; i < countries.size(); i++) {
            if(Objects.requireNonNull(countries.get(i).getCode()).equals(country_code))
            {
                position = i;
                break;
            }
        }
        spinner.setSelection(position);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                filterData(countries);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        GlideApp.with(getContext())
                .load(R.drawable.ic_loading)
                .into(imageView_coming_soon);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView()");
        unbinder.unbind();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MainMessageEvent event) {
        Timber.d("onMessageEvent:" + event.getData());
        if(event.getMessage().equals(API_ONLINE_MERCHANT))
        {
            datalist.clear();
            Gson gson = new Gson();
            ArrayList<Merchant> merchants = gson.fromJson(event.getData(), ArrayList.class);
            for (int i = 0; i < merchants.size(); i++) {
                Timber.d("merchant list:" + gson.toJson(merchants.get(i)));
                Merchant merchant = gson.fromJson(gson.toJson(merchants.get(i)), Merchant.class);
                datalist.add(merchant);
            }

            ArrayList<Country> countries = new ArrayList<>(AppUtility.getCompanyCountry(Objects.requireNonNull(getContext())));
            Collections.sort(countries, (o1, o2) -> o1.getName().compareTo(o2.getName()));
            filterData(countries);
        }
    }

    private void filterData(ArrayList<Country> countries)
    {
        filtereddatalist.clear();
        for (int i = 0; i < datalist.size(); i++) {
            if(datalist.get(i).getCompany_code().equals(countries.get(spinner.getSelectedItemPosition()).getCode()))
            {
                filtereddatalist.add(datalist.get(i));
            }
        }
        Collections.sort(filtereddatalist, (o1, o2) -> o1.getMerchant_name().compareTo(o2.getMerchant_name()));
        adapter.notifyDataSetChanged();
        adapter.animateTo(filtereddatalist);

        if(filtereddatalist.size()==0) {
            layout_coming_soon.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else {
            layout_coming_soon.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}