package com.edmark.loyaltyuser.view.activity.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.edmark.loyaltyuser.BuildConfig;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.InternetUtils;
import com.edmark.loyaltyuser.util.PermissionUtils;
import com.edmark.loyaltyuser.view.activity.login.LoginActivity;
import com.edmark.loyaltyuser.view.activity.main.MainActivity;
import com.edmark.loyaltyuser.view.activity.main.OnboardingActivity;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edmark.loyaltyuser.constant.Common.DEVICE_TYPE;
import static com.edmark.loyaltyuser.constant.Common.DISTRIBUTOR;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_APP_VER;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_TOPIC;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_REMEMBERED;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_EXIT;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_UPDATE;
import static com.edmark.loyaltyuser.constant.Common.SPLASH_SCREEN_TIME;
import static com.edmark.loyaltyuser.util.AppUtility.compareVersionNames;

/*
 * Created by DEV-Michael-ED2E on 04/05/2018.
 */

public class SplashActivity extends BaseActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();

    @BindView(R.id.activity_splash_imageview_logo)
    ImageView imageViewLogo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);

        initGUI();

        String[] permissions = new String[]{PermissionUtils.PER_CAMERA, PermissionUtils.PER_READ_STORAGE, PermissionUtils.PER_WRITE_STORAGE};
        PermissionUtils.RequestPermissions(SplashActivity.this, PermissionUtils.FEEDBACK_PERMISSION_REQUEST_CODE, permissions);

        // Assume thisActivity is the current activity
//        int permissionCheck = ContextCompat.checkSelfPermission(SplashActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
//        if(permissionCheck== PackageManager.PERMISSION_DENIED)
//        {
//            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        }

        InternetUtils internetChecker = new InternetUtils();
        if (internetChecker.isInternetOn(SplashActivity.this)) {
            FirebaseMessaging.getInstance().subscribeToTopic(FIREBASE_TOPIC);
            if(BuildConfig.DEBUG) {
                FirebaseMessaging.getInstance().subscribeToTopic(FIREBASE_TOPIC+"TESTING_TOPIC");
            }
            checkVersion();
        } else {
            showDialog(REQUEST_CODE_EXIT, getResources().getString(R.string.error_internet_connection), true);
//            Toast.makeText(getContext(), getResources().getString(R.string.error_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void initIntent() {
        AppPreference preference = AppPreference.getInstance(this);
        if (preference.getBoolean(KEY_REMEMBERED, false)) {
            String type = preference.getString(KEY_ACCOUNT_TYPE, "");
            if (type.equals("")) {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    preference.putString(KEY_EDCOIN_BIND_KEY, "");
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }, SPLASH_SCREEN_TIME);
            } else {
                if (type.equals(DISTRIBUTOR)) {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        Log.d(TAG, "this is distributor show card picker:");
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        intent.putExtra(KEY_ACCOUNT_TYPE, DISTRIBUTOR);
                        startActivity(intent);
                        finish();
                    }, SPLASH_SCREEN_TIME);
                } else {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        intent.putExtra(KEY_ACCOUNT_TYPE, MERCHANT);
                        startActivity(intent);
                        finish();
                    }, SPLASH_SCREEN_TIME);
                }
            }
        } else {
            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }, SPLASH_SCREEN_TIME);
        }
    }

    @Override
    protected void onDialogDismiss(int requestCode) {
        super.onDialogDismiss(requestCode);

        if (requestCode == REQUEST_CODE_UPDATE) {
            initGooglePlay();
        }
        else
        {
            finish();
        }
    }

    private void initGUI() {
        Glide.with(getContext())
                .load(R.drawable.ic_edpoints_logo)
                .into(imageViewLogo);
    }

    private void checkVersion() {
        Log.d(TAG, "checkVerions");
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(FIREBASE_KEY_APP_VER)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            Log.d(TAG, document.getId() + " => " + document.getData());
                        }
                        //direct document
                        for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                            if (doc.get(DEVICE_TYPE) != null) {
                                Log.d(TAG, "App version:" + doc.getId() + " => " + doc.getString(DEVICE_TYPE));
                                String app_ver = doc.getString(DEVICE_TYPE);
                                if (app_ver != null) {
                                    Log.d(TAG, "compareVersionNames:" + compareVersionNames(app_ver, BuildConfig.VERSION_NAME));
                                    if(compareVersionNames(app_ver, BuildConfig.VERSION_NAME)>0) //if 1 outdated app
                                    {
                                        showDialog(REQUEST_CODE_UPDATE, getResources().getString(R.string.error_app_not_updated), true);
                                    } else {
                                        final Handler handler = new Handler();
                                        handler.postDelayed(() -> {
                                            startActivity(new Intent(SplashActivity.this, OnboardingActivity.class));
                                            finish();
                                        }, SPLASH_SCREEN_TIME);
                                    }
                                }
                            }
                        }

                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
//                        initIntent();
                        final Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            startActivity(new Intent(SplashActivity.this, OnboardingActivity.class));
                            finish();
                        }, SPLASH_SCREEN_TIME);
                    }
                });
    }

    private void initGooglePlay()
    {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
        finish();
    }
}
