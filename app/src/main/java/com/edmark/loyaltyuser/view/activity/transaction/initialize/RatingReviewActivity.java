package com.edmark.loyaltyuser.view.activity.transaction.initialize;


import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import hyogeun.github.com.colorratingbarlib.ColorRatingBar;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_SUBMIT_REVIEW;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERNAME;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_EXIT;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKey;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/11/2018.
 */
public class RatingReviewActivity extends BaseActivity{
    public static final String TAG = RatingReviewActivity.class.getSimpleName();

    @BindView(R.id.activity_rating_and_review_textview_type) TextView textView_type;
    @BindView(R.id.activity_rating_and_review_button_ok) Button button_proceed;
    @BindView(R.id.activity_rating_and_review_ratingBar) ColorRatingBar ratingBar;
    @BindView(R.id.activity_rating_and_review_editText_description) EditText editText_description;
    @BindView(R.id.activity_rating_and_review_textview_description_count) TextView textView_description_count;
    @BindView(R.id.activity_rating_and_review_textview_rating) TextView textView_rating;
    private final CompositeDisposable disposables = new CompositeDisposable();

    private ApiInterface apiService;
    private AppPreference preference;

    private String merchantid;
    private String transaction_id;

    //for utility transaction applicable - formatting of user's review for biller identification
    private String biller_name;
    private String review;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_and_review);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        loadData(savedInstanceState);
        initGUI();

    }

    private void loadData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) { //if bundle is NULL - initialize w/ default value
                //transaction_id = "";
                //merchantid = "";
                transaction_id = preference.getString(KEY_TRANSACTION_ID, "");
                merchantid = preference.getString(KEY_MERCHANT_ID, "");
                biller_name = "";
            } else { //if bundle has value from previous activity
                //transaction_id = extras.getString(KEY_TRANSACTION_ID);
                if(preference.getString(KEY_MERCHANT_ID, "").isEmpty()){
                    transaction_id = extras.getString(KEY_TRANSACTION_ID);
                    preference.putString(KEY_TRANSACTION_ID, transaction_id);

                    merchantid = extras.getString(KEY_MERCHANT_ID);
                    preference.putString(KEY_MERCHANT_ID, merchantid);
                } else{
                    transaction_id = preference.getString(KEY_TRANSACTION_ID, "");
                    merchantid = preference.getString(KEY_MERCHANT_ID, "");
                }

                biller_name = extras.getString(KEY_UTILITY_BILLERNAME);
            }
        } else {
            //transaction_id = (String) savedInstanceState.getSerializable(KEY_TRANSACTION_ID);
            //merchantid = (String) savedInstanceState.getSerializable(KEY_MERCHANT_ID);
            transaction_id = preference.getString(KEY_TRANSACTION_ID, "");
            merchantid = preference.getString(KEY_MERCHANT_ID, "");
            biller_name = "";
        }
    }

    private void initGUI() {

        //TODO: update custid upon indication of express pay biller
        //update textview if merchant is an expresspay biller
        if(merchantid != null){
            if (merchantid.toUpperCase().equals("1354159")) {
                textView_type.setText("Please rate and review your biller:");
            }
        }


        button_proceed.setOnClickListener(v ->
        {
            if (ratingBar.getRating() != 0) {

                //if transaction is for Utility, format user's review
                if(biller_name.equalsIgnoreCase("") || biller_name == null){
                    review = editText_description.getText().toString();
                } else {
                    review = "For biller: " + biller_name + " - \"" + editText_description.getText().toString() + "\"";
                }

                initSendReview(merchantid, transaction_id, review, (int) ratingBar.getRating());
            } else {
                showToast(getResources().getString(R.string.error_invalid_rating));
            }
        });

        Observable<CharSequence> passwordChangeObservable = RxTextView.textChanges(editText_description);
        disposables.add(passwordChangeObservable.map(inputText -> (inputText.length() == 0 || inputText.length() >= 300))
                .subscribe(isValid ->
                {
                    String length = String.valueOf(editText_description.getText().length());
                    textView_description_count.setText(getResources().getString(R.string.label_description_count, length));
                }));
        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            String ratingMsg;
            if (rating == 1) {
                ratingMsg = getResources().getString(R.string.label_rate_hated_it);
            } else if (rating == 2) {
                ratingMsg = getResources().getString(R.string.label_rate_dislike_it);
            } else if (rating == 3) {
                ratingMsg = getResources().getString(R.string.label_rate_its_ok);
            } else if (rating == 4) {
                ratingMsg = getResources().getString(R.string.label_rate_liked_it);
            } else if (rating == 5) {
                ratingMsg = getResources().getString(R.string.label_rate_loved_it);
            } else {
                ratingMsg = "";
            }

            textView_rating.setText(ratingMsg);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Using clear will clear all, but can accept new disposable
        disposables.clear();
        // Using dispose will clear all and set isDisposed = true, so it will not accept any new disposable
        disposables.dispose();
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                showDialog(REQUEST_CODE_EXIT,getResources().getString(R.string.dialog_review_exit),false);

                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }



    private void initSendReview(String merchantid, String transactionid, String description, int rating) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.sendreview(API_SUBMIT_REVIEW, custId, merchantid, rating, transactionid, description, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = response.body().string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);
                    String message = (String) obj.get(MESSAGE);
                    if (status.equals(SUCCESS)) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    } else {
//                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                    finish();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    @Override
    protected void onDialogDismiss(int requestCode) {
        super.onDialogDismiss(requestCode);
        if(requestCode==REQUEST_CODE_EXIT)
        {
            finish();
        }
    }
}
