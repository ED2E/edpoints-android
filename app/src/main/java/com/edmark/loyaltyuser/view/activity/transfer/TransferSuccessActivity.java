package com.edmark.loyaltyuser.view.activity.transfer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.edcoin.EDCoinActivity;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_NAME;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_DATE_TIME;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT_FROM;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT_TO;
import static com.edmark.loyaltyuser.constant.Common.KEY_REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSFER_APP;
import static com.edmark.loyaltyuser.constant.Common.KEY_WALLET_COUNTRY_CODE;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_MMMMDDYYYY;
import static com.edmark.loyaltyuser.util.AppUtility.formatTime_HHMMAAA;

public class TransferSuccessActivity extends BaseActivity {

    public static final String TAG = TransferSuccessActivity.class.getSimpleName();

    /*@BindView(R.id.activity_conversion_success_textView_app)
    TextView textView_app;*/

    @BindView(R.id.activity_transfer_success_textView_app)
    TextView textView_app;
    @BindView(R.id.activity_transfer_success_textView_name)
    TextView textView_name;
    @BindView(R.id.activity_transfer_success_textView_edp_amount_label_from)
    TextView textView_edp_amount_label_from;
    @BindView(R.id.activity_transfer_success_textView_edp_amount_from)
    TextView textView_edp_amount_from;
    @BindView(R.id.activity_transfer_success_linearLayout_edp_amount_to)
    LinearLayout linearLayout_edp_amount_to;
    @BindView(R.id.activity_transfer_success_textView_edp_amount_label_to)
    TextView textView_edp_amount_label_to;
    @BindView(R.id.activity_transfer_success_textView_edp_amount_to)
    TextView textView_edp_amount_to;
    @BindView(R.id.activity_transfer_success_textView_date_time)
    TextView textView_date_time;
    @BindView(R.id.activity_transfer_success_textView_ref_num)
    TextView textView_ref_num;
    @BindView(R.id.activity_transfer_success_button_1)
    Button button_view_wallet;

    private ApiInterface apiInterface;
    private AppPreference preference;

    private String transfer_app = "", account_name = "", edp_amount_from = "", edp_amount_to = "", ref_no = "", date_time = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_success);
        ButterKnife.bind(this);
        preference = AppPreference.getInstance(this);
        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);

        //setupToolBar(R.id.activity_transfer_success_toolbar);
        initData(savedInstanceState);
        initGUI();
    }

    private void initData(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            transfer_app = "";
            account_name = "";
            edp_amount_from = "";
            edp_amount_to = "";
            ref_no = "";
            date_time = "";
        } else {
            transfer_app = extras.getString(KEY_TRANSFER_APP);
            account_name = extras.getString(KEY_ACCOUNT_NAME);
            edp_amount_from = extras.getString(KEY_EDP_AMOUNT_FROM);
            edp_amount_to = extras.getString(KEY_EDP_AMOUNT_TO);
            ref_no = extras.getString(KEY_REFERENCE_NO);
            date_time = extras.getString(KEY_DATE_TIME);
        }
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        Intent intent = new Intent(getContext(), EDCoinActivity.class);
        startActivity(intent);
        finish();
    }

    private void initGUI() {
        Log.d(TAG, "initGUI() triggered");

        DecimalFormat formatter = new DecimalFormat("###,##0.00");

        textView_app.setText(transfer_app);
        textView_name.setText(account_name);

        textView_edp_amount_label_from.setText("EDPOINTS Amount (" + preference.getString(KEY_WALLET_COUNTRY_CODE) + ")");
        textView_edp_amount_from.setText("" + formatter.format(Double.parseDouble(edp_amount_from.replace(",", ""))));
        textView_edp_amount_label_to.setText("EDPOINTS Amount (" + preference.getString(KEY_COUNTRY_CODE) + ")");
        textView_edp_amount_to.setText("" + formatter.format(Double.parseDouble(edp_amount_to.replace(",", ""))));

        //if same country code - hide to_country
        if(preference.getString(KEY_WALLET_COUNTRY_CODE).equalsIgnoreCase(preference.getString(KEY_COUNTRY_CODE))){
            linearLayout_edp_amount_to.setVisibility(View.GONE);
        } else {
            linearLayout_edp_amount_to.setVisibility(View.VISIBLE);
        }

        textView_ref_num.setText(ref_no);
        textView_date_time.setText(formatDate_MMMMDDYYYY(date_time) + "  " + formatTime_HHMMAAA(date_time).toUpperCase().replace(".", ""));

        button_view_wallet.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), EDCoinActivity.class);
            startActivity(intent);
            finish();
        });
    }




    /*@Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        //showToast_short("Input: " + input_data);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Intent intent = new Intent(getContext(), EDCoinActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
