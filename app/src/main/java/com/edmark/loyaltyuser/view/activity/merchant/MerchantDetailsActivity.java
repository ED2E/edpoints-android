package com.edmark.loyaltyuser.view.activity.merchant;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Merchant;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_CODE;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_MERCHANT_DETAILS;

/*
 * Created by DEV-Michael-ED2E on 04/27/2018.
 */
@SuppressWarnings("unchecked")
public class MerchantDetailsActivity extends BaseActivity {
    public static final String TAG = MerchantDetailsActivity.class.getSimpleName();

    @BindView(R.id.activity_merchant_detail_toolbar_title)
    TextView textView_tootbar_title;
    @BindView(R.id.activity_merchant_detail_textview_name)
    TextView textView_name;
    @BindView(R.id.activity_merchant_detail_webView)
    WebView webView;
    @BindView(R.id.activity_merchant_detail_button_review)
    Button button_review;
    @BindView(R.id.activity_merchant_detail_imageview_logo)
    ImageView imageView_logo;

    private String merchant_code;
    private Merchant merchant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_detail);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_merchant_detail_toolbar);
        merchant_code = initData(savedInstanceState);
        Timber.d("merchant_code:" + merchant_code);
        initGui();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private String initData(Bundle savedInstanceState) {
        Gson gson = new Gson();
        String merchant_code;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchant_code = null;
            } else {
                merchant = gson.fromJson(extras.getString(KEY_MERCHANT_CODE), Merchant.class);
                merchant_code = merchant.getMerchant_code();
            }
        } else {
            merchant = gson.fromJson((String) savedInstanceState.getSerializable(KEY_MERCHANT_CODE), Merchant.class);
            merchant_code = merchant.getMerchant_code();
        }
        return merchant_code;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initGui() {
        button_review.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MerchantReviewActivity.class);
            intent.putExtra(KEY_MERCHANT_CODE, merchant_code);
            startActivityForResult(intent, REQUEST_CODE_MERCHANT_DETAILS);
        });

        if (merchant != null) {

            Gson gson = new Gson();
            Timber.d("Description:" + merchant.getDescription());
            Timber.d("Merchant:" + gson.toJson(merchant));

            textView_name.setText(merchant.getMerchant_name());
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadDataWithBaseURL("", merchant.getDescription(), "text/html", "UTF-8", "");
            webView.setBackgroundColor(Color.TRANSPARENT);

            if(!merchant.getImage_url().equals("")) {
                Glide.with(getContext())
                        .load(merchant.getImage_url())
                        .into(imageView_logo);
            }
            else
            {
                Glide.with(getContext())
                        .load(R.drawable.ic_card_ordinary)
                        .into(imageView_logo);
            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_merchant_details, menu);//Menu Resource, Menu
        MenuItem item = menu.findItem(R.id.action_location);
        if (merchant_code != null) {
            Timber.e(TAG, "merchant location:" + merchant.getLatitude().equals("") + " " + merchant.getLongitude().equals(""));
            if (!merchant.getLatitude().equals("") && !merchant.getLongitude().equals(""))
                item.setVisible(true);
            else
                item.setVisible(false);
        } else {
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:
                Intent returnIntent = new Intent();
                returnIntent.putExtra(KEY_MERCHANT_CODE, merchant_code);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
