package com.edmark.loyaltyuser.view.fragment.merchant;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.adapter.SpinnerAdapter;
import com.edmark.loyaltyuser.model.Categories;
import com.edmark.loyaltyuser.model.Country;
import com.edmark.loyaltyuser.model.MainMessageEvent;
import com.edmark.loyaltyuser.model.Merchant;
import com.edmark.loyaltyuser.model.MerchantMessageEvent;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.AppUtility;
import com.glide.slider.library.svg.GlideApp;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_ONLINE_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_3C_PRODUCTS;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_ENTERTAINMENT;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_FOOD_AND_BEVERAGES;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_HEALTH_CARE;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_LIFESTYLE;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_LIVING;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_SERVICES;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_TRAVELS;
import static com.edmark.loyaltyuser.constant.Common.CAT_3C_PRODUCTS;
import static com.edmark.loyaltyuser.constant.Common.CAT_ENTERTAINMENT;
import static com.edmark.loyaltyuser.constant.Common.CAT_FOOD_BEV;
import static com.edmark.loyaltyuser.constant.Common.CAT_HEALT_CARE;
import static com.edmark.loyaltyuser.constant.Common.CAT_LIFESTYLE;
import static com.edmark.loyaltyuser.constant.Common.CAT_LIVING;
import static com.edmark.loyaltyuser.constant.Common.CAT_SERVICES;
import static com.edmark.loyaltyuser.constant.Common.CAT_TRAVELS;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_MERCHANT_LIST;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_ONLINE_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.EVENT_VIEW_ONLINE_MERCHANT_LIST;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.TAG_MERCHANT_OFFLINE;
import static com.edmark.loyaltyuser.constant.Common.TAG_MERCHANT_ONLINE;
import static com.edmark.loyaltyuser.constant.Common.URL_HEADER;
import static com.edmark.loyaltyuser.util.AppUtility.getCategories;

/*
 * Created by DEV-Michael-ED2E on 04/25/2018.
 */

@SuppressWarnings("unchecked")
public class OnlineCategoriesFragment extends Fragment {
    public static final String TAG = OnlineCategoriesFragment.class.getSimpleName();


    @BindView(R.id.fragment_merchant_online_layout_coming_soon)
    LinearLayout layout_coming_soon;
    @BindView(R.id.fragment_merchant_online_layout_content)
    LinearLayout layout_content;
    @BindView(R.id.fragment_merchant_online_imageview)
    ImageView imageView_coming_soon;
    @BindView(R.id.fragment_merchant_online_spinner_country)
    Spinner spinner;
    private Unbinder unbinder;
    private BaseRecyclerViewAdapter adapter;
    /*For category**/
    private BaseRecyclerViewAdapter adapterActiveLifeStyle;
    private BaseRecyclerViewAdapter adapterHealthBeauty;
    private ArrayList<Merchant> datalist = new ArrayList<>();
    private ArrayList<Merchant> filtereddatalist = new ArrayList<>();
    private ArrayList<Merchant> datalistActiveLifeStyle = new ArrayList<>();
    private ArrayList<Merchant> datalistHealthBeauty = new ArrayList<>();
    private BaseRecyclerViewAdapter[] test;
    private ArrayList<Merchant>[] merchants;
    private ArrayList<Categories> categories = new ArrayList<>();
    private TextView[] textViewNotif;
    private View[] merchantViews;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_merchant_online_cat, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getUserVisibleHint()) {
            initGUI();
        }
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Timber.d("setUserVisibleHint():" + isVisibleToUser + " " + getView());
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI();
            }
        }
    }

    private void initGUI() {
        Timber.d("initGUI()");
        ArrayList<Country> countries = new ArrayList<>(AppUtility.getCompanyCountry(Objects.requireNonNull(getContext())));
        Collections.sort(countries, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(), R.layout.child_spinner_country, countries);
        spinner.setAdapter(spinnerAdapter);
        Gson gson = new Gson();
        /* For category**/
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
//        adapter = new BaseRecyclerViewAdapter(filtereddatalist, R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
//        {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MERCHANT, gson.toJson(filtereddatalist.get(position))));
//        });
//        adapterActiveLifeStyle = new BaseRecyclerViewAdapter(datalistActiveLifeStyle, R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
//        {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MERCHANT, gson.toJson(datalistActiveLifeStyle.get(position))));
//        });
//        adapterHealthBeauty = new BaseRecyclerViewAdapter(datalistHealthBeauty, R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
//        {
//            EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_MERCHANT, gson.toJson(datalistHealthBeauty.get(position))));
//        });
//        recyclerView.setLayoutManager(gridLayoutManager);
//        recyclerView.setAdapter(adapter);
        EventBus.getDefault().post(new MerchantMessageEvent(TAG_MERCHANT_ONLINE, null));

        AppPreference preference = AppPreference.getInstance(getContext());
        String country_code = preference.getString(KEY_COUNTRY_CODE);
        int position = 0;
        for (int i = 0; i < countries.size(); i++) {
            if (Objects.requireNonNull(countries.get(i).getCode()).equals(country_code)) {
                position = i;
                break;
            }
        }

        GlideApp.with(getContext())

                .load(R.drawable.ic_loading)
                .into(imageView_coming_soon);

        test = new BaseRecyclerViewAdapter[3000];
        merchants = new ArrayList[3000];
        textViewNotif = new TextView[3000];
        merchantViews = new View[3000];
        layout_content.removeAllViews();

        categories = getCategories(getContext());
        for (int i = 0; i < categories.size(); i++) {
            merchants[categories.get(i).getId()] = new ArrayList<>();
            merchantViews[categories.get(i).getId()] = getMerchantCategories(getContext(),
                    categories.get(i).getId(),
                    categories.get(i).getName(),
                    countries,
                    merchants[categories.get(i).getId()]);
            layout_content.addView(merchantViews[categories.get(i).getId()]);
        }
        spinner.setSelection(position);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                filterData(countries);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView()");
        unbinder.unbind();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MainMessageEvent event) {
        Timber.d("onMessageEvent:" + event.getData());
        if (event.getMessage().equals(API_ONLINE_MERCHANT)) {
            datalist.clear();
            Gson gson = new Gson();
            ArrayList<Merchant> merchants = gson.fromJson(event.getData(), ArrayList.class);
            for (int i = 0; i < merchants.size(); i++) {
                Timber.d("merchant list:" + gson.toJson(merchants.get(i)));
                Merchant merchant = gson.fromJson(gson.toJson(merchants.get(i)), Merchant.class);
//                if(merchant.getMerchant_name().contains("Food/Beverages"))
                datalist.add(merchant);
            }
//            Collections.sort(datalist, (o1, o2) -> o1.getMerchant_name().compareTo(o2.getMerchant_name()));
//            adapter.notifyDataSetChanged();
//            adapter.animateTo(datalist);

            ArrayList<Country> countries = new ArrayList<>(AppUtility.getCompanyCountry(Objects.requireNonNull(getContext())));
            Collections.sort(countries, (o1, o2) -> o1.getName().compareTo(o2.getName()));

            filterData(countries);
        }
    }

    private void filterData(ArrayList<Country> countries) {
        Gson gson = new Gson();
        filtereddatalist.clear();
        merchants[CATEGORY_FOOD_AND_BEVERAGES].clear();
        merchants[CATEGORY_TRAVELS].clear();
        merchants[CATEGORY_LIFESTYLE].clear();
        merchants[CATEGORY_ENTERTAINMENT].clear();
//        merchants[CATEGORY_3C_PRODUCTS].clear();
        merchants[CATEGORY_HEALTH_CARE].clear();
        merchants[CATEGORY_SERVICES].clear();
        merchants[CATEGORY_LIVING].clear();

        for (int i = 0; i < datalist.size(); i++) {
            if (datalist.get(i).getCompany_code().equals(countries.get(spinner.getSelectedItemPosition()).getCode())) {
                Merchant merchant = gson.fromJson(gson.toJson(datalist.get(i)), Merchant.class);
                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_FOOD_BEV)) {
                    merchants[CATEGORY_FOOD_AND_BEVERAGES].add(datalist.get(i));
                }
                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_TRAVELS)) {
                    merchants[CATEGORY_TRAVELS].add(datalist.get(i));
                }
                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_LIFESTYLE)) {
                    merchants[CATEGORY_LIFESTYLE].add(datalist.get(i));
                }
                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_ENTERTAINMENT)) {
                    merchants[CATEGORY_ENTERTAINMENT].add(datalist.get(i));
                }
//                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_3C_PRODUCTS)) {
//                    merchants[CATEGORY_3C_PRODUCTS].add(datalist.get(i));
//                }
                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_HEALT_CARE)) {
                    merchants[CATEGORY_HEALTH_CARE].add(datalist.get(i));
                }
                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_SERVICES)) {
                    merchants[CATEGORY_SERVICES].add(datalist.get(i));
                }
                if (merchant.getMerchant_category().equalsIgnoreCase(CAT_LIVING)) {
                    merchants[CATEGORY_LIVING].add(datalist.get(i));
                }
            }
        }

        /*refreshing the items to the adapter*/
        int shownItems = 0;
        for (int i = 0; i < categories.size(); i++) {

            Collections.sort(merchants[categories.get(i).getId()], (o1, o2) -> o1.getMerchant_name().compareTo(o2.getMerchant_name()));
            test[categories.get(i).getId()].notifyDataSetChanged();
            test[categories.get(i).getId()].animateTo(merchants[categories.get(i).getId()]);

            if (merchants[categories.get(i).getId()].size() == 0) {
                merchantViews[categories.get(i).getId()].setVisibility(View.GONE);
                textViewNotif[categories.get(i).getId()].setVisibility(View.VISIBLE);
            } else {
                shownItems++;
                merchantViews[categories.get(i).getId()].setVisibility(View.VISIBLE);
                textViewNotif[categories.get(i).getId()].setVisibility(View.GONE);
            }
        }

        if(shownItems==0)
        {
            layout_coming_soon.setVisibility(View.VISIBLE);
        }
        else
        {
            layout_coming_soon.setVisibility(View.GONE);
        }

//        if(!merchantViews[CATEGORY_FOOD_AND_BEVERAGES].isShown()
//                &&!merchantViews[CATEGORY_TRAVELS].isShown()
//                &&!merchantViews[CATEGORY_LIFESTYLE].isShown()
//                &&!merchantViews[CATEGORY_ENTERTAINMENT].isShown()
//                &&!merchantViews[CATEGORY_3C_PRODUCTS].isShown()
//                &&!merchantViews[CATEGORY_HEALTH_CARE].isShown()
//                &&!merchantViews[CATEGORY_SERVICES].isShown()
//                &&!merchantViews[CATEGORY_LIVING].isShown())
//        {
//            layout_coming_soon.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            layout_coming_soon.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @SuppressLint("InflateParams")
    private View getMerchantCategories(Context context, int id, String title, ArrayList<Country> countries, ArrayList<Merchant> merchants) {
        Gson gson = new Gson();
        View[] view = new View[3000];
        TextView[] textViewTitle = new TextView[3000];
        TextView[] textViewMore = new TextView[3000];
        RecyclerView[] recyclerView = new RecyclerView[3000];
        view[id] = LayoutInflater.from(context).inflate(R.layout.merchant_categories_layout, null);
        view[id].setId(id);
        textViewTitle[id] = view[id].findViewById(R.id.merchant_categories_layout_textview_title);
        textViewMore[id] = view[id].findViewById(R.id.merchant_categories_layout_textview_more);
        recyclerView[id] = view[id].findViewById(R.id.merchant_categories_layout_recyclerview);
        textViewNotif[id] = view[id].findViewById(R.id.merchant_categories_layout_textview_no_items);
        textViewTitle[id].setText(title);
        textViewMore[id].setOnClickListener(v -> EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT_LIST, title + ":" + countries.get(spinner.getSelectedItemPosition()).getCode())));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        test[id] = new BaseRecyclerViewAdapter(merchants, R.layout.child_merchant_grid, BR.Merchant, (v, item, position) ->
        {
            String url = merchants.get(position).getWebsiteurl();
            if(url.equals(""))
            {
                EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT,null));
            }
            else
            {
                if(!url.contains(URL_HEADER))
                    url = URL_HEADER + merchants.get(position).getWebsiteurl();

                EventBus.getDefault().post(new MerchantMessageEvent(EVENT_VIEW_ONLINE_MERCHANT,url));
            }
        });
        recyclerView[id].setLayoutManager(gridLayoutManager);
        recyclerView[id].setAdapter(test[id]);
        return view[id];
    }
}