package com.edmark.loyaltyuser.view.fragment.main;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.ViewPagerAdapter;
import com.edmark.loyaltyuser.view.fragment.merchant.OfflineCategoriesFragment;
import com.edmark.loyaltyuser.view.fragment.merchant.OnlineCategoriesFragment;
import com.edmark.loyaltyuser.view.fragment.merchant.OnlineFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/*
 * Created by DEV-Michael-ED2E on 04/25/2018.
 */

public class MerchantFragment extends Fragment {
    public static final String TAG = MerchantFragment.class.getSimpleName();

    @BindView(R.id.fragment_merchant_tabs)
    TabLayout tabLayout;
    @BindView(R.id.fragment_merchant_viewpager)
    ViewPager viewPager;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_merchant, container, false);
        unbinder = ButterKnife.bind(this, view);
        initGUI();
        return view;
    }

    private void initGUI() {
        Timber.d("initGUI");
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
//        adapter.addFrag(new OfflineFragment(), getResources().getString(R.string.title_offline));
        adapter.addFrag(new OfflineCategoriesFragment(), getResources().getString(R.string.title_offline));
//        adapter.addFrag(new OnlineFragment(), getResources().getString(R.string.title_online));
        adapter.addFrag(new OnlineCategoriesFragment(), getResources().getString(R.string.title_online));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView()");
        if (getFragmentManager() != null) {
//            getFragmentManager().beginTransaction().remove((OfflineFragment) Objects.requireNonNull(viewPager.getAdapter()).instantiateItem(viewPager, 0)).commitAllowingStateLoss();
            getFragmentManager().beginTransaction().remove((OfflineCategoriesFragment) Objects.requireNonNull(viewPager.getAdapter()).instantiateItem(viewPager, 0)).commitAllowingStateLoss();
//            getFragmentManager().beginTransaction().remove((OnlineFragment) Objects.requireNonNull(viewPager.getAdapter()).instantiateItem(viewPager, 1)).commitAllowingStateLoss();
            getFragmentManager().beginTransaction().remove((OnlineCategoriesFragment) Objects.requireNonNull(viewPager.getAdapter()).instantiateItem(viewPager, 1)).commitAllowingStateLoss();
        }
        unbinder.unbind();
    }
}