package com.edmark.loyaltyuser.view.activity.transaction.qrcode

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent

import com.edmark.loyaltyuser.R
import com.edmark.loyaltyuser.base.BaseActivity
import com.edmark.loyaltyuser.model.CustomerInfo
import com.edmark.loyaltyuser.presenter.transaction.qrcode.ShowQRContract
import com.edmark.loyaltyuser.presenter.transaction.qrcode.ShowQRPresenter
import com.edmark.loyaltyuser.util.AppPreference
import com.edmark.loyaltyuser.util.AppUtility
import com.google.zxing.WriterException

import com.edmark.loyaltyuser.constant.Common.KEY_CUSID
import kotlinx.android.synthetic.main.activity_show_qr.*
import timber.log.Timber

/*
 * Created by DEV-Michael-ED2E on 04/16/2018.
 */

class ShowQRActivity : BaseActivity(), ShowQRContract.View {

    private var presenter: ShowQRContract.Presenter? = null
    private var preference: AppPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_qr)

        presenter = ShowQRPresenter(this)
        preference = AppPreference.getInstance(this)

        setupToolBar(R.id.activity_show_qr_toolbar)
        initGUI()
    }

    override fun onToolbarBackPress() {
        super.onToolbarBackPress()
        finish()
    }

    private fun initGUI() {
        val custId = preference!!.getString(KEY_CUSID)
        presenter!!.loadAccount(custId)
    }

    private fun showQrCode(eda: String) {
        Timber.d("ShowQRode")
        try {
            val options = BitmapFactory.Options()
            options.inScaled = false
            val bitmap = AppUtility.encodeAsBitmap(eda, BitmapFactory.decodeResource(resources, R.drawable.ic_edpoints_logo_with_bg))
            if (bitmap != null) {
                activity_show_qr_imageview_qr_code.setImageBitmap(bitmap)
            }
        } catch (e: WriterException) {
            e.printStackTrace()
        }

    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                finish()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }

    override fun onLoadSuccess(info: CustomerInfo) {
        activity_show_qr_textview_eda.text = info.eda
        activity_show_qr_textview_name.text = info.name
        activity_show_qr_textview_user_type.text = info.email
        showQrCode(info.eda)
    }

    override fun onLoadFailed() {
        finish()
    }

    companion object {
        private val TAG = ShowQRActivity::class.java.simpleName
    }
}