package com.edmark.loyaltyuser.view.activity.login;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.presenter.login.ForgotPassContract;
import com.edmark.loyaltyuser.presenter.login.ForgotPassPresenter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edmark.loyaltyuser.constant.Common.API_FORGOT_PASSWORD;
import static com.edmark.loyaltyuser.constant.Common.API_FORGOT_PIN;
import static com.edmark.loyaltyuser.constant.Common.KEY_FORGOT_TYPE;

/*
 * Created by DEV-Michael-ED2E on 04/05/2018.
 */

@SuppressWarnings("unchecked")
public class ForgotPassActivity extends BaseActivity implements ForgotPassContract.View {
    public static final String TAG = ForgotPassActivity.class.getSimpleName();

    private ForgotPassContract.Presenter presenter;

    @BindView(R.id.activity_forgot_pass_toolbar_title) TextView textView_toolbar_title;
    @BindView(R.id.activity_forgot_pass_textview_result) TextView textView_result;
    @BindView(R.id.activity_forgot_pass_edittext_email) EditText editTextEmail;
    @BindView(R.id.activity_forgot_pass_button_submit) Button buttonSubmit;
    @BindView(R.id.activity_forgot_pass_button_resend) Button buttonResend;
    @BindView(R.id.activity_forgot_pass_button_return) Button buttonReturn;
    @BindView(R.id.activity_forgot_pass_layout_info) LinearLayout layoutInfo;
    @BindView(R.id.activity_forgot_pass_layout_result) LinearLayout layoutResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ButterKnife.bind(this);

        presenter = new ForgotPassPresenter(this);
        initActionBar(savedInstanceState);
        initGUI();
    }

    private void initGUI() {
        buttonSubmit.setOnClickListener(v -> {
            sendEmail();
        });
        buttonResend.setOnClickListener(v -> {
            sendEmail();
        });

        if(textView_toolbar_title.getText().toString().equals(getResources().getString(R.string.label_forgot_password)))
            buttonReturn.setVisibility(View.VISIBLE);
        else
            buttonReturn.setVisibility(View.GONE);

        buttonReturn.setOnClickListener(v -> finish());
    }

    private void initActionBar(Bundle savedInstanceState) {
        setupToolBar(R.id.activity_forgot_pass_toolbar);

        String forgot_type;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                forgot_type = null;
            } else {
                forgot_type = extras.getString(KEY_FORGOT_TYPE);
            }
        } else {
            forgot_type = (String) savedInstanceState.getSerializable(KEY_FORGOT_TYPE);
        }

        presenter.loadPage(forgot_type);
    }

    private void sendEmail()
    {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            Objects.requireNonNull(imm).hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String page;
        if(textView_toolbar_title.getText().toString().equals(getResources().getString(R.string.label_forgot_password)))
            page = API_FORGOT_PASSWORD;
        else
            page = API_FORGOT_PIN;

        String email = editTextEmail.getText().toString();
        presenter.loadAPI(page,email);
    }

    @Override
    public void onLoadPageSuccess(String title) {
        textView_toolbar_title.setText(title);
    }

    @Override
    public void onEmailSentSuccess(String message) {
        layoutInfo.setVisibility(View.GONE);
        layoutResult.setVisibility(View.VISIBLE);
        textView_result.setText(message);
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        if(layoutResult.isShown())
        {
            layoutInfo.setVisibility(View.VISIBLE);
            layoutResult.setVisibility(View.GONE);
        }
        else
        {
            finish();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if(layoutResult.isShown())
                {
                    layoutInfo.setVisibility(View.VISIBLE);
                    layoutResult.setVisibility(View.GONE);
                }
                else
                {
                    finish();
                }

                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
