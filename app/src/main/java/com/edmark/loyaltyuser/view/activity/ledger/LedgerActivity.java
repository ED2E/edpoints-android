package com.edmark.loyaltyuser.view.activity.ledger;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.ViewPagerAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.DatePicker;
import com.edmark.loyaltyuser.model.DateRange;
import com.edmark.loyaltyuser.model.LedgerMessageEvent;
import com.edmark.loyaltyuser.model.ReferralBonus;
import com.edmark.loyaltyuser.model.ReferralData;
import com.edmark.loyaltyuser.model.SetDateDisplay;
import com.edmark.loyaltyuser.model.SpendAndEarn;
import com.edmark.loyaltyuser.model.SpendAndEarnData;
import com.edmark.loyaltyuser.model.Tier;
import com.edmark.loyaltyuser.model.TierData;
import com.edmark.loyaltyuser.model.TierSummary;
import com.edmark.loyaltyuser.model.TierSummaryData;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.fragment.ledger.ReferralBonusFragment;
import com.edmark.loyaltyuser.view.fragment.ledger.SpendEarnFragment;
import com.edmark.loyaltyuser.view.fragment.ledger.TierOneFragment;
import com.edmark.loyaltyuser.view.fragment.ledger.TierSummaryFragment;
import com.edmark.loyaltyuser.view.fragment.ledger.TierThreeFragment;
import com.edmark.loyaltyuser.view.fragment.ledger.TierTwoFragment;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_REFERRAL_BONUS;
import static com.edmark.loyaltyuser.constant.Common.API_SPEND_EARN;
import static com.edmark.loyaltyuser.constant.Common.API_TIER_ONE;
import static com.edmark.loyaltyuser.constant.Common.API_TIER_SUMMARY;
import static com.edmark.loyaltyuser.constant.Common.API_TIER_THREE;
import static com.edmark.loyaltyuser.constant.Common.API_TIER_TWO;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.EVENT_INIT_API;
import static com.edmark.loyaltyuser.constant.Common.EVENT_INIT_API_REFERRAL;
import static com.edmark.loyaltyuser.constant.Common.EVENT_INIT_API_SPEND_EARN;
import static com.edmark.loyaltyuser.constant.Common.EVENT_OPEN_DATE_PICKER;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_DATA_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_DATE_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_NO_DATA_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_REFERRAL;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_SPEND_EARN;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_SUMMARY;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_TIER_ONE;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_TIER_THREE;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_TIER_TWO;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.LEVEL_1;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getTimeinMilli;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/13/2018.
 */

@SuppressWarnings("unchecked")
public class LedgerActivity extends BaseActivity {
    private static final String TAG = LedgerActivity.class.getSimpleName();

    @BindView(R.id.activity_ledger_tabs)
    TabLayout tabLayout;
    @BindView(R.id.activity_ledger_viewpager)
    ViewPager viewPager;

    private ApiInterface apiService;
    private AppPreference preference;
    private Gson gson;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger);
        ButterKnife.bind(this);
        Log.d(TAG,"onCreate");

        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        gson = new Gson();

        setupToolBar(R.id.activity_ledger_toolbar);
        initGUI();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGUI() {
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            if (tabLayout.getTabAt(i) != null) {
                Objects.requireNonNull(tabLayout.getTabAt(i)).setCustomView(getTabIndicator(getContext(), Objects.requireNonNull(Objects.requireNonNull(tabLayout.getTabAt(i)).getText()).toString()));
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ReferralBonusFragment(), getResources().getString(R.string.title_referral_bonus));
        adapter.addFrag(new TierSummaryFragment(), getResources().getString(R.string.title_tier_summary));
        adapter.addFrag(new TierOneFragment(), getResources().getString(R.string.title_tier_one));
        adapter.addFrag(new TierTwoFragment(), getResources().getString(R.string.title_tier_two));
        adapter.addFrag(new TierThreeFragment(), getResources().getString(R.string.title_tier_three));
        adapter.addFrag(new SpendEarnFragment(), getResources().getString(R.string.title_spend_earn));
        viewPager.setAdapter(adapter);
    }

    private View getTabIndicator(Context context, String title) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(R.layout.tab_layout_header, null, false);
        TextView tv = view.findViewById(R.id.tab_header_layout_textview_title);
        tv.setText(title);
        return view;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LedgerMessageEvent event) {
        Timber.d("onMessageEvent:" + event.getMessage() + " " + event.getData());
        switch (event.getMessage()) {
            case EVENT_OPEN_DATE_PICKER:
                DatePicker datePicker = gson.fromJson(event.getData(), DatePicker.class);
                initDatePicker(datePicker.getPosition(), datePicker.getFromDate(), datePicker.getCalendar(), datePicker.getCalendarMin());
                break;
            case EVENT_INIT_API:
                if (Integer.valueOf(event.getData()) == FRAGMENT_SUMMARY) {
                    initAPITierSummary();
                } else if (Integer.valueOf(event.getData()) == FRAGMENT_TIER_ONE) {
                    initTierOne();
                } else if (Integer.valueOf(event.getData()) == FRAGMENT_TIER_TWO) {
                    initTierTwo();
                } else if (Integer.valueOf(event.getData()) == FRAGMENT_TIER_THREE) {
                    initTierThree();
                }
                break;
            case EVENT_INIT_API_REFERRAL: {
                DateRange model = gson.fromJson(event.getData(), DateRange.class);
                initAPIReferral(model.getDateFrom(), model.getDateTo());
                break;
            }
            case EVENT_INIT_API_SPEND_EARN: {
                DateRange model = gson.fromJson(event.getData(), DateRange.class);
                initSpendAndEarn(model.getDateFrom(), model.getDateTo());
                break;
            }
        }
    }

    private void initAPIReferral(String dateFrom, String dateTo) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        String dateFromMilli = "";
        String dateToMilli = "";
        try {
            dateFromMilli = getTimeinMilli(dateFrom, DATE_FORMAT);
            dateToMilli = getTimeinMilli(dateTo, DATE_FORMAT);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Call<ResponseBody> call = apiService.statistics(API_REFERRAL_BONUS, custId, dateFromMilli, dateToMilli, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            ArrayList<ReferralBonus> data = new ArrayList<>();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Gson gson = new Gson();
                                ReferralBonus bonus = gson.fromJson(jsonobject.toString(), ReferralBonus.class);
                                Timber.d("ReferralBonus:" + gson.toJson(bonus));
                                data.add(bonus);
                            }
                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATA_DISPLAY, gson.toJson(new ReferralData(FRAGMENT_REFERRAL, data))));
                        } catch (JSONException e) {
                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_NO_DATA_DISPLAY, String.valueOf(FRAGMENT_REFERRAL)));
                        }
                    } else {
                        String errorCode = (String) obj.get(ERRORCODE);
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    hideProgress();
                    showToast(getResources().getString(R.string.error_connection));
                    return;
                }

                hideProgress();
                showToast(getResources().getString(R.string.error_connection));
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }


    private void initAPITierSummary() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(API_TIER_SUMMARY, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                        ArrayList<TierSummary> data = new ArrayList<>();
                        for (int i = 0; i < jsonarrayMain.length(); i++) {
                            JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                            Gson gson = new Gson();
                            TierSummary model = gson.fromJson(jsonobject.toString(), TierSummary.class);
                            Timber.d("TierSummary:" + gson.toJson(model));
                            if (model.getLevel().equals(LEVEL_1) && model.getBonus_amount() == null && model.getPercentage() == null && model.getSpending_amount() == null) {

                                EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_NO_DATA_DISPLAY, String.valueOf(FRAGMENT_SUMMARY)));

                                break;
                            } else {
                                data.add(model);
                            }
                        }

                        if (data.size() != 0) {
                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATA_DISPLAY, gson.toJson(new TierSummaryData(FRAGMENT_SUMMARY, data))));
                        }

                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void initTierOne() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(API_TIER_ONE, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            String message = (String) obj.get(MESSAGE);
                            Timber.d("message:" + message);
                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_NO_DATA_DISPLAY, String.valueOf(FRAGMENT_TIER_ONE)));
                        } catch (JSONException e) {

                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            ArrayList<Tier> data = new ArrayList<>();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Gson gson = new Gson();
                                Tier bonus = gson.fromJson(jsonobject.toString(), Tier.class);
                                Timber.d("Tier:" + gson.toJson(bonus));
                                data.add(bonus);
                            }

                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATA_DISPLAY, gson.toJson(new TierData(FRAGMENT_TIER_ONE, data))));
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void initTierTwo() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(API_TIER_TWO, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            String message = (String) obj.get(MESSAGE);
                            Timber.d("message:" + message);
                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_NO_DATA_DISPLAY, String.valueOf(FRAGMENT_TIER_TWO)));
                        } catch (JSONException e) {

                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            ArrayList<Tier> data = new ArrayList<>();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Gson gson = new Gson();
                                Tier bonus = gson.fromJson(jsonobject.toString(), Tier.class);
                                Timber.d("Tier:" + gson.toJson(bonus));
                                data.add(bonus);
                            }

                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATA_DISPLAY, gson.toJson(new TierData(FRAGMENT_TIER_TWO, data))));
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void initTierThree() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(API_TIER_THREE, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            String message = (String) obj.get(MESSAGE);
                            Timber.d("message:" + message);
                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_NO_DATA_DISPLAY, String.valueOf(FRAGMENT_TIER_THREE)));
                        } catch (JSONException e) {

                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            ArrayList<Tier> data = new ArrayList<>();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Gson gson = new Gson();
                                Tier bonus = gson.fromJson(jsonobject.toString(), Tier.class);
                                Timber.d("Tier:" + gson.toJson(bonus));
                                data.add(bonus);
                            }
                            EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATA_DISPLAY, gson.toJson(new TierData(FRAGMENT_TIER_THREE, data))));
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void initSpendAndEarn(String dateFrom, String dateTo) {
        Timber.d("initSpendAndEarn:" + dateFrom + " " + dateTo);
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        String dateFromMilli = "";
        String dateToMilli = "";
        try {
            dateFromMilli = getTimeinMilli(dateFrom, DATE_FORMAT);
            dateToMilli = getTimeinMilli(dateTo, DATE_FORMAT);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Call<ResponseBody> call = apiService.statistics(API_SPEND_EARN, custId, dateFromMilli, dateToMilli, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    try {
                        String message = (String) obj.get(MESSAGE);
                        Timber.d("message:" + message);
                        EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_NO_DATA_DISPLAY, String.valueOf(FRAGMENT_SPEND_EARN)));
                    } catch (JSONException e) {

                        JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                        ArrayList<SpendAndEarn> data = new ArrayList<>();
                        for (int i = 0; i < jsonarrayMain.length(); i++) {
                            JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                            Gson gson = new Gson();
                            SpendAndEarn bonus = gson.fromJson(jsonobject.toString(), SpendAndEarn.class);
                            Timber.d("Tier:" + gson.toJson(bonus));
                            data.add(bonus);
                        }

                        EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATA_DISPLAY, gson.toJson(new SpendAndEarnData(FRAGMENT_SPEND_EARN, data))));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    public void initDatePicker(int position, boolean fromDate, Calendar calendar, Calendar calendarMin) {
        if (calendar == null)
            calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {
            if (fromDate) {
                EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATE_DISPLAY, gson.toJson(new SetDateDisplay(position, true, year, month, dayOfMonth))));
            } else {
                EventBus.getDefault().post(new LedgerMessageEvent(EVENT_SET_DATE_DISPLAY, gson.toJson(new SetDateDisplay(position, false, year, month, dayOfMonth))));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
        datePickerDialog.setTitle(R.string.dialog_date_from);

        if (!fromDate) {
            Date newDate = calendarMin.getTime();
            datePickerDialog.getDatePicker().setMinDate(newDate.getTime() - (newDate.getTime() % (24 * 60 * 60 * 1000)));
            datePickerDialog.setTitle(R.string.dialog_date_to);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
