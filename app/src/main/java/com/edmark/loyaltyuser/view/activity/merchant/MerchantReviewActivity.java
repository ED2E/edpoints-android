package com.edmark.loyaltyuser.view.activity.merchant;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Reviews;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppUtility;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import hyogeun.github.com.colorratingbarlib.ColorRatingBar;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_CUSTCODE;
import static com.edmark.loyaltyuser.constant.Common.API_REVIEW_LISTING;
import static com.edmark.loyaltyuser.constant.Common.CUSTID;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_CODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKey;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/27/2018.
 */
@SuppressWarnings("unchecked")
public class MerchantReviewActivity extends BaseActivity {
    public static final String TAG = MerchantReviewActivity.class.getSimpleName();

    @BindView(R.id.activity_merchant_review_recyclerview)
    RecyclerView recyclerView_option;
    @BindView(R.id.activity_merchant_review_toolbar_title)
    TextView textView_tootbar_title;
    @BindView(R.id.activity_merchant_review_textview_message)
    TextView textView_message;
    @BindView(R.id.activity_merchant_review_textview_review_average)
    TextView textView_average_review;
    @BindView(R.id.activity_merchant_review_textview_total_user_review)
    TextView textView_total_user_review;
    @BindView(R.id.activity_merchant_review_ratingBar_average)
    MaterialRatingBar ratingBar_average_review;
    @BindView(R.id.activity_merchant_review_barchart_ratings)
    BarChart barChart_ratings;

    private ArrayList<Reviews> reviewList = new ArrayList<>();
    private BaseRecyclerViewAdapter adapter;
    private ApiInterface apiService;
    private String merchant_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_review);
        ButterKnife.bind(this);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        setupToolBar(R.id.activity_merchant_review_toolbar);
        initGui();
        getMerchantID(initData(savedInstanceState));
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private String initData(Bundle savedInstanceState) {
        String merchantid;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchantid = null;
            } else {
                merchantid = extras.getString(KEY_MERCHANT_CODE);
            }
        } else {
            merchantid = (String) savedInstanceState.getSerializable(KEY_MERCHANT_CODE);
        }
        return merchantid;
    }

    private void initGui() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new BaseRecyclerViewAdapter(reviewList, R.layout.child_reviews, BR.Reviews, (v, item, position) ->
        {

        });
        recyclerView_option.setLayoutManager(linearLayoutManager);
        recyclerView_option.setAdapter(adapter);
        barChart_ratings.setDrawBorders(false);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void getMerchantID(String merchantCode)
    {
        showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getCustCode(API_CUSTCODE, merchantCode, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("Raw Url:" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = response.body().string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);
                    if (status.equals(SUCCESS)) {
                        merchant_code = merchantCode;
                        initLoadReview((String) obj.get(CUSTID));
                        invalidateOptionsMenu();
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Timber.d("onFailure:");
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                t.printStackTrace();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                dialog.dismiss();
                hideProgress();
            }
        });
    }

    private void initLoadReview(String merchatid) {
        showProgress();
//        String custId = preference.getString(KEY_CUSID);
//        merchatid = "1360365";
        Call<ResponseBody> call = apiService.mainCustInfo(API_REVIEW_LISTING, merchatid, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = response.body().string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        reviewList.clear();

                        try {
                            String message = (String) obj.get(MESSAGE);
                            textView_message.setVisibility(View.VISIBLE);
                            textView_message.setText(message
                            );
                        } catch (JSONException e) {
                            float averageReview = 0;
                            float star1 = 0;
                            float star2 = 0;
                            float star3 = 0;
                            float star4 = 0;
                            float star5 = 0;
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Reviews review = gson.fromJson(jsonobject.toString(), Reviews.class);
                                Timber.d("merchant list:" + gson.toJson(review));
                                reviewList.add(review);
                                averageReview = averageReview + review.getReview_star();
                                if (review.getReview_star() == 1) {
                                    star1++;
                                } else if (review.getReview_star() == 2) {
                                    star2++;
                                } else if (review.getReview_star() == 3) {
                                    star3++;
                                } else if (review.getReview_star() == 4) {
                                    star4++;
                                } else if (review.getReview_star() == 5) {
                                    star5++;
                                }
                            }
                            averageReview = averageReview / jsonarrayMain.length();
                            textView_message.setVisibility(View.GONE);
                            Collections.sort(reviewList, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));
                            adapter.animateTo(reviewList);
                            textView_average_review.setText(String.format(Locale.getDefault(), "%.1f", averageReview));
                            textView_total_user_review.setText(decimalOutputFormat(String.valueOf(jsonarrayMain.length())));
                            Handler handler = new Handler();
                            float finalAverageReview =Float.valueOf(String.format(Locale.getDefault(), "%.1f", averageReview));
                            handler.postDelayed(() -> {
                                ratingBar_average_review.setRating(finalAverageReview);
                                Timber.d("finalAverageReview:"+finalAverageReview);
                            }, 100);

                            initChartData(star1, star2, star3, star4, star5);

                            Timber.d("hideProgress()");
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Timber.d("hideProgress()2");
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void initChartData(float star1, float star2, float star3, float star4, float star5) {

        BarDataSet set1;
        set1 = new BarDataSet(getDataSet(star1, star2, star3, star4, star5), null);

        set1.setColors(Color.parseColor("#F78B5D"), Color.parseColor("#FCB232"), Color.parseColor("#FDD930"), Color.parseColor("#ADD137"), Color.parseColor("#79c9a1"));
        set1.setValueFormatter(new AppUtility.ValueFormatter());
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueFormatter(new AppUtility.ValueFormatter());
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.BLACK);

        // hide Y-axis
        YAxis left = barChart_ratings.getAxisLeft();
        left.setDrawLabels(false);

//                            // custom X-axis labels
        String[] values = new String[]{"1 star", "2 stars", "3 stars", "4 stars", "5 stars"};
        XAxis xAxis = barChart_ratings.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setCenterAxisLabels(false);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new AppUtility.XAxisValueFormatter(values));
        xAxis.setTextColor(Color.BLACK);

        barChart_ratings.setData(data);

        barChart_ratings.setDescription(null);
//        barChart_ratings.setTouchEnabled(false);

        // hide legend
        barChart_ratings.getLegend().setEnabled(false);
        barChart_ratings.getAxisRight().setDrawGridLines(false);
        barChart_ratings.getAxisLeft().setDrawGridLines(false);
        barChart_ratings.getXAxis().setDrawGridLines(false);
        barChart_ratings.getAxisRight().setEnabled(false);
        barChart_ratings.getAxisLeft().setEnabled(false);

        barChart_ratings.getAxisLeft().setAxisMinimum(0);

        barChart_ratings.animateY(1000);
        barChart_ratings.invalidate();
    }

    private ArrayList<BarEntry> getDataSet(float rate1, float rate2, float rate3, float rate4, float rate5) {

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

//        BarEntry v1e = new BarEntry(0, 0);
//        valueSet1.add(v1e);
        BarEntry v1e2 = new BarEntry(0, (int) rate1);
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(1, (int) rate2);
        valueSet1.add(v1e3);
        BarEntry v1e4 = new BarEntry(2, (int) rate3);
        valueSet1.add(v1e4);
        BarEntry v1e5 = new BarEntry(3, (int) rate4);
        valueSet1.add(v1e5);
        BarEntry v1e6 = new BarEntry(4, (int) rate5);
        valueSet1.add(v1e6);

        return valueSet1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_merchant_details, menu);//Menu Resource, Menu
        MenuItem item = menu.findItem(R.id.action_location);
//        if (merchant_code != null) {
//            if (!merchant_code.equals(""))
//                item.setVisible(true);
//        } else {
            item.setVisible(false);
//        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:
                Intent returnIntent = new Intent();
                returnIntent.putExtra(KEY_MERCHANT_CODE,merchant_code);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
