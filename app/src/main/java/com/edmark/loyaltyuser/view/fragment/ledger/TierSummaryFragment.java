package com.edmark.loyaltyuser.view.fragment.ledger;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewSectionedAdapter;
import com.edmark.loyaltyuser.model.LedgerMessageEvent;
import com.edmark.loyaltyuser.model.TierSummaryData;
import com.edmark.loyaltyuser.model.TierSummary;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.EVENT_INIT_API;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_DATA_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.EVENT_SET_NO_DATA_DISPLAY;
import static com.edmark.loyaltyuser.constant.Common.FRAGMENT_SUMMARY;
import static com.edmark.loyaltyuser.util.AppUtility.getDateSection;

/*
 * Created by DEV-Michael-ED2E on 04/13/2018.
 */
@SuppressWarnings("unchecked")
public class TierSummaryFragment extends Fragment {
    private static final String TAG = TierSummaryFragment.class.getSimpleName();

    @BindView(R.id.fragment_tier_summary_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.fragment_tier_summary_textview_message)
    TextView textView_message;
    private BaseRecyclerViewAdapter adapter;
    private BaseRecyclerViewSectionedAdapter mSectionedAdapter;
    private Gson gson;

    public TierSummaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tier_summary, container, false);
        ButterKnife.bind(this, view);

        gson = new Gson();
        if(getUserVisibleHint())
        {
            initGUI();
        }
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Timber.d("setUserVisibleHint():"+isVisibleToUser+" "+getView());
        if(getView()!=null) {
            if (isVisibleToUser) {
                initGUI();
            }
        }
    }

    private void initGUI() {
        Timber.d("initGUI()");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);
        EventBus.getDefault().post(new LedgerMessageEvent(EVENT_INIT_API,String.valueOf(FRAGMENT_SUMMARY)));
    }


    private void setNoResult() {
        textView_message.setVisibility(View.VISIBLE);
        if(adapter!=null)
        {
            adapter.clear();
        }
        if(mSectionedAdapter!=null)
        {
            mSectionedAdapter.notifyDataSetChanged();
        }
    }

    private void setData(ArrayList<TierSummary> data) {
        textView_message.setVisibility(View.GONE);
        adapter = new BaseRecyclerViewAdapter(data, R.layout.child_tier_summary, BR.TierSummary, (v, item, position) ->
        {
        });
        //Add your adapter to the sectionAdapter
        mSectionedAdapter = new BaseRecyclerViewSectionedAdapter(Objects.requireNonNull(getActivity()), R.layout.child_header, R.id.child_header_text, adapter);
        //Apply this adapter to the RecyclerView
        recyclerView.setAdapter(mSectionedAdapter);
        //Get all dates for the header/section
        ArrayList<String> dates = new ArrayList<>();
        for (TierSummary items : data) {
            dates.add(items.getLevel());
        }

        Collections.sort(data, (o1, o2) -> o1.getLevel().compareTo(o2.getLevel()));
        adapter.animateTo(data);

        //Setting up the headers/sections
        List<BaseRecyclerViewSectionedAdapter.Section> sections = new ArrayList<>();

        for (String level : getDateSection(dates)) {
//            int occurrences = Collections.frequency(data, new ReferralBonus(date));
//            Timber.d("Date:" + date + " occurrences:" + occurrences);
            int position = 0;
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getLevel().equals(level)) {
                    position = i;
                    Timber.d("Date:" + level + " position:" + position);
                    break;
                }
            }
            sections.add(new BaseRecyclerViewSectionedAdapter.Section(position, getResources().getString(R.string.label_level)+" "+level));
        }

        mSectionedAdapter.setSections(sections.toArray(new BaseRecyclerViewSectionedAdapter.Section[sections.size()]));
        mSectionedAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LedgerMessageEvent event) {
        Timber.d(TAG,"onMessageEvent:"+event.getMessage()+" "+event.getData());
        switch (event.getMessage()) {
            case EVENT_SET_DATA_DISPLAY:
                TierSummaryData data = gson.fromJson(event.getData(), TierSummaryData.class);
                if(data.getPosition()==FRAGMENT_SUMMARY) {
                    ArrayList<TierSummary> datalist = data.getData();
                    setData(datalist);
                }
                break;
            case EVENT_SET_NO_DATA_DISPLAY:
                if(Integer.valueOf(event.getData())==FRAGMENT_SUMMARY)
                setNoResult();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.d("onStart:");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        Timber.d("onStop:");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}