package com.edmark.loyaltyuser.view.activity.signup;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.SpinnerAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Country;
import com.edmark.loyaltyuser.rest.ApiClientMerchantRegistration;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppUtility;
import com.edmark.loyaltyuser.util.ImageSelector;
import com.edmark.loyaltyuser.view.activity.login.LoginActivity;
import com.edmark.loyaltyuser.view.activity.profile.ProfileActivity;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_EXIT;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.util.AppUtility.getRealPathFromUri;

public class RegistrationMerchantActivity extends BaseActivity {

    public static final String TAG = RegistrationMerchantActivity.class.getSimpleName();

    @BindView(R.id.activity_register_merchant_editText_full_name)
    EditText editText_full_name;
    @BindView(R.id.activity_register_merchant_editText_address)
    EditText editText_address;

    @BindView(R.id.activity_register_merchant_editText_trader_name)
    EditText editText_trader_name;
    @BindView(R.id.activity_register_merchant_editText_business_address)
    EditText editText_business_address;
    @BindView(R.id.activity_register_merchant_editText_products_services_offered)
    EditText editText_products_services_offered;
    @BindView(R.id.activity_register_merchant_editText_tin)
    EditText editText_tin;

    @BindView(R.id.activity_register_merchant_editText_bank_name)
    EditText editText_bank_name;
    @BindView(R.id.activity_register_merchant_editText_branch)
    EditText editText_branch;
    @BindView(R.id.activity_register_merchant_editText_account_name)
    EditText editText_account_name;
    @BindView(R.id.activity_register_merchant_editText_account_number)
    EditText editText_account_number;

    @BindView(R.id.activity_register_merchant_editText_name_of_contact)
    EditText editText_name_of_contact;
    @BindView(R.id.activity_register_merchant_editText_contact_number)
    EditText editText_contact_number;
    @BindView(R.id.activity_register_merchant_editText_operating_hours)
    EditText editText_operating_hours;
    @BindView(R.id.activity_register_merchant_editText_website)
    EditText editText_website;
    @BindView(R.id.activity_register_merchant_editText_email_address_merchant)
    EditText editText_email_address_merchant;
    @BindView(R.id.activity_register_merchant_editText_brief_description)
    EditText editText_brief_description;
    @BindView(R.id.activity_register_merchant_editText_merchant_location)
    EditText editText_merchant_location;

    @BindView(R.id.activity_register_merchant_checkBox_accept_1)
    CheckBox checkBox_accept_1;
    @BindView(R.id.activity_register_merchant_checkBox_accept_2)
    CheckBox checkBox_accept_2;
    @BindView(R.id.activity_register_merchant_checkBox_accept_3)
    CheckBox checkBox_accept_3;

//    @BindView(R.id.activity_register_merchant_imageview_facade)
//    ImageView imageView_facade;
//    @BindView(R.id.activity_register_merchant_button_upload_picture)
//    Button button_upload_picture;
    @BindView(R.id.activity_register_merchant_button_sign_up)
    Button button_sign_up;

    @BindView(R.id.activity_register_merchant_spinner_country)
    Spinner spinner;

    private String country, individualname, individualaddress, tradername, businessaddress, productservice, tin, bankname, branch, accountname, accountnumber, techname, technumber, openinghours, website, techemail, description, location;

    private boolean checkBox1_isChecked = false;
    private boolean checkBox2_isChecked = false;
    private boolean checkBox3_isChecked = false;

    private ImageSelector imageSelector;

    private ApiInterface apiInterface;

    private static final String IMAGE_DIRECTORY = "/tmpSignUp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_merchant);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_registration_merchant_toolbar);
        initGui();
    }

    private void initGui() {

        //imageView_facade.setImageResource(R.drawable.ic_merchant);
        //imageView_facade.setBackgroundColor(Color.WHITE);

        ArrayList<Country> countries = new ArrayList<>(AppUtility.getMerchantCountry(Objects.requireNonNull(getContext())));
        Collections.sort(countries, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, R.layout.child_spinner_country, countries);
        spinner.setAdapter(spinnerAdapter);
        int position = 1;

        spinner.setSelection(position);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //showToast("Selected: " + String.valueOf(countries.get(position).getCode()));
                country = String.valueOf(countries.get(position).getCode());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        button_sign_up.setEnabled(false);

        checkBox_accept_1.setOnCheckedChangeListener((buttonView, isChecked) -> {
            checkBox1_isChecked = isChecked;
            enableButton();
        });

        checkBox_accept_2.setOnCheckedChangeListener((buttonView, isChecked) -> {
            checkBox2_isChecked = isChecked;
            enableButton();
        });

        checkBox_accept_3.setOnCheckedChangeListener((buttonView, isChecked) -> {
            checkBox3_isChecked = isChecked;
            enableButton();
        });

        button_sign_up.setOnClickListener(v ->
                {
                    initData();
                }
        );

//        button_upload_picture.setOnClickListener(v ->
//                {
//                    selectImage();
//                }
//        );

//        imageView_facade.setOnLongClickListener(arg0 -> {
//            Toast.makeText(getApplicationContext(), "Long Clicked " ,
//                    Toast.LENGTH_SHORT).show();
//
//            return true;
//        });

    }

    private void initData() {

        individualname = editText_full_name.getText().toString().trim();
        individualaddress = editText_address.getText().toString().trim();

        tradername = editText_trader_name.getText().toString().trim();
        businessaddress = editText_business_address.getText().toString().trim();
        productservice = editText_products_services_offered.getText().toString().trim();
        tin = editText_tin.getText().toString().trim();

        bankname = editText_bank_name.getText().toString().trim();
        branch = editText_branch.getText().toString().trim();
        accountname = editText_account_name.getText().toString().trim();
        accountnumber = editText_account_number.getText().toString().trim();

        techname = editText_name_of_contact.getText().toString().trim();
        technumber = editText_contact_number.getText().toString().trim();
        openinghours = editText_operating_hours.getText().toString().trim();
        website = editText_website.getText().toString().trim();
        techemail = editText_email_address_merchant.getText().toString().trim();
        description = editText_brief_description.getText().toString().trim();
        location = editText_merchant_location.getText().toString().trim();

        initAPI();
    }

    private void initAPI() {
        showProgress();

        apiInterface = ApiClientMerchantRegistration.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.registerMerchant(
                country,
                individualname,
                individualaddress,
                tradername,
                businessaddress,
                productservice,
                tin,
                bankname,
                branch,
                accountname,
                accountnumber,
                techname,
                technumber,
                openinghours,
                website,
                techemail,
                description,
                location
                );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Timber.d("Raw Url:" + response.raw().request().url());

                /*Log.i("SUCCESS", "Pass 3.1: " + response.raw().request().url());
                Log.i("SUCCESS", "Pass 3.2: " + response);
                Log.i("SUCCESS", "Pass 3.3: " + response.message());
                Log.i("SUCCESS", "Pass 3.4: " + response.body());
                //Log.i("SUCCESS", "Pass 3.5: " + response.body().string());
                Log.i("SUCCESS", "Pass 3.6: " + response.errorBody());*/

                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                try{
                    String json = /*"\"data\": " + */response.body().string();
                    Timber.d("onResponse:" + response.code() + " | " + json);
                    JSONObject obj = new JSONObject(json);

                    if (response.code() == 200 && response.message().contains("OK") && !obj.has("errors")) {
                        Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        if (dialog.getWindow() != null)
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.dialog_main);

                        TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);
                        Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
                        Button button_cancel = dialog.findViewById(R.id.dialog_main_button_cancel);

                        textView_message.setText(getResources().getString(R.string.dialog_merchant_registration_confirmation));
                        button_ok.setText(getResources().getString(R.string.button_ok));

                        button_cancel.setVisibility(View.GONE);

                        button_ok.setOnClickListener(v -> {
                            Intent intent = new Intent(RegistrationMerchantActivity.this, LoginActivity.class);
                            startActivity(intent);
                            hideProgress();
                            finish();
                        });
                        dialog.show();
                    } else{
                        JSONArray errors = obj.getJSONArray("errors");
                        String firstError = errors.getString(0);

                        //showToast(firstError);
                        validateTextFields(firstError);
                        hideProgress();
                    }

                    hideProgress();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
            }
        });
    }

    private void validateTextFields(String firstError) {

       switch (firstError){
           case "Full Name is required":
               editText_full_name.setError(firstError);
               showToast(firstError);
               break;
           case "Address is required":
               editText_address.setError(firstError);
               showToast(firstError);
               break;
           case "Business Address is required":
               editText_business_address.setError(firstError);
               showToast(firstError);
               break;
           case "Products/Services Offered is required":
               editText_products_services_offered.setError(firstError);
               showToast(firstError);
               break;
           case "Bank Name is required":
               editText_bank_name.setError(firstError);
               showToast(firstError);
               break;
           case "Branch is required":
               editText_branch.setError(firstError);
               showToast(firstError);
               break;
           case "Account Name is required":
               editText_account_name.setError(firstError);
               showToast(firstError);
               break;
           case "Account Number is required":
               editText_account_number.setError(firstError);
               showToast(firstError);
               break;
           case "Name of Merchant is required":
               editText_name_of_contact.setError(firstError);
               showToast(firstError);
               break;
           case "Contact Number of Merchant is required":
               editText_contact_number.setError(firstError);
               showToast(firstError);
               break;
           case "Email of Merchant is required":
               editText_email_address_merchant.setError(firstError);
               showToast(firstError);
               break;
           case "Contact Number of Merchant must be in numerical value":
               editText_name_of_contact.setError(firstError);
               showToast(firstError);
               break;
           case "Email format is invalid":
               editText_email_address_merchant.setError(firstError);
               showToast(firstError);
               break;
           case "Account Number must be in numerical value":
               editText_account_number.setError(firstError);
               showToast(firstError);
               break;
           default:
               showToast("An error has occurred. Please try again.");
               break;
       }
    }

    private void enableButton() {
        if(checkBox1_isChecked && checkBox2_isChecked && checkBox3_isChecked) {
            button_sign_up.setEnabled(true);
        } else {
            button_sign_up.setEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("TEST RUN", "PART 3");

        if (resultCode == RESULT_OK) {

            Log.i("TEST RUN", "PART 4");
            Uri tempUri;

            Timber.d("requestCode " + requestCode);
            if (requestCode == ImageSelector.CHOICE_AVATAR_FROM_CAMERA) {
                Log.i("TEST RUN", "PART 5");
                tempUri = Uri.parse(imageSelector.getmCurrentPhotoPath());
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                int widthInInches = metrics.widthPixels;
                int heightInInches = metrics.heightPixels;
                imageSelector.doCropRectangle(RegistrationMerchantActivity.this, tempUri, this, widthInInches, heightInInches);
            } else if (requestCode == ImageSelector.CHOICE_AVATAR_FROM_GALLERY) {
                Log.i("TEST RUN", "PART 6");
                tempUri = data.getData();
                Timber.d("getRealPathFromURI " + getRealPathFromUri(getContext(), data.getData()));
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int widthInInches = metrics.widthPixels;
                int heightInInches = metrics.heightPixels;
                imageSelector.doCropRectangle(RegistrationMerchantActivity.this, tempUri, this, widthInInches, heightInInches);
            } else if (requestCode == ImageSelector.CHOICE_AVATAR_REMOVE_PHOTO) {
                Log.i("TEST RUN", "PART 7");
                //imageView_facade.setBackgroundResource(R.drawable.ic_merchant);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                Log.i("TEST RUN", "PART 8");
                Log.i("TEST RUN", "requestCode: " + requestCode + " | " + "resultCode: " + resultCode  + " | " + "data: " + data);
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                uploadFileCIStest(getRealPathFromUri(getContext(), resultUri), resultUri);

                if (data != null) {
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                        String path = saveImage(bitmap);
                        Toast.makeText(RegistrationMerchantActivity.this, "Image Saved! - " + path, Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "Passed: " + path);
//                        imageView_facade.setImageDrawable(null);
//                        imageView_facade.setBackgroundColor(Color.WHITE);
//                        imageView_facade.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(RegistrationMerchantActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void uploadFileCIStest(String realPathFromUri, Uri resultUri) {
        Log.i("SUCCESS", "uploadFileCIStest: " + realPathFromUri + " | " + resultUri);
        //imageView_facade.setBackgroundResource(R.drawable.ic_merchant);
    }

    public void selectImage() {
        Log.i("TEST RUN", "PART 1");
        Timber.d("selectImage");
        int hasAccounts;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasAccounts = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasAccounts != PackageManager.PERMISSION_GRANTED) {
                int REQUEST_CODE_ASK_PERMISSIONS = 123;
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);

                    return;
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
            cameraPermission();
        } else {
            cameraPermission();
        }
    }

    private void cameraPermission() {
        Log.i("TEST RUN", "PART 2");
        Timber.d("cameraPermission");
        int hasAccounts;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasAccounts = checkSelfPermission(Manifest.permission.CAMERA);

            if (hasAccounts != PackageManager.PERMISSION_GRANTED) {
                int REQUEST_CAMERA_ASK_PERMISSIONS = 12345;
                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_ASK_PERMISSIONS);

                    return;
                }
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_ASK_PERMISSIONS);
                return;
            }
            imageSelector = new ImageSelector(getContext());
            //imageSelector.selectImage(imageView_facade);

        } else {
            imageSelector = new ImageSelector(getContext());
            //imageSelector.selectImage(imageView_facade);
        }
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }


}
