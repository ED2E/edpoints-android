package com.edmark.loyaltyuser.view.activity.edcoin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.presenter.edcoin.EDCoinContract;
import com.edmark.loyaltyuser.presenter.edcoin.EDCoinPresenter;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.transfer.TransferLogActivity;
import com.edmark.loyaltyuser.view.activity.transfer.TransferMainActivity;
import com.edmark.loyaltyuser.view.activity.transfer.TransferTipActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_CUSTINFO;
import static com.edmark.loyaltyuser.constant.Common.COUNTRY;
import static com.edmark.loyaltyuser.constant.Common.COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.EDA_NUMBER;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_AUTO_CONVERTED_EP;
import static com.edmark.loyaltyuser.constant.Common.KEY_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA_NO_VALUE;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDC_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.KEY_ENABLE_TIP_TRANSFER;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERID;
import static com.edmark.loyaltyuser.constant.Common.KEY_WALLET_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.REFERRAL_BONUS;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.WALLET_AVAILABLE_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.WALLET_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.WALLET_LOCK_BALANCE;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

public class EDCoinActivity extends BaseActivity implements EDCoinContract.View {

    public static final String TAG = EDCoinActivity.class.getSimpleName();

    private EDCoinContract.Presenter presenter;

    @BindView(R.id.activity_edcoin_layout_bind)
    LinearLayout layout_bind;
    @BindView(R.id.activity_edcoin_layout_wallet)
    LinearLayout layout_wallet;

    @BindView(R.id.activity_edcoin_editText_bind)
    EditText editText_bind;
    @BindView(R.id.activity_edcoin_button_ok)
    Button button_bind;

    @BindView(R.id.activity_edcoin_textVIew_info)
    TextView textView_info;
    @BindView(R.id.activity_edcoin_textview_edc_balance)
    TextView textView_edc_balance;
    @BindView(R.id.activity_edcoin_textview_lockup_balance)
    TextView textView_lockup_balance;
    @BindView(R.id.activity_edcoin_textview_referral_bonus)
    TextView textView_referral_bonus;
    @BindView(R.id.activity_edcoin_textview_available)
    TextView textview_available;

    @BindView(R.id.activity_edcoin_textView_auto_converted_ep_country_code)
    TextView textView_auto_converted_ep_country_code;
    @BindView(R.id.activity_edcoin_textview_auto_converted_ep)
    TextView textview_auto_converted_ep;

    @BindView(R.id.activity_edcoin_button_convert)
    Button button_convert;
    @BindView(R.id.activity_edcoin_button_transfer)
    Button button_transfer;
    @BindView(R.id.activity_edcoin_button_conversion_log)
    Button button_conversion_log;
    @BindView(R.id.activity_edcoin_button_transfer_log)
    Button button_transfer_log;
    @BindView(R.id.activity_edcoin_layout_conversion_history)
    LinearLayout layout_conversion_history;

    private ApiInterface apiInterface;

    private AppPreference preference;
    private String account_type;

    private String bind_key;
    private boolean isBind = false;
    private boolean isEDALinked = false;
    private String selectedServer = "";
    private String eda_number_value;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edcoin);

        ButterKnife.bind(this);

        presenter = new EDCoinPresenter(this, this);

        preference = AppPreference.getInstance(this);
        account_type = preference.getString(KEY_ACCOUNT_TYPE);

        setupToolBar(R.id.activity_edcoin_toolbar);
        //loadCompanyCode();
        initGUI();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void loadCompanyCode(){
        String countryCode = preference.getString(KEY_COUNTRY_CODE);

        String[] sg_server_array = getResources().getStringArray(R.array.sg_server_array);
        String[] ams_server_array = getResources().getStringArray(R.array.ams_server_array);

        for (String i:sg_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "SG";
            }
        }
        for (String i:ams_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "AMS";
            }
        }
    }

    private void initGUI() {

        //textView_info.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_question_mark, 0, 0, 0);

        bind_key = preference.getString(KEY_EDCOIN_BIND_KEY, "");
        //showToast(bind_key);

        if(bind_key.trim().isEmpty()){
            isBind = false;
            layout_bind.setVisibility(View.VISIBLE);
            layout_wallet.setVisibility(View.GONE);
        } else {
            isBind = true;

            /* To Bind w/ EDA checking */
            //initCheckWalletCodeAPI(selectedServer, bind_key);

            /* To Bind w/out EDA checking */
            //initAPI(bind_key);
            presenter.loadWallet(selectedServer, bind_key);

            if(isEDALinked) {
                layout_bind.setVisibility(View.GONE);
                layout_wallet.setVisibility(View.VISIBLE);
            }

            /*if(!isEDALinked) {
                layout_bind.setVisibility(View.VISIBLE);
                layout_wallet.setVisibility(View.GONE);
            }*/
        }

        button_bind.setOnClickListener(v -> {
            bind_key = editText_bind.getText().toString().trim();
//                    showToast("Value: " + bind);

            if(bind_key.isEmpty()){
                editText_bind.setError(getResources().getString(R.string.error_key_missing));
            } else{
                /* To Bind w/ EDA checking */
                //initCheckWalletCodeAPI(selectedServer, bind_key);

                /* To Bind w/out EDA checking */
                //initAPI(bind_key);
                presenter.loadWallet(selectedServer, bind_key);

                layout_bind.setVisibility(View.GONE);

                if(isEDALinked) {
                    layout_bind.setVisibility(View.GONE);
                    layout_wallet.setVisibility(View.VISIBLE);
                } else {
                    layout_bind.setVisibility(View.VISIBLE);
                    layout_wallet.setVisibility(View.GONE);
                }
            }
        });

        button_convert.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ConversionActivity.class);
            //intent.putExtra(KEY_EDC_BALANCE, textView_edc_balance.getText().toString());
            intent.putExtra(KEY_BIND_KEY, bind_key);
            startActivity(intent);

            finish();
        });

        button_transfer.setOnClickListener(v -> {
            Intent intent;
            if(preference.getString(KEY_ENABLE_TIP_TRANSFER, "true").equalsIgnoreCase("true")){
                intent = new Intent(getContext(), TransferTipActivity.class);
                intent.putExtra(KEY_AUTO_CONVERTED_EP, textview_auto_converted_ep.getText().toString().trim());
                startActivity(intent);
                finish();
            } else {
                intent = new Intent(getContext(), TransferMainActivity.class);
                intent.putExtra(KEY_AUTO_CONVERTED_EP, textview_auto_converted_ep.getText().toString().trim());
                startActivity(intent);
                finish();
            }

            /*intent = new Intent(getContext(), TransferTipActivity.class);
            intent.putExtra(KEY_AUTO_CONVERTED_EP, textview_auto_converted_ep.getText().toString().trim());
            startActivity(intent);
            finish();*/
        });

        /* for code w/out EDA Checking */
        //eda_number_value = preference.getString(KEY_EDA, "");
        //initEDANumber();

        button_conversion_log.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ConversionHistoryActivity.class);
            //intent.putExtra(KEY_EDA_NO_VALUE, eda_number_value);
            intent.putExtra(KEY_BIND_KEY, bind_key);
            startActivity(intent);

            finish();
        });

        button_transfer_log.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), TransferLogActivity.class);
            startActivity(intent);
            finish();
        });

        layout_conversion_history.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ConversionHistoryActivity.class);
            //intent.putExtra(KEY_EDA_NO_VALUE, eda_number_value);
            intent.putExtra(KEY_BIND_KEY, bind_key);
            startActivity(intent);

            finish();
        });


        initAPIGetCountryCode();

    }

    private void initAPIGetCountryCode() {
        Log.d(TAG, "initAPIGetCountryCode() triggered");

        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getCountryCode(
                preference.getString(KEY_EDCOIN_BIND_KEY),
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        Log.i(TAG,
                "Data: " +
                        preference.getString(KEY_EDCOIN_BIND_KEY) + " | " +
                        getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.d(TAG, "Raw Url: " + response.raw().request().url());

                //showToast("Successful Response");
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();

                    //showToast(responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {

                        String country_code = (String) obj.get(COUNTRY_CODE);
                        preference.putString(KEY_WALLET_COUNTRY_CODE, country_code);
                        textView_auto_converted_ep_country_code.setText(getResources().getString(R.string.label_auto_converted_ep) + " (" + country_code + ")");

                    } else if (status.equals(FAIL)) {

                        String message = (String) obj.get(MESSAGE);
                        showToast(message);
                        //editText_bind.setError(message);

                    } else {
                        showToast(getResources().getString(R.string.error_try_again));
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    showToast(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //showToast("Failed Response");

                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){

                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();

                    return;
                }

                hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));

            }
        });
    }

    /*private void initEDANumber() {
        String custId = preference.getString(KEY_CUSID);

        showToast("custID: " + custId);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.mainCustInfo(API_CUSTINFO, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String responseString = response.body().string();
                    JSONObject obj = new JSONObject(responseString);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        eda_number_value = obj.getString(EDA);
                        showToast("API Triggered: " + eda_number_value);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initEDANumber();
            }
        });


    }*/

    private void initCheckWalletCodeAPI(String selectedServer, String bind) {
        apiInterface = ApiClientEDCoin.getClient(selectedServer).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.checkWalletCode(
                bind,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                //showToast("Successful Response");
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    if(isBind) {
                        finish();
                    }
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {

                        try {
                            eda_number_value = (String) obj.get(EDA_NUMBER);
                        } catch (Exception e){
                            eda_number_value = "";
                        }
                        initCompareEDANumberAPI(eda_number_value);

                    } else if (status.equals(FAIL)) {

                        String message = (String) obj.get(MESSAGE);
                        editText_bind.setError(message);

                    } else {
                        showToast(getResources().getString(R.string.error_try_again));
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    showToast(e.getMessage());
                    if(isBind) {
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //showToast("Failed Response");

                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){

                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    if(isBind) {
                        finish();
                    }
                    return;
                }

                hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                if(isBind) {
                    finish();
                }
            }
        });


    }

    private void initCompareEDANumberAPI(String eda_number) {

        String custId = preference.getString(KEY_CUSID);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.mainCustInfo(
                API_CUSTINFO,
                custId,
                String.valueOf(unixTimeStamp()),
                getVCKeyED2E()
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                //showToast("Successful Response");
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    if(isBind) {
                        finish();
                    }
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();
                    //showToast(responseString);



                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);


                    if (status.equals(SUCCESS)) {

                        String eda = (String) obj.get(EDA);

                        if(eda.equalsIgnoreCase(eda_number)){
                            isEDALinked = true;
                            eda_number_value = eda;

                            //initAPI(bind_key);
                            presenter.loadWallet(selectedServer, bind_key);
                        } else {
                            isEDALinked = false;
                            editText_bind.setError(getResources().getString(R.string.error_eda_mismatch));
                            preference.putString(KEY_EDCOIN_BIND_KEY, "");

                            layout_bind.setVisibility(View.VISIBLE);
                            layout_wallet.setVisibility(View.GONE);
                        }

                    } else if (status.equals(FAIL)) {
                        String message = (String) obj.get(MESSAGE);
                        showToast(message);

                        if(isBind) {
                            finish();
                        }

                    } else {
                        showToast(getResources().getString(R.string.error_try_again));
                        if(isBind) {
                            finish();
                        }
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));


                    hideProgress();
                    showToast(e.getMessage());
                    if(isBind) {
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //showToast("Failed Response");

                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){

                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    if(isBind) {
                        finish();
                    }
                    return;
                }

                hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                if(isBind) {
                    finish();
                }
            }
        });
    }

    @Override
    public void onLoadWalletStatusSuccess(String edc_balance, String lockup_balance, String referral_bonus, String available, String total_converted_edpoints) {

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
        formatter.applyPattern("###,##0.##################");
        formatter.setGroupingSize(3);

        DecimalFormat formatter2 = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
        formatter2.applyPattern("###,##0.00");
        formatter2.setGroupingSize(3);

        textView_edc_balance.setText(edc_balance);
        textView_lockup_balance.setText(lockup_balance);
        textView_referral_bonus.setText(referral_bonus);
        textview_available.setText(/*formatter.format(Double.parseDouble(*/available/*.replace(",", "").trim()))*/);

        int endIndex = total_converted_edpoints.lastIndexOf(".") + 3;
        textview_auto_converted_ep.setText(total_converted_edpoints.substring(0, endIndex));

        layout_bind.setVisibility(View.GONE);
        layout_wallet.setVisibility(View.VISIBLE);


        initAPIGetCountryCode();
    }

    @Override
    public void onLoadWalletStatusFail(String message) {
        if(isBind == true) {
            isBind = false;
            preference.putString(KEY_EDCOIN_BIND_KEY, "");
        } else {
            showToast(message);
            editText_bind.setError(message);
        }

        //showToast(message);
        layout_bind.setVisibility(View.VISIBLE);
        layout_wallet.setVisibility(View.GONE);
    }

    @Override
    public void onLoadWalletFailed() {
        if(isBind) {
            finish();
        }
    }
}
