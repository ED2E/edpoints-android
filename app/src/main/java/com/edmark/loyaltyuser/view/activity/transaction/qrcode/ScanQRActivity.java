package com.edmark.loyaltyuser.view.activity.transaction.qrcode;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
//import com.edmark.loyaltyuser.ui.camera.BarcodeReader;
import com.edmark.loyaltyuser.ui.camera.ScannerOverlay;
import com.edmark.loyaltyuser.util.ImageSelector;
import com.edmark.loyaltyuser.util.barcode.BarcodeTrackerFactory;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionActivity;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionMassageChairActivity;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.androidhive.barcode.BarcodeReader;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_CUSTCODE;
import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_TRANSACTION_CSB;
import static com.edmark.loyaltyuser.constant.Common.CSB_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.CUSTID;
import static com.edmark.loyaltyuser.constant.Common.DOCNO;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.KEY_SCAN_PROCESS;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANS_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANS_DEVICEID;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.MERCHANTID;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TYPE;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


/*
 * Created by DEV-Michael-ED2E on 04/05/2018.
 */

public class ScanQRActivity extends BaseActivity implements BarcodeReader.BarcodeReaderListener {

    public static final String TAG = ScanQRActivity.class.getSimpleName();

    @BindView(R.id.activity_scan_scanoverlay)
    ScannerOverlay scannerOverlay;
    @BindView(R.id.activity_scan_toolbar_upload)
    ImageView imageView_upload;
    BarcodeReader barcodeReader;
    private boolean isScanning = false;
    private static final Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_scan_toolbar);
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.activity_scan_barcode_scanner);
        imageView_upload.setVisibility(View.GONE);
        imageView_upload.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), ImageSelector.CHOICE_AVATAR_FROM_GALLERY);
        });
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    @Override
    public void onScanned(Barcode barcode) {

        Timber.d("barcodeReader:" + barcodeReader.getBoundingBox() + " " + barcodeReader.getBoundingBox().left + " " + barcodeReader.getBoundingBox().bottom);
        Timber.d("barcode detected:" + barcode.getBoundingBox() + " " + barcode.getBoundingBox().left + " " + barcode.getBoundingBox().bottom);
        Timber.d("scannerOverlay:" + scannerOverlay.getBoundingBox() + " " + scannerOverlay.getBoundingBox().left + " " + scannerOverlay.getBoundingBox().bottom);

        if (barcode.getBoundingBox().left >= scannerOverlay.getBoundingBox().left
                && barcode.getBoundingBox().bottom <= scannerOverlay.getBoundingBox().bottom
                && barcode.getBoundingBox().top >= scannerOverlay.getBoundingBox().top
                && barcode.getBoundingBox().right <= scannerOverlay.getBoundingBox().right) {
            Timber.d("inside the box");
        }
        if (barcode.getBoundingBox().left >= scannerOverlay.getBoundingBox().left
                && barcode.getBoundingBox().right <= scannerOverlay.getBoundingBox().right) {
            Timber.d("inside the box left right");
        }
        if ( barcode.getBoundingBox().bottom <= scannerOverlay.getBoundingBox().bottom
                && barcode.getBoundingBox().top >= scannerOverlay.getBoundingBox().top
                ) {
            Timber.d("inside the box top bottom");
        }

//        if(barcode.getBoundingBox().bottom<=scannerOverlay.getBoundingBox().bottom
//                &&barcode.getBoundingBox().top>=scannerOverlay.getBoundingBox().top)
//        {

        if (!isScanning)
            initBarCode(barcode);
//        }
    }

    private void initBarCode(Barcode barcode) {
        isScanning = true;
        barcodeReader.setBeepSoundFile("qrcode.mp3");
        // playing barcode reader beep sound
        barcodeReader.playBeep(getContext());
        //barcodeReader.pauseScanning();
        barcodeReader.resumeScanning();

        Timber.d("Barcode", "inside the box");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        // if scanned is ED2E Number -> initialize Transaction
        if (barcode.displayValue.length() <= 13 && barcode.displayValue.toString().contains("ED2E")) {
            showProgress();
            Call<ResponseBody> call = apiService.getCustCode(API_CUSTCODE, barcode.displayValue, String.valueOf(unixTimeStamp()), getVCKeyED2E());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Timber.d(TAG, "Raw Url:" + response.raw().request().url());
                    isScanning = false;
                    if (response.body() == null) {
                        showToast(getResources().getString(R.string.error_connection));
                        hideProgress();
                        return;
                    }
                    try {
                        String json = Objects.requireNonNull(response.body()).string();
                        Timber.d(TAG, "onResponse:" + response.code() + " " + json);
                        JSONObject obj = new JSONObject(json);
                        String status = (String) obj.get(STATUS);
                        if (status.equals(SUCCESS)) {
                            String type = (String) obj.get(TYPE);
                            if (type.equals(MERCHANT)) {
                                String custid = (String) obj.get(CUSTID);
                                String transaction = "transaction";
                                Intent intent = new Intent(ScanQRActivity.this, TransactionActivity.class);
                                intent.putExtra(KEY_TRANS_CUSTID, custid);
                                //intent.putExtra(KEY_SCAN_PROCESS, transaction);
                                startActivity(intent);
                            } else {
                                Timber.d(TAG, "Distributor QR");
                                Toast.makeText(getContext(), getResources().getString(R.string.label_error_transaction), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            String message = (String) obj.get(MESSAGE);
                            String errorCode = (String) obj.get(ERRORCODE);
                            //Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();

                            barcodeReader.pauseScanning();
                            showDialog(0, message,true);
                        }
                        hideProgress();
                    } catch (IOException | JSONException e) {
                        isScanning = false;
                        e.printStackTrace();
                        hideProgress();
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    //barcodeReader.resumeScanning();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    isScanning = false;
                    if (t instanceof SocketTimeoutException) {
                        showToast(getResources().getString(R.string.error_connection));
                        hideProgress();
                        barcodeReader.resumeScanning();
                        return;
                    }

                    Timber.d(TAG, "onFailure:");
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgress();
                    barcodeReader.resumeScanning();
                }
            });

        // if scanned is Massage Chair code -> initialize Massage Chair
        } else if (barcode.displayValue.length() > 13 && barcode.displayValue.toString().contains("ED2E")){
//          showProgress();
            Timber.d(TAG, "Massage Chair Transaction:" + barcode.displayValue.substring(0, 13) + " " + barcode.displayValue.substring(13));
//          hideProgress();
//          barcodeReader.resumeScanning();



            showProgress();
            Call<ResponseBody> call = apiService.getCustCode(API_CUSTCODE, barcode.displayValue.substring(0, 13), String.valueOf(unixTimeStamp()), getVCKeyED2E());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    isScanning = false;
                    if (response.body() == null) {
                        showToast(getResources().getString(R.string.error_connection));
                        hideProgress();
                        return;
                    }

                    Timber.d(TAG, "Raw Url:" + response.raw().request().url());
                    try {
                        String json = Objects.requireNonNull(response.body()).string();
                        Timber.d(TAG, "onResponse:" + response.code() + " " + json);
                        JSONObject obj = new JSONObject(json);
                        String status = (String) obj.get(STATUS);
                        if (status.equals(SUCCESS)) {
                            String type = (String) obj.get(TYPE);
                            if (type.equals(MERCHANT)) {
                                String custid = (String) obj.get(CUSTID);
                                Intent intent = new Intent(ScanQRActivity.this, TransactionMassageChairActivity.class);
                                intent.putExtra(KEY_TRANS_CUSTID, custid);
                                intent.putExtra(KEY_TRANS_DEVICEID, barcode.displayValue);
                                startActivity(intent);
                            } else {
                                Timber.d(TAG, "Distributor QR");
                            }
                        } else {
                            String message = (String) obj.get(MESSAGE);
                            String errorCode = (String) obj.get(ERRORCODE);
                            //Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();

                            barcodeReader.pauseScanning();
                            showDialog(0, message,true);
                        }
                        hideProgress();
                    } catch (IOException | JSONException e) {
                        isScanning = false;
                        e.printStackTrace();
                        hideProgress();
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    //barcodeReader.resumeScanning();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    isScanning = false;
                    if (t instanceof SocketTimeoutException) {
                        showToast(getResources().getString(R.string.error_connection));
                        hideProgress();
                        barcodeReader.resumeScanning();
                        return;
                    }

                    Timber.d(TAG, "onFailure:");
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgress();
                    barcodeReader.resumeScanning();
                }
            });

        // if scanned is JSON -> initialize Cash Bill
        } else if(isJSONValid(barcode.displayValue.toString())) {

//                isScanning = false;
//                barcodeReader.resumeScanning();
//
//                Toast.makeText(getContext(), barcode.displayValue.toString() , Toast.LENGTH_SHORT).show();
//                Log.d("YEAH", barcode.displayValue.toString());

                showProgress();

                String docno = null, custid = null;
                JSONObject object = null;

                try {
                    object = new JSONObject(barcode.displayValue.toString());
                    docno = object.getString(DOCNO);
                    custid = object.getString(CUSTID);

                    Call<ResponseBody> call = apiService.cashBillTrans(API_VALIDATE_TRANSACTION_CSB, custid, docno, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Timber.d(TAG, "Raw Url:" + response.raw().request().url());
                            isScanning = false;
                            if (response.body() == null) {
                                showToast(getResources().getString(R.string.error_connection));
                                hideProgress();
                                return;
                            }
                            try {
                                String json = Objects.requireNonNull(response.body()).string();
                                Timber.d(TAG, "onResponse:" + response.code() + " " + json);
                                JSONObject obj = new JSONObject(json);
                                String status = (String) obj.get(STATUS);
                                if (status.equals(SUCCESS)) {
                                    //String custid = (String) obj.get(CUSTID);

                                    String merchantid = null, docno = null;
                                    JSONObject object = null;
                                    object = new JSONObject(barcode.displayValue.toString());
                                    merchantid = object.getString(MERCHANTID);
                                    docno = object.getString(DOCNO);
                                    String amount = (String) obj.get(CSB_AMOUNT);
                                    String cashbill = "cashbill";

                                    Intent intent = new Intent(getContext(), TransactionActivity.class);
                                    intent.putExtra(KEY_TRANS_CUSTID, merchantid);
                                    intent.putExtra(KEY_TRANSACTION_AMOUNT, amount);
                                    intent.putExtra(KEY_DESCRIPTION, docno);
                                    intent.putExtra(KEY_SCAN_PROCESS, cashbill);
                                    startActivity(intent);
                                } else {
                                    String message = (String) obj.get(MESSAGE);
                                    String errorCode = (String) obj.get(ERRORCODE);
                                    //Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();

                                    barcodeReader.pauseScanning();
                                    showDialog(0, message,true);
                                }
                                hideProgress();
                            } catch (IOException | JSONException e) {
                                isScanning = false;
                                e.printStackTrace();
                                hideProgress();
                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                            //barcodeReader.resumeScanning();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            isScanning = false;
                            if (t instanceof SocketTimeoutException) {
                                showToast(getResources().getString(R.string.error_connection));
                                hideProgress();
                                barcodeReader.resumeScanning();
                                return;
                            }

                            Timber.d(TAG, "onFailure:");
                            t.printStackTrace();
                            showToast(getResources().getString(R.string.error_connection));
                            //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                            hideProgress();
                            barcodeReader.resumeScanning();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();

                    //Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    //showToast(e.getMessage());

                    runOnUiThread(() -> {
                        hideProgress();
                        isScanning = false;
                        barcodeReader.pauseScanning();
                        String message = "Invalid Information.";
                        showDialog(0, message,true);
                    });
                }


        // if scanned is random String -> initialize dialog
        } else {
            runOnUiThread(() -> {
                hideProgress();
                isScanning = false;
                barcodeReader.pauseScanning();
                String message = "Invalid Information.";
                showDialog(0, message,true);
            });
        }
    }

    // JSON String Validator
    public static boolean isJSONValid(String jsonInString) {
        try {
            new JSONObject(jsonInString);
        } catch (JSONException ex) {
            try {
                new JSONArray(jsonInString);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onScannedMultiple(List<Barcode> list) {
        Timber.d("Barcode onScannedMultiple", list);
        if (list.size() != 0) {
            if (!isScanning)
                initBarCode(list.get(0));
        }
    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {
        Timber.d("Barcode onBitmapScanned", sparseArray);
        if (sparseArray.size() != 0) {
            if (!isScanning)
                initBarCode(sparseArray.get(0));
        }
    }

    @Override
    public void onScanError(String s) {
        Toast.makeText(getApplicationContext(), "Error occurred while scanning " + s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCameraPermissionDenied() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ImageSelector.CHOICE_AVATAR_FROM_GALLERY && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
//            uploadFile(getRealPathFromUri(getContext(),uri),uri);
            try {
//                InputStream inputStream = getContext().getContentResolver().openInputStream(uri);
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                Bitmap myQRCode = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(getContext())
                        .setBarcodeFormats(Barcode.ALL_FORMATS)
                        .build();

                Frame frame = new Frame.Builder().setBitmap(myQRCode).build();
                SparseArray<Barcode> barcodes = barcodeDetector.detect(frame);

                Timber.d("QR CODE Data: " + barcodes);
                // Check if at least one barcode was detected
                if (barcodes.size() != 0) {
                    Timber.d("QR CODE Data: " + barcodes.valueAt(0).displayValue);
                    initBarCode((Barcode) barcodes.valueAt(0));
                    // Display the QR code's message
//        textView.setText("QR CODE Data: " + barcodes.valueAt(0).displayValue);
//        //Display QR code image to ImageView
//        imageView.setImageBitmap(myQRCode);
                } else {
                    Timber.d("No QR Code found!");
                    Toast.makeText(getContext(), "No QR Code found!", Toast.LENGTH_SHORT).show();
//        textView.setText("No QR Code found!");
//        textView.setTextColor(Color.RED);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDialogDismiss(int requestCode) {
        Timber.d("onDialogDismiss() initiated");
        //isScanning = false;
        super.onDialogDismiss(requestCode);
        barcodeReader.resumeScanning();
    }
}