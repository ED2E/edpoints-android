package com.edmark.loyaltyuser.view.activity.login;

import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import com.edmark.loyaltyuser.BuildConfig;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.presenter.login.LoginContract;
import com.edmark.loyaltyuser.presenter.login.LoginPresenter;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.main.WebviewActivity;
import com.edmark.loyaltyuser.view.activity.signup.RegistrationActivity;
import com.edmark.loyaltyuser.view.activity.main.MainActivity;
import com.edmark.loyaltyuser.view.activity.signup.RegistrationMerchantActivity;
import com.google.firebase.FirebaseApp;
import com.multidots.fingerprintauth.AuthErrorCodes;
import com.multidots.fingerprintauth.FingerPrintAuthCallback;
import com.multidots.fingerprintauth.FingerPrintAuthHelper;

import static com.edmark.loyaltyuser.constant.Common.DISTRIBUTOR;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUST_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_FORGOT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_PASSWORD;
import static com.edmark.loyaltyuser.constant.Common.KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_EXIT;
import static com.edmark.loyaltyuser.constant.Common.URL_MERCHANT_FORM;

/*
 * Created by DEV-Michael-ED2E on 04/02/2018.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View, FingerPrintAuthCallback{
    public static final String TAG = LoginActivity.class.getSimpleName();

    private LoginContract.Presenter loginPresenter;

    @BindView(R.id.activity_login_textView_forgot_password) TextView textView_forgot_password;
    @BindView(R.id.activity_login_radioButton_distributor) RadioButton radioButton_distributor;
    @BindView(R.id.activity_login_radioButton_merchant) RadioButton radioButton_merchant;
    @BindView(R.id.activity_login_editText_email) EditText editText_email;
    @BindView(R.id.activity_login_editText_password) EditText editText_password;
    @BindView(R.id.activity_login_button_login) Button button_login;
    @BindView(R.id.activity_login_button_distributor_sign_up) Button button_sign_up;
    @BindView(R.id.activity_login_button_merchant_sign_up) Button button_sign_up_merchant;
    @BindView(R.id.activity_login_checkBox_remember) CheckBox checkBox_remember;
    @BindView(R.id.activity_login_textView_version) TextView textView_version;
    @BindView(R.id.activity_login_imageView_eye) ImageView imageView_eye;

//    private FingerPrintAuthHelper mFingerPrintAuthHelper;

    private AppPreference preference;
    private String acc_type;

    private Boolean isPasswordHidden = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);
//        mFingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(this, this);

        preference = AppPreference.getInstance(this);

        // Initialize the Presenter
        loginPresenter = new LoginPresenter(this);
        initGUI();
    }

    private void initGUI() {

        acc_type = preference.getString(KEY_CUST_TYPE, "");

        if(acc_type.trim().isEmpty()){
            radioButton_distributor.setChecked(true);
            radioButton_merchant.setChecked(false);
        } else if (acc_type.trim().equalsIgnoreCase("distributor")){
            radioButton_distributor.setChecked(true);
            radioButton_merchant.setChecked(false);
        } else {
            radioButton_distributor.setChecked(false);
            radioButton_merchant.setChecked(true);
        }

        textView_version.setText("" + BuildConfig.VERSION_NAME);

        setTextViewStyle();

        imageView_eye.setOnClickListener(v -> {
            if(isPasswordHidden){
                isPasswordHidden = false;
                imageView_eye.setImageDrawable(getResources().getDrawable(R.drawable.ic_eye_show));
                editText_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_NORMAL);
                editText_password.setSelection(editText_password.getText().length());
            } else {
                isPasswordHidden = true;
                imageView_eye.setImageDrawable(getResources().getDrawable(R.drawable.ic_eye_hide));
                editText_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText_password.setSelection(editText_password.getText().length());
            }

            setTextViewStyle();
        });

        button_login.setOnClickListener(v -> {
            String email = editText_email.getText().toString();
            String password = editText_password.getText().toString();
            boolean autoLogin = checkBox_remember.isChecked();
            String type = "";
            if(radioButton_distributor.isChecked()) {
                type = DISTRIBUTOR;
                preference.putString(KEY_CUST_TYPE, "distributor");
            }
            if(radioButton_merchant.isChecked()) {
                type = MERCHANT;
                preference.putString(KEY_CUST_TYPE, "merchant");
            }

            loginPresenter.login(type, email, password, autoLogin);

        });

        button_sign_up.setOnClickListener(v ->
        {
            Intent intent = new Intent(getContext(), RegistrationActivity.class);
            startActivity(intent);
        });

        button_sign_up_merchant.setOnClickListener(v ->
        {
            Intent intent = new Intent(getContext(), RegistrationMerchantActivity.class);
//            Intent in = new Intent(getContext(), WebviewActivity.class);
//            in.putExtra(KEY_URL, URL_MERCHANT_FORM);
//            in.putExtra(KEY_TITLE, getResources().getString(R.string.title_merchant_sign_up));
            startActivity(intent);
        });

        textView_forgot_password.setOnClickListener(v ->
        {
            Intent intent = new Intent(getContext(), ForgotPassActivity.class);
            intent.putExtra(KEY_FORGOT_TYPE, KEY_PASSWORD);
            startActivity(intent);
        });
    }

    private void setTextViewStyle() {
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Calibri.ttf");
        editText_email.setTypeface(face);
        editText_email.setTextSize(18);
        editText_password.setTypeface(face);
        editText_password.setTextSize(18);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                showDialog(REQUEST_CODE_EXIT, getResources().getString(R.string.dialog_exit), false);

                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    protected void onDialogDismiss(int requestCode) {
        super.onDialogDismiss(requestCode);

        if (requestCode == REQUEST_CODE_EXIT) {
            finish();
        }
    }

    @Override
    public void onLoadEmailSuccess(String username) {
        editText_email.setText(username);
    }

    @Override
    public void onLoginSuccess(String userType) {

        if (userType.equals(DISTRIBUTOR)) {
            Timber.d("this is distributor show card picker:");
            Intent intent = new Intent(getContext(), AccountSelectionActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(getContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onNoFingerPrintHardwareFound() {
        //Device does not have finger print scanner.
    }

    @Override
    public void onNoFingerPrintRegistered() {
        //There are no finger prints registered on this device.
    }

    @Override
    public void onBelowMarshmallow() {
        //Device running below API 23 version of android that does not support finger print authentication.
    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        //Authentication sucessful.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d(TAG,"cryptoObject:"+cryptoObject.getSignature()+" "+cryptoObject.getCipher()+" "+cryptoObject.getMac());
        }
        String email = "zaframichaelangelo@gmail.com";
        String password = "4227877zax";
        boolean autoLogin = checkBox_remember.isChecked();
        String type = "";
        if(radioButton_distributor.isChecked())
            type = DISTRIBUTOR;

        if(radioButton_merchant.isChecked())
            type = MERCHANT;

        loginPresenter.login(type, email, password, autoLogin);
    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        switch (errorCode) {    //Parse the error code for recoverable/non recoverable error.
            case AuthErrorCodes.CANNOT_RECOGNIZE_ERROR:
                //Cannot recognize the fingerprint scanned.
                break;
            case AuthErrorCodes.NON_RECOVERABLE_ERROR:
                //This is not recoverable error. Try other options for user authentication. like pin, password.
                break;
            case AuthErrorCodes.RECOVERABLE_ERROR:
                //Any recoverable error. Display message to the user.
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //start finger print authentication
//        mFingerPrintAuthHelper.startAuth();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mFingerPrintAuthHelper.stopAuth();
    }
}
