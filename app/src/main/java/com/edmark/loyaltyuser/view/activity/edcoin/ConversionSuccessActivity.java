package com.edmark.loyaltyuser.view.activity.edcoin;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CustomerInfo;
import com.edmark.loyaltyuser.model.FirebaseToken;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_CUSTINFO;
import static com.edmark.loyaltyuser.constant.Common.CREATED_DATE;
import static com.edmark.loyaltyuser.constant.Common.CUSTID;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT_TIME_12_HRS;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.EMAIL;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_TOKEN;
import static com.edmark.loyaltyuser.constant.Common.KEY_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_COIN_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_RECEIVED_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RECORD;
import static com.edmark.loyaltyuser.constant.Common.REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_DASHBOARD;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.service.FirebasePushNotif.pushNotificationDistributor;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


public class ConversionSuccessActivity extends BaseActivity {

    public static final String TAG = ConversionSuccessActivity.class.getSimpleName();

    @BindView(R.id.activity_conversion_success_textview_edc_value)
    TextView textview_edc_value;
    @BindView(R.id.activity_conversion_success_textview_edp_value)
    TextView textview_edp_value;
    @BindView(R.id.activity_conversion_success_textview_ref_no_value)
    TextView textview_ref_no_value;
    @BindView(R.id.activity_conversion_success_textview_date_value)
    TextView textview_date_value;
    @BindView(R.id.activity_conversion_success_textview_time_value)
    TextView textview_time_value;
    @BindView(R.id.activity_conversion_success_button_view_wallet)
    Button button_view_wallet;

    private String ref_no = "";
    private String coin_amount = "";
    private String received_amount = "";

    private ApiInterface apiService;
    private AppPreference preference;

    private String dateStr = "";
    private String timeStr = "";
    private String bind_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_success);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        //setupToolBar(R.id.activity_conversion_success_toolbar);
        initData();
        //initCompanyCode();
        initGUI();

    }

    /*@Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }*/

    private void initData() {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            ref_no = "0";
            coin_amount = "0";
            received_amount = "0";
            //eda_no = "0";
            bind_key = "";
        } else {
            ref_no = extras.getString(KEY_REFERENCE_NO);
            coin_amount = extras.getString(KEY_COIN_AMOUNT);
            received_amount = extras.getString(KEY_RECEIVED_AMOUNT);
            //eda_no = extras.getString(KEY_EDA_NO_VALUE);
            bind_key = extras.getString(KEY_BIND_KEY);
        }
    }

    /*private void initCompanyCode(){
        countryCode = preference.getString(KEY_COUNTRY_CODE);

        String[] sg_server_array = getResources().getStringArray(R.array.sg_server_array);
        String[] ams_server_array = getResources().getStringArray(R.array.ams_server_array);

        for (String i:sg_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "SG";
            }
        }
        for (String i:ams_server_array) {
            if (i.equalsIgnoreCase(countryCode)) {
                selectedServer = "AMS";
            }
        }
    }*/

    private void initGUI() {

        //DecimalFormat formatter = new DecimalFormat("###,##0.00");
        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,##0.00");
        formatter.setGroupingSize(3);

        DecimalFormat formatter2 = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter2.applyPattern("###,##0.000000000000000000");

        //double edc_value_formatted = Double.valueOf(coin_amount.replace(",", "").trim());
        //double edp_value_formatted = Double.valueOf(received_amount.replace(",", "").trim());

        String edc_value_formatted_toString = String.valueOf(formatter2.format(Double.valueOf(coin_amount.replace(",", "").trim())));
        String edp_value_formatted_toString = String.valueOf(formatter.format(Double.valueOf(received_amount.replace(",", "").trim())));

        textview_edc_value.setText(getResources().getString(R.string.label_edc_value, edc_value_formatted_toString));
        textview_edp_value.setText(getResources().getString(R.string.label_edp_value, edp_value_formatted_toString));
        textview_ref_no_value.setText(getResources().getString(R.string.label_ref_no_value, ref_no));

        textview_date_value.setText("");
        textview_time_value.setText("");

        button_view_wallet.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), EDCoinActivity.class);
            startActivity(intent);

            finish();
        });

        initDateTimeAPI();
        initPushNotif();
    }

    private void initPushNotif() {
        String custId = preference.getString(KEY_CUSID);
        showProgress();
        Call<ResponseBody> call = apiService.mainCustInfo(API_CUSTINFO, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerInfo info = gson.fromJson(json, CustomerInfo.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());

                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        //Load distributor token
                        ArrayList<String> distributorToken = new ArrayList<>();
                        db.collection(FIREBASE_KEY_TOKEN)
                                .whereEqualTo(CUSTID, custId)
                                .whereEqualTo(EDA, info.getEda())
                                .whereEqualTo(EMAIL, info.getEmail())
                                .get()
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        for (DocumentSnapshot document : task.getResult()) {
                                            Timber.d(document.getId() + " => " + document.getData());

                                        }
                                        //direct document
                                        for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                                            if (doc.get(CUSTID) != null) {
                                                Timber.d("FirebaseToken:" + doc.getString(CUSTID) + " => " + doc.getString(EDA));
                                            }
                                        }

                                        if (task.getResult() != null) {
                                            List<FirebaseToken> tokens = task.getResult().toObjects(FirebaseToken.class);
                                            for (FirebaseToken item : tokens) {
                                                distributorToken.add(item.getToken());
                                            }
                                        }

                                        Timber.d("notificationToken:" + distributorToken);
                                        String msg = getResources().getString(R.string.conversion_successful, ref_no, coin_amount);
                                        new Thread(() -> pushNotificationDistributor(custId, distributorToken, msg, "", "", "", TAG_NOTIF_DASHBOARD)).start();

                                    } else {
                                        Timber.w(TAG, "Error getting documents.", task.getException());
                                    }
                                });
//                        initMerchant();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initPushNotif();
                hideProgress();
            }
        });
    }

    private void initDateTimeAPI() {
        showProgress();

        String selectedServer = "AMS";

        ApiInterface apiInterface = ApiClientEDCoin.getClient(selectedServer).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getTransactionLogs(
                //eda_no,
                bind_key,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                //showToast("Successful Response");
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                try {
                    hideProgress();
                    String responseString = response.body().string();
                    //showToast(responseString);
                    //Log.i("SUCCESS", "Pass 3.5: " + responseString);
                    //textview_family_tree.setText(responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);


                    switch (status) {
                        case SUCCESS:
                        /*String rate = (String) obj.get(RATE);
                        double rateToDouble = Double.valueOf(rate.replace(",", "").trim());
                        textview_current_conversion_rate_value.setText(getResources().getString(R.string.label_current_conversion_rate_value,String.valueOf(rateToDouble)));*/


                            JSONArray record_array = obj.getJSONArray(RECORD);

                            for (int i = (record_array.length() - 1); i >= 0; i--) {
                                JSONObject record_object = record_array.getJSONObject(i);

                                if (ref_no.equalsIgnoreCase(record_object.getString(REFERENCE_NO))) {

                                    try {
                                        dateStr = record_object.getString(CREATED_DATE);

                                        TimeZone tz = TimeZone.getDefault();
                                        DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
                                        Timber.d("TimeZone: " + tz.getDisplayName(false, TimeZone.SHORT) + " | Timezone ID: " + tz.getID());
                                        srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

                                        //srcDf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                        // parse the date string into Date object
                                        Date date = srcDf.parse(dateStr);

                                        DateFormat destDf = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
                                        DateFormat destTf = new SimpleDateFormat(DATE_FORMAT_TIME_12_HRS, Locale.getDefault());

                                        // format the date into another format
                                        dateStr = destDf.format(date);
                                        timeStr = destTf.format(date);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    break;
                                }
                            /*EDCoin list = new EDCoin(
                                    record_object.getString(REFERENCE_NO),
                                    record_object.getString(EDC_AMOUNT),
                                    record_object.getString(EDP_AMOUNT),
                                    record_object.getString(DESCRIPTION),
                                    record_object.getString(CREATED_DATE)

                            );*/
                                //Log.i("Pass", "TRY - Model Placed: " + modelList.toString());
                            }

                            textview_date_value.setText(dateStr);
                            textview_time_value.setText(timeStr.toUpperCase().replace(".", ""));


                            break;
                        case FAIL:
                            String message = (String) obj.get(MESSAGE);
                            showToast(message);
                            break;
                        default:
                            showToast(getResources().getString(R.string.error_try_again));
                            break;
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    hideProgress();
                    showToast(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //showToast("Failed Response");

                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Intent intent = new Intent(getContext(), EDCoinActivity.class);
                startActivity(intent);

                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
