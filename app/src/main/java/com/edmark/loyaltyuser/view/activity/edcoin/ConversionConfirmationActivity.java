package com.edmark.loyaltyuser.view.activity.edcoin;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.COIN_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.EDCOIN_AVAILABLE_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_COIN_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDC_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDC_VALUE;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_VALUE;
import static com.edmark.loyaltyuser.constant.Common.KEY_RATE;
import static com.edmark.loyaltyuser.constant.Common.KEY_RECEIVED_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RECEIVED_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.playBeep;

public class ConversionConfirmationActivity extends BaseActivity {

    public static final String TAG = ConversionConfirmationActivity.class.getSimpleName();

    @BindView(R.id.activity_conversion_confirmation_textview_edc_value)
    TextView textview_edc_value;
    @BindView(R.id.activity_conversion_confirmation_textview_edp_value)
    TextView textview_edp_value;
    @BindView(R.id.activity_conversion_confirmation_textview_timer_value)
    TextView textview_timer_value;
    @BindView(R.id.activity_conversion_confirmation_button_confirm)
    Button button_confirm;
    @BindView(R.id.activity_conversion_confirmation_button_cancel)
    Button button_cancel;

    private String edc_value = "";
    private String edp_value = "";

    private String cust_id;
    private String rate;
    private String bind_key;
    private String available;

    CountDownTimer cTimer = null;

    private ApiInterface apiInterface;
    private AppPreference preference;

    private boolean isTransacting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_confirmation);
        ButterKnife.bind(this);
        preference = AppPreference.getInstance(this);

        //setupToolBar(R.id.activity_conversion_confirmation_toolbar);
        initData(savedInstanceState);
        initGUI();
    }

    /*@Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        cancelTimer();
        finish();
    }*/

    private void initData(Bundle savedInstanceState) {

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            edc_value = "";
            edp_value = "";
            cust_id = "";
            rate = "";
            bind_key = "";
            available = "";
        } else {
            edc_value = extras.getString(KEY_EDC_VALUE);
            edp_value = extras.getString(KEY_EDP_VALUE);
            cust_id = extras.getString(KEY_CUSTID);
            rate = extras.getString(KEY_RATE);
            bind_key = extras.getString(KEY_BIND_KEY);
            available = extras.getString(KEY_EDC_BALANCE);
        }

        Timber.d("edc_value" + edc_value + " | edp_value: " + edp_value + " | cust_id: " + cust_id + " | rate: " + rate + " | bind_key: " + bind_key + " | available: " + available + " | access: " + getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`"));
    }

    private void initGUI() {

        //DecimalFormat formatter = new DecimalFormat("###,##0.00");
        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,##0.00");
        formatter.setGroupingSize(3);

        DecimalFormat formatter2 = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter2.applyPattern("###,##0.000000000000000000");

        //double edc_value_formatted = Double.valueOf(edc_value.replace(",", "").trim());
        //double edp_value_formatted = Double.valueOf(edp_value.replace(",", "").trim());

        String edc_value_formatted_toString = String.valueOf(formatter2.format(Double.valueOf(edc_value.replace(",", "").trim())));
        String edp_value_formatted_toString = String.valueOf(formatter.format(Double.valueOf(edp_value.replace(",", "").trim())));

        textview_edc_value.setText(getResources().getString(R.string.label_edc_value, edc_value_formatted_toString));
        textview_edp_value.setText(getResources().getString(R.string.label_edp_value, edp_value_formatted_toString));

        textview_timer_value.setText(getResources().getString(R.string.label_conversion_confirmation_timer_value, "30"));

        startCountDownTimer();

        button_confirm.setOnClickListener(v -> {
            if(!isTransacting) {
                isTransacting = true;
                initConvertEDCoinAPI();
            }
        });

        button_cancel.setOnClickListener(v -> {
            cancelTimer();
            loadConversionActivity();
        });

    }

    private void initConvertEDCoinAPI() {
        if (dialog != null) {
            dialog.dismiss();
        }
        showProgress();

        Double amount_input = Double.valueOf(textview_edc_value.getText().toString().replace(",", "").trim());
        String countryCode = preference.getString(KEY_COUNTRY_CODE);

        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.convertEDCoin(
                cust_id,
                rate,
                edc_value,
                countryCode,
                bind_key,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransacting = false;
                    showToast(getResources().getString(R.string.error_connection));
                    //showToast("response body is null");
                    hideProgress();
                    cancelTimer();
                    loadConversionActivity();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();

                    //showToast("onResponse:" + response.code() + " " + json);

                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        isTransacting = true;
                        cancelTimer();

                        String ref_no = (String) obj.get(REFERENCE_NO);
                        String coin_amount = (String) obj.get(COIN_AMOUNT);
                        String received_amount = (String) obj.get(RECEIVED_AMOUNT);

                        Timber.d("coin_amount: " + (String) obj.get(COIN_AMOUNT) + " | received_amount: " + (String) obj.get(RECEIVED_AMOUNT));

                        hideProgress();
                        //showToast("JSON: " + json);

                        playBeep(getContext(),"transaction.mp3");
                        Intent intent = new Intent(getContext(), ConversionSuccessActivity.class);

                        intent.putExtra(KEY_REFERENCE_NO, ref_no);
                        intent.putExtra(KEY_COIN_AMOUNT, coin_amount);
                        intent.putExtra(KEY_RECEIVED_AMOUNT, received_amount);
                        //intent.putExtra(KEY_EDA_NO_VALUE, eda_number);
                        intent.putExtra(KEY_BIND_KEY, bind_key);

                        startActivity(intent);

                        finish();

                    } else if (status.equals(FAIL)) {
                        isTransacting = false;
                        hideProgress();
                        cancelTimer();
                        String message = (String) obj.get(MESSAGE);

                        if(message.equalsIgnoreCase("You are trying to convert an amount that exceeds your available EDC Balance: ")){
                            String edcoin_available_balance = (String) obj.get(EDCOIN_AVAILABLE_BALANCE);
                            initWarningDialog(edcoin_available_balance);
                        } else {
                            showToast(message);
                            cancelTimer();
                            loadConversionActivity();
                        }

                    } else {
                        isTransacting = false;
                        hideProgress();
                        showToast(getResources().getString(R.string.error_connection));
                        cancelTimer();
                        loadConversionActivity();
                    }
                } catch (IOException | JSONException e) {
                    isTransacting = false;
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    cancelTimer();
                    loadConversionActivity();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                isTransacting = false;

                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    //showToast(t.getMessage());
                    hideProgress();
                    cancelTimer();
                    loadConversionActivity();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                //showToast(t.getMessage());
                hideProgress();
                cancelTimer();
                loadConversionActivity();
            }
        });
    }

    private void initWarningDialog(String edcoin_available_balance) {

        cancelTimer();

        Dialog dialog2 = new Dialog(this, R.style.DialogTheme);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog2.getWindow() != null)
            dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.setContentView(R.layout.dialog_main_2);

        TextView textView_message = dialog2.findViewById(R.id.dialog_main_2_textview_message);
        TextView textView_message_2 = dialog2.findViewById(R.id.dialog_main_2_textview_message_2);
        Button button_ok = dialog2.findViewById(R.id.dialog_main_2_button_ok);
        Button button_cancel = dialog2.findViewById(R.id.dialog_main_2_button_cancel);

        Typeface custom_font_NORMAL = Typeface.createFromAsset(getAssets(), "fonts/Calibri.ttf");
        Typeface custom_font_BOLD = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");


        textView_message.setTypeface(custom_font_NORMAL);
        textView_message_2.setTypeface(custom_font_BOLD);

        textView_message.setText(getResources().getString(R.string.dialog_conversion_warning));
        textView_message_2.setText("" + edcoin_available_balance);

        button_ok.setText(getResources().getString(R.string.button_change_edc_amount));

        button_cancel.setVisibility(View.GONE);

        button_ok.setOnClickListener(v -> {
            dialog2.dismiss();
            loadConversionActivity();
        });

        dialog2.setOnDismissListener(dialog -> {
            loadConversionActivity();
        });

        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
    }

    void startCountDownTimer() {
        cTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                Timber.d("Time: " + millisUntilFinished/1000);
                textview_timer_value.setText(getResources().getString(R.string.label_conversion_confirmation_timer_value, String.valueOf(millisUntilFinished/1000)));
            }
            public void onFinish() {
                cancelTimer();
                textview_timer_value.setText(getResources().getString(R.string.label_conversion_confirmation_timer_value, "0"));
                initTimeOutDialog();
            }
        };
        cTimer.start();
    }

    void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

    private void initTimeOutDialog(){
        cancelTimer();

        Dialog dialog = new Dialog(this, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_main_2);

        TextView textView_message = dialog.findViewById(R.id.dialog_main_2_textview_message);
        TextView textView_message_2 = dialog.findViewById(R.id.dialog_main_2_textview_message_2);
        Button button_ok = dialog.findViewById(R.id.dialog_main_2_button_ok);
        Button button_cancel = dialog.findViewById(R.id.dialog_main_2_button_cancel);

        textView_message.setText(getResources().getString(R.string.dialog_timeout_warning));
        textView_message_2.setText(getResources().getString(R.string.dialog_timeout_warning_message));
        button_ok.setText(getResources().getString(R.string.button_go_back_to_conversion).toUpperCase());

        button_cancel.setVisibility(View.GONE);

        button_ok.setOnClickListener(v -> {
            dialog.dismiss();
            loadConversionActivity();
        });

        dialog.setOnDismissListener(dialog1 -> {
            loadConversionActivity();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void loadConversionActivity(){
        if (dialog != null) {
            dialog.dismiss();
        }

        cancelTimer();

        Intent intent = new Intent(getContext(), ConversionActivity.class);
        intent.putExtra(KEY_BIND_KEY, bind_key);
        startActivity(intent);

        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                cancelTimer();
                loadConversionActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
