package com.edmark.loyaltyuser.view.activity.profile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.InputFilter;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CardType;
import com.edmark.loyaltyuser.model.CustomerProfile;
import com.edmark.loyaltyuser.model.CustomerProfileUpdate;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImageContract;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImagePresenter;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.ImageSelector;
import com.edmark.loyaltyuser.view.activity.login.ForgotPassActivity;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_ACCOUNT_SELECT;
import static com.edmark.loyaltyuser.constant.Common.API_CHANGE_PASS;
import static com.edmark.loyaltyuser.constant.Common.API_CHANGE_PIN;
import static com.edmark.loyaltyuser.constant.Common.API_CUST_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.API_CUST_PROFILE_UPDATE;
import static com.edmark.loyaltyuser.constant.Common.API_MERCHANT_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.API_MERCHANT_PROFILE_UPDATE;
import static com.edmark.loyaltyuser.constant.Common.CUSTID;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE_UPLOAD;
import static com.edmark.loyaltyuser.constant.Common.ERROR_MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.FONT_CALIBRI;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_FORGOT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_PASSWORD;
import static com.edmark.loyaltyuser.constant.Common.KEY_PIN;
import static com.edmark.loyaltyuser.constant.Common.MAIN_ACCOUNT;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.NAME;
import static com.edmark.loyaltyuser.constant.Common.PACKAGE_TYPE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.STATUS_CODE_INACTIVE;
import static com.edmark.loyaltyuser.constant.Common.SUB_ACCOUNT;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getRealPathFromUri;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.saveBitmapToFile;
import static com.edmark.loyaltyuser.util.AppUtility.setHintFont;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/16/2018.
 */

public class ProfileActivity extends BaseActivity implements LoadImageContract{
    private static final String TAG = ProfileActivity.class.getSimpleName();

    @BindView(R.id.activity_profile_imageview_profile)
    ImageView imageView_profile;
    @BindView(R.id.activity_profile_textview_eda)
    TextView textView_eda;
    @BindView(R.id.activity_profile_textview_name)
    TextView textView_name;
    @BindView(R.id.activity_profile_textview_email)
    TextView textView_email;
    @BindView(R.id.activity_profile_textview_contact)
    TextView textView_contact;
    @BindView(R.id.activity_profile_textview_dob)
    TextView textView_dob;
    @BindView(R.id.activity_profile_textview_change_pass)
    TextView textView_change_pass;
    @BindView(R.id.activity_profile_textview_change_pin)
    TextView textView_change_pin;
    @BindView(R.id.activity_profile_textview_forgot_pin)
    TextView textView_forgot_pin;
    @BindView(R.id.activity_profile_button_edit_profile)
    LinearLayout button_edit_profile;
    @BindView(R.id.activity_profile_layout_profile)
    LinearLayout layout_profile;
    @BindView(R.id.activity_profile_layout_edit_profile)
    LinearLayout layout_edit_profile;
    @BindView(R.id.activity_profile_edittext_edit_name)
    EditText editText_name;
    @BindView(R.id.activity_profile_edittext_edit_email)
    EditText editText_email;
    @BindView(R.id.activity_profile_edittext_edit_phone)
    EditText editText_phone;
    @BindView(R.id.activity_profile_edittext_edit_dob)
    TextView editText_dob;
    @BindView(R.id.activity_profile_edittext_edit_website)
    EditText editText_website;
    @BindView(R.id.activity_profile_textview_edit_website_label)
    TextView textview_website_label;
    @BindView(R.id.activity_profile_textview_website)
    TextView textview_website;
    @BindView(R.id.activity_profile_divider_website)
    View divider_website;
    @BindView(R.id.activity_profile_divider_edit_website)
    View divider_edit_website;
    @BindView(R.id.activity_profile_button_save)
    Button button_save;
    @BindView(R.id.activity_profile_layout_website)
    LinearLayout layout_website;
    @BindView(R.id.activity_profile_progress)
    ProgressBar progressbar_profile;
    @BindView(R.id.activity_profile_webview)
    WebView webView;

    private ApiInterface apiService;
    private AppPreference preference;
    private ImageSelector imageSelector;
    private String account_type;
    private ArrayList<CardType> datalist = new ArrayList<>();
    private int index = 1;
    private int requestImage_profile = 0;
    private LoadImagePresenter imageLoader;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        imageLoader = new LoadImagePresenter(this);
        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        account_type = preference.getString(KEY_ACCOUNT_TYPE);
        setupToolBar(R.id.activity_profile_toolbar);
        initGUI();

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        if (layout_edit_profile.isShown()) {
            layout_profile.setVisibility(View.VISIBLE);
            layout_edit_profile.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

    private void initGUI() {
        Log.d(TAG,"initGUI");
        textView_change_pass.setOnClickListener(v -> initInputDialog(KEY_PASSWORD));
        textView_change_pin.setOnClickListener(v -> initInputDialog(KEY_PIN));
        button_edit_profile.setOnClickListener(v ->
        {
            layout_edit_profile.setVisibility(View.VISIBLE);
            layout_profile.setVisibility(View.GONE);
        });
        editText_dob.setOnClickListener(v ->
        {
            //TODO must change the initial value when the original date of birth is visible
            Calendar calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    (view, year, month, dayOfMonth) ->
                            editText_dob.setText(new StringBuilder().append(year).append("-").append(month + 1).append("-").append(dayOfMonth)), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });
        initLoadAccountInfo(account_type);

        textView_forgot_pin.setOnClickListener(v ->
        {
            Intent intent = new Intent(ProfileActivity.this, ForgotPassActivity.class);
            intent.putExtra(KEY_FORGOT_TYPE, KEY_PIN);
            startActivity(intent);
        });

        button_save.setOnClickListener(v -> initSaveProfile(account_type));

        imageView_profile.setOnClickListener(v -> selectImage());

        //TODO uncomment if website is already available
//        if(!account_type.equals(MERCHANT))
//        {
//            layout_website.setVisibility(View.GONE);
//            divider_edit_website.setVisibility(View.GONE);
//            divider_website.setVisibility(View.GONE);
//            textview_website_label.setVisibility(View.GONE);
//            editText_website.setVisibility(View.GONE);
//        }
//        else
//        {
//            layout_website.setVisibility(View.VISIBLE);
//            divider_edit_website.setVisibility(View.VISIBLE);
//            divider_website.setVisibility(View.VISIBLE);
//            textview_website_label.setVisibility(View.VISIBLE);
//            editText_website.setVisibility(View.VISIBLE);
//        }
    }

    private void initSaveProfile(String account_type) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call;
        if (account_type.equals(MERCHANT))
            call = apiService.merchantProfileUpdate(API_MERCHANT_PROFILE_UPDATE,
                    custId,
                    editText_name.getText().toString(),
                    editText_website.getText().toString(),
                    textView_email.getText().toString(),
                    editText_phone.getText().toString(),
                    editText_dob.getText().toString(),
                    getVCKeyED2E(),
                    String.valueOf(unixTimeStamp()));
        else
            call = apiService.edaProfileUpdate(API_CUST_PROFILE_UPDATE,
                    custId,
                    editText_name.getText().toString(),
                    textView_email.getText().toString(),
                    editText_phone.getText().toString(),
                    editText_dob.getText().toString(),
                    getVCKeyED2E(),
                    String.valueOf(unixTimeStamp()));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfileUpdate info = gson.fromJson(json, CustomerProfileUpdate.class);
                        Timber.d("CustomerInfo:" + info.getF_name());
                        textView_name.setText(info.getF_name());
                        textView_contact.setText(info.getPhoneno());
                        textView_email.setText(info.getEmail());
                        textview_website.setText(info.getWebsite_url());
                        textView_dob.setText(info.getDOB());

                        editText_name.setText(info.getF_name());
                        editText_email.setText(info.getEmail());
                        editText_phone.setText(info.getPhoneno());
                        editText_website.setText(info.getWebsite_url());
                        editText_dob.setText(info.getDOB());

                        showToast((String) obj.get(MESSAGE));
                        if (layout_edit_profile.isShown()) {
                            layout_profile.setVisibility(View.VISIBLE);
                            layout_edit_profile.setVisibility(View.GONE);
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo(account_type);
            }
        });
    }

    private void initLoadAccountInfo(String account_type) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        String page;
        if (account_type.equals(MERCHANT))
            page = API_MERCHANT_PROFILE;
        else
            page = API_CUST_PROFILE;

        Call<ResponseBody> call = apiService.mainCustInfo(page, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfile info = gson.fromJson(json, CustomerProfile.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());
                        textView_eda.setText(getResources().getString(R.string.label_edit_eda, info.getEda()));
                        textView_name.setText(info.getName());
                        textView_contact.setText(info.getTelno());
                        textView_email.setText(info.getEmail());
                        textView_email.setText(info.getEmail());
                        textview_website.setText(info.getWebsiteurl());
                        textView_dob.setText(info.getDOB());

                        editText_name.setText(info.getName());
                        editText_email.setText(info.getEmail());
                        editText_website.setText(info.getWebsiteurl());
                        editText_phone.setText(info.getTelno());
                        editText_website.setText(info.getWebsiteurl());
                        editText_dob.setText(info.getDOB());

                        progressbar_profile.setVisibility(View.VISIBLE);
                        String imageurl = info.getImage_url();
                        if(getContext()!=null) {
                            imageLoader.loadImage(getContext(),TAG,requestImage_profile,imageurl,imageView_profile,R.drawable.ic_default_user, true);
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    hideProgress();
                    showToast(getResources().getString(R.string.error_connection));

                    // dismiss profile activity
                    if (layout_edit_profile.isShown()) {
                        layout_profile.setVisibility(View.VISIBLE);
                        layout_edit_profile.setVisibility(View.GONE);
                    } else {
                        finish();
                    }

                    return;
                }

                hideProgress();
                showToast(getResources().getString(R.string.error_connection));
                Timber.d("onFailure:" + t.getMessage());
                t.printStackTrace();

                // dismiss profile activity
                if (layout_edit_profile.isShown()) {
                    layout_profile.setVisibility(View.VISIBLE);
                    layout_edit_profile.setVisibility(View.GONE);
                } else {
                    finish();
                }
                //initLoadAccountInfo(account_type);
            }
        });
    }


    public void initInputDialog(String type) {
        Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input_3);

        Button button_ok = dialog.findViewById(R.id.dialog_input_3_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_3_button_cancel);
        EditText editText_old_pass_pin = dialog.findViewById(R.id.dialog_input_3_edittext);
        TextView textView_old_pass_pin_title = dialog.findViewById(R.id.dialog_input_3_textview_message);
        EditText editText_new_pass_pin = dialog.findViewById(R.id.dialog_input_3_edittext_2);
        TextView textView_new_pass_pin_title = dialog.findViewById(R.id.dialog_input_3_textview_message_2);
        EditText editText_confirm_pass_pin = dialog.findViewById(R.id.dialog_input_3_edittext_3);
        TextView textView_confirm_pass_pin_title = dialog.findViewById(R.id.dialog_input_3_textview_message_3);

        String title;

        if (type.equals(KEY_PASSWORD)) {
            editText_old_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_enter_password),FONT_CALIBRI));
            editText_new_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_enter_password),FONT_CALIBRI));
            editText_confirm_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_confirm_password),FONT_CALIBRI));
            title = getResources().getString(R.string.dialog_pass);
            editText_old_pass_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editText_new_pass_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editText_confirm_pass_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editText_old_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
            editText_new_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
            editText_confirm_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        } else {
            editText_old_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_enter_pin),FONT_CALIBRI));
            editText_new_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_enter_pin),FONT_CALIBRI));
            editText_confirm_pass_pin.setHint(setHintFont(getContext(),getResources().getString(R.string.hint_confirm_pin),FONT_CALIBRI));
            title = getResources().getString(R.string.dialog_pin);
            editText_old_pass_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            editText_new_pass_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            editText_confirm_pass_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            editText_old_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            editText_new_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            editText_confirm_pass_pin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        }

        textView_old_pass_pin_title.setText(getResources().getString(R.string.dialog_old, title));
        textView_new_pass_pin_title.setText(getResources().getString(R.string.dialog_new, title));
        textView_confirm_pass_pin_title.setText(getResources().getString(R.string.dialog_re_type, title));

        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_dismiss.setText(getResources().getString(R.string.button_cancel));
        button_ok.setOnClickListener(v ->
        {
            String oldpass = editText_old_pass_pin.getText().toString();
            String newpass = editText_new_pass_pin.getText().toString();
            String confirmpass = editText_confirm_pass_pin.getText().toString();
            if (type.equals(KEY_PASSWORD)) {
                if (oldpass.length() < 6 || newpass.length() < 6 || confirmpass.length() < 6) {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pass), Toast.LENGTH_SHORT).show();
                } else {
                    if (newpass.equals(confirmpass)) {
                        initChangePass(dialog, oldpass, newpass, confirmpass);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pass_mismatch), Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                if (oldpass.length() < 4 || newpass.length() < 4 || confirmpass.length() < 4) {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
                } else {
                    if (newpass.equals(confirmpass)) {
                        String custId = preference.getString(KEY_CUSID);
                        initLoadAccounts(dialog, custId, oldpass, newpass, confirmpass);
//                        initChangePin(custId, oldpass, newpass, confirmpass);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin_mismatch), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        button_dismiss.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void initChangePass(Dialog dialog, String oldpass, String newpass, String confirmpass) {
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.change_pass(API_CHANGE_PASS, custId, getHash(oldpass), getHash(newpass), getHash(confirmpass), String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);
                    String message = (String) obj.get(MESSAGE);
                    if (status.equals(SUCCESS)) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    } else {
//                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo(account_type);
            }
        });
    }

    private void initLoadAccounts(Dialog dialog, String custId, String oldpass, String newpass, String confirmpass) {
        showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.mainCustInfo(API_ACCOUNT_SELECT, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("response.raw().request().url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(MAIN_ACCOUNT)));
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                String package_type = jsonobject.getString(PACKAGE_TYPE);
                                String eda = jsonobject.getString(EDA);
                                String name = jsonobject.getString(NAME);
                                int custid = jsonobject.getInt(CUSTID);
                                CardType cardType = new CardType(package_type, eda, name, custid, STATUS_CODE_INACTIVE);
                                datalist.add(cardType);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            JSONArray jsonarraySub = new JSONArray(String.valueOf(obj.get(SUB_ACCOUNT)));
                            for (int i = 0; i < jsonarraySub.length(); i++) {
                                JSONObject jsonobject = jsonarraySub.getJSONObject(i);
                                String package_type = jsonobject.getString(PACKAGE_TYPE);
                                String eda = jsonobject.getString(EDA);
                                String name = jsonobject.getString(NAME);
                                int custid = jsonobject.getInt(CUSTID);
                                CardType cardType = new CardType(package_type, eda, name, custid, STATUS_CODE_INACTIVE);
                                datalist.add(cardType);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Timber.d("datalist:" + datalist.size());
                        initChangePin(String.valueOf(datalist.get(0).getCustid()), oldpass, newpass, confirmpass);
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        showToast(getResources().getString(R.string.label_error_code, message, errorCode));
                    }

                    dialog.dismiss();

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    showToast(e.getMessage());
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(t.getMessage());
                hideProgress();
            }
        });
    }

    private void initChangePin(String custId, String oldpass, String newpass, String confirmpass) {
//        String custId = preference.getString(KEY_CUSID);
        showProgress();
        Call<ResponseBody> call = apiService.change_pin(API_CHANGE_PIN, custId, getHash(oldpass), getHash(newpass), getHash(confirmpass), String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);
                    String message = (String) obj.get(MESSAGE);
                    if (status.equals(SUCCESS)) {
                        if ((datalist.size()) != index) {
                            initChangePin(String.valueOf(datalist.get(index).getCustid()), oldpass, newpass, confirmpass);
                            index++;
                        } else {
                            index = 1;
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
//                    dialog.dismiss();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection));
                hideProgress();
                initLoadAccountInfo(account_type);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (layout_edit_profile.isShown()) {
                    layout_profile.setVisibility(View.VISIBLE);
                    layout_edit_profile.setVisibility(View.GONE);
                } else {
                    finish();
                }
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
//            Uri uri = data.getData();
//            uploadFile(getRealPathFromUri(getContext(),uri),uri);
//        }
        if (resultCode == RESULT_OK) {
            Uri tempUri;

            Timber.d("requestCode " + requestCode);
            if (requestCode == ImageSelector.CHOICE_AVATAR_FROM_CAMERA) {
                tempUri = Uri.parse(imageSelector.getmCurrentPhotoPath());
                Log.d("tempUri", "tempUri: " + tempUri.toString());
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                int widthInInches = metrics.widthPixels;
                int heightInInches = metrics.heightPixels;
                imageSelector.doCropRectangle(ProfileActivity.this, tempUri, this, widthInInches, heightInInches);
            } else if (requestCode == ImageSelector.CHOICE_AVATAR_FROM_GALLERY) {
                tempUri = data.getData();
                Log.d("tempUri", "tempUri: " + tempUri.toString());
                Timber.d("getRealPathFromURI " + getRealPathFromUri(getContext(), data.getData()));
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int widthInInches = metrics.widthPixels;
                int heightInInches = metrics.heightPixels;
                imageSelector.doCropRectangle(ProfileActivity.this, tempUri, this, widthInInches, heightInInches);
            } else if (requestCode == ImageSelector.CHOICE_AVATAR_REMOVE_PHOTO) {
                imageView_profile.setBackgroundResource(R.drawable.ic_default_user);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                uploadFileCIStest(getRealPathFromUri(getContext(), resultUri), resultUri);
            }
        }
    }

    private void uploadFileCIStest(String mediaPath, Uri uri) {
        showProgress();
        // Map is used to multipart the file using okhttp3.RequestBody
        File file = saveBitmapToFile(new File(mediaPath));

        int file_size;
        if (file != null) {
            file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
            ContentResolver cR = getContext().getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String type = mime.getExtensionFromMimeType(cR.getType(uri));
            Timber.d("file name:" + file.getName() + " file_size:" + file_size + " filetype:" + type);
        }
        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), Objects.requireNonNull(file));
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("uploadedfile", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody vc = RequestBody.create(MediaType.parse("text/plain"), getVCKeyED2E());
        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(unixTimeStamp()));
        RequestBody custid = RequestBody.create(MediaType.parse("text/plain"), preference.getString(KEY_CUSID));

        Call<ResponseBody> call = apiService.uploadImage(fileToUpload, filename, vc, date, custid);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String responseData = Objects.requireNonNull(response.body()).string();
                    int startIndex = responseData.lastIndexOf("</form>") + 7;
                    String json = responseData.substring(startIndex);
                    JSONObject obj = new JSONObject(json);

                    Timber.d("onResponse:" + response.code() + " " + json);

                    String status = (String) obj.get(STATUS);
                    if (status.equals(SUCCESS)) {
                        imageLoader.loadImage(getContext(),TAG,requestImage_profile,String.valueOf(obj.get(DATA)),imageView_profile,R.drawable.ic_default_user, true);
                    } else {
                        String errorCode = (String) obj.get(ERRORCODE_UPLOAD);
                        String message = (String) obj.get(ERROR_MESSAGE);
                        showToast(getResources().getString(R.string.label_error_code, message, errorCode));
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                Timber.d("onFailure:");
                t.printStackTrace();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }

    //Choose image whether camera or gallery
    public void selectImage() {
        Timber.d("selectImage");
        int hasAccounts;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasAccounts = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasAccounts != PackageManager.PERMISSION_GRANTED) {
                int REQUEST_CODE_ASK_PERMISSIONS = 123;
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);

                    return;
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
            cameraPermission();
        } else {
            cameraPermission();
        }
    }

    private void cameraPermission() {
        Timber.d("cameraPermission");
        int hasAccounts;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasAccounts = checkSelfPermission(Manifest.permission.CAMERA);

            if (hasAccounts != PackageManager.PERMISSION_GRANTED) {
                int REQUEST_CAMERA_ASK_PERMISSIONS = 12345;
                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_ASK_PERMISSIONS);

                    return;
                }
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_ASK_PERMISSIONS);
                return;
            }
            imageSelector = new ImageSelector(getContext());
            imageSelector.selectImage(imageView_profile);

        } else {
            imageSelector = new ImageSelector(getContext());
            imageSelector.selectImage(imageView_profile);
        }
    }

    @Override
    public void onLoadfailed(int requestCode) {
        if(requestCode == requestImage_profile)
            progressbar_profile.setVisibility(View.GONE);
    }

    @Override
    public void onResourceready(int requestCode) {
        if(requestCode == requestImage_profile)
            progressbar_profile.setVisibility(View.GONE);
    }
}
