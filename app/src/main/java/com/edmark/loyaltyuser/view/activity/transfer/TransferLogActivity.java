package com.edmark.loyaltyuser.view.activity.transfer;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewSectionedAdapter;
import com.edmark.loyaltyuser.adapter.TransferLogsAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.TransactionHistory;
import com.edmark.loyaltyuser.model.TransferLogs;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.edcoin.EDCoinActivity;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.ACCOUNT_ID;
import static com.edmark.loyaltyuser.constant.Common.API_TRANSACTION_HISTORY;
import static com.edmark.loyaltyuser.constant.Common.APP_TYPE;
import static com.edmark.loyaltyuser.constant.Common.CONVERTED_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.CREATED_DATE;
import static com.edmark.loyaltyuser.constant.Common.CURRENT_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.PREVIOUS_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.PURCHASER_COUNTRY;
import static com.edmark.loyaltyuser.constant.Common.PURCHASER_NAME;
import static com.edmark.loyaltyuser.constant.Common.RATE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TOPUP_ID;
import static com.edmark.loyaltyuser.constant.Common.TRANSFERRED_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.TRANSFER_DATA;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_MM;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_MMDDYYYY;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_YYYYMMDD;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_dd;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_yyyy;
import static com.edmark.loyaltyuser.util.AppUtility.get30DaysBeforeCurrentDate_YYYYMMDD;
import static com.edmark.loyaltyuser.util.AppUtility.getCurrentDate_YYYYMMDD;
import static com.edmark.loyaltyuser.util.AppUtility.getDate;
import static com.edmark.loyaltyuser.util.AppUtility.getDateSection;
import static com.edmark.loyaltyuser.util.AppUtility.getFormattedDate;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getTimeinMilli;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/18/2018.
 */
@SuppressWarnings("unchecked")
public class TransferLogActivity extends BaseActivity {
    public static final String TAG = TransferLogActivity.class.getSimpleName();


    @BindView(R.id.activity_transfer_log_textview_date_start)
    TextView textView_date_from;
    @BindView(R.id.activity_transfer_log_textview_date_end)
    TextView textView_date_to;
    @BindView(R.id.activity_transfer_log_textView_result_count)
    TextView textView_result_count;
    @BindView(R.id.activity_transfer_log_textView_total_result)
    TextView textView_total_result;
    @BindView(R.id.activity_transfer_log_textview_message)
    TextView textView_message;
//    @BindView(R.id.activity_transfer_log_recyclerview)
//    RecyclerView recyclerView;
    @BindView(R.id.activity_transfer_log_button_calendar)
    ImageView button_calendar;
    @BindView(R.id.activity_transfer_log_button_search)
    ImageView button_search;

    private BaseRecyclerViewAdapter adapter;
    private BaseRecyclerViewSectionedAdapter mSectionedAdapter;

    private String date_from;
    private String date_to;

    private ApiInterface apiInterface;
    private AppPreference preference;

    private List<TransferLogs> modelList;

    static RecyclerView recyclerView;
    TransferLogsAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_log);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);

        //initLoadType(savedInstanceState);
        setupToolBar(R.id.activity_transfer_log_toolbar);
        initGUI();

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        Intent intent = new Intent(getContext(), EDCoinActivity.class);
        startActivity(intent);

        finish();
    }

    private void initGUI() {

        //-- initialize recycler view, layout, and adapter
        recyclerView = findViewById(R.id.activity_transfer_log_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new TransferLogsAdapter(TransferLogActivity.this, modelList);
        recyclerView.setAdapter(recyclerAdapter);

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,#00.##");

        textView_date_from.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Integer.parseInt(formatDate_yyyy(textView_date_from.getText().toString().trim())), Integer.parseInt(formatDate_MM(textView_date_from.getText().toString().trim())) - 1, Integer.parseInt(formatDate_dd(textView_date_from.getText().toString().trim())));

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                textView_date_from.setText(new StringBuilder()
                        .append(formatter.format(month + 1)).append("-")
                        .append(formatter.format(dayOfMonth)).append("-")
                        .append(year)
                );

                //showToast_long("Date: " + formatDate_YYYYMMDD(editText_date_from.getText().toString().trim()));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        textView_date_to.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Integer.parseInt(formatDate_yyyy(textView_date_to.getText().toString().trim())), Integer.parseInt(formatDate_MM(textView_date_to.getText().toString().trim())) - 1, Integer.parseInt(formatDate_dd(textView_date_to.getText().toString().trim())));

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                textView_date_to.setText(new StringBuilder()
                        .append(formatter.format(month + 1)).append("-")
                        .append(formatter.format(dayOfMonth)).append("-")
                        .append(year)
                );

                //showToast_long("Date: " + formatDate_YYYYMMDD(editText_date_to.getText().toString().trim()));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        set30DayPeriodToCalendar();

        textView_date_from.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(compareDates(textView_date_from.getText().toString().trim(), textView_date_to.getText().toString().trim())){
                    //initAPIGetTransferConvertedEPLogs()
                } else {
                    textView_date_from.setText(textView_date_to.getText().toString().trim());
                }

            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        textView_date_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(compareDates(textView_date_from.getText().toString().trim(), textView_date_to.getText().toString().trim())){
                    initAPIGetTransferConvertedEPLogs();
                } else {
                    textView_date_to.setText(textView_date_from.getText().toString().trim());
                }
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        initAPIGetTransferConvertedEPLogs();
    }

    private void set30DayPeriodToCalendar() {
        textView_date_from.setText(formatDate_MMDDYYYY(getCurrentDate_YYYYMMDD()));
        textView_date_to.setText(formatDate_MMDDYYYY(getCurrentDate_YYYYMMDD()));
    }

    private boolean compareDates(String date_from, String date_to) {
        String dateStr_from = "";
        String dateStr_to = "";

        try {
            dateStr_from = date_from.replace("/", "-");
            dateStr_to = date_to.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("MM-dd-yyyy");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date dateFrom = srcDf.parse(dateStr_from);
            Date dateTo = srcDf.parse(dateStr_to);

            DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
            // format the date into another format
            dateStr_from = destDf.format(dateFrom);
            dateStr_to = destDf.format(dateTo);

            if (dateStr_from.compareTo(dateStr_to) > 0) {
                return false;
            } else if (dateStr_from.compareTo(dateStr_to) < 0) {
                return true;
            } else if (dateStr_from.compareTo(dateStr_to) == 0) {
                return true;
            } else {
                return false;
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void initAPIGetTransferConvertedEPLogs() {
        Timber.d(TAG,"initAPIGetTransferConvertedEPLogs() triggered");
        showProgress();

        date_from = formatDate_YYYYMMDD(textView_date_from.getText().toString().trim());
        date_to = formatDate_YYYYMMDD(textView_date_to.getText().toString().trim());

        Call<ResponseBody> call = apiInterface.getTransferConvertedEPLogs(
                preference.getString(KEY_EDCOIN_BIND_KEY, ""),
                date_from,
                date_to,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );



        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                hideProgress();
                Log.i(TAG, "onResponse: " + response.toString());
                Log.i(TAG,"Raw Url:" + response.raw().request().url());

                if(response.body()==null)
                {
                    //progressDialog.dismiss();
                    hideProgress();
                    showToast(getResources().getString(R.string.error_connection) + " (" + String.valueOf(response.code()) + ")");
                    //showToast_long(getResources().getString(R.string.error_connection_with_message, getResources().getString(R.string.error_response_body_null)));
                    return;
                }

                try{
                    hideProgress();
                    String responseString = response.body().string();
                    Log.i(TAG, "Response Body: " + responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);
                    String message;
                    JSONArray obj_message;

                    modelList = new ArrayList<>();
                    modelList.clear();


                    if (status.equals(SUCCESS)) {

                        //showToast(responseString);

                        JSONArray transfer_data = obj.getJSONArray(TRANSFER_DATA);
                        Double temp_amount = 0.00;

                        for(int i = transfer_data.length()-1 ; i >= 0; i--){
                            JSONObject transfer_data_object = transfer_data.getJSONObject(i);

                            /*String value_bonusDesc = "";
                            if(record_object.get(BONUS_DESCRIPTION) != null) {
                                value_bonusDesc = "" + record_object.get(BONUS_DESCRIPTION);
                            } else {
                                value_bonusDesc = "";
                            }*/

                            temp_amount = Double.parseDouble(transfer_data_object.getString(TRANSFERRED_AMOUNT)) + temp_amount;
                            TransferLogs list = new TransferLogs(
                                    transfer_data_object.getString(ID),
                                    transfer_data_object.getString(ACCOUNT_ID),
                                    transfer_data_object.getString(TOPUP_ID),
                                    transfer_data_object.getString(APP_TYPE),
                                    transfer_data_object.getString(PREVIOUS_BALANCE),
                                    transfer_data_object.getString(TRANSFERRED_AMOUNT),
                                    transfer_data_object.getString(CURRENT_BALANCE),
                                    transfer_data_object.getString(RATE),
                                    transfer_data_object.getString(CONVERTED_AMOUNT),
                                    transfer_data_object.getString(DESCRIPTION),
                                    transfer_data_object.getString(CREATED_DATE),
                                    transfer_data_object.getString(PURCHASER_NAME),
                                    transfer_data_object.getString(PURCHASER_COUNTRY)
                            );

                            modelList.add(list);
                        }

                        DecimalFormat formatter = new DecimalFormat("###,##0.00");

                        if (modelList.size() != 0){
                            textView_message.setVisibility(View.GONE);
                            textView_result_count.setText("" + transfer_data.length());
                            textView_total_result.setText("" + formatter.format(temp_amount) + " EP");
                        } else {
                            textView_message.setVisibility(View.VISIBLE);
                            textView_result_count.setText("0");
                            textView_total_result.setText("0 EP");
                        }

                        recyclerAdapter.setModelList(modelList);



                    } else if (status.equals(FAIL)) {
                        try{
                            message = (String) obj.get(MESSAGE);
                            if(message.equalsIgnoreCase("No Data Found")){

                            } else {
                                showToast(message);
                                //showToast_short(message);
                            }
                        } catch(JSONException | IllegalArgumentException | ClassCastException s) {
                            obj_message = obj.getJSONArray(MESSAGE);
                            message =  obj_message.toString().replace("[", "").replace("\"", "").replace("]", "").trim();
                            //showToast_short("Message is in Array: " + message);
                            String[] array_message = message.split(",");
                            for (String string: array_message) {
                                Log.i(TAG, "Message Array: " + string);
                                showToast(string);
                                //showToast_short(string);
                                break;
                            }
                        }

                        recyclerAdapter.setModelList(modelList);
                        textView_message.setVisibility(View.VISIBLE);
                        textView_result_count.setText("0");
                        textView_total_result.setText("0 EP");

                    } else {
                        hideProgress();
                        showToast(getResources().getString(R.string.error_try_again));
                        //showToast_short(getResources().getString(R.string.error_something_went_wrong));
                    }


                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    //progressDialog.dismiss();
                    hideProgress();
                    showToast(e.getMessage());
                    //showToast_long(getResources().getString(R.string.error_connection_with_message, e.getMessage()));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    hideProgress();
                    showToast(getResources().getString(R.string.error_connection) + "\n(" + t.getMessage() + ")");
                    //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
                    //finish();
                    return;
                }

                hideProgress();
                Log.i(TAG, "onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(getResources().getString(R.string.error_connection) + "\n(" + t.getMessage() + ")");
                //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
                //finish();
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Intent intent = new Intent(getContext(), EDCoinActivity.class);
                startActivity(intent);

                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }


}
