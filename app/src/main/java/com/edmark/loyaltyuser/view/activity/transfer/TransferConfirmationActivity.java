package com.edmark.loyaltyuser.view.activity.transfer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.DATE_TIME;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_NAME;
import static com.edmark.loyaltyuser.constant.Common.KEY_AUTO_CONVERTED_EP;
import static com.edmark.loyaltyuser.constant.Common.KEY_AVAILABLE_EDP;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUST_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_DATE_TIME;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA_NUMBER;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT_FROM;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT_TO;
import static com.edmark.loyaltyuser.constant.Common.KEY_NAME;
import static com.edmark.loyaltyuser.constant.Common.KEY_RATE;
import static com.edmark.loyaltyuser.constant.Common.KEY_REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSFER_APP;
import static com.edmark.loyaltyuser.constant.Common.KEY_WALLET_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;

public class TransferConfirmationActivity extends BaseActivity {

    public static final String TAG = TransferConfirmationActivity.class.getSimpleName();

    /*@BindView(R.id.activity_conversion_success_textView_app)
    TextView textView_app;*/

    @BindView(R.id.activity_transfer_confirmation_textView_app)
    TextView textView_app;
    @BindView(R.id.activity_transfer_confirmation_textView_name)
    TextView textView_name;
    @BindView(R.id.activity_transfer_confirmation_textView_edp_amount_label_from)
    TextView textView_edp_amount_label_from;
    @BindView(R.id.activity_transfer_confirmation_textView_edp_amount_from)
    TextView textView_edp_amount_from;
    @BindView(R.id.activity_transfer_confirmation_linearLayout_edp_amount_to)
    LinearLayout linearLayout_edp_amount_to;
    @BindView(R.id.activity_transfer_confirmation_textView_edp_amount_label_to)
    TextView textView_edp_amount_label_to;
    @BindView(R.id.activity_transfer_confirmation_textView_edp_amount_to)
    TextView textView_edp_amount_to;
    @BindView(R.id.activity_transfer_confirmation_textView_timer)
    TextView textView_timer;
    @BindView(R.id.activity_transfer_confirmation_button_1)
    Button button_confirm;
    @BindView(R.id.activity_transfer_confirmation_button_2)
    Button button_cancel;

    private ApiInterface apiInterface;
    private AppPreference preference;

    private String transfer_app = "", account_name = "", eda_number = "", edp_amount_from = "", edp_amount_to = "", custid = "", available_edp = "", edpoints_balance = "", rate = "";
    CountDownTimer cTimer = null;

    protected Dialog dialog_insufficientEDP;

    private boolean isTransacting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_confirmation);
        ButterKnife.bind(this);
        preference = AppPreference.getInstance(this);
        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);

        setupToolBar(R.id.activity_transfer_confirmation_toolbar);
        initData(savedInstanceState);
        initGUI();
    }

    private void initData(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            transfer_app = "";
            //account_name = "";
            eda_number = "";
            edp_amount_from = "";
            edp_amount_to = "";
            //custid = "";
            available_edp = "";
            edpoints_balance = "";
            rate = "";
        } else {
            transfer_app = extras.getString(KEY_TRANSFER_APP);
            //account_name = extras.getString(KEY_ACCOUNT_NAME);
            eda_number = extras.getString(KEY_EDA_NUMBER);
            edp_amount_from = extras.getString(KEY_EDP_AMOUNT_FROM);
            edp_amount_to = extras.getString(KEY_EDP_AMOUNT_TO);
            //custid = extras.getString(KEY_CUST_ID);
            available_edp = extras.getString(KEY_AVAILABLE_EDP);
            edpoints_balance = extras.getString(KEY_AUTO_CONVERTED_EP);
            rate = extras.getString(KEY_RATE);
        }

        //showToast("Rate: " + rate);
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        initPreviousActivity();
    }

    private void initPreviousActivity() {
        cancelTimer();
        Intent intent = new Intent(getContext(), TransferMainActivity.class);
        intent.putExtra(KEY_AUTO_CONVERTED_EP, edpoints_balance);
        startActivity(intent);
        finish();
    }

    private void initGUI() {
        Log.d(TAG, "initGUI() triggered");

        DecimalFormat formatter = new DecimalFormat("###,##0.00");

        textView_app.setText(transfer_app);
        textView_name.setText(preference.getString(KEY_NAME));
        textView_edp_amount_label_from.setText("EDPOINTS Amount (" + preference.getString(KEY_WALLET_COUNTRY_CODE) + ")");
        textView_edp_amount_from.setText(String.valueOf(formatter.format(Double.parseDouble(edp_amount_from.replace(",","")))));
        textView_edp_amount_label_to.setText("EDPOINTS Amount (" + preference.getString(KEY_COUNTRY_CODE) + ")");
        textView_edp_amount_to.setText(String.valueOf(formatter.format(Double.parseDouble(edp_amount_to.replace(",","")))));

        //if same country code - hide to_country
        if(preference.getString(KEY_WALLET_COUNTRY_CODE).equalsIgnoreCase(preference.getString(KEY_COUNTRY_CODE))){
            linearLayout_edp_amount_to.setVisibility(View.GONE);
        } else {
            linearLayout_edp_amount_to.setVisibility(View.VISIBLE);
        }

        if(transfer_app.equalsIgnoreCase("EDPOINTS App")){
            transfer_app = "0";
        } else if(transfer_app.equalsIgnoreCase("ED2E App")){
            transfer_app = "1";
        } else {
            transfer_app = "";
        }

        textView_timer.setText(getResources().getString(R.string.label_timer, "30"));
        startCountDownTimer();

        button_confirm.setOnClickListener(v -> {
            cancelTimer();

            if(!isTransacting) {
                isTransacting = true;
                initAPITransferConvertedEP();
            }
        });

        button_cancel.setOnClickListener(v -> {
            initPreviousActivity();
        });
    }

    private void startCountDownTimer() {
        Log.d(TAG, "startCountDownTimer() triggered");

        cTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                Timber.d("Time: " + millisUntilFinished/1000);
                textView_timer.setText(getResources().getString(R.string.label_timer, String.valueOf(millisUntilFinished/1000)));
            }
            public void onFinish() {
                cancelTimer();
                textView_timer.setText(getResources().getString(R.string.label_timer, "0"));
                initTimeOutDialog();
            }
        };
        cTimer.start();
    }

    void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

    private void initTimeOutDialog(){
        cancelTimer();

        Dialog dialog = new Dialog(this, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_main_2);

        TextView textView_message = dialog.findViewById(R.id.dialog_main_2_textview_message);
        TextView textView_message_2 = dialog.findViewById(R.id.dialog_main_2_textview_message_2);
        Button button_ok = dialog.findViewById(R.id.dialog_main_2_button_ok);
        Button button_cancel = dialog.findViewById(R.id.dialog_main_2_button_cancel);

        textView_message.setText(getResources().getString(R.string.dialog_timeout_warning));
        textView_message_2.setText(getResources().getString(R.string.dialog_timeout_warning_transfer_message));
        button_ok.setText(getResources().getString(R.string.button_go_back_to_transfer).toUpperCase());

        button_cancel.setVisibility(View.GONE);

        button_ok.setOnClickListener(v -> {
            dialog.dismiss();
            initPreviousActivity();
        });

        dialog.setOnDismissListener(dialog1 -> {
            initPreviousActivity();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void initAPITransferConvertedEP() {
        Log.d(TAG, "initAPITransferConvertedEP() triggered");
        showProgress();

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
        formatter.applyPattern("###,##0.00");

        Call<ResponseBody> call = apiInterface.transferConvertedEP(
                "2",
                preference.getString(KEY_EDCOIN_BIND_KEY),
                preference.getString(KEY_CUSID),
                eda_number,
                edp_amount_from.replace(",", "").trim(),
                formatter.format(Double.parseDouble(rate)).replace(",", "").trim(),
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        Log.i(TAG,
                "Data: " +
                        "2" + " | " +
                        preference.getString(KEY_EDCOIN_BIND_KEY) + " | " +
                        preference.getString(KEY_CUSID) + " | " +
                        eda_number + " | " +
                        edp_amount_from.replace(",", "").trim() + " | " +
                        formatter.format(Double.parseDouble(rate)).replace(",", "").trim()
        );


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransacting = false;
                    showToast(getResources().getString(R.string.error_connection));
                    //showToast("response body is null");
                    hideProgress();
                    //cancelTimer();
                    //loadConversionActivity();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();

                    //showToast("onResponse:" + response.code() + " " + json);

                    Log.d(TAG, "onResponse:" + response.code() + " | " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        isTransacting = true;
                        hideProgress();

                        //showToast_short("Transfer Success");

                        cancelTimer();

                        //Log.d(TAG, "onResponse: " + json);

                        String ref_no_response = "" + obj.get(REFERENCE_NO);
                        String date_time_response = "" + obj.get(DATE_TIME);

                        JSONObject cis_data = obj.getJSONObject("cis_data");
                        String received_amount = "" + cis_data.get("received_amount");


                        //Timber.d("coin_amount: " + (String) obj.get(COIN_AMOUNT) + " | received_amount: " + (String) obj.get(RECEIVED_AMOUNT));

                        //showToast("JSON: " + json);

                        MediaPlayer beep;
                        beep= MediaPlayer.create(TransferConfirmationActivity.this, R.raw.transaction);
                        beep.start();

                        Intent intent = new Intent(getContext(), TransferSuccessActivity.class);

                        DecimalFormat formatter = new DecimalFormat("###,##0.00");


                        /*transfer_app = extras.getString(KEY_TRANSFER_APP);
                        account_name = extras.getString(KEY_NAME);
                        edp_amount = extras.getString(KEY_EDP_AMOUNT);
                        ref_no = extras.getString(KEY_REFERENCE_NO);
                        date_time = extras.getString(KEY_DATE_TIME_CONVERT);*/


                        intent.putExtra(KEY_TRANSFER_APP, textView_app.getText().toString().trim());
                        intent.putExtra(KEY_ACCOUNT_NAME, textView_name.getText().toString().trim());
                        intent.putExtra(KEY_EDP_AMOUNT_FROM, String.valueOf(formatter.format(Double.parseDouble(edp_amount_from.replace(",","")))));
                        intent.putExtra(KEY_EDP_AMOUNT_TO,  String.valueOf(formatter.format(Double.parseDouble(edp_amount_to.replace(",","")))));
                        //intent.putExtra(KEY_CUST_ID, custid);
                        //intent.putExtra(KEY_RATE, rate);
                        //intent.putExtra(KEY_AVAILABLE_EDC, available_edc);
                        intent.putExtra(KEY_REFERENCE_NO, ref_no_response);
                        intent.putExtra(KEY_DATE_TIME, date_time_response);

//                        intent.putExtra(KEY_COIN_AMOUNT, coin_amount);
//                        intent.putExtra(KEY_RECEIVED_AMOUNT, received_amount);
//                        intent.putExtra(KEY_BIND_KEY, bind_key);

                        startActivity(intent);

                        finish();

                    } else if (status.equals(FAIL)) {
                        isTransacting = false;
                        hideProgress();
                        //cancelTimer();
                        String message = "";
                        try {
                            message = (String) obj.get(MESSAGE);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        //String error_code = (String) obj.get(ERROR_CODE);

                        /*if(error_code.equalsIgnoreCase("E-05")){
                            initWarningDialog();
                            //hideDialog_insufficientEDP();
                            //showDialog_InsufficientEDP();
                        } else {*/
                            //showToast(getResources().getString(R.string.dialog_default_error_title), message/* + " (" + error_code + ")"*/);
                        //cancelTimer();
                        //loadConversionActivity();


                        /*if(message.equalsIgnoreCase("You are trying to transfer an amount that exceeds your available EDPOINT Balance: ")){
                            //showToast_short(message);

                            showDialog_InsufficientEDP();

                            //showDialog_main_2(REQUEST_TAG_CHANGE_AMOUNT_CONVERSION, "", false, true, false, getResources().getString(R.string.dialog_time_out), getResources().getString(R.string.dialog_time_out_message), R.drawable.ic_dialog_time_out, getResources().getString(R.string.button_go_back_conversion), "");

//                            String edcoin_available_balance = (String) obj.get(EDCOIN_AVAILABLE_BALANCE);
//                            initWarningDialog(edcoin_available_balance);
                        } else {*/
                            showToast(message);
                            initPreviousActivity();
                            //cancelTimer();
                            //loadConversionActivity();
                        //}

                    } else {
                        isTransacting = false;
                        hideProgress();
                        showToast(getResources().getString(R.string.error_connection));
                        //cancelTimer();
                        //loadConversionActivity();
                    }
                } catch (IOException | JSONException e) {
                    isTransacting = false;
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    //cancelTimer();
                    //loadConversionActivity();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                isTransacting = false;

                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    //showToast(t.getMessage());
                    hideProgress();
                    //cancelTimer();
                    //loadConversionActivity();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                //showToast(t.getMessage());
                hideProgress();
                //cancelTimer();
                //loadConversionActivity();
            }
        });
    }

    private void initWarningDialog(String edcoin_available_balance) {

        cancelTimer();

        Dialog dialog2 = new Dialog(this, R.style.DialogTheme);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog2.getWindow() != null)
            dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.setContentView(R.layout.dialog_main_2);

        TextView textView_message = dialog2.findViewById(R.id.dialog_main_2_textview_message);
        TextView textView_message_2 = dialog2.findViewById(R.id.dialog_main_2_textview_message_2);
        Button button_ok = dialog2.findViewById(R.id.dialog_main_2_button_ok);
        Button button_cancel = dialog2.findViewById(R.id.dialog_main_2_button_cancel);

        Typeface custom_font_NORMAL = Typeface.createFromAsset(getAssets(), "fonts/Calibri.ttf");
        Typeface custom_font_BOLD = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");


        textView_message.setTypeface(custom_font_NORMAL);
        textView_message_2.setTypeface(custom_font_BOLD);

        textView_message.setText(getResources().getString(R.string.dialog_conversion_warning));
        textView_message_2.setText("" + edcoin_available_balance);

        button_ok.setText(getResources().getString(R.string.button_change_edc_amount));

        button_cancel.setVisibility(View.GONE);

        button_ok.setOnClickListener(v -> {
            dialog2.dismiss();
            initPreviousActivity();
        });

        dialog2.setOnDismissListener(dialog -> {
            initPreviousActivity();
        });

        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
    }

    /*private void showDialog_InsufficientEDP() {
        Log.d(TAG, "showDialog_InsufficientEDC() triggered");

        if(dialog_insufficientEDP != null){
            hideDialog_insufficientEDP();
        }

        dialog_insufficientEDP = new Dialog(this, R.style.DialogTheme);
        dialog_insufficientEDP.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog_insufficientEDP.getWindow() != null)
            dialog_insufficientEDP.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_insufficientEDP.setContentView(R.layout.dialog_insufficient_edc);

        TextView textView_message = dialog_insufficientEDP.findViewById(R.id.dialog_insufficient_edc_textView_insufficient_message);
        TextView textView_edc_amount = dialog_insufficientEDP.findViewById(R.id.dialog_insufficient_edc_textView_edc_amount);
        Button button_1 = dialog_insufficientEDP.findViewById(R.id.dialog_insufficient_edc_button_1);

        button_1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_button_light_black_selector));

        Typeface custom_font_NORMAL = Typeface.createFromAsset(getAssets(), "fonts/Calibri.ttf");
        Typeface custom_font_BOLD = Typeface.createFromAsset(getAssets(), "fonts/Calibri Bold.ttf");

        textView_message.setTypeface(custom_font_BOLD);
        textView_edc_amount.setTypeface(custom_font_BOLD);
        button_1.setTypeface(custom_font_BOLD);

        textView_message.setText("You are trying to transfer an amount that exceeds your available EDP Balance:");
        textView_edc_amount.setText(available_edp);

        button_1.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), TransferMainActivity.class);
            startActivity(intent);
            finish();
            hideDialog_insufficientEDP();
        });

        dialog_insufficientEDP.setCancelable(false);
        dialog_insufficientEDP.setCanceledOnTouchOutside(false);
        dialog_insufficientEDP.show();
    }

    public void hideDialog_insufficientEDP() {
        try {
            dialog_insufficientEDP.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /*@Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag){
            case REQUEST_TAG_GO_BACK_CONVERSION:
                cancelTimer();
                intent = new Intent(getContext(), TransferMainActivity.class);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }
    }*/

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
