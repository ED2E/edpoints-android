package com.edmark.loyaltyuser.view.activity.utility;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.SpinnerAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Country;
import com.edmark.loyaltyuser.model.CustomerProfile;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientExpressPay;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.ui.slideview.SlideView;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.AppUtility;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionActivity;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionSuccessActivity;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_MERCHANT_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.API_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA;
import static com.edmark.loyaltyuser.constant.Common.KEY_EMAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_LOYALTY_EARNED;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERID;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERNAME;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RESPONSE_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.playBeep;
import static com.edmark.loyaltyuser.util.AppUtility.sha256;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

import static com.edmark.loyaltyuser.constant.Common.STATUS;

public class TransactionUtilityActivity extends BaseActivity {
    private static final String TAG = TransactionUtilityActivity.class.getSimpleName();

    @BindView(R.id.activity_transaction_utility_scrollview)
    ScrollView scrollview;
    @BindView(R.id.activity_transaction_utility_textview_biller_name)
    TextView textview_biller_name;
    @BindView(R.id.activity_transaction_utility_editText_reference_number)
    EditText editText_reference_number;
    @BindView(R.id.activity_transaction_utility_editText_account_name)
    EditText editText_account_name;
    @BindView(R.id.activity_transaction_utility_editText_account_number)
    EditText editText_account_number;
    @BindView(R.id.activity_transaction_utility_editText_remarks)
    EditText editText_remarks;
    @BindView(R.id.activity_transaction_utility_editText_amount)
    EditText editText_amount;
    @BindView(R.id.activity_transaction_utility_editText_due_date)
    EditText editText_due_date;
    @BindView(R.id.activity_transaction_utility_view_due_date)
    View view_due_date;
    @BindView(R.id.activity_transaction_utility_spinner_month)
    Spinner spinner;
    @BindView(R.id.activity_transaction_utility_editText_book_id)
    EditText editText_book_id;
    @BindView(R.id.activity_transaction_utility_button_slide_to_pay)
    SlideView button_slide;
    @BindView(R.id.activity_transaction_utility_button_cancel)
    Button button_abort;
    @BindView(R.id.activity_transaction_utility_button_submit)
    Button button_submit;

    private ApiInterface apiService;
    private ApiInterface apiServiceEP;
    private AppPreference preference;

    private boolean isSliding = false;
    private boolean isTransact = false;

    private String biller_id, ref_no, acc_name, acc_num, remarks, amount, due_date, bill_month, book_id;
    private String biller_id_header, biller_name;
    private String merchantid;

    private String eda;
    private String email;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_utility);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_transaction_utility_toolbar);
        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        apiServiceEP = ApiClientExpressPay.getClient().create(ApiInterface.class);

        initBillerInfo(savedInstanceState);
        initGUI();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void initBillerInfo(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            biller_id = "BILLER ID";

            biller_name = "BILLER NAME";
        } else {
            biller_id = "" + extras.getInt(KEY_UTILITY_BILLERID);
            biller_name = extras.getString(KEY_UTILITY_BILLERNAME);
        }

        merchantid = "1354159";
        //showToast("Biller ID: " + biller_id + "\n" + "Biller Name: " + biller_name);
    }

    private void initGUI() {

        //init biller name
        textview_biller_name.setText(biller_name.toUpperCase());

        //init current date
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");
        String currentDate = sdf1.format(new Date());
        String currentYear = sdf2.format(new Date());
        editText_due_date.setText(currentDate);
        //editText_book_id.setText(currentYear);

        view_due_date.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                editText_due_date.setText(new StringBuilder().append(year).append("-").append(month + 1).append("-").append(dayOfMonth));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        /*ArrayList<Month> months = new ArrayList<>(AppUtility.getMonth(Objects.requireNonNull(getContext())));
        Collections.sort(months, (o1, o2) -> o1.getMonth().compareTo(o2.getCode()));
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, R.layout.child_spinner_month, months);
        spinner.setAdapter(spinnerAdapter);
        int position = 1;

        spinner.setSelection(position);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //showToast("Selected: " + String.valueOf(countries.get(position).getCode()));
                month = String.valueOf(months.get(position).getCode());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        int position = 0;

        spinner.setSelection(position);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //showToast("Selected: " + String.valueOf(countries.get(position).getCode()));
                bill_month = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        /*scrollview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                if(isSliding){
//                    showToast("Scrolling disabled!");
//                    return false;
//                } else {
                    showToast("Scrolling enabled!");
                    return false;

            }
        });*/

        /*button_slide.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                showToast("Slide touched!");
                isSliding = true;
                return true;
            }
        });*/

        button_slide.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {


                if (event.getAction() == DragEvent.ACTION_DRAG_STARTED) {
                    showToast("Dragged");

                }
                return true;
            }
        });

        button_slide.setOnFinishListener(() -> {

                        if(validateTextFields()){
                            initData();

                            initPinDialog();

                            /*Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            if (dialog.getWindow() != null)
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setContentView(R.layout.dialog_main);

                            Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
                            Button button_dismiss = dialog.findViewById(R.id.dialog_main_button_cancel);
                            TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);

                            textView_message.setText(getResources().getString(R.string.dialog_bill_payment_confirmation, amount, biller_name, acc_num));
                            button_ok.setText(getResources().getString(R.string.button_proceed));
                            button_ok.setOnClickListener(v1 -> {
                                initData();
                                dialog.dismiss();
                                //initTransactionExpressPay();
                                initPinDialog();
                            });
                            button_dismiss.setOnClickListener(v1 -> {
                                button_slide.reset();
                                dialog.dismiss();
                            });

                            dialog.setOnDismissListener(v1 -> {
                                button_slide.reset();
                            });

                            dialog.setCanceledOnTouchOutside(false);

                            dialog.show();*/
                        }
                }
        );

        button_abort.setOnClickListener(v -> finish());

        button_submit.setOnClickListener(v ->
                {
                    if(validateTextFields()){
                        initData();

                        Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        if (dialog.getWindow() != null)
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.dialog_main);

                        Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
                        Button button_dismiss = dialog.findViewById(R.id.dialog_main_button_cancel);
                        TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);

                        textView_message.setText(getResources().getString(R.string.dialog_bill_payment_confirmation, amount, biller_name, acc_num));
                        button_ok.setText(getResources().getString(R.string.button_proceed));
                        button_ok.setOnClickListener(v1 -> {
                            initData();
                            dialog.dismiss();
                            //initTransactionExpressPay();
                            initPinDialog();
                        });
                        button_dismiss.setOnClickListener(v1 -> {
                            dialog.dismiss();
                        });

                        dialog.show();
                    }
                }
        );

    }

    private void initPinDialog() {
        Timber.d("initPinDialog");
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                if(!isTransact) {
                    isTransact = true;

                    String pin = editText_pin.getText().toString();
                    String amount = editText_amount.getText().toString().replace(",", "");
                    String description = "UTILITY PAYMENT - " + biller_name + "\nAcc No: " + acc_num/*editText_description.getText().toString()*/;

                    validateTransaction(pin, amount, description);

                    button_slide.reset();
                    dialog.dismiss();
                }
            }
        });
        button_dismiss.setOnClickListener(v ->
        {
            button_slide.reset();
            dialog.dismiss();
        });

        dialog.setOnDismissListener(dialog -> {
            button_slide.reset();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void validateTransaction(String pin, String amount, String description) {
        Timber.d("validateTransaction");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.validateTransaction(
                API_VALIDATE_TRANSACTION,
                custId,
                merchantid,
                getHash(pin),
                amount,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
//                        getTransactionID();
                        String transactionid = (String) obj.get(RESPONSE_TRANSACTION_ID);

                        //Log.e("CHECK", scan_process + "RESPONSE: " + response.toString());

                        //Transaction Process (ExpressPay)
                        initTransactionExpressPay(transactionid,description);
                        //initTransaction(transactionid,description);
                        //initCheckRefNumStatus(transactionid);
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
                button_slide.reset();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo();
                hideProgress();
            }
        });
    }

    private void initLoadAccountInfo() {
        Timber.d("initLoadAccountInfo");
        showProgress();
        Call<ResponseBody> call = apiService.mainCustInfo(API_MERCHANT_PROFILE, merchantid, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfile info = gson.fromJson(json, CustomerProfile.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());
                        eda = info.getEda();
                        email = info.getEmail();
                        name = info.getName();
                        /*textView_name.setText(info.getName());

                        progressbar_profile.setVisibility(View.VISIBLE);
                        imageLoader.loadImage(getContext(), TAG, 0, info.getImage_url(), imageView_profile, R.drawable.ic_default_user, true);*/
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo();
                hideProgress();
            }
        });
    }

    private void initTransaction(String transactionid, String description) {
        Timber.d("initTransaction");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.transaction(
                API_TRANSACTION,
                custId,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()),
                merchantid,
                transactionid,
                amount/*editText_amount.getText().toString().replace(",", "")*/,
                description);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));

                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        isTransact = false;
                        playBeep(getContext(),"transaction.mp3");
                        Intent intent = new Intent(TransactionUtilityActivity.this, TransactionSuccessActivity.class);
                        intent.putExtra(KEY_MERCHANT, biller_name/*textView_name.getText().toString()*/);
                        double trans_amount = Double.valueOf(editText_amount.getText().toString().replace(",", "").trim());
                        float loyalty_earned = (float) (trans_amount / 100d);
                        intent.putExtra(KEY_TRANSACTION_ID, transactionid);
                        intent.putExtra(KEY_TRANSACTION_AMOUNT, String.valueOf(trans_amount));
                        intent.putExtra(KEY_LOYALTY_EARNED, String.valueOf(loyalty_earned));
                        intent.putExtra(KEY_DESCRIPTION, description);

                        intent.putExtra(KEY_MERCHANT_ID, merchantid);
                        intent.putExtra(KEY_EDA, eda);
                        intent.putExtra(KEY_EMAIL, email);
                        startActivity(intent);
                        hideProgress();

                        finish();
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();

                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    private void initTransactionExpressPay(String transactionid, String description) {
        Timber.d("initTransactionExpressPay");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");
        String currentYear = sdf2.format(new Date());
        int month = spinner.getSelectedItemPosition() + 1;
        bill_month = currentYear + month;

        //biller_id =
        ref_no = transactionid;

        initData();

        Timber.d("initTransaction");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiServiceEP.submitBillsPayment(
                getHash(sha256(biller_id + acc_name + acc_num + remarks + amount + book_id + due_date + bill_month + ref_no + "expresspayapikey")),
                biller_id,
                acc_name,
                acc_num,
                remarks,
                amount,
                book_id,
                due_date,
                bill_month,
                ref_no
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("SUCCESS", "onResponse: " + response.toString());
                progressDialog.dismiss();

                //showToast("onResponse: Completed!");

                Timber.d("url():" + response.raw().request().url());

                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));
                    progressDialog.dismiss();
                    hideProgress();
                    return;
                }

                if(response.isSuccessful()) {
                    isTransact = false;
                    //Log.i("SUCCESS PT 2", "Successful Response: " + json);
                    progressDialog.dismiss();
                    try {
                        String json = "{\"data\":" + response.body().string() + "}";
                        //showToast(json);
                        Log.i("SUCCESS TRY", "JSON: " + json);
                        JSONObject obj = new JSONObject(json);
                        JSONArray data = obj.getJSONArray("data");
                        int status = data.getJSONObject(0).getInt(STATUS);

                        //showToast("status: " + status);

                        progressDialog.dismiss();

                        if(status == 0) {
                            isTransact = false;
                            //Transaction Process (Normal)
                            initTransaction(transactionid,description);
                        } else if(status == 1) {
                            isTransact = false;
                            Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            if (dialog.getWindow() != null)
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setContentView(R.layout.dialog_main);

                            TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);
                            Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
                            Button button_cancel = dialog.findViewById(R.id.dialog_main_button_cancel);

                            textView_message.setText(data.getJSONObject(0).getString(MESSAGE));
                            button_ok.setText(getResources().getString(R.string.button_ok));

                            button_cancel.setVisibility(View.GONE);

                            button_ok.setOnClickListener(v -> {
                                dialog.dismiss();
                            });
                            dialog.show();
                            //showToast(data.getJSONObject(0).getString(MESSAGE));
                        } else {
                            isTransact = false;
                            showToast(getResources().getString(R.string.error_try_again));
                        }


                    } catch (JSONException e) {
                        isTransact = false;
                        Log.i("SUCCESS CATCH", "JSONException: " + e.getMessage());
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        isTransact = false;
                        Log.i("SUCCESS CATCH", "IOException: " + e.getMessage());
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                Log.i("ERROR", "onFailure: " + t.getMessage());
                showToast(getResources().getString(R.string.error_try_again)/* + "\n" + t.getMessage()*/);
                //showToast("onFailure: " + t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    private void initCheckRefNumStatus(String transactionid){
        Timber.d("initCheckRefNumStatus");
        showProgress();
        Call<ResponseBody> call = apiServiceEP.checkReferenceNumberStatus(
                getHash(sha256(transactionid + "expresspayapikey")),
                transactionid
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();

                showToast("onResponse: Completed!");

                Timber.d("url():" + response.raw().request().url());

                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));
                    progressDialog.dismiss();
                    hideProgress();
                    return;
                }

                if(response.isSuccessful()) {
                    isTransact = false;
                    //Log.i("SUCCESS PT 2", "Successful Response: " + json);
                    progressDialog.dismiss();
                    try {
                        String json = /*"{\"data\":" +*/ response.body().string() /*+ "}"*/;
                        showToast(json);
                        Log.i("SUCCESS TRY", "JSON: " + json);
                        JSONObject obj = new JSONObject(json);
                        String status = obj.getString(STATUS);

                        //showToast("status: " + status);

                        progressDialog.dismiss();

                        if(response.code() == 200){

                            Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            if (dialog.getWindow() != null)
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setContentView(R.layout.dialog_main);

                            TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);
                            Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
                            Button button_cancel = dialog.findViewById(R.id.dialog_main_button_cancel);

                            if(status.equalsIgnoreCase("Error")) {
                                isTransact = false;
                                //Transaction Process (Normal)
                                //initTransactionExpressPay(transactionid,description);
                                String message = obj.getString(MESSAGE);
                                textView_message.setText(message);

                            } else {
                                isTransact = false;
                                String date = obj.getString("date");
                                textView_message.setText(status + "\n" + date);
                            }

                            button_ok.setText(getResources().getString(R.string.button_ok));
                            button_cancel.setVisibility(View.GONE);

                            button_ok.setOnClickListener(v -> {
                                dialog.dismiss();
                            });

                            dialog.show();

                        } else {
                            isTransact = false;
                            showToast(getResources().getString(R.string.error_try_again));
                        }

                    } catch (JSONException e) {
                        isTransact = false;
                        Log.i("SUCCESS CATCH", "JSONException: " + e.getMessage());
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        isTransact = false;
                        Log.i("SUCCESS CATCH", "IOException: " + e.getMessage());
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private boolean validateTextFields() {
        button_slide.reset();

        /*if(editText_reference_number.getText().toString().trim().isEmpty()){
            editText_reference_number.setError("Reference No. is required.");
            showToast("Reference No. is required.");
            return false;
        } else */if(editText_account_name.getText().toString().trim().isEmpty()){
            editText_account_name.setError("Account Name is required.");
            showToast("Account Name is required.");
            return false;
        } else if(editText_account_number.getText().toString().trim().isEmpty()){
            editText_account_number.setError("Account Number is required.");
            showToast("Account Number is required.");
            return false;
        } else if(editText_amount.getText().toString().trim().isEmpty()){
            editText_amount.setError("Please enter a valid amount.");
            showToast("Please enter a valid amount.");
            return false;
        } else if(editText_due_date.getText().toString().trim().isEmpty()){
            editText_due_date.setError("Please enter a Due Date.");
            showToast("Please enter a Due Date.");
            return false;
        } else if(editText_book_id.getText().toString().trim().isEmpty()){
            editText_book_id.setError("Book ID is required.");
            showToast("Book ID is required.");
            return false;
        } else {
            try{
                double amount = Double.parseDouble(editText_amount.getText().toString().trim());
                if(amount <= 0){
                    editText_amount.setError("Please enter a valid amount.");
                    showToast("Please enter a valid amount.");
                    return false;
                } else {
                    return true;
                }
            }catch(NumberFormatException e){
                editText_amount.setError("Please enter a valid amount.");
                showToast("Please enter a valid amount.");
                return false;
            }
        }
    }

    private void initData() {
        //biller_id = editText_reference_number.getText().toString().trim();
        ref_no = editText_reference_number.getText().toString().trim();
        acc_name = editText_account_name.getText().toString().trim();
        acc_num = editText_account_number.getText().toString().trim();
        remarks = editText_remarks.getText().toString().trim();
        amount = editText_amount.getText().toString().trim();
        due_date = editText_due_date.getText().toString().trim();
        book_id = editText_book_id.getText().toString().trim();

                /*showToast(
                                "Biller Name (ID): " + biller_id + "\n" +
                                "Reference No: " + ref_no + "\n" +
                                "Account Name: " + acc_name + "\n" +
                                "Account Number: " + acc_num + "\n" +
                                "Remarks: " + remarks + "\n" +
                                "Amount: " + amount + "\n" +
                                "Due Date: " + due_date + "\n" +
                                "Month of Billing: " + bill_month + "\n" +
                                "Book ID: " + book_id + "\n"
                );*/
    }
}
