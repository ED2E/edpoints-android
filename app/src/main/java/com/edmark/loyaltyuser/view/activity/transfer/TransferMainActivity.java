package com.edmark.loyaltyuser.view.activity.transfer;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.ui.slideview.SlideView;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.edcoin.EDCoinActivity;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_CUSTINFO;
import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_PIN_COIN;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.ERROR_CODE;
import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_AUTO_CONVERTED_EP;
import static com.edmark.loyaltyuser.constant.Common.KEY_AVAILABLE_EDP;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA_NUMBER;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT_FROM;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT_TO;
import static com.edmark.loyaltyuser.constant.Common.KEY_ENABLE_TIP_TRANSFER;
import static com.edmark.loyaltyuser.constant.Common.KEY_RATE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSFER_APP;
import static com.edmark.loyaltyuser.constant.Common.KEY_WALLET_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RATE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

public class TransferMainActivity extends BaseActivity {

    public static final String TAG = TransferMainActivity.class.getSimpleName();

    @BindView(R.id.activity_transfer_main_textView_country_code)
    TextView textView_country_code;
    @BindView(R.id.activity_transfer_main_textView_edpoint_balance)
    TextView textView_edpoint_balance;

    @BindView(R.id.activity_transfer_main_imageView_flag_from)
    ImageView imageView_flag_from;
    @BindView(R.id.activity_transfer_main_textView_transfer_rate_value)
    TextView textView_transfer_rate_value;
    @BindView(R.id.activity_transfer_main_imageView_flag_to)
    ImageView imageView_flag_to;
    @BindView(R.id.activity_transfer_main_linearLayout_amount_to_transfer)
    LinearLayout linearLayout_amount_to_transfer;
    @BindView(R.id.activity_transfer_main_textView_amount_to_transfer)
    TextView textView_amount_to_transfer;

    @BindView(R.id.activity_transfer_main_editText_edpoint_input)
    EditText editText_edpoint_input;
    @BindView(R.id.activity_transfer_main_button_full_amount)
    Button button_full_amount;
    @BindView(R.id.activity_transfer_main_button_slide_to_transfer)
    SlideView button_slide_to_transfer;
    @BindView(R.id.activity_transfer_main_relativeLayout_app_selection)
    RelativeLayout relativeLayout_app_selection;
    @BindView(R.id.activity_transfer_main_textView_app)
    TextView textView_app;
    @BindView(R.id.activity_transfer_main_layout_update)
    LinearLayout layout_update;

    private ApiInterface apiInterface;
    private ApiInterface apiInterfaceCIS;
    private AppPreference preference;

    private String edpoints_value = "";
    private String transfer_app = "", account_name = "", edp_amount = "", eda_number_intent = "", custid_intent = "", rate_intent = "", available_edc_intent = "", converted_amount_intent = "";
    private String company_code_CIS = "";
    private String edpoints_balance = "";

    private String rate;
    private Double rate_toDouble;

    private boolean isTransact = false;

    protected Dialog dialog_transferSelection, dialog_inputEDA, dialog_verifyIdentity, dialog_inputPIN;
    private ArrayList<EditText> mEditTextListPIN;
    private StringBuilder pin_input;

    private String[] country_code_array;
    TypedArray images_array;

    Double rate_global = 0.00;

    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_main);
        ButterKnife.bind(this);
        preference = AppPreference.getInstance(this);
        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);
        apiInterfaceCIS = ApiClient.getClient().create(ApiInterface.class);

        transfer_app = "EDPOINTS App";
        country_code_array = getContext().getResources().getStringArray(R.array.array_country_code);
        images_array = getContext().getResources().obtainTypedArray(R.array.array_country_flag_icons);

        textView_transfer_rate_value.setText(getResources().getString(R.string.label_current_transfer_rate_value, "0.00"));
        textView_amount_to_transfer.setText(getResources().getString(R.string.label_amount_to_be_transferred, "0.00", "0.00"));
        linearLayout_amount_to_transfer.setVisibility(View.GONE);

        setupToolBar(R.id.activity_transfer_main_toolbar);
        initData(savedInstanceState);
        initGUI();
    }

    private void initData(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            edpoints_balance = "";
        } else {
            edpoints_balance = extras.getString(KEY_AUTO_CONVERTED_EP);
        }
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        initPreviousActivity();
    }

    private void initGUI() {
        Log.d(TAG, "initGUI() triggered");

        initUpdateAmountToBeTransferred();

        textView_country_code.setText("" + preference.getString(KEY_WALLET_COUNTRY_CODE, ""));

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
        formatter.applyPattern("###,##0.00");
        formatter.setGroupingSize(3);

        textView_edpoint_balance.setText(edpoints_balance);

        button_full_amount.setOnClickListener(v -> {
            editText_edpoint_input.setText(textView_edpoint_balance.getText().toString());
            editText_edpoint_input.setSelection(editText_edpoint_input.length());
        });

        relativeLayout_app_selection.setOnClickListener(v -> {
            //showToast_short("Select App");
        });

        button_slide_to_transfer.setOnFinishListener(() -> {

            if (editText_edpoint_input.getText().toString().equals("")
                    || editText_edpoint_input.getText().toString().isEmpty()
                    || converted_amount_intent.equals("")
                    || converted_amount_intent.isEmpty())
            {
                editText_edpoint_input.setError(getResources().getString(R.string.error_transfer_invalid_amount));
                editText_edpoint_input.requestFocus();
                button_slide_to_transfer.reset();
                return;
            }

            if (Double.valueOf(editText_edpoint_input.getText().toString().replace(",","")) == 0.00
                    ||Double.valueOf(converted_amount_intent.replace(",","")) == 0.00)
            {
                editText_edpoint_input.setError(getResources().getString(R.string.error_transfer_invalid_amount));
                editText_edpoint_input.requestFocus();
                button_slide_to_transfer.reset();
                return;
            }

            button_slide_to_transfer.reset();
            initPinDialog();
        });

        layout_update.setOnClickListener(v -> {
            initAPIGetCrossBorderRate();
        });


        //showDialog_InputEDA();
        //initAPIGetWalletDetails();

        initEDANumber();
    }

    private void initPinDialog() {
        Timber.d("initPinDialog");
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                if(!isTransact) {

                    String pin = editText_pin.getText().toString();

                    initValidatePinAPI(pin);

                    button_slide_to_transfer.reset();
                    dialog.dismiss();

                }
            }
        });
        button_dismiss.setOnClickListener(v ->
        {
            button_slide_to_transfer.reset();
            dialog.dismiss();
        });

        dialog.setOnDismissListener(dialog -> {
            button_slide_to_transfer.reset();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void initValidatePinAPI(String pin) {
        Timber.d("validatePin");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call2 = apiInterfaceCIS.validatePinCoin(
                API_VALIDATE_PIN_COIN,
                custId,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()),
                getHash(pin));
        call2.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        //initConvertEDCoinAPI();
                        //initWarningDialog(edc_balance);
                        //showToast("JSON: " + json);
                        hideProgress();
                        initConfirmationScreeen();
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
                button_slide_to_transfer.reset();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    private void initConfirmationScreeen() {

        //showToast("initConfirmationScreen");

        Intent intent = new Intent(getContext(), TransferConfirmationActivity.class);
        intent.putExtra(KEY_TRANSFER_APP, transfer_app);
        //intent.putExtra(KEY_ACCOUNT_NAME, account_name);
        intent.putExtra(KEY_EDA_NUMBER, eda_number_intent);
        intent.putExtra(KEY_EDP_AMOUNT_FROM, editText_edpoint_input.getText().toString().trim().replace(",", ""));
        intent.putExtra(KEY_EDP_AMOUNT_TO, converted_amount_intent);
        //intent.putExtra(KEY_EDC_AMOUNT, editText_edpoint_input.getText().toString().trim().replace(",", ""));
        //intent.putExtra(KEY_CUST_ID, custid_intent);
        intent.putExtra(KEY_AVAILABLE_EDP, textView_edpoint_balance.getText().toString().trim().replace(",", ""));
        intent.putExtra(KEY_AUTO_CONVERTED_EP, edpoints_balance);
        intent.putExtra(KEY_RATE, String.valueOf(rate_global));

        startActivity(intent);
        finish();

        /*String edp_value = editText_edpoint_input.getText().toString().trim();
        DecimalFormat formatter = new DecimalFormat("###,###.##");
        //edp_value = String.valueOf(formatter.format(Double.parseDouble(edp_value)));
        edp_value = String.valueOf(amount_input);

        String edc_value = editText_amount_converted.getText().toString().trim(); String.valueOf(amount_input * toEDP);

        //showToast("EDC: " + edc_value + " | EDP: " + edp_value);
        button_slide.reset();

        Intent intent = new Intent(getContext(), ConversionConfirmationActivity.class);

        intent.putExtra(KEY_EDC_VALUE, edc_value);
        intent.putExtra(KEY_EDP_VALUE, edp_value);
        intent.putExtra(KEY_CUSTID, cust_id);
        intent.putExtra(KEY_RATE, rate);
        intent.putExtra(KEY_BIND_KEY, bind_key);
        intent.putExtra(KEY_EDC_BALANCE, available);

        startActivity(intent);
        finish();*/
    }

//    private void initAPICheckCoinRate(String company_code) {
//        Timber.d(TAG,"initAPICheckCoinRate() triggered");
//        showProgress();
//
//
//        Call<ResponseBody> call = apiInterface.checkCoinRate(
//                /*preference.getString(KEY_COUNTRY_CODE, "")*/company_code,
//                getHash(API_ACCESS_CODE)
//        );
//
//        Log.i(TAG,
//                "Data: " +
//                        preference.getString(KEY_COUNTRY_CODE, "")
//        );
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                Log.i(TAG, "onResponse: " + response.toString());
//                Log.i(TAG,"Raw Url:" + response.raw().request().url());
//
//                if(response.body()==null)
//                {
//                    //progressDialog.dismiss();
//                    hideProgress();
//                    showDialog_connectTimeOut(String.valueOf(response.code()));
//                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, String.valueOf(response.code())), 0, getResources().getString(R.string.button_go_back), "");
//                    //showToast_long(getResources().getString(R.string.error_connection_with_message, getResources().getString(R.string.error_response_body_null)));
//                    return;
//                }
//
//                try{
//                    String responseString = response.body().string();
//                    Log.i(TAG, "Response Body: " + responseString);
//
//                    JSONObject obj = new JSONObject(responseString);
//                    String status = (String) obj.get(STATUS);
//                    String message;
//                    String error_code;
//                    JSONArray obj_message;
//
//                    if (status.equals(SUCCESS)) {
//                        hideProgress();
//                        rate = (String) obj.get(RATE);
//                        rate_intent = rate;
//                        rate_toDouble = 1/Double.parseDouble(rate);
//
////                        Log.i(TAG, "rate_double: "  + rate_toDouble);
////                        showToast_short("rate_double: "  + rate_toDouble);
//
//                        /*
//                        rate_double: 0.16286644951140067
//                        rate_big: 0.162866449511400660
//
//                        rate_double: 0.16286644951140067
//                        rate_big: 0.162866449511400660
//                        * */
//
////                        BigDecimal big_one = new BigDecimal(1);
////                        BigDecimal big_rate = new BigDecimal(Double.parseDouble(rate));
////                        BigDecimal big_result = big_one.divide(big_rate,18, RoundingMode.UP);
//                        //big_result.setScale());
//
////                        Log.i(TAG, "rate_big: "  + big_result);
////                        showToast_short("rate_big: "  + big_result);
//
//                        /*double scale = Math.pow(10, 18);
//                        return Math.round(value * scale) / scale;*/
//
//                        /*// create 3 BigDecimal objects
//                        BigDecimal bg1, bg2, bg3;
//
//                        bg1 = new BigDecimal("16");
//                        bg2 = new BigDecimal("3");sssfsf
//
//                        // divide bg1 with bg2
//                        // 0 specifies BigDecimal.ROUND_UP
//                        bg3 = bg1.divide(bg2, 0);*/
//
//                        Log.i(TAG,
//                                "rate_ToDouble: " + rate_toDouble
//                        );
//
//                        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
//                        formatter.applyPattern("###,##0.00");
//
//                        //textView_current_conversion_rate_value.setText(getResources().getString(R.string.label_current_conversion_rate_value, String.valueOf(formatter.format(rate_toDouble))));
//                        initAPIGetWalletDetails();
//
//                    } else if (status.equals(FAIL)) {
//                        hideProgress();
//                        //error_code = (String) obj.get(ERROR_CODE);
//                        try{
//                            message = (String) obj.get(MESSAGE);
//
//                            if(message.equalsIgnoreCase("No Data Found.")){
//
//                            } else {
//                                showDialog_defaultError(getResources().getString(R.string.dialog_default_error_title), message);
//                                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", message, 0, getResources().getString(R.string.button_go_back), "");
//                            }
//
//                            //showToast_short(message + " (" + error_code + ")");
//                        } catch(JSONException | IllegalArgumentException | ClassCastException s) {
//                            obj_message = obj.getJSONArray(MESSAGE);
//                            message =  obj_message.toString().replace("[", "").replace("\"", "").replace("]", "").trim();
//                            //showToast_short("Message is in Array: " + message);
//                            String[] array_message = message.split(",");
//                            for (String string: array_message) {
//                                Log.i(TAG, "Message Array: " + string);
//                                //showToast_short(string + " (" + error_code + ")");
//                                if(string.equalsIgnoreCase("No Data Found.")){
//
//                                } else {
//                                    showDialog_defaultError(getResources().getString(R.string.dialog_default_error_title), string);
//                                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", string, 0, getResources().getString(R.string.button_go_back), "");
//                                }
//                                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", string, 0, getResources().getString(R.string.button_go_back), "");
//                                break;
//                            }
//                        }
//                    } else {
//                        hideProgress();
//                        showDialog_connectTimeOut(String.valueOf(response.code()));
//                        //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_something_went_wrong), 0, getResources().getString(R.string.button_go_back), "");
//                        //showToast_short(getResources().getString(R.string.error_something_went_wrong));
//                    }
//
//
//                } catch (JSONException | IOException e) {
//                    hideProgress();
//                    e.printStackTrace();
//                    Writer writer = new StringWriter();
//                    e.printStackTrace(new PrintWriter(writer));
//
//                    //progressDialog.dismiss();
//                    hideProgress();
//                    showDialog_connectTimeOut(String.valueOf(response.code()));
//                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, e.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
//                    //showToast_long(getResources().getString(R.string.error_connection_with_message, e.getMessage()));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
//                    hideProgress();
//                    showDialog_connectTimeOut(String.valueOf(t.getMessage()));
//                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, t.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
//                    //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
//                    //finish();
//                    return;
//                }
//
//                hideProgress();
//                Log.i(TAG, "onFailure: " + t.getMessage());
//                t.printStackTrace();
//                showDialog_connectTimeOut(String.valueOf(t.getMessage()));
//                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, t.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
//                //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
//                //finish();
//            }
//        });
//    }
//
//    private void initAPIGetWalletDetails() {
//        Timber.d(TAG,"initAPIGetWalletDetails() triggered");
//        showProgress();
//
//        Call<ResponseBody> call = apiInterface.getWalletDetails(
//                preference.getString(KEY_EMAIL, ""),
//                preference.getString(KEY_WALLET_CODE, ""),
//                getHash(API_ACCESS_CODE)
//        );
//
//        Log.i(TAG,
//                "Data: " +
//                        preference.getString(KEY_EMAIL, "") + " | " +
//                        preference.getString(KEY_WALLET_CODE, "")
//        );
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                hideProgress();
//                Log.i(TAG, "onResponse: " + response.toString());
//                Log.i(TAG,"Raw Url:" + response.raw().request().url());
//
//                if(response.body()==null)
//                {
//                    //progressDialog.dismiss();
//                    hideProgress();
//                    showDialog_connectTimeOut(String.valueOf(response.code()));
//                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, getResources().getString(R.string.error_response_body_null)), 0, getResources().getString(R.string.button_go_back), "");
//                    //showToast_long(getResources().getString(R.string.error_connection_with_message, getResources().getString(R.string.error_response_body_null)));
//                    return;
//                }
//
//                try{
//                    hideProgress();
//                    String responseString = response.body().string();
//                    Log.i(TAG, "Response Body: " + responseString);
//
//                    JSONObject obj = new JSONObject(responseString);
//                    String status = (String) obj.get(STATUS);
//                    String message;
//                    String error_code;
//                    JSONArray obj_message;
//
//                    if (status.equals(SUCCESS)) {
//
//                        JSONObject wallet_data = obj.getJSONObject(WALLET_DATA);
//                        String total_converted_edpoints = "" + (String) wallet_data.get(TOTAL_CONVERTED_EDPOINTS);
//
//                        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
//                        formatter.applyPattern("###,##0.00");
//
//                        edpoints_value = formatter.format(Double.valueOf(total_converted_edpoints));
//                        textView_edpoint_balance.setText(formatter.format(Double.valueOf(total_converted_edpoints)));
//
//                    } else if (status.equals(FAIL)) {
//                        error_code = (String) obj.get(ERROR_CODE);
//                        try{
//                            message = (String) obj.get(MESSAGE);
//                            if(error_code.equalsIgnoreCase("E-04") || error_code.equalsIgnoreCase("E-05")){
//                                showDialog_main_2("", REQUEST_TAG_GO_BACK_INVALID_LOGIN, true,true, false, getResources().getString(R.string.dialog_invalid_login), getResources().getString(R.string.dialog_invalid_login_message), R.drawable.ic_dialog_exclamation, "", getResources().getString(R.string.button_go_back));
//                            } else if (error_code.equalsIgnoreCase("E-06")) {
//                                showDialog_main(REQUEST_TAG_RESEND_UNVERIFIED_EMAIL, REQUEST_TAG_CANCEL_UNVERIFIED_EMAIL, true,true, false, getResources().getString(R.string.dialog_unverified_email), getResources().getString(R.string.dialog_unverified_email_message), R.drawable.ic_dialog_unverified_email, getResources().getString(R.string.button_resend), getResources().getString(R.string.button_cancel));
//                            } else {
//
//                                showDialog_defaultError(getResources().getString(R.string.dialog_default_error_title), message + " (" + error_code + ")");
//                                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", message + " (" + error_code + ")", 0, getResources().getString(R.string.button_go_back), "");
//                                //showToast_short(message + " (" + error_code + ")");
//                            }
//
//                            //showToast_short(message + " (" + error_code + ")");
//                        } catch(JSONException | IllegalArgumentException | ClassCastException s) {
//                            obj_message = obj.getJSONArray(MESSAGE);
//                            message =  obj_message.toString().replace("[", "").replace("\"", "").replace("]", "").trim();
//                            //showToast_short("Message is in Array: " + message);
//                            String[] array_message = message.split(",");
//                            for (String string: array_message) {
//                                Log.i(TAG, "Message Array: " + string);
//
//                                showDialog_defaultError(getResources().getString(R.string.dialog_default_error_title), string + " (" + error_code + ")");
//                                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", string + " (" + error_code + ")", 0, getResources().getString(R.string.button_go_back), "");
//                                //showToast_short(string + " (" + error_code + ")");
//                                break;
//                            }
//                        }
//                    } else {
//                        hideProgress();
//                        showDialog_connectTimeOut(String.valueOf(response.code()));
//                        //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_something_went_wrong), 0, getResources().getString(R.string.button_go_back), "");
//                        //showToast_short(getResources().getString(R.string.error_something_went_wrong));
//                    }
//
//
//                } catch (JSONException | IOException e) {
//                    hideProgress();
//                    e.printStackTrace();
//                    Writer writer = new StringWriter();
//                    e.printStackTrace(new PrintWriter(writer));
//
//                    //progressDialog.dismiss();
//                    hideProgress();
//                    showDialog_connectTimeOut(String.valueOf(response.code()));
//                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, e.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
//                    //showToast_long(getResources().getString(R.string.error_connection_with_message, e.getMessage()));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
//                    hideProgress();
//                    showDialog_connectTimeOut(String.valueOf(t.getMessage()));
//                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, t.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
//                    //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
//                    //finish();
//                    return;
//                }
//
//                hideProgress();
//                Log.i(TAG, "onFailure: " + t.getMessage());
//                t.printStackTrace();
//                showDialog_connectTimeOut(String.valueOf(t.getMessage()));
//                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, t.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
//                //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
//                //finish();
//            }
//        });
//    }

    private void initEDANumber() {
        String custId = preference.getString(KEY_CUSID);

        //showToast("custID: " + custId);

        //apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterfaceCIS.mainCustInfo(API_CUSTINFO, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if (response.body() == null) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String responseString = response.body().string();
                    JSONObject obj = new JSONObject(responseString);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        eda_number_intent = obj.getString(EDA);
                        //showToast("API Triggered: " + eda_number_value);

                        initAPIGetCrossBorderRate();

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initEDANumber();
            }
        });
    }

    private void initAPIGetCrossBorderRate() {
        Timber.d(TAG,"initAPIGetCrossBorderRate() triggered");
        showProgress();

        Call<ResponseBody> call = apiInterface.getCrossBorderRate(
                preference.getString(KEY_WALLET_COUNTRY_CODE, ""),
                eda_number_intent,
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );

        Log.i(TAG,
                "Data: " +
                        preference.getString(KEY_WALLET_COUNTRY_CODE, "") + " | " +
                        eda_number_intent
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                hideProgress();
                Log.i(TAG, "onResponse: " + response.toString());
                Log.i(TAG,"Raw Url:" + response.raw().request().url());

                if(response.body()==null)
                {
                    //progressDialog.dismiss();
                    hideProgress();
                    showToast(String.valueOf(response.code()));
                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, getResources().getString(R.string.error_response_body_null)), 0, getResources().getString(R.string.button_go_back), "");
                    //showToast_long(getResources().getString(R.string.error_connection_with_message, getResources().getString(R.string.error_response_body_null)));
                    return;
                }

                try{
                    hideProgress();
                    String responseString = response.body().string();
                    Log.i(TAG, "Response Body: " + responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);
                    String message;
                    String error_code;
                    JSONArray obj_message;

                    if (status.equals(SUCCESS)) {

                        //showToast(responseString);

                        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);
                        formatter.applyPattern("###,##0.00");

                        double rate = obj.getDouble(RATE);
                        rate_global = rate;

                        //set flag icon country from
                        int temp_position = 0;
                        for (String temp_countryCode : country_code_array){
                            if(preference.getString(KEY_WALLET_COUNTRY_CODE).equalsIgnoreCase(temp_countryCode)){
                                break;
                            } else  {
                                temp_position++;
                            }
                        }
                        imageView_flag_from.setImageResource(images_array.getResourceId(temp_position, -1));

                        //set flag icon country to
                        temp_position = 0;
                        for (String temp_countryCode : country_code_array){
                            if(preference.getString(KEY_COUNTRY_CODE).equalsIgnoreCase(temp_countryCode)){
                                break;
                            } else  {
                                temp_position++;
                            }
                        }
                        imageView_flag_to.setImageResource(images_array.getResourceId(temp_position, -1));

                        //set transfer rate
                        textView_transfer_rate_value.setText(getResources().getString(R.string.label_current_transfer_rate_value, "" + formatter.format(rate)));
                        //editText_edpoint_input.setText("0.00");
                        //editText_edpoint_input.setSelection(editText_edpoint_input.getText().length());

                        if(preference.getString(KEY_WALLET_COUNTRY_CODE).equalsIgnoreCase(preference.getString(KEY_COUNTRY_CODE))){
                            linearLayout_amount_to_transfer.setVisibility(View.GONE);
                        } else {
                            linearLayout_amount_to_transfer.setVisibility(View.VISIBLE);
                        }

                       /* JSONObject wallet_data = obj.getJSONObject(WALLET_DATA);
                        String total_converted_edpoints = "" + (String) wallet_data.get(TOTAL_CONVERTED_EDPOINTS);

                        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);*//* new DecimalFormat("###,###.##");*//*
                        formatter.applyPattern("###,##0.00");

                        edpoints_value = formatter.format(Double.valueOf(total_converted_edpoints));
                        textView_edpoint_balance.setText(formatter.format(Double.valueOf(total_converted_edpoints)));

                        initAPIGetCrossBorderRate();*/

                    } else if (status.equals(FAIL)) {
                        error_code = (String) obj.get(ERROR_CODE);
                        try{
                            message = (String) obj.get(MESSAGE);

                            showToast(message + " (" + error_code + ")");
                            //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", message + " (" + error_code + ")", 0, getResources().getString(R.string.button_go_back), "");
                            //showToast_short(message + " (" + error_code + ")");

                            //showToast_short(message + " (" + error_code + ")");
                        } catch(JSONException | IllegalArgumentException | ClassCastException s) {
                            obj_message = obj.getJSONArray(MESSAGE);
                            message =  obj_message.toString().replace("[", "").replace("\"", "").replace("]", "").trim();
                            //showToast_short("Message is in Array: " + message);
                            String[] array_message = message.split(",");
                            for (String string: array_message) {
                                Log.i(TAG, "Message Array: " + string);

                                showToast(string + " (" + error_code + ")");
                                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", string + " (" + error_code + ")", 0, getResources().getString(R.string.button_go_back), "");
                                //showToast_short(string + " (" + error_code + ")");
                                break;
                          }
                        }
                    } else {
                        hideProgress();
                        showToast(String.valueOf(response.code()));
                        //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_something_went_wrong), 0, getResources().getString(R.string.button_go_back), "");
                        //showToast_short(getResources().getString(R.string.error_something_went_wrong));
                    }


                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    //progressDialog.dismiss();
                    hideProgress();
                    showToast(String.valueOf(response.code()));
                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, e.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
                    //showToast_long(getResources().getString(R.string.error_connection_with_message, e.getMessage()));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){
                    hideProgress();
                    showToast(String.valueOf(t.getMessage()));
                    //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, t.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
                    //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
                    //finish();
                    return;
                }

                hideProgress();
                Log.i(TAG, "onFailure: " + t.getMessage());
                t.printStackTrace();
                showToast(String.valueOf(t.getMessage()));
                //showDialog_main(REQUEST_TAG_POP_UP_GO_BACK, "", true, false, false, "", getResources().getString(R.string.error_connection_with_message, t.getMessage()), 0, getResources().getString(R.string.button_go_back), "");
                //showToast_long(getResources().getString(R.string.error_connection_with_message, t.getMessage()));
                //finish();
            }
        });
    }

    private Double amount_input;

    private void initUpdateAmountToBeTransferred() {
        Observable<CharSequence> changeObservableSearch = RxTextView.textChanges(editText_edpoint_input);

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,##0.00");

        disposables.add(changeObservableSearch.map(inputText -> (inputText.length() == 0))
                .subscribe(isValid ->
                {
                    if(!isValid)
                    {
                        if(editText_edpoint_input.getText().toString().trim().equalsIgnoreCase(".")){
                            amount_input = Double.valueOf("0.00");
                            //editText_edpoint_input.setText(formatter.format(amount_input));
                        } else {
                            amount_input = Double.valueOf(editText_edpoint_input.getText().toString().trim().replace(",", ""));
                            //editText_edpoint_input.setText(formatter.format(amount_input));
                        }

                        convertValue(amount_input);
                    }
                    else
                    {
                        amount_input = 0.00;
                        //editText_edpoint_input.setText(formatter.format(amount_input));
                        convertValue(amount_input);
                    }
                }));
    }

    public void convertValue(Double amount_input){
        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,##0.00");

        if(amount_input != null) {
            if(editText_edpoint_input.getText().toString().equalsIgnoreCase("")){
                //editText_edpoint_input.setText("0.00");
                //editText_edpoint_input.setSelection(editText_edpoint_input.getText().length());
                textView_amount_to_transfer.setText(getResources().getString(R.string.label_amount_to_be_transferred, "" + formatter.format(amount_input), "" + formatter.format(amount_input)));

            } else {
                Double double_converted_value = amount_input * rate_global;
                //showToast("" + double_converted_value);
                converted_amount_intent = formatter.format(double_converted_value);
                textView_amount_to_transfer.setText(getResources().getString(R.string.label_amount_to_be_transferred, "" + formatter.format(amount_input), "" + formatter.format(double_converted_value)));
            }
        }
    }


    private void initPreviousActivity() {
        Intent intent;
        if(preference.getString(KEY_ENABLE_TIP_TRANSFER, "true").equalsIgnoreCase("true")){
            intent = new Intent(getContext(), TransferTipActivity.class);
            intent.putExtra(KEY_AUTO_CONVERTED_EP, edpoints_balance);
            startActivity(intent);
            finish();
        } else {
            intent = new Intent(getContext(), EDCoinActivity.class);
            startActivity(intent);
            finish();
        }

        /*intent = new Intent(getContext(), TransferTipActivity.class);
        intent.putExtra(KEY_AUTO_CONVERTED_EP, edpoints_balance);
        startActivity(intent);
        finish();*/
    }

    /*@Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        //showToast_short("Input: " + input_data);
    }*/

   /* @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
