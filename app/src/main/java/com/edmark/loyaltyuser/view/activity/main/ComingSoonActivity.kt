package com.edmark.loyaltyuser.view.activity.main

import android.os.Bundle
import android.util.Log

import com.bumptech.glide.Glide
import com.edmark.loyaltyuser.R
import com.edmark.loyaltyuser.base.BaseActivity
import com.google.firebase.FirebaseApp

import kotlinx.android.synthetic.main.activity_comming_soon.*

/*
 * Created by DEV-Michael-ED2E on 04/05/2018.
 */

class ComingSoonActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comming_soon)
        FirebaseApp.initializeApp(this)

        setupToolBar(R.id.activity_coming_soon_toolbar)
        initGUI()

    }

    override fun onToolbarBackPress() {
        super.onToolbarBackPress()
        finish()
    }

    private fun initGUI() {
        Log.d(TAG, "initGUI()")
        Glide.with(context)
                .load(R.drawable.ic_loading)
                .into(activity_coming_soon_imageview)
    }

    companion object {
        private val TAG = ComingSoonActivity::class.java.simpleName
    }

}
