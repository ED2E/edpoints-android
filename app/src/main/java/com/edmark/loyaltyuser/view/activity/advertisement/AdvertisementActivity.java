package com.edmark.loyaltyuser.view.activity.advertisement;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.BR;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.adapter.BaseRecyclerViewAdapter;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.Ads;
import com.edmark.loyaltyuser.presenter.advertisement.AdvertisementPresenter;
import com.edmark.loyaltyuser.presenter.advertisement.AdvertsimentContract;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_ADS_LIST;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_ADS_DETAILS;
import static com.edmark.loyaltyuser.constant.Common.KEY_COUNTRY_CODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 04/18/2018.
 */
@SuppressWarnings("unchecked")
public class AdvertisementActivity extends BaseActivity implements AdvertsimentContract.View{
    public static final String TAG = AdvertisementActivity.class.getSimpleName();

    @BindView(R.id.activity_advertisement_recyclerview_merchant)
    RecyclerView recyclerView_option;
    @BindView(R.id.activity_advertisement_toolbar_title)
    TextView textView_tootbar_title;
    @BindView(R.id.activity_advertisement_textview_message)
    TextView textView_message;

    private ArrayList<Ads> adsList = new ArrayList<>();
    private BaseRecyclerViewAdapter adapter;
    private AdvertsimentContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement);
        ButterKnife.bind(this);

        presenter = new AdvertisementPresenter(this);
        setupToolBar(R.id.activity_advertisement_toolbar);
        initGui();

    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGui() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        Gson gson = new Gson();
        adapter = new BaseRecyclerViewAdapter(adsList, R.layout.child_ads, BR.Ads, (v, item, position) ->
        {
            Intent intent = new Intent(getContext(), AdvertisementDetailsActivity.class);
            intent.putExtra(KEY_ADS_DETAILS, gson.toJson(adsList.get(position)));
            startActivity(intent);
        });
        recyclerView_option.setLayoutManager(linearLayoutManager);
        recyclerView_option.setAdapter(adapter);

        AppPreference preference = AppPreference.getInstance(getContext());
        String country_code = preference.getString(KEY_COUNTRY_CODE);
//        getAdvertisement(country_code);
        presenter.getAdvertisement(country_code);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void getAdvertisement(String companycode) {
        showProgress();
        adsList.clear();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getAds(API_ADS_LIST, companycode, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Timber.d("Raw Url:" + response.raw().request().url());
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);
                    if (status.equals(SUCCESS)) {

                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            Gson gson = new Gson();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Ads ads = gson.fromJson(jsonobject.toString(), Ads.class);
                                Timber.d("Ads list:" + gson.toJson(ads));
                                adsList.add(ads);
                                adapter.notifyDataSetChanged();
                                adapter.animateTo(adsList);
                            }
                            if (jsonarrayMain.length() != 0)
                                textView_message.setVisibility(View.GONE);
                            else
                                textView_message.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            hideProgress();
                            textView_message.setVisibility(View.VISIBLE);
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Timber.d("onFailure:");
                t.printStackTrace();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                dialog.dismiss();
                hideProgress();
            }
        });
    }

    @Override
    public void onLoadSuccess(ArrayList<Ads> adsList) {
        this.adsList.clear();
        this.adsList.addAll(adsList);
        adapter.notifyDataSetChanged();
        adapter.animateTo(this.adsList);
        if (adsList.size() != 0)
            textView_message.setVisibility(View.GONE);
        else
            textView_message.setVisibility(View.VISIBLE);
    }
}
