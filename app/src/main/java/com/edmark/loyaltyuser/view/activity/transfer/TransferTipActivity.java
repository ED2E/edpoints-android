package com.edmark.loyaltyuser.view.activity.transfer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.edcoin.EDCoinActivity;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_NAME;
import static com.edmark.loyaltyuser.constant.Common.KEY_AUTO_CONVERTED_EP;
import static com.edmark.loyaltyuser.constant.Common.KEY_DATE_TIME;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDP_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_ENABLE_TIP_TRANSFER;
import static com.edmark.loyaltyuser.constant.Common.KEY_REFERENCE_NO;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSFER_APP;
import static com.edmark.loyaltyuser.util.AppUtility.formatDate_MMMMDDYYYY;
import static com.edmark.loyaltyuser.util.AppUtility.formatTime_HHMMAAA;

public class TransferTipActivity extends BaseActivity {

    public static final String TAG = TransferTipActivity.class.getSimpleName();

    @BindView(R.id.activity_transfer_tip_imageView_checkbox)
    ImageView imageView_checkbox;
    @BindView(R.id.activity_transfer_tip_button_1)
    Button button_ok;

    private ApiInterface apiInterface;
    private AppPreference preference;

    private String auto_converted_ep = "";
    private boolean isChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_tip);
        ButterKnife.bind(this);
        preference = AppPreference.getInstance(this);
        apiInterface = ApiClientEDCoin.getClient("").create(ApiInterface.class);

        isChecked = false;
        imageView_checkbox.setImageResource(R.drawable.ic_tip_unchecked);

        setupToolBar(R.id.activity_transfer_tip_toolbar);
        initData(savedInstanceState);
        initGUI();
    }

    private void initData(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            auto_converted_ep = "";
        } else {
            auto_converted_ep = extras.getString(KEY_AUTO_CONVERTED_EP);
        }
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        Intent intent = new Intent(getContext(), EDCoinActivity.class);
        startActivity(intent);
        finish();
    }

    private void initGUI() {
        Log.d(TAG, "initGUI() triggered");


        imageView_checkbox.setOnClickListener(v -> {
            if(isChecked){
                isChecked = false;
                imageView_checkbox.setImageResource(R.drawable.ic_tip_unchecked);
                preference.putString(KEY_ENABLE_TIP_TRANSFER, "true");
            } else {
                isChecked = true;
                imageView_checkbox.setImageResource(R.drawable.ic_tip_checked);
                preference.putString(KEY_ENABLE_TIP_TRANSFER, "");
            }


        });

        button_ok.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), TransferMainActivity.class);
            intent.putExtra(KEY_AUTO_CONVERTED_EP, auto_converted_ep);
            startActivity(intent);
            finish();
        });
    }


    /*@Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        //showToast_short("Input: " + input_data);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Intent intent = new Intent(getContext(), EDCoinActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
