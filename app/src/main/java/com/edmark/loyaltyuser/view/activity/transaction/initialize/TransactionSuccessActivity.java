package com.edmark.loyaltyuser.view.activity.transaction.initialize;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CustomerInfo;
import com.edmark.loyaltyuser.model.CustomerProfile;
import com.edmark.loyaltyuser.model.FirebaseToken;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImageContract;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImagePresenter;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.glide.slider.library.svg.GlideApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_CUSTINFO;
import static com.edmark.loyaltyuser.constant.Common.API_MASSAGE_CHAIR_USAGE;
import static com.edmark.loyaltyuser.constant.Common.API_MERCHANT_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.CUSTID;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.EMAIL;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_TOKEN;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA;
import static com.edmark.loyaltyuser.constant.Common.KEY_EMAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_LOYALTY_EARNED;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERNAME;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_TRANSACTION_HISTORY;
import static com.edmark.loyaltyuser.service.FirebasePushNotif.pushNotificationDistributor;
import static com.edmark.loyaltyuser.service.FirebasePushNotif.pushNotificationMerchant;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


/*
 * Created by DEV-Michael-ED2E on 04/11/2018.
 */
public class TransactionSuccessActivity extends BaseActivity implements LoadImageContract {
    private static final String TAG = TransactionSuccessActivity.class.getSimpleName();


    @BindView(R.id.activity_transaction_success_button_ok)
    Button button_proceed;
    @BindView(R.id.activity_transaction_success_textview_type)
    TextView textView_type;
    @BindView(R.id.activity_transaction_success_textview_name)
    TextView textView_name;
    @BindView(R.id.activity_transaction_success_textview_transaction_amount)
    TextView textView_amount;
    @BindView(R.id.activity_transaction_success_textview_loyalty_point_earned)
    TextView textView_loyalty_earned;
    @BindView(R.id.activity_transaction_success_textview_description)
    TextView textView_description;
    @BindView(R.id.activity_transaction_success_imageview_profile)
    ImageView imageView_profile;
    @BindView(R.id.activity_transaction_success_progress)
    ProgressBar progressbar_profile;
    @BindView(R.id.activity_transaction_success_textview_transaction_id)
    TextView textView_transactionID;

    private LoadImagePresenter imageLoader;

    private ApiInterface apiService;
    private AppPreference preference;
    private String merchant_name;
    private String transaction_amount;
    private String transaction_id;
    private String loyalty_earned;
    private String description = "";
    private String merchantid = "";
    private String eda = "";
    private String email = "";
    private String imageurl = "";

    private String biller_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_success);
        ButterKnife.bind(this);

        imageLoader = new LoadImagePresenter(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        preference = AppPreference.getInstance(this);

        loadData(savedInstanceState);
        initGUI();

    }

    private void loadData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchant_name = "";
                transaction_amount = "";
                transaction_id = "";
                loyalty_earned = "";
                description = "";
                merchantid = "";
                eda = "";
                email = "";
            } else {
                merchant_name = extras.getString(KEY_MERCHANT);
                transaction_amount = extras.getString(KEY_TRANSACTION_AMOUNT);
                transaction_id = extras.getString(KEY_TRANSACTION_ID);
                loyalty_earned = extras.getString(KEY_LOYALTY_EARNED);
                description = extras.getString(KEY_DESCRIPTION);

                merchantid = extras.getString(KEY_MERCHANT_ID);
                eda = extras.getString(KEY_EDA);
                email = extras.getString(KEY_EMAIL);
            }
        } else {
            merchant_name = (String) savedInstanceState.getSerializable(KEY_MERCHANT);
            transaction_amount = (String) savedInstanceState.getSerializable(KEY_TRANSACTION_AMOUNT);
            transaction_id = (String) savedInstanceState.getSerializable(KEY_TRANSACTION_ID);
            loyalty_earned = (String) savedInstanceState.getSerializable(KEY_LOYALTY_EARNED);
            description = (String) savedInstanceState.getSerializable(KEY_DESCRIPTION);

            merchantid = (String) savedInstanceState.getSerializable(KEY_MERCHANT_ID);
            eda = (String) savedInstanceState.getSerializable(KEY_EDA);
            email = (String) savedInstanceState.getSerializable(KEY_EMAIL);

        }
    }

    private void initGUI() {

        if(description != null) {
            if(description.toUpperCase().contains("UTILITY PAYMENT")){
                textView_type.setText("Biller:");
                biller_name = merchant_name;
                textView_transactionID.setVisibility(View.GONE);
            } else {
                biller_name = "";
            }
        } else {
           finish();
        }



        textView_name.setText(merchant_name);
        textView_amount.setText(transaction_amount);
        textView_loyalty_earned.setText(loyalty_earned);
        textView_description.setText(description);
        textView_transactionID.setText(getResources().getString(R.string.label_transaction_id,transaction_id));

        button_proceed.setOnClickListener(v ->
        {
            if (description != null) {
                if (description.contains(getResources().getString(R.string.label_massage_chair))) {
                    getUsage();
                }
                else
                {
                    preference.putString(KEY_TRANSACTION_ID, "");
                    preference.putString(KEY_MERCHANT_ID, "");
                    Intent intent = new Intent(TransactionSuccessActivity.this, RatingReviewActivity.class);
                    intent.putExtra(KEY_TRANSACTION_ID, transaction_id);
                    intent.putExtra(KEY_MERCHANT_ID, merchantid);
                    intent.putExtra(KEY_UTILITY_BILLERNAME, biller_name);
                    startActivity(intent);
                    finish();
                }
            } else {
                finish();
            }

        });


//        initLoadAccountInfo();
        initMerchant();
        initFirebasedb(merchantid, eda, email);
    }

    private void initFirebasedb(String custId, String eda, String email) {
        //Load merchant token
        ArrayList<String> merchantToken = new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(FIREBASE_KEY_TOKEN)
                .whereEqualTo(CUSTID, custId)
                .whereEqualTo(EDA, eda)
                .whereEqualTo(EMAIL, email)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            Timber.d(document.getId() + " => " + document.getData());

                        }
                        //direct document
                        for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                            if (doc.get(CUSTID) != null) {
                                Timber.d("FirebaseToken:" + doc.getString(CUSTID) + " => " + doc.getString(EDA));
                            }
                        }

                        if (task.getResult() != null) {
                            List<FirebaseToken> tokens = task.getResult().toObjects(FirebaseToken.class);
                            for (FirebaseToken item : tokens) {
                                merchantToken.add(item.getToken());
                            }
                        }

                        Timber.d("notificationToken:" + merchantToken);
                        String msg = getResources().getString(R.string.payment_successful, transaction_id, transaction_amount);
                        new Thread(() -> pushNotificationMerchant(custId, merchantToken, msg, "", imageurl, "", TAG_NOTIF_TRANSACTION_HISTORY)).start();
                    } else {
                        Timber.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    private void initLoadAccountInfo() {
        String custId = preference.getString(KEY_CUSID);
        showProgress();
        Call<ResponseBody> call = apiService.mainCustInfo(API_CUSTINFO, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerInfo info = gson.fromJson(json, CustomerInfo.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());

                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        //Load distributor token
                        ArrayList<String> distributorToken = new ArrayList<>();
                        db.collection(FIREBASE_KEY_TOKEN)
                                .whereEqualTo(CUSTID, custId)
                                .whereEqualTo(EDA, info.getEda())
                                .whereEqualTo(EMAIL, info.getEmail())
                                .get()
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        for (DocumentSnapshot document : task.getResult()) {
                                            Timber.d(document.getId() + " => " + document.getData());

                                        }
                                        //direct document
                                        for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                                            if (doc.get(CUSTID) != null) {
                                                Timber.d("FirebaseToken:" + doc.getString(CUSTID) + " => " + doc.getString(EDA));
                                            }
                                        }

                                        if (task.getResult() != null) {
                                            List<FirebaseToken> tokens = task.getResult().toObjects(FirebaseToken.class);
                                            for (FirebaseToken item : tokens) {
                                                distributorToken.add(item.getToken());
                                            }
                                        }

                                        Timber.d("notificationToken:" + distributorToken);
                                        String msg = getResources().getString(R.string.payment_successful, transaction_id, transaction_amount);
                                        new Thread(() -> pushNotificationDistributor(custId, distributorToken, msg, "", imageurl, "", TAG_NOTIF_TRANSACTION_HISTORY)).start();

                                    } else {
                                        Timber.w(TAG, "Error getting documents.", task.getException());
                                    }
                                });
//                        initMerchant();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo();
                hideProgress();
            }
        });
    }

    private void initMerchant() {
        //for loading merchant image
        if(description!=null) {
            if (description.contains(getResources().getString(R.string.label_massage_chair))) {
                GlideApp.with(Objects.requireNonNull(getContext()))
                        .load(R.drawable.ic_massage)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .onlyRetrieveFromCache(false)
                        .placeholder(R.drawable.ic_shimmer_user)
                        .error(R.drawable.ic_default_user)
                        .listener(new RequestListener<Drawable>() {

                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                progressbar_profile.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                progressbar_profile.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(imageView_profile);
                initLoadAccountInfo();
            } else {
                getMerchantProfile();
            }
        }
        else
        {
            getMerchantProfile();
        }
    }

    private void getMerchantProfile()
    {
        Call<ResponseBody> call = apiService.mainCustInfo(API_MERCHANT_PROFILE, merchantid, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if (response.body() == null) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfile info = gson.fromJson(json, CustomerProfile.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());
                        progressbar_profile.setVisibility(View.VISIBLE);
                        imageurl = info.getImage_url();

                        imageLoader.loadImage(getContext(), TAG, 0, imageurl, imageView_profile, R.drawable.ic_card_ordinary, true);
                        initLoadAccountInfo();
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initMerchant();
            }
        });
    }

    private void getUsage() {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.mainCustInfo(
                API_MASSAGE_CHAIR_USAGE,
                custId,
                String.valueOf(unixTimeStamp()),
                getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        hideProgress();
                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            Timber.d("jsonarrayMain:" + jsonarrayMain.get(jsonarrayMain.length() - 1).toString());
                            Intent in = new Intent(getContext(), TransactionWebviewActivity.class);
                            in.putExtra(KEY_URL, jsonarrayMain.get(jsonarrayMain.length() - 1).toString());
                            in.putExtra(KEY_TITLE, getResources().getString(R.string.label_massage_chair_merchant));
                            startActivity(in);
                        } catch (JSONException e) {
                            Timber.d("DeviceID is not active");
                            String message = (String) obj.get(MESSAGE);
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    } else {
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    hideProgress();
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("validateTransaction onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    @Override
    public void onLoadfailed(int requestCode) {
        if (progressbar_profile != null)
            progressbar_profile.setVisibility(View.GONE);
    }

    @Override
    public void onResourceready(int requestCode) {
        if (progressbar_profile != null)
            progressbar_profile.setVisibility(View.GONE);
    }

}
