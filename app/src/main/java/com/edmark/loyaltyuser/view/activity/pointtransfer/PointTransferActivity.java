package com.edmark.loyaltyuser.view.activity.pointtransfer;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.CustomerProfile;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImageContract;
import com.edmark.loyaltyuser.presenter.imageLoader.LoadImagePresenter;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.ui.imageview.CircleImageView;
import com.edmark.loyaltyuser.ui.slideview.SlideView;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.util.ThousandTextWatcher;
import com.edmark.loyaltyuser.view.activity.transaction.initialize.TransactionSuccessActivity;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.SocketTimeoutException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_MERCHANT_PROFILE;
import static com.edmark.loyaltyuser.constant.Common.API_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.API_UPDATE_TRANSACTION_CSB;
import static com.edmark.loyaltyuser.constant.Common.API_VALIDATE_TRANSACTION;
import static com.edmark.loyaltyuser.constant.Common.CSB;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_DESCRIPTION;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDA;
import static com.edmark.loyaltyuser.constant.Common.KEY_EMAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_LOYALTY_EARNED;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT;
import static com.edmark.loyaltyuser.constant.Common.KEY_MERCHANT_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_SCAN_PROCESS;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_AMOUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.KEY_TRANS_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.RESPONSE_TRANSACTION_ID;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.playBeep;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


/*
 * Created by DEV-Michael-ED2E on 04/10/2018.
 */
public class PointTransferActivity extends BaseActivity {
    private static final String TAG = PointTransferActivity.class.getSimpleName();


    @BindView(R.id.activity_point_transfer_button_slide_to_pay)
    SlideView button_slide;
    @BindView(R.id.activity_point_transfer_button_cancel)
    Button button_abort;
    @BindView(R.id.activity_point_transfer_editText_edpoints_amount)
    EditText editText_edpoints_amount;
    @BindView(R.id.activity_point_transfer_editText_ed2e_number)
    EditText editText_ed2e_number;
    @BindView(R.id.activity_point_transfer_layout_conversion_history)
    LinearLayout layout_conversion_history;


    private ApiInterface apiService;
    private ApiInterface apiService2;
    private AppPreference preference;
    private String merchantid;
    private String eda;
    private String email;
    private Dialog dialog;
    private String amount;
    private String description;
    private String scan_process;
    private boolean isTransact = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_transfer);
        ButterKnife.bind(this);

        setupToolBar(R.id.activity_point_transfer_toolbar);
        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        apiService2 = ApiClientEDCoin.getClient("").create(ApiInterface.class);

        merchantid = "1354881";

        //initLoadType(savedInstanceState);
        initLoadAccountInfo();
        initGUI();
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    /*private void initLoadType(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                merchantid = null;
                amount = null;
                description = null;
                scan_process = null;
            } else {
                merchantid = extras.getString(KEY_TRANS_CUSTID);
                amount = extras.getString(KEY_TRANSACTION_AMOUNT);
                description = extras.getString(KEY_DESCRIPTION);
                scan_process = extras.getString(KEY_SCAN_PROCESS);

            }
        } else {
            merchantid = (String) savedInstanceState.getSerializable(KEY_TRANS_CUSTID);
            amount = (String) savedInstanceState.getSerializable(KEY_TRANSACTION_AMOUNT);
            description = (String) savedInstanceState.getSerializable(KEY_DESCRIPTION);
            scan_process = (String) savedInstanceState.getSerializable(KEY_SCAN_PROCESS);
        }
    }*/

    private void initGUI() {
        Log.d(TAG,"initGUI");
//        editText_amount.Currency = Currency.NONE;
//        editText_amount.Spacing = false;
//        editText_amount.Decimals = false;

        editText_edpoints_amount.addTextChangedListener(new ThousandTextWatcher(editText_edpoints_amount));

        button_slide.setOnFinishListener(() -> {
                    if (!editText_edpoints_amount.getText().toString().equals("")) {
                        initPinDialog();
                    } else {
                        button_slide.reset();
                        showToast(getResources().getString(R.string.error_invalid_amount));
                    }
                }
        );

        button_abort.setOnClickListener(v -> finish());

        layout_conversion_history.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), PointTransferHistoryActivity.class);
            startActivity(intent);

        });

        if(amount!=null)
        {
            if(!amount.equals(""))
            {
                editText_edpoints_amount.setText(amount);
                editText_edpoints_amount.setEnabled(false);
                editText_ed2e_number.setText(description);
                editText_ed2e_number.setEnabled(false);
            }
        }
    }

    private void initPinDialog() {
        Timber.d("initPinDialog");
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_input);

        Button button_ok = dialog.findViewById(R.id.dialog_input_button_ok);
        Button button_dismiss = dialog.findViewById(R.id.dialog_input_button_cancel);
        EditText editText_pin = dialog.findViewById(R.id.dialog_input_edittext);
        TextView textView_message = dialog.findViewById(R.id.dialog_input_textview_message);

        editText_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        textView_message.setText(getResources().getString(R.string.dialog_insert_pin));
        button_ok.setText(getResources().getString(R.string.button_proceed));
        button_ok.setOnClickListener(v -> {
            if (editText_pin.getText().length() < 4) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_pin), Toast.LENGTH_SHORT).show();
            } else {
                if(!isTransact) {
                    isTransact = true;

                    String pin = editText_pin.getText().toString();
                    String amount = editText_edpoints_amount.getText().toString().replace(",", "");
                    String description = editText_ed2e_number.getText().toString() + " - EDPoints Transfer";

                    validateTransaction(pin, amount, description);

                    button_slide.reset();
                    dialog.dismiss();
                }
            }
        });
        button_dismiss.setOnClickListener(v ->
        {
            button_slide.reset();
            dialog.dismiss();
        });

        dialog.setOnDismissListener(dialog -> {
            button_slide.reset();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void validateTransaction(String pin, String amount, String description) {
        Timber.d("validateTransaction");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.validateTransaction(
                API_VALIDATE_TRANSACTION,
                custId,
                merchantid,
                getHash(pin),
                amount,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
//                        getTransactionID();
                        String transactionid = (String) obj.get(RESPONSE_TRANSACTION_ID);

                        //Log.e("CHECK", scan_process + "RESPONSE: " + response.toString());

                        //Transaction Process (ExpressPay)
                        initTransaction(transactionid,description);
                        //showToast("initTransferEDPoints()");
                        //initTransaction(transactionid,description);
                        //initCheckRefNumStatus(transactionid);
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
                button_slide.reset();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                //initLoadAccountInfo();
                hideProgress();
            }
        });
    }

    private void initLoadAccountInfo() {
        Timber.d("initLoadAccountInfo");
        showProgress();
        Call<ResponseBody> call = apiService.mainCustInfo(API_MERCHANT_PROFILE, merchantid, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerProfile info = gson.fromJson(json, CustomerProfile.class);
                        Timber.d("CustomerInfo:" + info.getName() + " " + info.getEda());
                        eda = info.getEda();
                        email = info.getEmail();
                        //name = info.getName();
                        //textView_name.setText(info.getName());

                        //progressbar_profile.setVisibility(View.VISIBLE);
                        //imageLoader.loadImage(getContext(), TAG, 0, info.getImage_url(), imageView_profile, R.drawable.ic_default_user, true);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                initLoadAccountInfo();
                hideProgress();
            }
        });
    }

    private void initTransaction(String transactionid, String description) {
        Timber.d("initTransaction");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService.transaction(
                API_TRANSACTION,
                custId,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp()),
                merchantid,
                transactionid,
                editText_edpoints_amount.getText().toString().replace(",", ""),
                description);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));

                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
//                        isTransact = false;
//                        playBeep(getContext(),"transaction.mp3");

                        //showToast("TRANSFER SUCCESS");

                        initAPITransferEDPoints(transactionid);
                        /*Intent intent = new Intent(PointTransferActivity.this, PointTransferSuccessActivity.class);

                        intent.putExtra(KEY_TRANSACTION_ID, transactionid);
                        double trans_amount = Double.valueOf(editText_edpoints_amount.getText().toString().replace(",", "").trim());
                        intent.putExtra(KEY_TRANSACTION_AMOUNT, String.valueOf(trans_amount));
                        intent.putExtra(KEY_DESCRIPTION, editText_ed2e_number.getText().toString().trim());

                        intent.putExtra(KEY_MERCHANT_ID, merchantid);
                        intent.putExtra(KEY_EDA, eda);
                        intent.putExtra(KEY_EMAIL, email);
                        startActivity(intent);
                        hideProgress();

                        finish();*/
                    } else {
                        isTransact = false;
                        hideProgress();
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();

                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }

    private void initAPITransferEDPoints(String transactionid) {
        Timber.d("initAPITransferEDPoints");
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        Call<ResponseBody> call = apiService2.transferEDPoints(
                custId,
                editText_ed2e_number.getText().toString().trim(),
                editText_edpoints_amount.getText().toString().trim().replace(",", ""),
                getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`"));

       /* Log.i(TAG,
                "Data: " +
                        custId + " | " +
                        editText_ed2e_number.getText().toString().trim() + " | " +
                        editText_edpoints_amount.getText().toString().trim().replace(",", "") + " | " +
                        getHash("`_`3Dm@rK`4dm1n`ConV3rs10n`_`")
        );*/

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    isTransact = false;
                    showToast(getResources().getString(R.string.error_connection));

                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);
                    JSONArray obj_message;

                    if (status.equals(SUCCESS)) {
                        hideProgress();
                        isTransact = false;
                        playBeep(getContext(),"transaction.mp3");

                        //showToast("TRANSFER PHP SIDE SUCCESS");

                        Intent intent = new Intent(PointTransferActivity.this, PointTransferSuccessActivity.class);

                        intent.putExtra(KEY_TRANSACTION_ID, transactionid);
                        double trans_amount = Double.valueOf(editText_edpoints_amount.getText().toString().replace(",", "").trim());
                        intent.putExtra(KEY_TRANSACTION_AMOUNT, String.valueOf(trans_amount));
                        intent.putExtra(KEY_DESCRIPTION, editText_ed2e_number.getText().toString().trim());

                        intent.putExtra(KEY_MERCHANT_ID, merchantid);
                        intent.putExtra(KEY_EDA, eda);
                        intent.putExtra(KEY_EMAIL, email);
                        startActivity(intent);
                        hideProgress();

                        finish();
                    } else {
                        isTransact = false;
                        String message;
                        hideProgress();

                        try{
                            message = (String) obj.get(MESSAGE);
                            showToast(message);

                        } catch(JSONException | IllegalArgumentException | ClassCastException s) {
                            obj_message = obj.getJSONArray(MESSAGE);
                            message =  obj_message.toString().replace("[", "").replace("\"", "").replace("]", "").trim();
                            //showToast_short("Message is in Array: " + message);
                            String[] array_message = message.split(",");
                            for (String string: array_message) {
                                Log.i(TAG, "Message Array: " + string);
                                showToast(string);
                                break;
                            }
                        }

                       /* String message = (String) obj.get(MESSAGE);
                        //String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), message , Toast.LENGTH_SHORT).show();
*/
                    }
                } catch (IOException | JSONException e) {
                    isTransact = false;
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransact = false;
                if(t instanceof SocketTimeoutException){
                    showToast(t.getMessage());
                    hideProgress();
                    return;
                }

                Timber.d("onFailure:");
                t.printStackTrace();
                hideProgress();
            }
        });
    }


    private void initTransactionCSB(String custid, String docno){
        Timber.d("initTransactionCSB");
        showProgress();
        Call<ResponseBody> call = apiService.cashBillTrans(
                API_UPDATE_TRANSACTION_CSB,
                custid,
                docno,
                getVCKeyED2E(),
                String.valueOf(unixTimeStamp())
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("Raw Url:" + response.raw().request().url());

//                Log.i("tagconvertstr", "["+response+"]");
//                Log.i("tagconvertstr", "["+response.body()+"]");
//                Log.i("tagconvertstr", "["+response.body().toString()+"]");
//                Log.i("tagconvertstr", "["+response.message()+"]");

                if (response.body() == null) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body().string());
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);

                    if (status.equals("Success")) {
                        isTransact = false;
                        playBeep(getContext(),"transaction.mp3");
                        Intent intent = new Intent(PointTransferActivity.this, TransactionSuccessActivity.class);
                        //intent.putExtra(KEY_MERCHANT, textView_name.getText().toString());
                        double trans_amount = Double.valueOf(editText_edpoints_amount.getText().toString().replace(",", "").trim());
                        float loyalty_earned = (float) (trans_amount / 100d);
                        String transactionID = (String) obj.get(CSB);

                        intent.putExtra(KEY_TRANSACTION_ID, transactionID);
                        intent.putExtra(KEY_TRANSACTION_AMOUNT, String.valueOf(trans_amount));
                        intent.putExtra(KEY_LOYALTY_EARNED, String.valueOf(loyalty_earned));
                        intent.putExtra(KEY_DESCRIPTION, description);
                        intent.putExtra(KEY_MERCHANT_ID, merchantid);
                        intent.putExtra(KEY_EDA, eda);
                        intent.putExtra(KEY_EMAIL, email);
                        startActivity(intent);
                        hideProgress();
                        finish();
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        hideProgress();

                        Dialog dialog = new Dialog(getContext(), R.style.DialogTheme);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        if (dialog.getWindow() != null)
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.dialog_main);

                        TextView textView_message = dialog.findViewById(R.id.dialog_main_textview_message);
                        Button button_ok = dialog.findViewById(R.id.dialog_main_button_ok);
                        Button button_cancel = dialog.findViewById(R.id.dialog_main_button_cancel);

                        textView_message.setText(message);
                        button_ok.setText(getResources().getString(R.string.button_ok));

                        button_cancel.setVisibility(View.GONE);

                        button_ok.setOnClickListener(v -> {
                            dialog.dismiss();
                            finish();
                        });
                        dialog.show();

                        //showDialog(0, message,true);
                        //Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();

                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));
                    Log.i("TEST", "e.printStackTrace(): " + writer.toString());
                    Log.i("TEST", " e.getMessage(): " + e.getMessage());


                    hideProgress();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                Timber.d(TAG, "onFailure:");
                t.printStackTrace();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }
}
