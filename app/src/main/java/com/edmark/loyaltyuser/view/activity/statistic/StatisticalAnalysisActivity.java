package com.edmark.loyaltyuser.view.activity.statistic;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.DateRange;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_STATISTICS_ANALYSIS;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT_MONTH;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TAG_STAT_LAST_MONTH;
import static com.edmark.loyaltyuser.constant.Common.TAG_STAT_LAST_WEEK;
import static com.edmark.loyaltyuser.constant.Common.TAG_STAT_TODAY;
import static com.edmark.loyaltyuser.constant.Common.TAG_STAT_TOTAL;
import static com.edmark.loyaltyuser.constant.Common.TAG_STAT_YESTERDAY;
import static com.edmark.loyaltyuser.constant.Common.TOTAL_SALES;
import static com.edmark.loyaltyuser.util.AppUtility.getFormattedDate;
import static com.edmark.loyaltyuser.util.AppUtility.getTimeinMilli;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;


/*
 * Created by DEV-Michael-ED2E on 04/25/2018.
 */
public class StatisticalAnalysisActivity extends BaseActivity {
    public static final String TAG = StatisticalAnalysisActivity.class.getSimpleName();

    @BindView(R.id.activity_statistical_analysis_textview_today)
    TextView textView_today;
    @BindView(R.id.activity_statistical_analysis_textview_yesterday)
    TextView textView_yesterday;
    @BindView(R.id.activity_statistical_analysis_textview_last_week)
    TextView textView_last_week;
    @BindView(R.id.activity_statistical_analysis_textview_last_month)
    TextView textView_last_month;
    @BindView(R.id.activity_statistical_analysis_textview_total)
    TextView textView_total;
    @BindView(R.id.activity_statistical_analysis_chart)
    LineChartView chart;

    private ApiInterface apiService;
    private AppPreference preference;
    private ArrayList<DateRange> dateRanges = new ArrayList<>();
    private ArrayList<DateRange> chartdateRanges = new ArrayList<>();
    private int index = 0;
    private int chartIndex = 0;
    private boolean isChartDataFinished = false;
    private boolean isTotalFinished = false;
    private int maxSales = 0;
    private Calendar cal;

    private LineChartData data;

    private ValueShape shape = ValueShape.CIRCLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistical_analysis);
        ButterKnife.bind(this);

        preference = AppPreference.getInstance(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        setupToolBar(R.id.activity_statistical_analysis_toolbar);
        initGUI();
        initDateRange();
        initChartSource();
        initStatisticAPI(dateRanges.get(index).getDateFrom(), dateRanges.get(index).getDateTo());
    }

    @Override
    protected void onToolbarBackPress() {
        super.onToolbarBackPress();
        finish();
    }

    private void initGUI() {
        chart.setLineChartData(data);
        resetViewport();
        chart.setViewportCalculationEnabled(false);
    }

    private void initDateRange() {

        //Total
        dateRanges.add(new DateRange(TAG_STAT_TOTAL, "", getFormattedDate(new Date(), DATE_FORMAT), 0));
        //Today
        dateRanges.add(new DateRange(TAG_STAT_TODAY, getFormattedDate(new Date(), DATE_FORMAT), getFormattedDate(new Date(), DATE_FORMAT), 0));

        //yesterday
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        dateRanges.add(new DateRange(TAG_STAT_YESTERDAY, getFormattedDate(cal.getTime(), DATE_FORMAT), getFormattedDate(cal.getTime(), DATE_FORMAT), 0));

        //last week
        cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        cal.add(Calendar.DAY_OF_YEAR, -7);

        Date lastweekDateFrom = null;
        Date lastweekDateTo = null;
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                lastweekDateFrom = cal.getTime();
            } else if (i == 6) {
                lastweekDateTo = cal.getTime();
            }
            cal.add(Calendar.DAY_OF_WEEK, 1);
        }
        dateRanges.add(new DateRange(TAG_STAT_LAST_WEEK, getFormattedDate(lastweekDateFrom, DATE_FORMAT), getFormattedDate(lastweekDateTo, DATE_FORMAT), 0));

        //last month
        cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, -1);
        Date monthDateFrom = cal.getTime();
        int max = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, max);
        dateRanges.add(new DateRange(TAG_STAT_LAST_MONTH, getFormattedDate(monthDateFrom, DATE_FORMAT), getFormattedDate(cal.getTime(), DATE_FORMAT), 0));
    }

    private void initChartSource() {

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        cal = Calendar.getInstance();
        int max = cal.get(Calendar.DATE);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        for (int i = 0; i < max; i++) {
            Timber.i(TAG, sdf.format(cal.getTime()));
            chartdateRanges.add(new DateRange(String.valueOf(i + 1), getFormattedDate(cal.getTime(), DATE_FORMAT), getFormattedDate(cal.getTime(), DATE_FORMAT), 0));
            cal.add(Calendar.DAY_OF_WEEK, 1);
        }
    }

    public void initStatisticAPI(String dateFrom, String dateTo) {
        showProgress();
        String custId = preference.getString(KEY_CUSID);
        String dateFrominMilli = "0";
        String dateToinMilli = "0";
        try {
            if (!dateFrom.equals(""))
                dateFrominMilli = getTimeinMilli(dateFrom, DATE_FORMAT);
            dateToinMilli = getTimeinMilli(dateTo, DATE_FORMAT);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Call<ResponseBody> call = apiService.statistics(API_STATISTICS_ANALYSIS, custId, dateFrominMilli, dateToinMilli, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {

                        String sales = (String) obj.get(TOTAL_SALES);
                        if (index != dateRanges.size() - 1) {
                            dateRanges.get(index).setTotal_sales(Integer.valueOf(sales));
                            index++;
                            initStatisticAPI(dateRanges.get(index).getDateFrom(), dateRanges.get(index).getDateTo());
                            isTotalFinished = false;
                        } else {
                            if (!isTotalFinished) {
                                dateRanges.get(index).setTotal_sales(Integer.valueOf(sales));
                                isTotalFinished = true;
                            }

                            if (chartIndex != chartdateRanges.size()) {
                                if (chartIndex != 0) {
                                    chartdateRanges.get(chartIndex - 1).setTotal_sales(Integer.valueOf(sales));
                                }
                                initStatisticAPI(chartdateRanges.get(chartIndex).getDateFrom(), chartdateRanges.get(chartIndex).getDateTo());
                                chartIndex++;
                                isChartDataFinished = false;
                            } else {
                                chartdateRanges.get(chartIndex - 1).setTotal_sales(Integer.valueOf(sales));
                                isChartDataFinished = true;
                            }
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        Toast.makeText(getContext(), getResources().getString(R.string.label_error_code, message, errorCode), Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (index == (dateRanges.size() - 1) && chartIndex == chartdateRanges.size() && isChartDataFinished) {
                    Gson gson = new Gson();
                    for (DateRange date : chartdateRanges) {
                        Timber.d("test chartdateRanges " + gson.toJson(date));
                        if (maxSales <= date.getTotal_sales()) {
                            maxSales = date.getTotal_sales();
                        }
                    }
                    for (DateRange date : dateRanges) {
                        Timber.d("test date " + gson.toJson(date));
                        switch (date.getTag()) {
                            case TAG_STAT_TOTAL:
                                textView_total.setText(String.valueOf(date.getTotal_sales()));
                                break;
                            case TAG_STAT_TODAY:
                                textView_today.setText(String.valueOf(date.getTotal_sales()));
                                break;
                            case TAG_STAT_YESTERDAY:
                                textView_yesterday.setText(String.valueOf(date.getTotal_sales()));
                                break;
                            case TAG_STAT_LAST_WEEK:
                                textView_last_week.setText(String.valueOf(date.getTotal_sales()));
                                break;
                            case TAG_STAT_LAST_MONTH:
                                textView_last_month.setText(String.valueOf(date.getTotal_sales()));
                                break;
                        }
                    }
                    maxSales = maxSales + 5; //add some leeway or margin
                    hideProgress();
                    generateData();
                    resetViewport();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    showToast(getResources().getString(R.string.error_connection));
                    hideProgress();
                    return;
                }

                hideProgress();
                Timber.d("onFailure:");
                t.printStackTrace();
            }
        });
    }

    private void resetViewport() {
        final Viewport v = new Viewport(chart.getMaximumViewport());
        v.bottom = 0;
        v.top = maxSales;
        v.left = 0;
        v.right = chartdateRanges.size();
        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }

    private void generateData() {

        List<Line> lines = new ArrayList<>();
        int numberOfLines = 1;
        for (int i = 0; i < numberOfLines; ++i) {

            List<PointValue> values = new ArrayList<>();
            for (int j = 0; j < chartdateRanges.size(); ++j) {
                values.add(new PointValue(j, chartdateRanges.get(j).getTotal_sales()));
            }

            Line line = new Line(values);
            line.setColor(ChartUtils.COLORS[i]);
            line.setShape(shape);
            line.setCubic(true);
            line.setFilled(true);
            line.setHasLabels(false);
            line.setHasLabelsOnlyForSelected(false);
            line.setHasLines(true);
            line.setHasPoints(true);
            lines.add(line);
        }

        data = new LineChartData(lines);

        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(true);
        axisX.setName(getResources().getString(R.string.label_month_or_day));
        List<AxisValue> values = new ArrayList<>();
        for (int i = 0; i < chartdateRanges.size(); i++) {
            AxisValue value = new AxisValue(i);
            if (i == 0) {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_MONTH, Locale.getDefault());
                Calendar cal = Calendar.getInstance();
                String label = sdf.format(cal.getTime()) + " " + (i + 1);
                value.setLabel(label);
            } else {
                value.setLabel(String.valueOf(i + 1));
            }
            values.add(value);
        }
        axisX.setValues(values);
        axisX.setTextColor(Color.BLACK);

        axisY.setName(getResources().getString(R.string.label_number_of_transaction));
        values = new ArrayList<>();
        for (int i = 0; i < maxSales; i++) {
            AxisValue value = new AxisValue(i);
            value.setLabel(String.valueOf(i));
            values.add(value);
        }
        axisY.setValues(values);
        axisY.setTextColor(Color.BLACK);
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        chart.setLineChartData(data);

    }
}
