package com.edmark.loyaltyuser.rest;

import com.edmark.loyaltyuser.model.Utility;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/*
 * Created by DEV-Michael-ED2E on 04/02/2018.
 */
public interface ApiInterface {

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> login(@Query("page") String page,
                             @Query("type") String type,
                             @Query("username") String username,
                             @Query("password") String password,
                             @Query("date") String date,
                             @Query("vc") String vc);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> mainCustInfo(@Query("page") String page,
                                    @Query("custid") String custid,
                                    @Query("date") String date,
                                    @Query("vc") String vc);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> getCustCode(@Query("page") String page,
                                   @Query("eda") String eda,
                                   @Query("date") String date,
                                   @Query("vc") String vc);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> transaction(@Query("page") String page,
                                   @Query("custid") String custid,
                                   @Query("vc") String vc,
                                   @Query("date") String date,
                                   @Query("merchantid") String merchantid,
                                   @Query("transactionid") String transactionid,
                                   @Query("point") String point,
                                   @Query("description") String description);

    @POST("index.php")
    Call<ResponseBody> account_signup_upgrade(@Query("page") String page,
                                              @Query("custid") String custid,
                                              @Query("serial_no") String serial_no,
                                              @Query("activate_code") String activate_code,
                                              @Query("vc") String vc,
                                              @Query("date") String date);

    @POST("index.php")
    Call<ResponseBody> change_pass(@Query("page") String page,
                                   @Query("custid") String custid,
                                   @Query("oldpassword") String oldpassword,
                                   @Query("newpassword") String newpassword,
                                   @Query("newpassword2") String newpassword2,
                                   @Query("date") String date,
                                   @Query("vc") String vc);

    @POST("index.php")
    Call<ResponseBody> change_pin(@Query("page") String page,
                                  @Query("custid") String custid,
                                  @Query("oldpin") String oldpassword,
                                  @Query("newpin") String newpassword,
                                  @Query("newpin2") String newpassword2,
                                  @Query("date") String date,
                                  @Query("vc") String vc);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> transactions(@Query("page") String page,
                                    @Query("custid") String custid,
                                    @Query("type") String type,
                                    @Query("datefrom") String datefrom,
                                    @Query("dateto") String dateto,
                                    @Query("vc") String vc,
                                    @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> sendreview(@Query("page") String page,
                                  @Query("custid") String custid,
                                  @Query("merchantid") String merchantid,
                                  @Query("review") int review,
                                  @Query("transactionid") String transactionid,
                                  @Query("remark") String remark,
                                  @Query("vc") String vc,
                                  @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> getmerchant(@Query("page") String page,
                                   @Query("date") String date,
                                   @Query("vc") String vc);


    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> statistics(@Query("page") String page,
                                  @Query("custid") String custid,
                                  @Query("datefrom") String datefrom,
                                  @Query("dateto") String dateto,
                                  @Query("vc") String vc,
                                  @Query("date") String date);


    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> validateTransaction(@Query("page") String page,
                                           @Query("custid") String custid,
                                           @Query("merchantid") String merchantid,
                                           @Query("pinno") String pinno,
                                           @Query("totalamt") String totalamt,
                                           @Query("vc") String vc,
                                           @Query("date") String date);


    @POST("api/getOfflineMerchants.php")
    Call<ResponseBody> offlineMerchants(@Query("vc") String vc,
                                        @Query("date") String date);

    @POST("api/getOnlineMerchants.php")
    Call<ResponseBody> onlineMerchants(@Query("vc") String vc,
                                       @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> getAds(@Query("page") String page,
                              @Query("companycode") String companycode,
                              @Query("date") String date,
                              @Query("vc") String vc);


    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> edaProfileUpdate(@Query("page") String page,
                                        @Query("custid") String custid,
                                        @Query("name") String name,
                                        @Query("email") String email,
                                        @Query("phoneno") String phoneno,
                                        @Query("dob") String dob,
                                        @Query("vc") String vc,
                                        @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> merchantProfileUpdate(@Query("page") String page,
                                             @Query("custid") String custid,
                                             @Query("name") String name,
                                             @Query("websiteurl") String websiteurl,
                                             @Query("email") String email,
                                             @Query("phoneno") String phoneno,
                                             @Query("dob") String dob,
                                             @Query("vc") String vc,
                                             @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> signup(@Query("page") String page,
                              @Query("eda") String eda,
                              @Query("name") String name,
                              @Query("email") String email,
                              @Query("phone") String phone,
                              @Query("serial_no") String serial_no,
                              @Query("activate_code") String activate_code,
                              @Query("pass") String pass,
                              @Query("pin") String pin,
                              @Query("type") String type,
                              @Query("dob") String dob,
                              @Query("vc") String vc,
                              @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> forgotapi(@Query("page") String page,
                                 @Query("email") String email,
                                 @Query("vc") String vc,
                                 @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> massageChairStatus(@Query("page") String page,
                                          @Query("custid") String custid,
                                          @Query("did") String did,
                                          @Query("vc") String vc,
                                          @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> massageTransaction(@Query("page") String page,
                                          @Query("custid") String custid,
                                          @Query("vc") String vc,
                                          @Query("date") String date,
                                          @Query("merchantid") String merchantid,
                                          @Query("transactionid") String transactionid,
                                          @Query("point") String point,
                                          @Query("did") String did,
                                          @Query("chairperiod") String chairperiod);

    @Multipart
    @POST("index.php?page=Solucis:apiCurl_upload")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Part("uploadedfile") RequestBody name,
                                   @Part("vc") RequestBody vc,
                                   @Part("date") RequestBody date,
                                   @Part("custid") RequestBody custid);

    @POST("index.php")
    Call<ResponseBody> insert_pin(@Query("page") String page,
                                  @Query("custid") String custid,
                                  @Query("pin") String pin,
                                  @Query("date") String date,
                                  @Query("vc") String vc);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> edaVerification(@Query("page") String page,
                                       @Query("custcode") String custcode,
                                       @Query("vc") String vc,
                                       @Query("date") String date);

    @POST("index.php")
//    @FormUrlEncoded
    Call<ResponseBody> cashBillTrans(@Query("page") String page,
                                    @Query("custid") String custid,
                                    @Query("docno") String docno,
                                    @Query("vc") String vc,
                                    @Query("date") String date);

    @FormUrlEncoded
    @POST("signup.php")
    Call<ResponseBody> registerMerchant(
            @Field("country") String country,
            @Field("individualname") String individualname,
            @Field("individualaddress") String individualaddress,
            @Field("tradername") String tradername,
            @Field("businessaddress") String businessaddress,
            @Field("productservice") String productservice,
            @Field("tin") String tin,
            @Field("bankname") String bankname,
            @Field("branch") String branch,
            @Field("accountname") String accountname,
            @Field("accountnumber") String accountnumber,
            @Field("techname") String techname,
            @Field("technumber") String technumber,
            @Field("openinghours") String openinghours,
            @Field("website") String website,
            @Field("techemail") String techemail,
            @Field("description") String description,
            @Field("location") String location
    );

//    @POST("signup.php")
//    Call<ResponseBody> registerMerchant(@Body String s);


//    @POST(".php")
//    Call<List<Model>> getInfo();

    @POST("index.php")
    Call<ResponseBody> getFamilyTree(
            @Query("page") String page,
            @Query("custid") String custid,
            @Query("vc") String vc,
            @Query("date") String date);

    //ExpressPay API - Retrieve Billers
    @POST("epi/ext/api/transaction/connection/params/retrieve_billers/&id={id}")
    Call<List<Utility>> retrieveBillers(@Path("id") String id);

    //ExpressPay API - Submit Bills Payment
    @POST("epi/ext/api/transaction/connection/params/submit_bills_payment/&id={id}")
    @FormUrlEncoded
    Call<ResponseBody> submitBillsPayment(@Path("id") String id,
                                          @Field("biller_id") String biller_id,
                                          @Field("account_name") String account_name,
                                          @Field("account_number") String account_number,
                                          @Field("remarks") String remarks,
                                          @Field("amount") String amount,
                                          @Field("book_id") String book_id,
                                          @Field("due_date") String due_date,
                                          @Field("bill_month") String bill_month,
                                          @Field("reference_number") String reference_number
    );

    //ExpressPay API - Check Reference Number Status
    @POST("epi/ext/api/transaction/connection/params/check_reference_number_status/&id={id}")
    @FormUrlEncoded
    Call<ResponseBody> checkReferenceNumberStatus(@Path("id") String id,
                                                  @Field("reference_number") String reference_number
    );



    @POST("index.php")
    @FormUrlEncoded
    Call<ResponseBody> validatePinCoin(@Field("page") String page,
                                       @Field("custid") String custid,
                                       @Field("vc") String vc,
                                       @Field("date") String date,
                                       @Field("pinno") String pinno);



    //EDCoin API - Retrieve Wallet Balance
    @GET("api/wallet/balance")
    Call<ResponseBody> retrieveWalletBalance(@Query("wallet_code") String wallet_code);

    //EDCoin API - Check Coin Rate
    @POST("api/check/coin/rate")
    @FormUrlEncoded
    Call<ResponseBody> checkCoinRate(
            @Field("custid") String custid,
            @Field("company_code") String company_code,
            @Field("access_code") String access_code
    );

    //EDCoin API - Check EDCoin to EDPoint
    @POST("/api/check/edpoint/total")
    @FormUrlEncoded
    Call<ResponseBody> checkEDCoinToEDPoint(
            @Field("custid") String custid,
            @Field("company_code") String company_code,
            @Field("coinamount") double coinamount,
            @Field("access_code") String access_code
    );

    //EDCoin API - Check Wallet Code
    @POST("/api/check/walletcode")
    @FormUrlEncoded
    Call<ResponseBody> checkWalletCode(
            @Field("wallet_code") String wallet_code,
            @Field("access_code") String access_code
    );

    //EDCoin API - EDCoin Conversion
    @POST("/api/convert/edcoin")
    @FormUrlEncoded
    Call<ResponseBody> convertEDCoin(
            @Field("custid") String custid,
            @Field("rate") String rate,
            @Field("coinamount") String coinamount,
            @Field("country_code") String country_code,
            @Field("wallet_code") String wallet_code,
            @Field("access_code") String access_code
    );

    //EDCoin API - Get Transaction Logs
    @POST("/api/get/transaction/logs")
    @FormUrlEncoded
    Call<ResponseBody> getTransactionLogs(
            @Field("wallet_code") String wallet_code,
            @Field("access_code") String access_code
    );

    //EDCoin API - Transfer EDPoints
    @POST("api/ed2e/migrate/points")
    @FormUrlEncoded
    Call<ResponseBody> transferEDPoints(
            @Field("edp_id") String edp_id ,
            @Field("ed2e_id") String ed2e_id,
            @Field("edp_amount") String edp_amount,
            @Field("access_code") String access_code
    );

    //EDCoin API - Get Transfer Logs
    @POST("api/ed2e/migrate/logs")
    @FormUrlEncoded
    Call<ResponseBody> getTransferLogs(
            @Field("edp_id") String edp_id,
            @Field("date_from") String date_from,
            @Field("date_to") String date_to,
            @Field("access_code") String access_code
    );

    //EDCOIN API - Transfer Converted EP
    @POST("/api/edpoints/app/transfer/converted/points")
    @FormUrlEncoded
    Call<ResponseBody> transferConvertedEP(
            @Field("app_type") String app_type,
            @Field("wallet_code") String wallet_code,
            @Field("custid") String custid,
            @Field("eda_number") String eda_number,
            @Field("amount") String amount,
            @Field("rate") String rate,
            @Field("access_code") String access_code
    );

    //EDCOIN API - Get Country Code
    @POST("/api/edpoints/app/wallet/country/code")
    @FormUrlEncoded
    Call<ResponseBody> getCountryCode(
            @Field("wallet_code") String wallet_code,
            @Field("access_code") String access_code
    );

    //EDCOIN API - Get Transfer Logs
    @POST("/api/edpoints/app/transfer/logs")
    @FormUrlEncoded
    Call<ResponseBody> getTransferConvertedEPLogs(
            @Field("wallet_code") String wallet_code,
            @Field("date_from") String date_from,
            @Field("date_to") String date_to,
            @Field("access_code") String access_code
    );

    //EDCOIN API - Get Cross Border Rate
    @POST("/api/edc/app/cross/border/rate")
    @FormUrlEncoded
    Call<ResponseBody> getCrossBorderRate(
            @Field("from_country") String from_country,
            @Field("eda_number") String eda_number,
            @Field("access_code") String access_code
    );

}
