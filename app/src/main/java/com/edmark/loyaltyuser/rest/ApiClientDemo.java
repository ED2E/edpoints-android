package com.edmark.loyaltyuser.rest;

import com.edmark.loyaltyuser.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClientDemo {
//TODO this is for demo only
    private static final String BASE_URL;
    private static Retrofit retrofit = null;

    static {
//        if(BuildConfig.DEBUG) {
//            /* Staging */
//            BASE_URL = "http://183.81.166.237/ed2e/";
//        } else {
//            /* Production */
            BASE_URL = "http://183.81.166.231/edmark/";
//        }
    }

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .addConverterFactory(JSONConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
