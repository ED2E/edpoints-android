package com.edmark.loyaltyuser.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClientUploadTesting {
//TODO this is for demo only
    private static final String BASE_URL;
    private static Retrofit retrofit = null;

    static {
//        if(BuildConfig.DEBUG) {
//            /* Staging */
//            BASE_URL = "http://183.81.166.237/ed2e/";
//        } else {
//            /* Production */
            BASE_URL = "https://cube-canvasser.000webhostapp.com/";
//        }
    }

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .addConverterFactory(JSONConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
