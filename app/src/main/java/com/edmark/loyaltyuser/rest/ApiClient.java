package com.edmark.loyaltyuser.rest;

import com.edmark.loyaltyuser.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    public static final String BASE_URL;
    private static Retrofit retrofit = null;

    static {
        if(BuildConfig.DEBUG) {
            /* Staging */
            BASE_URL = "http://183.81.166.237/ed2e/";
        } else {
            /* Production */
            BASE_URL = "http://183.81.166.237/edmark/";
        }
    }

    public static Retrofit getClient() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .addConverterFactory(JSONConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
