package com.edmark.loyaltyuser.rest;

import com.edmark.loyaltyuser.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientExpressPay {

    private static final String BASE_URL;
    private static Retrofit retrofit;

    static {
//        if(BuildConfig.DEBUG) {
//            // for staging
//            BASE_URL = "";
//        } else {
//            // for production
        BASE_URL = "http://34.192.201.218/";
//        }
    }

    public static Retrofit getClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        httpClient.addInterceptor(new NetworkInterceptors());

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static class NetworkInterceptors implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {

            Request request = chain.request();
            Request newRequest;

            newRequest = request.newBuilder()
                    .addHeader("Authorization", "Basic ZXhwcmVzc3BheTozeHByM3Nz")
                    .addHeader("EPI-API-USERNAME", "testEdmark")
                    .addHeader("EPI-API-AUTHKEY", "ccafef1a7876d0fcf0395b19adade450204e5f96")
                    .build();

            return chain.proceed(newRequest);
        }
    }
}
