package com.edmark.loyaltyuser.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by DEV-Michael-ED2E on 02/04/2018.
 */

public class BaseRecyclerViewAdapter<T> extends RecyclerView.Adapter<BaseRecyclerViewAdapter.BindingHolder> {
    public static final String TAG = BaseRecyclerViewAdapter.class.getSimpleName();

    private final List<T> items;
    private int layoutId;
    private int brId;
    public ItemClickListener<T> listener;
    private BaseRecyclerViewSectionedAdapter mSectionedAdapter;

    public interface ItemClickListener<T> {

        void onItemClick(View v, T item, int position);
    }

    public class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ViewDataBinding binding;

        public BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
            v.setOnClickListener(this);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            if (mSectionedAdapter != null) {
                if (position != RecyclerView.NO_POSITION && !mSectionedAdapter.isSectionHeaderPosition(position))
                {
                    //For click if there is a header but this is for child only
                    position = mSectionedAdapter.sectionedPositionToPosition(position);
                    T item = items.get(position);
                    listener.onItemClick(v, item, position);
                }
            } else {
                try {
                    T item = items.get(position);
                    listener.onItemClick(v, item, position);
                    Log.w(TAG, "item:" + item);
                } catch (ArrayIndexOutOfBoundsException e) {
                    Log.w(TAG, "Still Refreshing the view. not ready to be clicked");
                    e.printStackTrace();
                } catch (IndexOutOfBoundsException e) {
                    Log.w(TAG, "Header Count must be missing");
                    e.printStackTrace();
                }
            }
        }
    }

    public BaseRecyclerViewAdapter(List<T> items, int layoutId, int brId, ItemClickListener<T> listener) {
        this.items = new ArrayList<>(items);
        this.layoutId = layoutId;
        this.brId = brId;
        this.listener = listener;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new BindingHolder(v);
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewAdapter.BindingHolder holder, int position) {
        T item = items.get(position);
        holder.getBinding().setVariable(brId, item);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void animateTo(List<T> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<T> newModels) {
        for (int i = items.size() - 1; i >= 0; i--) {
            final T model = items.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<T> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final T model = newModels.get(i);
            if (!items.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<T> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final T model = newModels.get(toPosition);
            final int fromPosition = items.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public T removeItem(int position) {
        final T model = items.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, T model) {
        items.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final T model = items.remove(fromPosition);
        items.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void clear() {
        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void setSectionAdapter(BaseRecyclerViewSectionedAdapter mSectionedAdapter) {

        this.mSectionedAdapter = mSectionedAdapter;
    }
}
