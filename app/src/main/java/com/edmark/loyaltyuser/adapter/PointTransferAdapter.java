package com.edmark.loyaltyuser.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.PointTransfer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PointTransferAdapter extends RecyclerView.Adapter<PointTransferAdapter.MyViewHolder> {

    Context context;
    List<PointTransfer> modelList;
    int counter;

    public PointTransferAdapter(Context context, List<PointTransfer> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    public void setModelList(List<PointTransfer> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PointTransferAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_point_transfer_history, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PointTransferAdapter.MyViewHolder myViewHolder, int position) {

        /*myViewHolder.tv.setText(
                "downline_totaldownlines: " + modelList.get(position).getDownline_totaldownlines() + "\n" +
                "downline_custid: " + modelList.get(position).getDownline_custid() + "\n" +
                "downline_custserialno: " + modelList.get(position).getDownline_custserialno() + "\n" +
                "downline_custeda: " + modelList.get(position).getDownline_custeda() + "\n" +
                "downline_custname: " + modelList.get(position).getDownline_custname() + "\n" +
                "downline_custtype: " + modelList.get(position).getDownline_custtype() + "\n" +
                "downline_custjoindate: " + modelList.get(position).getDownline_custjoindate()
        );*/

        /*
        TextView textview_name, textView_direct_downline, textView_contributed_value, textView_edc_purchased_value, textview_direct_downline, textView_bonus_gained_value, textView_package_type;
        ImageView imageView_package_type, imageView_check_downline;
        * */

        switch(modelList.get(position).getStatus()) {
            case "0":
                myViewHolder.textView_status.setText("PROCESSING");
                myViewHolder.textView_status.setTextColor(context.getResources().getColor(R.color.status_processing_color));
                break;
            case "1":
                myViewHolder.textView_status.setText("PROCESSED");
                myViewHolder.textView_status.setTextColor(context.getResources().getColor(R.color.status_processed_color));
                break;
            case "2":
                myViewHolder.textView_status.setText("CANCELLED");
                myViewHolder.textView_status.setTextColor(context.getResources().getColor(R.color.status_cancelled_color));
                break;

            default:
                myViewHolder.textView_status.setText("");
                myViewHolder.textView_status.setTextColor(context.getResources().getColor(R.color.colorBlack));
                break;
        }

        //myViewHolder.textView_status.setText(modelList.get(position).getStatus());
        myViewHolder.textView_amount.setText(modelList.get(position).getEdp_amount());

        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = modelList.get(position).getCreated_date();

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));


            //srcDf.setTimeZone(TimeZone.getTimeZone("GMT"));
            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);


            DateFormat destDf = new SimpleDateFormat("MMM-dd-yy");
            DateFormat destTf = new SimpleDateFormat("hh:mm a");

            // format the date into another format
            dateStr = destDf.format(date);
            timeStr = destTf.format(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        myViewHolder.textView_date.setText(dateStr);
        myViewHolder.textView_time.setText(timeStr);
        myViewHolder.textView_ed2e_num.setText(modelList.get(position).getEd2e_id());
        myViewHolder.textView_transaction_num.setText(modelList.get(position).getId());

       /* myViewHolder.textView_name.setText("" + modelList.get(position).getName());
        myViewHolder.textView_direct_downline.setText("" + modelList.get(position).getChildren());
        myViewHolder.textView_contributed_value.setText("" + modelList.get(position).getContributions());
        myViewHolder.textView_edc_purchased_value.setText("" + modelList.get(position).getPurchased_edc());
        myViewHolder.textView_bonus_gained_value.setText("" + modelList.get(position).getReferral_bonus());
        myViewHolder.textView_package_type.setText("" + modelList.get(position).getMembership());

        myViewHolder.imageView_check_downline.setImageResource(R.drawable.ic_right_double_arrow_white);

        switch(modelList.get(position).getMembership()){
            case GOLD:
                myViewHolder.imageView_package_type.setImageResource(R.drawable.bg_circle_gold_solid);
                break;
            case SILVER:
                myViewHolder.imageView_package_type.setImageResource(R.drawable.bg_circle_silver_solid);
                break;
            case BRONZE:
                myViewHolder.imageView_package_type.setImageResource(R.drawable.bg_circle_bronze_solid);
                break;
            case ORDINARY:
                myViewHolder.imageView_package_type.setImageResource(R.drawable.bg_circle_ordinary_solid);
                break;
            default:
                myViewHolder.imageView_package_type.setImageResource(R.drawable.bg_circle_ordinary_solid);
                break;
        }

            myViewHolder.imageView_check_downline.setOnClickListener(v -> {

                //Toast.makeText(v.getContext(), "@" + position + " | " + modelList.get(position).getLevel(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent (v.getContext(), FamilyTreeActivity.class);
                intent.putExtra(KEY_LEVEL, modelList.get(position).getLevel());
                intent.putExtra(KEY_CHILD_ID, modelList.get(position).getChild_id());

                context.startActivity(intent);
                try {
                    ((Activity) context).finish();
                } catch (ClassCastException e) {
                    Log.d("FamilyTreeActivity", "Error Starting Activity: " + e);
                    //Toast.makeText(v.getContext(), "Error Starting Activity: " + e, Toast.LENGTH_SHORT).show();
                }
            });*/
    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textView_status, textView_amount, textView_date, textView_time, textView_ed2e_num, textView_transaction_num;

        public MyViewHolder(View itemView) {
            super(itemView);

            textView_status = itemView.findViewById(R.id.child_point_transfer_textView_status);
            textView_amount = itemView.findViewById(R.id.child_point_transfer_textView_amount);
            textView_date = itemView.findViewById(R.id.child_point_transfer_textView_date);
            textView_time = itemView.findViewById(R.id.child_point_transfer_textView_time);
            textView_ed2e_num = itemView.findViewById(R.id.child_point_transfer_textView_ed2e_num);
            textView_transaction_num = itemView.findViewById(R.id.child_point_transfer_textView_transaction_num);
        }
    }
}
