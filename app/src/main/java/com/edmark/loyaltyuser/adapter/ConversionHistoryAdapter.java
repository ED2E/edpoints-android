package com.edmark.loyaltyuser.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.EDCoin;
import com.edmark.loyaltyuser.model.Utility;
import com.edmark.loyaltyuser.view.activity.utility.TransactionUtilityActivity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERID;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERNAME;

public class ConversionHistoryAdapter extends RecyclerView.Adapter<ConversionHistoryAdapter.MyViewHolder> {

    Context context;
    List<EDCoin> modelList;

    public ConversionHistoryAdapter(Context context, List<EDCoin> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    public void setModelList(List<EDCoin> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ConversionHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_conversion_history, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Calendar cal = Calendar.getInstance();
        //TimeZone tz = cal.getTimeZone();

        /* debug: is it local time? */
        //Log.d("Time Zone: ", tz.getDisplayName());

        myViewHolder.textview_reference_number.setText(modelList.get(i).getReference_no());

        Double edc_double = Double.parseDouble(modelList.get(i).getEdc_amount());
        Double edp_double = Double.parseDouble(modelList.get(i).getEdp_amount());

        DecimalFormat formatter1 = new DecimalFormat("###,##0.000000000000000000");
        DecimalFormat formatter2 = new DecimalFormat("###,##0.00");

        myViewHolder.textview_textview_edc.setText("-EDC " + formatter1.format(edc_double));
        myViewHolder.textview_textview_edp.setText("+EP " + formatter2.format(edp_double));

        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = modelList.get(i).getCreated_date();

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));


            //srcDf.setTimeZone(TimeZone.getTimeZone("GMT"));
            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);


            DateFormat destDf = new SimpleDateFormat("MMM-dd-yyyy");
            DateFormat destTf = new SimpleDateFormat("hh:mm a");

            // format the date into another format
            dateStr = destDf.format(date);
            timeStr = destTf.format(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        myViewHolder.textview_textview_date.setText(dateStr);
        myViewHolder.textview_textview_time.setText(timeStr.toUpperCase().replace(".", ""));
        myViewHolder.textview_description.setText(modelList.get(i).getDescription());

        //myViewHolder.textview_textview_time.setText(modelList.get(i).);

        /*myViewHolder.linearLayout.setOnClickListener(v -> {
            Log.e("CLICKED REMOVE", "" + modelList.get(i).getBillerID());
            modelList.remove(i);
            notifyItemRemoved(i);
            notifyItemRangeChanged(i, modelList.size());
            notifyDataSetChanged();

            Toast.makeText(v.getContext(), "@" + i + " | " + modelList.get(i).getReference_no(), Toast.LENGTH_SHORT).show();
            //Log.e("CLICKED", "@" + i);
        });*/
    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textview_reference_number;
        TextView textview_textview_edc;
        TextView textview_textview_edp;
        TextView textview_textview_date;
        TextView textview_textview_time;
        TextView textview_description;

        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            textview_reference_number = itemView.findViewById(R.id.child_conversion_history_textview_reference_number);
            textview_textview_edc = itemView.findViewById(R.id.child_conversion_history_textview_edc);
            textview_textview_edp = itemView.findViewById(R.id.child_conversion_history_textview_edp);
            textview_textview_date = itemView.findViewById(R.id.child_conversion_history_textview_date);
            textview_textview_time = itemView.findViewById(R.id.child_conversion_history_textview_time);
            textview_description = itemView.findViewById(R.id.child_conversion_history_textview_desc);

            linearLayout = itemView.findViewById(R.id.child_conversion_history_linearlayout);
        }
    }

}
