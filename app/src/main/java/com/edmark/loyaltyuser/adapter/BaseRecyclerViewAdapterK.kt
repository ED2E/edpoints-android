package com.edmark.loyaltyuser.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

import java.util.ArrayList

/*
 * Created by DEV-Michael-ED2E on 02/04/2018.
 */

class BaseRecyclerViewAdapterK<T>(items: List<T>, private val layoutId: Int, private val brId: Int, var listener: ItemClickListener<T>) :
        RecyclerView.Adapter<BaseRecyclerViewAdapterK.BindingHolder>() {

    val items: MutableList<T>

    interface ItemClickListener<T> {

        fun onItemClick(v: View, item: T, position: Int)
    }

    class BindingHolder(v: View) : RecyclerView.ViewHolder(v) {
        val binding: ViewDataBinding
        var view: View

        init {
            binding = DataBindingUtil.bind(v)!!
            view = v
        }
    }

    init {
        this.items = ArrayList(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): BindingHolder {
        val v = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return BindingHolder(v)
    }

    override fun onBindViewHolder(holder: BindingHolder, position: Int) {
        val item = items[position]
        holder.binding.setVariable(brId, item)
        holder.binding.executePendingBindings()
        holder.view.setOnClickListener({ listener.onItemClick(holder.view, item, position) })
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun animateTo(models: List<T>) {
        applyAndAnimateRemovals(models)
        applyAndAnimateAdditions(models)
        applyAndAnimateMovedItems(models)
    }

    private fun applyAndAnimateRemovals(newModels: List<T>) {
        for (i in items.indices.reversed()) {
            val model = items[i]
            if (!newModels.contains(model)) {
                removeItem(i)
            }
        }
    }

    private fun applyAndAnimateAdditions(newModels: List<T>) {
        var i = 0
        val count = newModels.size
        while (i < count) {
            val model = newModels[i]
            if (!items.contains(model)) {
                addItem(i, model)
            }
            i++
        }
    }

    private fun applyAndAnimateMovedItems(newModels: List<T>) {
        for (toPosition in newModels.indices.reversed()) {
            val model = newModels[toPosition]
            val fromPosition = items.indexOf(model)
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition)
            }
        }
    }

    fun removeItem(position: Int): T {
        val model = items.removeAt(position)
        notifyItemRemoved(position)
        return model
    }

    fun addItem(position: Int, model: T) {
        items.add(position, model)
        notifyItemInserted(position)
    }

    fun moveItem(fromPosition: Int, toPosition: Int) {
        val model = items.removeAt(fromPosition)
        items.add(toPosition, model)
        notifyItemMoved(fromPosition, toPosition)
    }

    fun clear() {
        val size = this.items.size
        if (size > 0) {
            for (i in 0 until size) {
                this.items.removeAt(0)
            }
            this.notifyItemRangeRemoved(0, size)
        }
    }
}

