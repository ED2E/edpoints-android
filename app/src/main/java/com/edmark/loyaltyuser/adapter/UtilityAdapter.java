package com.edmark.loyaltyuser.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.Utility;
import com.edmark.loyaltyuser.view.activity.familytree.FamilyTreeActivity;
import com.edmark.loyaltyuser.view.activity.utility.TransactionUtilityActivity;

import java.util.List;

import static com.edmark.loyaltyuser.constant.Common.KEY_FAMILY_TREE_COUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERID;
import static com.edmark.loyaltyuser.constant.Common.KEY_UTILITY_BILLERNAME;

public class UtilityAdapter extends RecyclerView.Adapter<UtilityAdapter.MyViewHolder> {

    Context context;
    List<Utility> modelList;

    public UtilityAdapter(Context context, List<Utility> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    public void setModelList(List<Utility> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UtilityAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_utility, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UtilityAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.tvBillerID.setText(""+modelList.get(i).getBillerID());
        myViewHolder.tvBillerName.setText(modelList.get(i).getBillerName());

        myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Log.e("CLICKED REMOVE", "" + modelList.get(i).getBillerID());
                modelList.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, modelList.size());
                //notifyDataSetChanged();*/

                //Toast.makeText(v.getContext(), "@" + i + " | " + modelList.get(i).getBillerID(), Toast.LENGTH_SHORT).show();
                Log.e("CLICKED", "@" + i);

                Intent intent = new Intent (v.getContext(), TransactionUtilityActivity.class);
                intent.putExtra(KEY_UTILITY_BILLERID, modelList.get(i).getBillerID());
                intent.putExtra(KEY_UTILITY_BILLERNAME, modelList.get(i).getBillerName());
                //intent.putExtra(KEY_FAMILY_TREE_COUNT, modelList.get(i).getBillerID());

                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });

        myViewHolder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                /*Log.e("CLICKED ADD", "" + modelList.get(i).getBillerID());
                modelList.add(i, new Model(101, "Test"));
                notifyItemInserted(i);
                notifyItemRangeChanged(i, modelList.size());
                //notifyDataSetChanged();*/

                Log.e("LONG CLICKED", "@" + i);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvBillerID;
        TextView tvBillerName;

        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvBillerID = itemView.findViewById(R.id.child_utility_textview_biller_id);
            tvBillerName = itemView.findViewById(R.id.child_utility_textview_biller_name);

            linearLayout = itemView.findViewById(R.id.child_utility_linearlayout);
        }
    }

}
