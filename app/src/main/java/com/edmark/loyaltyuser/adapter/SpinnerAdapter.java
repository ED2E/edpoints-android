package com.edmark.loyaltyuser.adapter;


import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.Country;

import java.util.List;
import java.util.Objects;

import static com.edmark.loyaltyuser.util.AppUtility.getCompanyFlagDrawable;

/*
 * Created by DEV-Michael-ED2E on 05/22/2018.
 */
public class SpinnerAdapter extends ArrayAdapter<Country> {
    public static final String TAG = SpinnerAdapter.class.getSimpleName();

    private Activity context;
    public SpinnerAdapter(Activity context, int textViewResourceId, List<Country> list) {
        super(context, textViewResourceId, list);
        this.context = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    private View getCustomView(int position, ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        Country rowItem = getItem(position);
        LayoutInflater inflater = context.getLayoutInflater();
        View rowview = inflater.inflate(R.layout.child_spinner_country, parent, false);
        TextView txtTitle = rowview.findViewById(R.id.child_spinner_country_textview_name);
        if (rowItem != null) {
            txtTitle.setText(rowItem.getName());
        }

        ImageView imageView = rowview.findViewById(R.id.child_spinner_country_imageview_icon);
        Resources resources = imageView.getContext().getResources();
        int resourceId = 0;
        if (rowItem != null) {
//            resourceId = resources.getIdentifier(Objects.requireNonNull(rowItem.getCode()).toLowerCase(), "drawable", imageView.getContext().getPackageName());
            resourceId = getCompanyFlagDrawable(context, Objects.requireNonNull(Objects.requireNonNull(rowItem).getCode()));
        }
        imageView.setImageResource(resourceId);

        return rowview;
    }
}