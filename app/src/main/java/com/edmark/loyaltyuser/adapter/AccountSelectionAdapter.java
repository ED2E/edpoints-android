package com.edmark.loyaltyuser.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.CardType;

import java.util.List;

public class AccountSelectionAdapter extends RecyclerView.Adapter<AccountSelectionAdapter.AccountSelectionViewHolder> {

    private List<CardType> cardTypes;
    private int rowLayout;
    private Context context;


    public static class AccountSelectionViewHolder extends RecyclerView.ViewHolder {
//        LinearLayout moviesLayout;
        TextView textView_cardType;
//        TextView data;
//        TextView movieDescription;
//        TextView rating;


        public AccountSelectionViewHolder(View v) {
            super(v);
//            moviesLayout = (LinearLayout) v.findViewById(R.id.movies_layout);
//            textView_cardType = (TextView) v.findViewById(R.id.child_account_selection_textview_card_type);
//            data = (TextView) v.findViewById(R.id.subtitle);
//            movieDescription = (TextView) v.findViewById(R.id.description);
//            rating = (TextView) v.findViewById(R.id.rating);
        }
    }

    public AccountSelectionAdapter(List<CardType> movies, int rowLayout, Context context) {
        this.cardTypes = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public AccountSelectionViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new AccountSelectionViewHolder(view);
    }


    @Override
    public void onBindViewHolder(AccountSelectionViewHolder holder, final int position) {
//        holder.movieTitle.setText(cardTypes.get(position).getTitle());
//        holder.data.setText(cardTypes.get(position).getReleaseDate());
//        holder.movieDescription.setText(cardTypes.get(position).getOverview());
//        holder.rating.setText(cardTypes.get(position).getVoteAverage().toString());
    }

    @Override
    public int getItemCount() {
        return cardTypes.size();
    }
}