package com.edmark.loyaltyuser.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.base.BaseActivity;
import com.edmark.loyaltyuser.model.FamilyTree;
import com.edmark.loyaltyuser.view.activity.familytree.FamilyTreeActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.edmark.loyaltyuser.constant.Common.KEY_FAMILY_TREE_COUNT;
import static com.edmark.loyaltyuser.constant.Common.KEY_FAMILY_TREE_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;

public class FamilyTreeAdapter extends RecyclerView.Adapter<FamilyTreeAdapter.MyViewHolder> {

    Context context;
    List<FamilyTree> modelList;
    int counter;

    public FamilyTreeAdapter(Context context, List<FamilyTree> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    public void setModelList(List<FamilyTree> modelList, int counter) {
        this.modelList = modelList;
        this.counter = counter;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FamilyTreeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_family_tree, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FamilyTreeAdapter.MyViewHolder myViewHolder, int position) {

        /*myViewHolder.tv.setText(
                "downline_totaldownlines: " + modelList.get(position).getDownline_totaldownlines() + "\n" +
                "downline_custid: " + modelList.get(position).getDownline_custid() + "\n" +
                "downline_custserialno: " + modelList.get(position).getDownline_custserialno() + "\n" +
                "downline_custeda: " + modelList.get(position).getDownline_custeda() + "\n" +
                "downline_custname: " + modelList.get(position).getDownline_custname() + "\n" +
                "downline_custtype: " + modelList.get(position).getDownline_custtype() + "\n" +
                "downline_custjoindate: " + modelList.get(position).getDownline_custjoindate()
        );*/



        String dateStr = "";

        try {
            dateStr = modelList.get(position).getDownline_custjoindate();
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);
            DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");

            // format the date into another format
            dateStr = destDf.format(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        myViewHolder.textview_name.setText(capitalize(modelList.get(position).getDownline_custname()));
        myViewHolder.textview_date_started.setText("Since: " + dateStr);
        myViewHolder.textview_code.setText(modelList.get(position).getDownline_custserialno());
        myViewHolder.textview_direct_downline.setText(modelList.get(position).getDownline_totaldownlines());
        myViewHolder.textview_card_type.setText(modelList.get(position).getDownline_custtype());

        if(Integer.parseInt(modelList.get(position).getDownline_totaldownlines()) == 0 || counter >= 3){
            //myViewHolder.imageview_view_downline.setVisibility(View.INVISIBLE);
            myViewHolder.imageview_view_downline.setImageResource(R.drawable.ic_view_downline_disabled);
        } else {
            //myViewHolder.imageview_view_downline.setVisibility(View.VISIBLE);
            myViewHolder.imageview_view_downline.setImageResource(R.drawable.ic_view_downline_enabled);

            myViewHolder.imageview_view_downline.setOnClickListener(v -> {
//            Log.e("CLICKED REMOVE", "" + modelList.get(i).getBillerID());
//            modelList.remove(i);
//            notifyItemRemoved(i);
//            notifyItemRangeChanged(i, modelList.size());
//            //notifyDataSetChanged();

                //Toast.makeText(v.getContext(), "Counter: " + counter, Toast.LENGTH_SHORT).show();
                //Log.e("CLICKED", "@" + position);

                Intent intent = new Intent (v.getContext(), FamilyTreeActivity.class);
                intent.putExtra(KEY_FAMILY_TREE_CUSTID, modelList.get(position).getDownline_custid());
                intent.putExtra(KEY_FAMILY_TREE_COUNT, counter);
                //LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                //Toast.makeText(v.getContext(), "@" + position + " | " + modelList.get(position).getDownline_custid(), Toast.LENGTH_SHORT).show();
                //((Activity)context).finish();

                context.startActivity(intent);
                try {
                    ((Activity) context).finish();
                } catch (ClassCastException e) {
                    Toast.makeText(v.getContext(), "Error Starting Activity: " + e, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv, textview_name, textview_date_started, textview_code, textview_direct_downline, textview_card_type;
        ImageView imageview_view_downline;

        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            //tv = itemView.findViewById(R.id.child_family_tree_textview);
            textview_name= itemView.findViewById(R.id.child_family_tree_textview_name);
            textview_date_started = itemView.findViewById(R.id.child_family_tree_textview_date_started);
            textview_code = itemView.findViewById(R.id.child_family_tree_textview_code);
            textview_direct_downline = itemView.findViewById(R.id.child_family_tree_textview_downline);
            textview_card_type = itemView.findViewById(R.id.child_family_tree_textview_card_type);
            imageview_view_downline = itemView.findViewById(R.id.child_family_tree_imageview_view_downline);

            linearLayout = itemView.findViewById(R.id.child_family_tree_linearlayout);
        }
    }
}
