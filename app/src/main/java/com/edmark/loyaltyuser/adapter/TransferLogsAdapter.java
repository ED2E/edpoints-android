package com.edmark.loyaltyuser.adapter;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.TransferLogs;
import com.edmark.loyaltyuser.view.activity.transfer.TransferLogActivity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class TransferLogsAdapter extends RecyclerView.Adapter<TransferLogsAdapter.MyViewHolder> {

    TransferLogActivity context;
    List<TransferLogs> modelList;

    public TransferLogsAdapter(TransferLogActivity context, List<TransferLogs> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    public void setModelList(List<TransferLogs> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TransferLogsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_transfer_logs, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Calendar cal = Calendar.getInstance();
        //TimeZone tz = cal.getTimeZone();

        /* debug: is it local time? */
        //Log.d("Time Zone: ", tz.getDisplayName());

        myViewHolder.textview_reference_number.setText(modelList.get(i).getTopup_id());

        DecimalFormat formatter1 = new DecimalFormat("###,##0.000000000000000000");
        DecimalFormat formatter2 = new DecimalFormat("###,##0.00");

        Double edp_from_double;
        try{
            edp_from_double = Double.parseDouble(modelList.get(i).getTransferred_amount());
            myViewHolder.textview_edp_from.setText("-EP " + formatter2.format(edp_from_double));
        } catch (NumberFormatException e) {
            myViewHolder.textview_edp_from.setText("-EP ");
        }

        Double edp_to_double;
        try{
            edp_to_double = Double.parseDouble(modelList.get(i).getConverted_amount());
            myViewHolder.textview_edp_to.setText("+EP " + formatter2.format(edp_to_double));
        } catch (NumberFormatException e) {
            myViewHolder.textview_edp_to.setText("+EP ");
        }


        String[] desc_acc = modelList.get(i).getDescription().split("to");
        String acc = desc_acc[1].trim();

        myViewHolder.textview_ed2e_number.setText("" + acc);



        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = modelList.get(i).getCreated_date();

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));


            //srcDf.setTimeZone(TimeZone.getTimeZone("GMT"));
            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);


            DateFormat destDf = new SimpleDateFormat("MMMM dd, yyyy");
            DateFormat destTf = new SimpleDateFormat("hh:mm a");

            // format the date into another format
            dateStr = destDf.format(date);
            timeStr = destTf.format(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        myViewHolder.textview_textview_date.setText(dateStr);
        myViewHolder.textview_textview_time.setText(timeStr.toUpperCase().replace(".", ""));

        String app_name = "";
        if(modelList.get(i).getApp_type().equalsIgnoreCase("0")){
            app_name = "EDPOINTS App";
        } else if(modelList.get(i).getApp_type().equalsIgnoreCase("1")){
            app_name = "ED2E App";
        } else if(modelList.get(i).getApp_type().equalsIgnoreCase("2")){
            app_name = "EDPOINTS App";
        } else {
            app_name = "";
        }
        myViewHolder.textview_description.setText("Transferred To " + app_name);

        Typeface custom_font_BOLD = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri Bold.ttf");
        myViewHolder.textview_description.setTypeface(custom_font_BOLD);

        myViewHolder.textview_from_country.setText(context.getResources().getString(R.string.label_edpoints_amount_from, "" + modelList.get(i).getPurchaser_country()));
        myViewHolder.textview_to_country.setText(context.getResources().getString(R.string.label_edpoints_amount_to, ""));

    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textview_ed2e_number;
        TextView textview_reference_number;
        TextView textview_edp_from;
        TextView textview_edp_to;
        TextView textview_from_country;
        TextView textview_to_country;
        TextView textview_textview_date;
        TextView textview_textview_time;
        TextView textview_description;

        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            textview_ed2e_number = itemView.findViewById(R.id.child_transfer_history_textview_ed2e_number);
            textview_reference_number = itemView.findViewById(R.id.child_transfer_history_textview_reference_number);
            textview_edp_from = itemView.findViewById(R.id.child_transfer_history_textview_edp_from);
            textview_edp_to = itemView.findViewById(R.id.child_transfer_history_textview_edp_to);
            textview_from_country = itemView.findViewById(R.id.child_transfer_history_textview_from_country);
            textview_to_country = itemView.findViewById(R.id.child_transfer_history_textview_to_country);
            textview_textview_date = itemView.findViewById(R.id.child_transfer_history_textview_date);
            textview_textview_time = itemView.findViewById(R.id.child_transfer_history_textview_time);
            textview_description = itemView.findViewById(R.id.child_transfer_history_textview_desc);

            linearLayout = itemView.findViewById(R.id.child_transfer_history_linearlayout);
        }
    }

}

