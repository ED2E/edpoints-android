package com.edmark.loyaltyuser.adapter;

import android.graphics.drawable.Drawable;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edmark.loyaltyuser.R;
import com.glide.slider.library.svg.GlideApp;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import static com.edmark.loyaltyuser.constant.Common.STATUS_CODE_INACTIVE;
import static com.edmark.loyaltyuser.util.AppUtility.capitalize;
import static com.edmark.loyaltyuser.util.AppUtility.getCardDrawable;

public class BindingAdapter {

    @androidx.databinding.BindingAdapter("loadImageUrl")
    public static void setImageUrl(ImageView view, String imageUrl) {

        int drawable = 0;
        if(imageUrl!=null)
        {
            if(imageUrl.isEmpty())
            {
                view.setImageResource(R.drawable.ic_card_ordinary);
                return;
            }
        }
        else
        {
            view.setImageResource(R.drawable.ic_card_ordinary);
            return;
        }

        GlideApp.with(view.getContext())
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .onlyRetrieveFromCache(false)
                .into(view);
    }

    @androidx.databinding.BindingAdapter({"app:loadImageUrl", "app:visibility"})
    public static void loadImage(ImageView view, String loadImageUrl, @IdRes int layourId) {
        ProgressBar progressbar = view.getRootView().findViewById(layourId);

        progressbar.setVisibility(View.VISIBLE);
        int drawable = 0;
        if(loadImageUrl!=null)
        {
            if(loadImageUrl.isEmpty())
            {
                view.setImageResource(R.drawable.ic_card_ordinary);
                progressbar.setVisibility(View.GONE);
                return;
            }
        }
        else
        {
            view.setImageResource(R.drawable.ic_card_ordinary);
            progressbar.setVisibility(View.GONE);
            return;
        }

        GlideApp.with(view.getContext())
                .load(loadImageUrl)
                .placeholder(R.drawable.ic_shimmer_user)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .onlyRetrieveFromCache(false)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressbar.setVisibility(View.GONE);
                        view.setImageResource(R.drawable.ic_card_ordinary);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressbar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(view);
    }

    @androidx.databinding.BindingAdapter("loadImage")
    public static void setImageDrawable(ImageView view, String imageUrl) {

        if(imageUrl!=null)
        {
            if(imageUrl.isEmpty())
            {
                view.setImageResource(R.drawable.ic_card_ordinary);
                return;
            }
        }
        else
        {
            view.setImageResource(R.drawable.ic_card_ordinary);
            return;
        }

        GlideApp.with(view.getContext())
                .load(getCardDrawable(imageUrl))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .onlyRetrieveFromCache(false)
                .into(view);

//        Glide.with(view.getContext()).load(imageUrl).placeholder(R.drawable.progress_animation).into(view, new Callback() {
//            @Override
//            public void onSuccess() {
//
//            }
//            @Override
//            public void onError() {
//                //Try again online if cache failed
//                Log.v("Picasso","Could not fetch image");
//                view.setImageResource(R.drawable.button_no_image_selector);
//            }
//        });

    }

    @androidx.databinding.BindingAdapter("loadIcon")
    public static void setSupportIcon(ImageView view, int imageUrl) {
        GlideApp.with(view.getContext())
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .onlyRetrieveFromCache(false)
                .into(view);
    }

    @androidx.databinding.BindingAdapter("setCheck")
    public static void setCheckStatus(RadioButton view, String status) {

        if(status==null||status.equals(STATUS_CODE_INACTIVE))
        {
            view.setChecked(false);
        }
        else
        {
            view.setChecked(true);
        }
    }

    @androidx.databinding.BindingAdapter("setCapitalize")
    public static void setCapitalize(TextView view, String type) {
        view.setText(capitalize(type.toLowerCase()));
    }

    @androidx.databinding.BindingAdapter({"app:loadFirebaseUrl", "app:visibility"})
    public static void loadImageFirebase(ImageView view, String loadFirebaseUrl, @IdRes int layourId) {
        ProgressBar progressbar = view.getRootView().findViewById(layourId);

        progressbar.setVisibility(View.VISIBLE);
        int drawable = 0;
        if(loadFirebaseUrl!=null)
        {
            if(loadFirebaseUrl.isEmpty())
            {
                view.setImageResource(R.drawable.ic_card_ordinary);
                progressbar.setVisibility(View.GONE);
                return;
            }
        }
        else
        {
            view.setImageResource(R.drawable.ic_card_ordinary);
            progressbar.setVisibility(View.GONE);
            return;
        }

//        StorageReference storageRef = storage.getReference();
//
//        // Create a reference from an HTTPS URL
//        // Note that in the URL, characters are URL escaped!
//        StorageReference httpsReference = storage.getReferenceFromUrl("https://firebasestorage.googleapis.com/b/bucket/o/images%20stars.jpg");
//
//        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//            @Override
//            public void onSuccess(Uri uri) {
//                imageURL = uri.toString();
//                Glide.with(getApplicationContext()).load(imageURL).into(i1);
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Handle any errors
//            }
//        });

        Log.d("BindingAdapter","loadFirebaseUrl:"+loadFirebaseUrl);
        StorageReference mImageRef = FirebaseStorage.getInstance().getReferenceFromUrl(loadFirebaseUrl);
        GlideApp.with(view.getContext())
                .load(mImageRef)
                .placeholder(R.drawable.ic_shimmer_user)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .onlyRetrieveFromCache(false)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressbar.setVisibility(View.GONE);
                        view.setImageResource(R.drawable.ic_card_ordinary);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressbar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(view);
    }
}
