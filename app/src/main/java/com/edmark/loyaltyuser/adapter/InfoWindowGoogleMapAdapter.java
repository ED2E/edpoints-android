package com.edmark.loyaltyuser.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestFutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.Merchant;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/*
 * Created by DEV-Michael-ED2E on 04/27/2018.
 */

public class InfoWindowGoogleMapAdapter implements GoogleMap.InfoWindowAdapter {
    private static final String TAG = InfoWindowGoogleMapAdapter.class.getSimpleName();

    private Context context;

    public InfoWindowGoogleMapAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        Log.d(TAG, "getInfoContents");
        @SuppressLint("InflateParams") View view = ((Activity) context).getLayoutInflater().inflate(R.layout.map_merchant, null);

        TextView textView_name = view.findViewById(R.id.map_merchant_textview_name);
        TextView textView_contact = view.findViewById(R.id.map_merchant_textview_contact);
        TextView textView_address = view.findViewById(R.id.map_merchant_textview_address);
        ImageView imageView_logo = view.findViewById(R.id.map_merchant_imageview_logo);
        ProgressBar progressBar = view.findViewById(R.id.map_merchant_progress);
        Button button_show = view.findViewById(R.id.map_merchant_button);

        textView_name.setText(marker.getTitle());
        textView_address.setText(marker.getSnippet());
        progressBar.setVisibility(View.VISIBLE);

        Merchant merchant = (Merchant) marker.getTag();
        if (merchant != null) {
            textView_contact.setText(merchant.getContact_no());
            if(!merchant.getImage_url().equals(""))
            {
            Glide.with(context)
                    .load(merchant.getImage_url())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            Log.d(TAG, "markerImageStatus:"+merchant.getMarkerImageStatus());
                            //for refreshing the image after getting the image from the url
                            if(!merchant.getMarkerImageStatus()) {
                                Handler handler = new Handler();
                                handler.postDelayed(() -> {
                                    if (marker.isInfoWindowShown()) {
                                        marker.hideInfoWindow();
                                        if(!merchant.getImage_url().equals("")) {
                                            Glide.with(context)
                                                    .load(merchant.getImage_url())
                                                    .into(imageView_logo);
                                        }else {
                                            Glide.with(context)
                                                    .load(R.drawable.ic_card_ordinary)
                                                    .into(imageView_logo);
                                        }
                                        marker.showInfoWindow();
                                        progressBar.setVisibility(View.GONE);
                                    }
                                }, 500);
                                merchant.setMarkerImageStatus(true);
                            }

                            return false;
                        }

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                            Log.d(TAG, "onLoadFailed:"+merchant.getMarkerImageStatus());
                            if(!merchant.getMarkerImageStatus()) {
                                Handler handler = new Handler();
                                handler.postDelayed(() -> {
                                    if (marker.isInfoWindowShown()) {
                                        marker.hideInfoWindow();
                                        Glide.with(context)
                                                .load(R.drawable.ic_card_ordinary)
                                                .into(imageView_logo);
                                        marker.showInfoWindow();
                                    }
                                }, 500);
                                merchant.setMarkerImageStatus(true);
                            }
                            else
                            {
                                if (marker.isInfoWindowShown()) {
                                    marker.hideInfoWindow();
                                    Glide.with(context)
                                            .load(R.drawable.ic_card_ordinary)
                                            .into(imageView_logo);
                                    marker.showInfoWindow();
                                }
                            }
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView_logo);

            }
            else
            {
                Glide.with(context)
                        .load(R.drawable.ic_card_ordinary)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                Log.d(TAG, "markerImageStatus:"+merchant.getMarkerImageStatus());
                                //for refreshing the image after getting the image from the url
                                if(!merchant.getMarkerImageStatus()) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(() -> {
                                        if (marker.isInfoWindowShown()) {
                                            marker.hideInfoWindow();
                                                Glide.with(context)
                                                        .load(R.drawable.ic_card_ordinary)
                                                        .into(imageView_logo);
                                            marker.showInfoWindow();
                                            progressBar.setVisibility(View.GONE);
                                        }
                                    }, 500);
                                    merchant.setMarkerImageStatus(true);
                                }

                                return false;
                            }

                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                                Log.d(TAG, "onLoadFailed:"+merchant.getMarkerImageStatus());
                                if(!merchant.getMarkerImageStatus()) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(() -> {
                                        if (marker.isInfoWindowShown()) {
                                            marker.hideInfoWindow();
                                            Glide.with(context)
                                                    .load(R.drawable.ic_card_ordinary)
                                                    .into(imageView_logo);
                                            marker.showInfoWindow();
                                        }
                                    }, 500);
                                    merchant.setMarkerImageStatus(true);
                                }
                                else
                                {
                                    if (marker.isInfoWindowShown()) {
                                        marker.hideInfoWindow();
                                        Glide.with(context)
                                                .load(R.drawable.ic_card_ordinary)
                                                .into(imageView_logo);
                                        marker.showInfoWindow();
                                    }
                                }
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(imageView_logo);
            }
                    //code is deprecated
//                    .listener(new RequestListener<String, GlideDrawable>() {
//                        @Override
//                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            // log exception
//                            Log.e(TAG, "Error loading image", e);
//                            return false; // important to return false so the error placeholder can be placed
//                        }
//
//                        @Override
//                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            Log.d(TAG, "onResourceReady");
//                            return false;
//                        }
//                    })
//                    .into(new GlideDrawableImageViewTarget(imageView_logo) {
//                        @Override
//                        protected void setResource(GlideDrawable resource) {
//                            super.setResource(resource);
//                            Log.d(TAG, "GlideDrawableImageViewTarget");
//                            //for refreshing the image after getting the image from the url
//                            if (marker.isInfoWindowShown()) {
//                                marker.hideInfoWindow();
//
//                                Glide.with(context)
//                                        .load(merchant.getImageurl())
//                                        .into(imageView_logo);
//
//                                marker.showInfoWindow();
//                            }
//                        }
//                    });
        }

        button_show.setOnClickListener(v ->
        {
            Log.d(TAG, "Click");
        });

        return view;
    }
}
