package com.edmark.loyaltyuser.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.edmark.loyaltyuser.R;
import com.glide.slider.library.svg.GlideApp;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 *  Created by DEV-Michael-ED2E on 05/23/2018.
 */
public class OnBoardingViewPagerAdapter extends PagerAdapter
{
    private int[] layouts;
    private Context context;
    @BindView(R.id.child_onboarding_desc)
    TextView textview_desc;
    @BindView(R.id.child_onboarding_title)
    TextView textview_title;
    @BindView(R.id.child_onboarding_image)
    ImageView imageview_image;

    public OnBoardingViewPagerAdapter(Context context, int[] layouts)
    {
        this.context = context;
        this.layouts = layouts;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(layoutInflater!=null)
        {
            View view = layoutInflater.inflate(layouts[position], container, false);
            ButterKnife.bind(this,view);
            String[] stringlist_title = context.getResources().getStringArray(R.array.array_tutorial_title);
            String[] stringlist_desc = context.getResources().getStringArray(R.array.array_tutorial_desc);
            TypedArray stringlist_image = context.getResources().obtainTypedArray(R.array.array_tutorial_image);
            textview_title.setText(stringlist_title[position]);
            textview_desc.setText(stringlist_desc[position]);
            imageview_image.setImageDrawable(stringlist_image.getDrawable(position));
            RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false).signature(new ObjectKey(String.valueOf(System.currentTimeMillis())));
            GlideApp.with(context)
                    .load(stringlist_image.getDrawable(position))
                    .apply(options)
                    .placeholder(R.drawable.ic_shimmer_user)
                    .error(R.drawable.ic_default_user)
                    .listener(new RequestListener<Drawable>() {

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imageview_image);
            stringlist_image.recycle();
            container.addView(view);
            return view;
        }
        return false;
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
