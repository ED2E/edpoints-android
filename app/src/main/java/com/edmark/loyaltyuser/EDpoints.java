package com.edmark.loyaltyuser;

import android.content.Context;
import android.content.res.Configuration;

import androidx.multidex.MultiDex;

import com.orm.SugarApp;
import com.orm.SugarContext;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import timber.log.Timber;

public class EDpoints extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(getApplicationContext());

        //Not working when getting the class domain
        // create table if not exists
//        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
//        schemaGenerator.createDatabase(new SugarDb(this).getDB());
//        Log.d("EDPOINTS","getDomainClasses:"+getDomainClasses(this));

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/Calibri.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .setFontMapper(font -> font)
//                                .addCustomViewWithSetTypeface(CustomViewWithTypefaceSupport.class)
//                                .addCustomStyle(TextField.class, R.attr.textFieldStyle)
                                .build()))
                .build());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        super.attachBaseContext(ViewPumpContextWrapper.wrap(base));
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

}
