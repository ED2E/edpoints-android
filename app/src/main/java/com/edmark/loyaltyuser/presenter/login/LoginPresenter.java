package com.edmark.loyaltyuser.presenter.login;

import android.util.Log;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edmark.loyaltyuser.constant.Common.API_LOGIN;
import static com.edmark.loyaltyuser.constant.Common.CUSTID;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_EMAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_NAME;
import static com.edmark.loyaltyuser.constant.Common.KEY_REMEMBERED;
import static com.edmark.loyaltyuser.constant.Common.LOGIN_SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.NAME;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TYPE;
import static com.edmark.loyaltyuser.util.AppUtility.getHash;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 05/08/2018.
 */

public class LoginPresenter implements LoginContract.Presenter {
    public static final String TAG = LoginPresenter.class.getSimpleName();

    private LoginContract.View view;
    private AppPreference preference;

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
        preference = AppPreference.getInstance(view.getContext());
        view.onLoadEmailSuccess(preference.getString(KEY_EMAIL,""));
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void login(String type, String email, String password, Boolean autoLogin) {
        view.showProgress();
        preference.putString(KEY_EMAIL, email);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.login(API_LOGIN, type, email, getHash(password), String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "Raw Url:" + response.raw().request().url());
                view.hideProgress();
                if(response.body()==null)
                {
                    view.hideProgress();
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Log.d(TAG, "onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);
                    String message = (String) obj.get(MESSAGE);
                    if (status.equals(SUCCESS) && message.contains(LOGIN_SUCCESS)) {
                        String type = (String) obj.get(TYPE);
                        view.onLoginSuccess(type);

                        String custid = (String) obj.get(CUSTID);
                        String name = (String) obj.get(NAME);
                        preference.putString(KEY_CUSID, custid);
                        preference.putString(KEY_NAME, name);
                        preference.putString(KEY_ACCOUNT_TYPE, type);
                        Log.d(TAG, "preference:" + preference.getString(KEY_CUSID));

                        if (autoLogin) {
                            preference.putBoolean(KEY_REMEMBERED, true);
                        }
                    } else {
                        String errorCode = (String) obj.get(ERRORCODE);
                        view.hideProgress();
                        view.showToast(view.getContext().getResources().getString(R.string.label_error_code, message, errorCode));
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    view.hideProgress();
                    view.showToast(e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure:");
                if(t instanceof SocketTimeoutException){
                    view.hideProgress();
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    return;
                }
                t.printStackTrace();
                view.hideProgress();
                view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                //view.showToast(t.getMessage());
            }
        });
    }
}
