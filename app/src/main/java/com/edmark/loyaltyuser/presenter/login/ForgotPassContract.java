package com.edmark.loyaltyuser.presenter.login;

import com.edmark.loyaltyuser.presenter.BasePresenter;
import com.edmark.loyaltyuser.presenter.BaseView;

/*
 * Created by DEV-Michael-ED2E on 05/30/2018.
 */

public interface ForgotPassContract {

    interface View extends BaseView {

        void onLoadPageSuccess(String title);
        void onEmailSentSuccess(String message);
    }

    interface Presenter extends BasePresenter {

        void loadPage(String type);
        void loadAPI(String page, String email);
    }
}
