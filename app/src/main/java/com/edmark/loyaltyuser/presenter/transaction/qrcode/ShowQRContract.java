package com.edmark.loyaltyuser.presenter.transaction.qrcode;

import com.edmark.loyaltyuser.model.CustomerInfo;
import com.edmark.loyaltyuser.presenter.BasePresenter;
import com.edmark.loyaltyuser.presenter.BaseView;

/*
 * Created by DEV-Michael-ED2E on 05/08/2018.
 */

public interface ShowQRContract {

    interface View extends BaseView {

        void onLoadSuccess(CustomerInfo info);
        void onLoadFailed();
    }

    interface Presenter extends BasePresenter {

        void loadAccount(String custId);
    }
}
