package com.edmark.loyaltyuser.presenter.advertisement;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.Ads;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.API_ADS_LIST;
import static com.edmark.loyaltyuser.constant.Common.DATA;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 05/08/2018.
 */

public class AdvertisementPresenter implements AdvertsimentContract.Presenter {
    public static final String TAG = AdvertisementPresenter.class.getSimpleName();

    private AdvertsimentContract.View view;
    private AppPreference preference;

    public AdvertisementPresenter(AdvertsimentContract.View view) {
        this.view = view;
        preference = AppPreference.getInstance(view.getContext());
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void getAdvertisement(String companyCode) {
        view.showProgress();
        ArrayList<Ads> adsList = new ArrayList<>();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getAds(API_ADS_LIST, companyCode, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Timber.d("Raw Url:" + response.raw().request().url());
                if(response.body()==null)
                {
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Timber.d("onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);
                    if (status.equals(SUCCESS)) {

                        try {
                            JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(DATA)));
                            Gson gson = new Gson();
                            for (int i = 0; i < jsonarrayMain.length(); i++) {
                                JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                                Ads ads = gson.fromJson(jsonobject.toString(), Ads.class);
                                Timber.d("Ads list:" + gson.toJson(ads));
                                adsList.add(ads);
                            }
                            view.onLoadSuccess(adsList);
                        } catch (JSONException e) {
                            view.hideProgress();
                            view.onLoadSuccess(adsList);
                        }
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        view.showToast(view.getContext().getResources().getString(R.string.label_error_code, message, errorCode));
                    }
                    view.hideProgress();

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    view.hideProgress();
                    view.showToast(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    view.hideProgress();
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    return;
                }
                Timber.d("onFailure:");
                t.printStackTrace();
                view.onLoadSuccess(adsList);
                view.hideProgress();
                view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                //view.showToast(t.getMessage());
            }
        });
    }
}
