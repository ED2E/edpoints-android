package com.edmark.loyaltyuser.presenter.edcoin;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.presenter.account.AccountPresenter;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiClientEDCoin;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.edmark.loyaltyuser.constant.Common.FAIL;
import static com.edmark.loyaltyuser.constant.Common.KEY_EDCOIN_BIND_KEY;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.REFERRAL_BONUS;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.constant.Common.TOTAL_CONVERTED_EDPOINTS;
import static com.edmark.loyaltyuser.constant.Common.WALLET_AVAILABLE_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.WALLET_BALANCE;
import static com.edmark.loyaltyuser.constant.Common.WALLET_LOCK_BALANCE;

public class EDCoinPresenter implements EDCoinContract.Presenter{

    public static final String TAG = EDCoinPresenter.class.getSimpleName();

    private EDCoinContract.View view;
    private AppPreference preference;
    private Context context;

    public EDCoinPresenter(Context context, EDCoinContract.View view) {
        this.context = context;
        this.view = view;
        preference = AppPreference.getInstance(view.getContext());
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void loadWallet(String selectedServer, String bind) {
        view.showProgress();
        //loadCompanyCode();

        //showToast("Selected Server: " + selectedServer);
        ApiInterface apiInterface = ApiClientEDCoin.getClient(selectedServer).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.retrieveWalletBalance(
                bind
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.body()==null)
                {
                    view.showToast(context.getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    view.onLoadWalletFailed();
                    return;
                }

                try {
                    view.hideProgress();
                    String responseString = response.body().string();
                    //showToast(responseString);

                    Log.d(TAG, "onResponse: " + responseString);

                    JSONObject obj = new JSONObject(responseString);
                    String status = (String) obj.get(STATUS);


                    if (status.equals(SUCCESS)) {

                        preference.putString(KEY_EDCOIN_BIND_KEY, bind);

                        JSONObject data = obj.getJSONObject("data");
                        String edc_balance = (String) data.get(WALLET_BALANCE);
                        String lockup_balance = (String) data.get(WALLET_LOCK_BALANCE);
                        String referral_bonus = (String) data.get(REFERRAL_BONUS);
                        String available = (String) data.get(WALLET_AVAILABLE_BALANCE);
                        String total_converted_edpoints = (String) data.get(TOTAL_CONVERTED_EDPOINTS);

                        view.onLoadWalletStatusSuccess(edc_balance, lockup_balance, referral_bonus, available, total_converted_edpoints);

                    } else if (status.equals(FAIL)) {

                        String message = (String) obj.get(MESSAGE);
                        view.onLoadWalletStatusFail(message);
                    } else {
                        view.showToast(context.getResources().getString(R.string.error_try_again));
                    }
                } catch (JSONException | IOException e) {
                    view.hideProgress();
                    e.printStackTrace();
                    Writer writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));

                    view.hideProgress();
                    view.showToast(e.getMessage());
                    view.onLoadWalletFailed();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //showToast("Failed Response");

                if(t instanceof SocketTimeoutException ||t instanceof ConnectException){

                    Timber.d("onFailure: " + t.getMessage());
                    t.printStackTrace();
                    view.showToast(context.getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    view.onLoadWalletFailed();
                    return;
                }

                view.hideProgress();
                Timber.d("onFailure: " + t.getMessage());
                t.printStackTrace();
                view.showToast(context.getResources().getString(R.string.error_connection));
                view.onLoadWalletFailed();
            }
        });
    }
}
