package com.edmark.loyaltyuser.presenter.edcoin;

import com.edmark.loyaltyuser.presenter.BasePresenter;
import com.edmark.loyaltyuser.presenter.BaseView;

public interface EDCoinContract {

    interface View extends BaseView {

        void onLoadWalletStatusSuccess(String edc_balance, String lockup_balance, String referral_bonus, String available, String total_converted_edpoints);
        void onLoadWalletStatusFail(String message);
        void onLoadWalletFailed();
    }

    interface Presenter extends BasePresenter {
        void loadWallet(String selectedServer, String bind);
    }

}
