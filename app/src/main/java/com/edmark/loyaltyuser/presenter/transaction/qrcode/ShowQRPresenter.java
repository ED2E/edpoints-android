package com.edmark.loyaltyuser.presenter.transaction.qrcode;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.CustomerInfo;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edmark.loyaltyuser.constant.Common.API_CUSTINFO;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

public class ShowQRPresenter implements ShowQRContract.Presenter {
    public static final String TAG = ShowQRPresenter.class.getSimpleName();

    private ShowQRContract.View view;
    private ApiInterface apiService;

    public ShowQRPresenter(ShowQRContract.View view) {
        this.view = view;
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    @Override
    public void loadAccount(String custId) {
        view.showProgress();
        Call<ResponseBody> call = apiService.mainCustInfo(API_CUSTINFO, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    view.onLoadFailed();
                    return;
                }
                try {
                    view.hideProgress();
                    String json = Objects.requireNonNull(response.body()).string();
                    Log.d(TAG, "onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        Gson gson = new Gson();
                        CustomerInfo info = gson.fromJson(json, CustomerInfo.class);
                        view.onLoadSuccess(info);
                        Log.d(TAG, "CustomerInfo:" + info.getName() + " " + info.getEda());
                    }

                } catch (IOException |JSONException e) {
                    e.printStackTrace();
                    view.showToast(e.getMessage());
                    view.hideProgress();
                    view.onLoadFailed();
                }
                view.hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    view.onLoadFailed();
                    return;
                }

                Log.d(TAG, "onFailure:");
                t.printStackTrace();
                //loadAccount(custId);
                view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                view.hideProgress();
                view.onLoadFailed();
            }
        });
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }
}
