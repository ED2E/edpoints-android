package com.edmark.loyaltyuser.presenter.imageLoader;

import android.content.Context;
import android.widget.ImageView;

import com.edmark.loyaltyuser.presenter.BasePresenter;
import com.edmark.loyaltyuser.presenter.BaseView;

//public interface LoadImageContract {
//    void onLoadfailed(int requestCode);
//    void onResourceready(int requestCode);
//    // or void onEvent(); as per your need
//
//}
public interface LoadImageContract {

//    interface View extends BaseView {

        void onLoadfailed(int requestCode);

        void onResourceready(int requestCode);
//    }

    interface Presenter extends BasePresenter {
        void loadImage(Context context, String tag, int requestCode, String imageURL, ImageView imageView, int errorImage, boolean saveCache);
    }
}