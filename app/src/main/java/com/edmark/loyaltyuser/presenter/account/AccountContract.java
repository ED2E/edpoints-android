package com.edmark.loyaltyuser.presenter.account;

import com.edmark.loyaltyuser.model.CardType;
import com.edmark.loyaltyuser.presenter.BasePresenter;
import com.edmark.loyaltyuser.presenter.BaseView;

import java.util.ArrayList;

/*
 * Created by DEV-Michael-ED2E on 05/08/2018.
 */

public interface AccountContract {

    interface View extends BaseView {

        void onLoadAccountSuccess(ArrayList<CardType> datalist);
        void onDialogDismiss();
        void onLoadAccountFailed();
    }

    interface Presenter extends BasePresenter {

        void loadAccount();
        void loadAPI(String currentCust, String API, String serial_no, String activation_code);
    }
}
