package com.edmark.loyaltyuser.presenter.account;

import android.util.Log;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.CardType;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;
import com.edmark.loyaltyuser.util.AppPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edmark.loyaltyuser.constant.Common.API_ACCOUNT_SELECT;
import static com.edmark.loyaltyuser.constant.Common.CUSTID;
import static com.edmark.loyaltyuser.constant.Common.EDA;
import static com.edmark.loyaltyuser.constant.Common.ERRORCODE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.MAIN_ACCOUNT;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.NAME;
import static com.edmark.loyaltyuser.constant.Common.PACKAGE_TYPE;
import static com.edmark.loyaltyuser.constant.Common.REQUEST_CODE_REFRESH;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.STATUS_CODE_INACTIVE;
import static com.edmark.loyaltyuser.constant.Common.SUB_ACCOUNT;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 06/01/2018.
 */

public class AccountPresenter implements AccountContract.Presenter {
    public static final String TAG = AccountPresenter.class.getSimpleName();

    private AccountContract.View view;
    private AppPreference preference;

    public AccountPresenter(AccountContract.View view) {
        this.view = view;
        preference = AppPreference.getInstance(view.getContext());
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void loadAccount() {
        ArrayList<CardType> datalist = new ArrayList<>();
        view.showProgress();
        String custId = preference.getString(KEY_CUSID);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.mainCustInfo(API_ACCOUNT_SELECT, custId, String.valueOf(unixTimeStamp()), getVCKeyED2E());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "response.raw().request().url():" + response.raw().request().url());
                if(response.body()==null)
                {
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    view.onLoadAccountFailed();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Log.d(TAG, "onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);

                    String status = (String) obj.get(STATUS);

                    if (status.equals(SUCCESS)) {
                        JSONArray jsonarrayMain = new JSONArray(String.valueOf(obj.get(MAIN_ACCOUNT)));
                        for (int i = 0; i < jsonarrayMain.length(); i++) {
                            JSONObject jsonobject = jsonarrayMain.getJSONObject(i);
                            String package_type = jsonobject.getString(PACKAGE_TYPE);
                            String eda = jsonobject.getString(EDA);
                            String name = jsonobject.getString(NAME);
                            int custid = jsonobject.getInt(CUSTID);
                            CardType cardType = new CardType(package_type, eda, name, custid, STATUS_CODE_INACTIVE);
                            datalist.add(cardType);

                        }

                        try {
                            JSONArray jsonarraySub = new JSONArray(String.valueOf(obj.get(SUB_ACCOUNT)));
                            for (int i = 0; i < jsonarraySub.length(); i++) {
                                JSONObject jsonobject = jsonarraySub.getJSONObject(i);
                                String package_type = jsonobject.getString(PACKAGE_TYPE);
                                String eda = jsonobject.getString(EDA);
                                String name = jsonobject.getString(NAME);
                                int custid = jsonobject.getInt(CUSTID);
                                CardType cardType = new CardType(package_type, eda, name, custid, STATUS_CODE_INACTIVE);
                                datalist.add(cardType);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "datalist:" + datalist.size());
                        view.onLoadAccountSuccess(datalist);
                    } else {
                        String message = (String) obj.get(MESSAGE);
                        String errorCode = (String) obj.get(ERRORCODE);
                        view.showToast(view.getContext().getResources().getString(R.string.label_error_code, message, errorCode));
                        view.onLoadAccountFailed();
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    view.showToast(e.getMessage());
                    view.onLoadAccountFailed();
                }
                view.hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    view.hideProgress();
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    view.onLoadAccountFailed();
                    return;
                }

                Log.d(TAG, "onFailure:");
                t.printStackTrace();
                view.hideProgress();
                view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                view.onLoadAccountFailed();
                //view.showToast(t.getMessage());
            }
        });
    }

    @Override
    public void loadAPI(String currentCust, String API, String serial_no, String activation_code) {
        view.showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.account_signup_upgrade(API, currentCust, serial_no, activation_code, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.d(TAG, "Raw Url:" + response.raw().request().url());
                if(response.body()==null)
                {
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Log.d(TAG, "onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);
                    if (status.equals(SUCCESS)) {
                        String message = (String) obj.get(MESSAGE);
                        view.onDialogDismiss();
                        view.showDialog(REQUEST_CODE_REFRESH, message, true);
                        view.showToast(message);
                    } else {
                        view.onDialogDismiss();
                        String message = (String) obj.get(MESSAGE);
                        //                            errorCode = (String) obj.get(ERRORCODE);
                        view.showToast(message);
                    }
                    view.hideProgress();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    view.hideProgress();
                    view.onDialogDismiss();
                    view.showToast(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    view.hideProgress();
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    return;
                }

                Log.d(TAG, "onFailure:");
                t.printStackTrace();
                view.hideProgress();
                view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                //view.showToast(t.getMessage());
                view.onDialogDismiss();
            }
        });
    }
}
