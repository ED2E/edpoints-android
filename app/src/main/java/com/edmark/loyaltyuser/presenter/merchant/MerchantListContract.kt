package com.edmark.loyaltyuser.presenter.merchant

import com.edmark.loyaltyuser.model.Merchant
import com.edmark.loyaltyuser.presenter.BasePresenter
import com.edmark.loyaltyuser.presenter.BaseView
import java.util.*

/*
 * Created by DEV-Michael-ED2E on 07/06/2018.
 */

interface MerchantListContract {

    interface View : BaseView {

        fun onLoadSuccess(merchantList: ArrayList<Merchant>)
    }

    interface Presenter : BasePresenter {

        fun getMerchants(api: String, category: String, companyCode: String)
    }
}
