package com.edmark.loyaltyuser.presenter;

/*
 * Created by DEV-Michael-ED2E on 04/29/2018.
 */

public interface BasePresenter {

    void start();

    void stop();
}
