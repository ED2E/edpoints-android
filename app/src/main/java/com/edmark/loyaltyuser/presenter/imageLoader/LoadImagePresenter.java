package com.edmark.loyaltyuser.presenter.imageLoader;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.edmark.loyaltyuser.util.AppPreference;
import com.glide.slider.library.svg.GlideApp;

import timber.log.Timber;

/*
 * Created by DEV-Michael-ED2E on 06/01/2018.
 */

public class LoadImagePresenter implements LoadImageContract.Presenter {
    public static final String TAG = LoadImagePresenter.class.getSimpleName();

    private LoadImageContract view;

    public LoadImagePresenter(LoadImageContract view) {
        this.view = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void loadImage(Context context, String tag, int requestCode, String imageURL, ImageView imageView, int errorImage, boolean saveCache) {
        Timber.d("loadImage:"+tag);
        try {
            RequestOptions options;
            if (saveCache)
                options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);
            else
                options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false).signature(new ObjectKey(String.valueOf(System.currentTimeMillis())));

            GlideApp.with(context)
                    .load(imageURL)
                    .apply(options)
                    .placeholder(errorImage)
                    .error(errorImage)
                    .listener(new RequestListener<Drawable>() {

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            view.onLoadfailed(requestCode);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            view.onResourceready(requestCode);
                            return false;
                        }
                    })
                    .into(imageView);
        } catch (Exception e) {
            Timber.d(tag + " Class Already Close");
            e.printStackTrace();
        }
    }
}
