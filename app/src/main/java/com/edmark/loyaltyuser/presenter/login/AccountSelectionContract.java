package com.edmark.loyaltyuser.presenter.login;

import com.edmark.loyaltyuser.model.CardType;
import com.edmark.loyaltyuser.presenter.BasePresenter;
import com.edmark.loyaltyuser.presenter.BaseView;

import java.util.ArrayList;

/*
 * Created by DEV-Michael-ED2E on 05/08/2018.
 */

public interface AccountSelectionContract {

    interface View extends BaseView {

        void onLoadSuccess(ArrayList<CardType> data);
        void onLoadFailed();
    }

    interface Presenter extends BasePresenter {

        void accounts();
        void saveCustID(String custid);
    }
}
