package com.edmark.loyaltyuser.presenter.login;

import com.edmark.loyaltyuser.presenter.BasePresenter;
import com.edmark.loyaltyuser.presenter.BaseView;

/*
 * Created by DEV-Michael-ED2E on 05/08/2018.
 */

public interface LoginContract {

    interface View extends BaseView {

        void onLoadEmailSuccess(String username);
        void onLoginSuccess(String userType);
    }

    interface Presenter extends BasePresenter {

        void login(String type, String email, String password, Boolean autoLogin);
    }
}
