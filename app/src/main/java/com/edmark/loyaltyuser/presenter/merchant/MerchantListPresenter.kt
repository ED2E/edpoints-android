package com.edmark.loyaltyuser.presenter.merchant

import com.edmark.loyaltyuser.R
import com.edmark.loyaltyuser.constant.Common.*
import com.edmark.loyaltyuser.model.Merchant
import com.edmark.loyaltyuser.rest.ApiClient
import com.edmark.loyaltyuser.rest.ApiInterface
import com.edmark.loyaltyuser.util.AppPreference
import com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E
import com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.*

/*
 * Created by DEV-Michael-ED2E on 05/08/2018.
 */

class MerchantListPresenter(private val view: MerchantListContract.View) : MerchantListContract.Presenter {
    private val preference: AppPreference
    private var apiService: ApiInterface? = null
    private var gson: Gson? = null

    init {
        preference = AppPreference.getInstance(view.context)
        apiService = ApiClient.getClient().create(ApiInterface::class.java)
        gson = Gson()
    }

    override fun start() {

    }

    override fun stop() {

    }

    override fun getMerchants(api: String, category: String, companyCode: String) {
        view.showProgress()
        val datalist = ArrayList<Merchant>()
        val call = apiService!!.getmerchant(api, unixTimeStamp().toString(), getVCKeyED2E())
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Timber.d("url():" + response.raw().request().url())
                if (response.body() == null) {
                    view.showToast(view.context.resources.getString(R.string.error_connection))
                    view.hideProgress()
                    return
                }
                try {
                    val json = Objects.requireNonNull<ResponseBody>(response.body()).string()
                    Timber.d("onResponse:" + response.code() + " " + json)
                    val obj = JSONObject(json)
                    datalist.clear()
                    val jsonarrayMain = JSONArray(obj.get(DATA).toString())
                    for (i in 0 until jsonarrayMain.length()) {
                        val jsonobject = jsonarrayMain.getJSONObject(i)
                        val merchant = gson!!.fromJson(jsonobject.toString(), Merchant::class.java)
                        if (merchant.company_code != null) {
                            if (merchant.company_code == companyCode) {

                                if (category == view.context.getResources().getString(R.string.label_merchant_category_food)) {
                                    if (merchant.merchant_category.equals(CAT_FOOD_BEV, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                } else if (category == view.context.getResources().getString(R.string.label_merchant_category_travels)) {
                                    if (merchant.merchant_category.equals(CAT_TRAVELS, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                } else if (category == view.context.getResources().getString(R.string.label_merchant_category_life_style)) {
                                    if (merchant.merchant_category.equals(CAT_LIFESTYLE, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                } else if (category == view.context.getResources().getString(R.string.label_merchant_category_entertainment)) {
                                    if (merchant.merchant_category.equals(CAT_ENTERTAINMENT, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                } else if (category == view.context.getResources().getString(R.string.label_merchant_category_3c_products)) {
                                    if (merchant.merchant_category.equals(CAT_3C_PRODUCTS, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                } else if (category == view.context.getResources().getString(R.string.label_merchant_category_health_care)) {
                                    if (merchant.merchant_category.equals(CAT_HEALT_CARE, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                } else if (category == view.context.getResources().getString(R.string.label_merchant_category_services)) {
                                    if (merchant.merchant_category.equals(CAT_SERVICES, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                } else if (category == view.context.getResources().getString(R.string.label_merchant_category_living)) {
                                    if (merchant.merchant_category.equals(CAT_LIVING, ignoreCase = true)) {
                                        datalist.add(merchant)
                                    }
                                }
                            }
                        }
                    }

                    view.onLoadSuccess(datalist)
                } catch (e: IOException) {
                    e.printStackTrace()
                    view.showToast(e.message)
                } catch (e: JSONException) {
                    e.printStackTrace()
                    view.showToast(e.message)
                }

                view.hideProgress()
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                Timber.d("onFailure:")
                if (t is SocketTimeoutException) {
                    view.showToast(view.context.getResources().getString(R.string.error_connection))
                    view.hideProgress()
                    return
                }
                t.printStackTrace()
                view.showToast(t.message)
                view.hideProgress()
            }
        })
    }

    companion object {
        val TAG = MerchantListPresenter::class.java.simpleName
    }
}
