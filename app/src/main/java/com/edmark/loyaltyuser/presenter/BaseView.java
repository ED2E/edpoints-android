package com.edmark.loyaltyuser.presenter;

import android.content.Context;

/*
 * Created by DEV-Michael-ED2E on 04/29/2018.
 */

public interface BaseView {

    Context getContext();

    void showProgress();

    void hideProgress();

    void showDialog(int requestCode, String message, boolean isSingleButton);

    void hideDialog();

    void showError(String message);

    void showToast(String message);

}
