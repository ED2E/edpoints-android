package com.edmark.loyaltyuser.presenter.advertisement

import com.edmark.loyaltyuser.model.Ads
import com.edmark.loyaltyuser.presenter.BasePresenter
import com.edmark.loyaltyuser.presenter.BaseView

import java.util.ArrayList

/*
 * Created by DEV-Michael-ED2E on 07/06/2018.
 */

interface AdvertsimentContract {

    interface View : BaseView {

        fun onLoadSuccess(adsList: ArrayList<Ads>)
    }

    interface Presenter : BasePresenter {

        fun getAdvertisement(companyCode: String)
    }
}
