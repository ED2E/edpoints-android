package com.edmark.loyaltyuser.presenter.login;

import android.util.Log;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.rest.ApiClient;
import com.edmark.loyaltyuser.rest.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edmark.loyaltyuser.constant.Common.KEY_PASSWORD;
import static com.edmark.loyaltyuser.constant.Common.MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.STATUS;
import static com.edmark.loyaltyuser.constant.Common.SUCCESS;
import static com.edmark.loyaltyuser.util.AppUtility.getVCKeyED2E;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;

/*
 * Created by DEV-Michael-ED2E on 05/30/2018.
 */
public class ForgotPassPresenter implements ForgotPassContract.Presenter {
    public static final String TAG = ForgotPassPresenter.class.getSimpleName();

    private ForgotPassContract.View view;

    public ForgotPassPresenter(ForgotPassContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void loadPage(String type) {
        if (type != null) {
            if (type.equals(KEY_PASSWORD)) {
                view.onLoadPageSuccess(view.getContext().getResources().getString(R.string.label_forgot_password));
            } else {
                view.onLoadPageSuccess(view.getContext().getResources().getString(R.string.label_forgot_pin));
            }
        }
    }

    @Override
    public void loadAPI(String page, String email) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.forgotapi(page, email, getVCKeyED2E(), String.valueOf(unixTimeStamp()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "Raw Url:" + response.raw().request().url());
                view.hideProgress();
                if(response.body()==null)
                {
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    return;
                }
                try {
                    String json = Objects.requireNonNull(response.body()).string();
                    Log.d(TAG, "onResponse:" + response.code() + " " + json);
                    JSONObject obj = new JSONObject(json);
                    String status = (String) obj.get(STATUS);
                    String message = (String) obj.get(MESSAGE);
                    if (status.equals(SUCCESS)) {
                        view.showToast(message);
                        view.onEmailSentSuccess(message);
                    } else {
                        view.showToast(message);
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    view.showToast(e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure:");
                if(t instanceof SocketTimeoutException){
                    view.showToast(view.getContext().getResources().getString(R.string.error_connection));
                    view.hideProgress();
                    return;
                }
                t.printStackTrace();
                view.hideProgress();
                view.showToast(t.getMessage());
            }
        });
    }
}
