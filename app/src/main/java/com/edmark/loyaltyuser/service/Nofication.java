package com.edmark.loyaltyuser.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;import android.text.Html;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.util.AppPreference;
import com.edmark.loyaltyuser.view.activity.advertisement.AdvertisementActivity;
import com.edmark.loyaltyuser.view.activity.advertisement.AdvertisementDetailsActivity;
import com.edmark.loyaltyuser.view.activity.login.LoginActivity;
import com.edmark.loyaltyuser.view.activity.main.MainActivity;
import com.edmark.loyaltyuser.view.activity.transaction.history.TransactionHistoryActivity;

import static com.edmark.loyaltyuser.constant.Common.KEY_ACCOUNT_TYPE;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_ADVERTISEMENT;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_ANNOUNCEMENT;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_DASHBOARD;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_TRANSACTION_HISTORY;

public class Nofication {
    private static final String TAG = Nofication.class.getSimpleName();

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    public static void showNotification(Context context, String id, String notifType, String url, Bitmap icon, String title, String messageBody) {
        // Intent to go on click on notification

        PendingIntent pendingIntent = getPendingIntent(context, id, notifType, url);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        NotificationCompat.Builder notificationBuilder;

        if (Build.VERSION.SDK_INT >= 21) {
            notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setLargeIcon(icon)
                    .setContentText(messageBody)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        } else {
            notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setLargeIcon(icon)
                    .setContentText(messageBody)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        }
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = (int) System.currentTimeMillis();
        assert notificationManager != null;
        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
    }

    public static void showBigNotification(Context context, String id, String notifType, String url, Bitmap bitmap, Bitmap icon, String title, String message) {

        NotificationCompat.Builder notificationBuilder;

        Log.d(TAG, "Show Notif with image");
        PendingIntent pendingIntent = getPendingIntent(context, id, notifType, url);
        notificationBuilder = new NotificationCompat.Builder(context);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = notificationBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setSound(defaultSoundUri)
                .setStyle(bigPictureStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();

        int notificationId = (int) System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(notificationId, notification);
        }
    }

    private static PendingIntent getPendingIntent(Context context, String id, String notifType, String url)
    {
        PendingIntent pendingIntent;
        if (notifType != null && notifType.equalsIgnoreCase(TAG_NOTIF_DASHBOARD)) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

        } else if (notifType != null && notifType.equalsIgnoreCase(TAG_NOTIF_ADVERTISEMENT)) {
            AppPreference preference = AppPreference.getInstance(context);
            String custId = preference.getString(KEY_CUSID);
            Log.d(TAG, "FCM custId : " + custId);
            if(custId!=null)
            {
                if(!custId.equals("")) {
                    Intent intent = new Intent(context, AdvertisementActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                }
                else
                {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                }
            }
            else
            {
                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
            }

        } /*else if (notifType != null && notifType.equalsIgnoreCase(TAG_NOTIF_ANNOUNCEMENT)) {

            Intent intent = new Intent(context, AdvertisementDetailsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(KEY_URL,url);
            pendingIntent = PendingIntent.getActivity(context, 0 *//* Request code *//*, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

        }*/ else if (notifType != null && notifType.equalsIgnoreCase(TAG_NOTIF_TRANSACTION_HISTORY)) {

            Intent intent = new Intent(context, TransactionHistoryActivity.class);
            intent.putExtra(KEY_ACCOUNT_TYPE, id);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

        } else {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return pendingIntent;
    }
}
