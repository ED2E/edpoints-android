package com.edmark.loyaltyuser.service;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.Patterns;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.sugardb.FirebasePendingNotif;
import com.edmark.loyaltyuser.util.AppPreference;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.orm.SugarDb;

import java.util.Objects;

import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_ICON;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_ID;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_NOTIFTYPE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_PICTURE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_TITLE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.KEY_CUSID;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_ADVERTISEMENT;
import static com.edmark.loyaltyuser.constant.Common.TAG_NOTIF_ANNOUNCEMENT;
import static com.edmark.loyaltyuser.service.Nofication.showNotification;
import static com.edmark.loyaltyuser.service.Nofication.showBigNotification;
import static com.edmark.loyaltyuser.util.AppUtility.getBitmapFromURL;
import static com.edmark.loyaltyuser.util.AppUtility.unixTimeStamp;
import static com.edmark.loyaltyuser.util.SugarDBUtils.createTable;

public class FirebasemessagingService extends FirebaseMessagingService {

    private static final String TAG = FirebasemessagingService.class.getSimpleName();
    private String id, url = "", message, notitype = "";

    @Override
    public void onMessageSent(String msgId) {
        Log.e(TAG, "onMessageSent: " + msgId);
    }

    @Override
    public void onSendError(String msgId, Exception e) {
        Log.e(TAG, "onSendError: " + msgId);
        Log.e(TAG, "Exception: " + e);
    }
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        AppPreference preference = AppPreference.getInstance(this);
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See showNotification method below.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.d(TAG, "FCM Data: " + remoteMessage.getData());

        if (remoteMessage.getData().get(FIREBASE_KEY_ID) != null) {
            id = remoteMessage.getData().get(FIREBASE_KEY_ID);
        }

        if (remoteMessage.getData().get(FIREBASE_KEY_URL) != null) {
            url = remoteMessage.getData().get(FIREBASE_KEY_URL);
        }

        if (remoteMessage.getData().get(FIREBASE_KEY_MESSAGE) != null) {
            message = remoteMessage.getData().get(FIREBASE_KEY_MESSAGE);
        }

        if (remoteMessage.getData().get(FIREBASE_KEY_NOTIFTYPE) != null) {
            notitype = remoteMessage.getData().get(FIREBASE_KEY_NOTIFTYPE);
        }

        String notifCustid = "";
        if (remoteMessage.getData().get(FIREBASE_KEY_CUSTID) != null) {
            notifCustid = remoteMessage.getData().get(FIREBASE_KEY_CUSTID);
        }

        String imageUrl = "";
        if (remoteMessage.getData().get(FIREBASE_KEY_PICTURE) != null) {
            imageUrl = remoteMessage.getData().get(FIREBASE_KEY_PICTURE);
        }

        String iconUrl = "";
        if (remoteMessage.getData().get(FIREBASE_KEY_ICON) != null) {
            iconUrl = remoteMessage.getData().get(FIREBASE_KEY_ICON);
        }

        String title = getString(R.string.app_name);
        if (remoteMessage.getData().get(FIREBASE_KEY_TITLE) != null) {
            title = remoteMessage.getData().get(FIREBASE_KEY_TITLE);
        }

        Gson gson = new Gson();
        if(notitype.equals(TAG_NOTIF_ADVERTISEMENT)||notitype.equals(TAG_NOTIF_ANNOUNCEMENT))
        {
            Log.d(TAG, "Filtered Data: " + id + " " + url + " " + notitype + " " +imageUrl);
            Log.d(TAG, "message: " + message);
            if (message == null) {
                String msg = Objects.requireNonNull(remoteMessage.getNotification()).getBody();
                Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                showNotification(this, id, notitype, url, icon, title, msg);
            } else {

                Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                if(iconUrl != null && iconUrl.length() > 4 && Patterns.WEB_URL.matcher(iconUrl).matches())
                {
                    icon = getBitmapFromURL(iconUrl);
                    if(icon!=null)
                    {
                        icon = Bitmap.createScaledBitmap(icon, 192, 192, true);
                    }
                }
                if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                    Bitmap bitmap = getBitmapFromURL(imageUrl);

                    if (bitmap != null) {
                        showBigNotification(this, id, notitype, url, bitmap, icon,title, message);
                    } else {
                        showNotification(this, id, notitype, url, icon, title, message);
                    }
                } else {
                    showNotification(this, id, notitype, url, icon, title, message);
                }
            }
        }
        else
        {
            String custId = preference.getString(KEY_CUSID);
            Log.d(TAG, "FCM custId : " + custId + " " + notifCustid + ":");
            if(custId!=null)
            {
                if(custId.equals(notifCustid))
                {
                    // Receiving pushnotification from FCM server
                    Log.d(TAG, "Filtered Data: " + id + " " + url + " " + notitype + " " +imageUrl);
                    Log.d(TAG, "message: " + message);
                    //image = getBitmapFromURL(img);
                    if (message == null) {
                        String msg = Objects.requireNonNull(remoteMessage.getNotification()).getBody();
                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                        showNotification(this, id, notitype, url, icon, title, msg);
                    } else {

                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                        if(iconUrl != null && iconUrl.length() > 4 && Patterns.WEB_URL.matcher(iconUrl).matches())
                        {
                            icon = getBitmapFromURL(iconUrl);
                            if(icon!=null)
                            {
                                icon = Bitmap.createScaledBitmap(icon, 192, 192, true);
                            }
                        }
                        if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                            Bitmap bitmap = getBitmapFromURL(imageUrl);

                            if (bitmap != null) {
                                showBigNotification(this, id, notitype, url, bitmap, icon, title, message);
                            } else {
                                showNotification(this, id, notitype, url, icon, title, message);
                            }
                        } else {
                            showNotification(this, id, notitype, url, icon, title, message);
                        }
                    }
                }
                else
                {
                    if(!custId.equals("")) {
                        Log.d(TAG, "FCM Data to pending: " + gson.toJson(remoteMessage.getData()));
                        try {
                            FirebasePendingNotif notif = new FirebasePendingNotif(notifCustid, gson.toJson(remoteMessage.getData()), String.valueOf(unixTimeStamp()));
                            notif.save();
                        } catch (Exception e) {
                            e.printStackTrace();
                            createTable(FirebasePendingNotif.class, new SugarDb(this).getDB());
                            FirebasePendingNotif notif = new FirebasePendingNotif(notifCustid, remoteMessage.getData().toString(), String.valueOf(unixTimeStamp()));
                            notif.save();
                        }
                    }
                }

            }
        }


    }
    // [END receive_message]
}
