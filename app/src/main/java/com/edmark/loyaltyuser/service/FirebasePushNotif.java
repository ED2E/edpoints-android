package com.edmark.loyaltyuser.service;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import static com.edmark.loyaltyuser.constant.Common.DISTRIBUTOR;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_CATEGORY;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_CUSTID;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_ICON;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_ID;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_MESSAGE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_NOTIFTYPE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_PICTURE;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_KEY_URL;
import static com.edmark.loyaltyuser.constant.Common.FIREBASE_SERVER_KEY;
import static com.edmark.loyaltyuser.constant.Common.MERCHANT;

public class FirebasePushNotif {

    public static final String TAG = FirebasePushNotif.class.getSimpleName();

    public static void pushNotificationMerchant(String custId, ArrayList<String> merchantTokens, String message,String imagepath, String iconpath, String notifUrl, String notifType) {
        JSONObject jPayload = new JSONObject();
        JSONObject jData = new JSONObject();
        JSONObject jNotif = new JSONObject();
        try {

//            jNotif.put("body", message);
//            jNotif.put("title", "Payment Successful");
            jNotif.put(FIREBASE_KEY_CATEGORY, "edpointsNotifCategory");

            jData.put(FIREBASE_KEY_ID, MERCHANT);
            jData.put(FIREBASE_KEY_PICTURE, imagepath);
            jData.put(FIREBASE_KEY_URL, notifUrl);
            jData.put(FIREBASE_KEY_MESSAGE, message);
            jData.put(FIREBASE_KEY_NOTIFTYPE, notifType);
            jData.put(FIREBASE_KEY_CUSTID, custId);
            jData.put(FIREBASE_KEY_ICON, iconpath);

            JSONArray ja = new JSONArray();
            for (String token: merchantTokens) {
                Log.d(TAG,"merchantTokens:"+token);
                ja.put(token); //merchant token
            }
            jPayload.put("registration_ids", ja);
            jPayload.put("priority", "high");
            jPayload.put("data", jData);
            //for ios to accept the notifications
            jPayload.put("content_available", true);
            jPayload.put("notification", jNotif);

            /* This is static don't change if you don't want to die */
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key="+FIREBASE_SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(() -> {
//                    mTextView.setText(resp);
                Log.d(TAG,"firebase resp:"+resp);
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void pushNotificationDistributor(String custId, ArrayList<String> distributor, String message,String imagepath, String iconpath, String notifUrl, String notifType) {
        JSONObject jPayload = new JSONObject();
        JSONObject jData = new JSONObject();
        JSONObject jNotif = new JSONObject();
        try {

//            jNotif.put("body", message);
//            jNotif.put("title", "Payment Successful");
            jNotif.put(FIREBASE_KEY_CATEGORY, "edpointsNotifCategory");

            jData.put(FIREBASE_KEY_ID, DISTRIBUTOR);
            jData.put(FIREBASE_KEY_PICTURE, imagepath);
            jData.put(FIREBASE_KEY_URL, notifUrl);
            jData.put(FIREBASE_KEY_MESSAGE, message);
            jData.put(FIREBASE_KEY_NOTIFTYPE, notifType);
            jData.put(FIREBASE_KEY_CUSTID, custId);
            jData.put(FIREBASE_KEY_ICON, iconpath);

            JSONArray ja = new JSONArray();
            for (String token: distributor) {
                Log.d(TAG,"merchantTokens:"+token);
                ja.put(token); //merchant token
            }
            jPayload.put("registration_ids", ja);
            jPayload.put("priority", "high");
            jPayload.put("data", jData);
            //for ios to accept the notification
            jPayload.put("content_available", true);
            jPayload.put("notification", jNotif);

            /* This is static don't change if you don't want to die */
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key="+FIREBASE_SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(() -> {
//                    mTextView.setText(resp);
                Log.d(TAG,"firebase resp:"+resp);
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}
