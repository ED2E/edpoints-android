package com.edmark.loyaltyuser.constant;

/*
 * Created by DEV-Michael-ED2E on 02/04/2018.
 */

public class Common {

    //Firebase
    public static final String FIREBASE_SERVER_KEY              = "AAAAxgtywnA:APA91bEz8cgSCHTrGFOHbYtcF1t3ba0KpVxbIlQtZg31o0LjH4dhzzkL5jQ1hvEnp4jlPHhOevfoOxXKW-GaP3Dtx5RFGvl-fqhT-JhKzD1yctLszR8y1Lg8b6w6eofwmpnBpbU6Hjik";
    public static final String FIREBASE_TOPIC                   = "EDPoints";
    public static final String GOOGLE_MAP_API_KEY               = "AIzaSyC7LTbPIUW__0wh47zrz6TukWXLnJ6KMps";

    //API
    public static final String AUTH_KEY                         = "fw!98*edmark#Se";         // For Authentication
    public static final String AUTH_KEY_ED2E                    = "ED2E*edmark#Se";         // For Authentication New
    public static final String API_LOGIN                        = "Solucis:apiLoginV2";
    public static final String API_ACCOUNT_SELECT               = "Solucis:apiMainCustInfoV2";
    public static final String API_CUSTINFO                     = "Solucis:apiCustInfoV2";
    public static final String API_CUSTPOINT                    = "Solucis:apiCustPointV2";
    public static final String API_LOYALTYPOINT                 = "Solucis:apiCustLEPointV2";
    public static final String API_CUSTCODE                     = "Solucis:apiCustCodeV2";
    public static final String API_TRANSACTION                  = "Solucis:apiCustUpdatePointV2";
    public static final String API_UPGRADE_PACKAGE              = "Solucis:apiUpgradePackageV2";
    public static final String API_SIGN_UP_SUB_ACC              = "Solucis:apiSignupSubAccV2";
    public static final String API_REFERRAL_BONUS               = "Solucis:apiCustRBonusV2";
    public static final String API_TIER_SUMMARY                 = "Solucis:apiCustTierBonusSummaryV2";
    public static final String API_TIER_ONE                     = "Solucis:apiCustTierBonusDL1V2";
    public static final String API_TIER_TWO                     = "Solucis:apiCustTierBonusDL2V2";
    public static final String API_TIER_THREE                   = "Solucis:apiCustTierBonusDL3V2";
    public static final String API_SPEND_EARN                   = "Solucis:apiCustEarnLEPV2";
    public static final String API_CHANGE_PASS                  = "Solucis:apiUpdatePasswordV2";
    public static final String API_CHANGE_PIN                   = "Solucis:apiUpdatePinV2";
    public static final String API_TRANSACTION_HISTORY          = "Solucis:apiTransactionLedger";
    public static final String API_SUBMIT_REVIEW                = "Solucis:apiSubmitReview";
    public static final String API_ONLINE_MERCHANT              = "Solucis:apiOnlineMerchantIisting";
    public static final String API_OFFLINE_MERCHANT             = "Solucis:apiOffMerchantIisting";
    public static final String API_STATISTICS_ANALYSIS          = "Solucis:apiTransactionAnalysis";
    public static final String API_REVIEW_LISTING               = "Solucis:apiReviewList";
    public static final String API_GET_TRANSACTION_ID           = "Solucis:apiGetTransNo";
    public static final String API_VALIDATE_TRANSACTION         = "Solucis:apiValidateTransaction";
    public static final String API_CUST_PROFILE                 = "Solucis:apiCustProfile";
    public static final String API_MERCHANT_PROFILE             = "Solucis:apiMerchantProfile";
    public static final String API_ADS_LIST                     = "Solucis:apiAdsListing";
    public static final String API_CUST_PROFILE_UPDATE          = "Solucis:apiUpdCustProfile";
    public static final String API_MERCHANT_PROFILE_UPDATE      = "Solucis:apiUpdMerchantProfile";
    public static final String API_DISTRIBUTOR_SIGN_UP          = "Solucis:apiSignUpV2";
    public static final String API_CHECK_PIN_STATUS             = "Solucis:apiCheckPin";
    public static final String API_FORGOT_PASSWORD              = "Solucis:apiForgotPasswordStep";
    public static final String API_FORGOT_PIN                   = "Solucis:apiForgotPinStep";
    public static final String API_MASSAGE_CHAIR_STATUS         = "Solucis:apiGetMassageChairStatus";
    public static final String API_MASSAGE_CHAIR_TRANSACTION    = "Solucis:apiCustUpdatePointChair";
    public static final String API_MASSAGE_CHAIR_USAGE          = "Solucis:apiMassageChairUsage";
    public static final String API_INSERT_PIN                   = "Solucis:apiInsertPin";
    public static final String API_EDA_VERIFY                   = "Solucis:apiGetCustCodeV2";
    public static final String API_VALIDATE_TRANSACTION_CSB     = "Solucis:apiValidateTransactionCSB";
    public static final String API_UPDATE_TRANSACTION_CSB       = "Solucis:apiUpdateTransactionCSB";
    public static final String API_VALIDATE_PIN_COIN            = "Solucis:apiValidatePinCoin";


    //Response
    public static final String STATUS                           = "status";
    public static final String MESSAGE                          = "message";
    public static final String CUSTID                           = "custid";
    public static final String NAME                             = "name";
    public static final String EDA                              = "eda";
    public static final String EDA_NAME                         = "eda_name";
    public static final String MAIN_ACCOUNT                     = "MainAccount";
    public static final String SUB_ACCOUNT                      = "SubAccount";
    public static final String PACKAGE_TYPE                     = "package_type";
    public static final String TYPE                             = "type";
    public static final String ERRORCODE                        = "errorcode";
    public static final String POINT                            = "point";
    public static final String DATA                             = "data";
    public static final String EMAIL                            = "email";
    public static final String DEVICE_NAME                      = "deviceName";
    public static final String DEVICE_OS_VER                    = "deviceOSver";
    public static final String DEVICE_ID                        = "deviceid";
    public static final String DEVICE_TOKEN                     = "token";
    public static final String DEVICE_TYPE                      = "android";
    public static final String COUNTRY                          = "company_code";
    public static final String TOTAL_SALES                      = "total_sales";
    public static final String TRANSACTION_ID                   = "TrackingNo";
    public static final String CHAIR_ERRORCODE                  = "chair_error_code";
    public static final String ERROR_MESSAGE                    = "errmsg";
    public static final String ERRORCODE_UPLOAD                 = "error";
    public static final String PIN                              = "pin";
    public static final String COMPANY_CODE                     = "company_code";
    public static final String RESPONSE_TRANSACTION_ID          = "new_transaction_no";
    public static final String DOCNO                            = "tmp_docno";
    public static final String MERCHANTID                       = "merchant_id";
    public static final String CSB_AMOUNT                       = "Total Amount of CSB is ";
    public static final String CSB                              = "csb";
    public static final String TOTAL_EDP_TRANSFERRED            = "total_edp_transferred";
    public static final String LOGS                             = "logs";
    public static final String ID                               = "id";
    public static final String ED2E_ID                          = "ed2e_id";
    public static final String CREATED_DATE                     = "created_date";
    public static final String ERROR_CODE                       = "error_code";


    //Response status
    public static final String SUCCESS                          = "success";
    public static final String FAIL                             = "fail";
    public static final String SUCCESSFUL                       = "successful";

    //Response level
    public static final String LEVEL_1                          = "1";

    //Response type
    public static final String DISTRIBUTOR                      = "distributor";
    public static final String MERCHANT                         = "merchant";

    //Response message
    public static final String LOGIN_SUCCESS                    = "you have login successfully!";
    public static final String NO_RESULT                        = "No result.";

    //Response chair error code
    public static final int CHAIR_ERROR_CODE_0                  = 0;

    //USER Profile
    public static final String USERNAME_STR                     = "userName";

    //Card Type
    public static final String ORDINARY                         = "ORDINARY";
    public static final String ORDINARYP                        = "ORDINARY-P";
    public static final String GOLD                             = "GOLD";
    public static final String SILVER                           = "SILVER";
    public static final String BRONZE                           = "BRONZE";

    //Preference Key & Intent Key
    public static final String KEY_REMEMBERED                   = "KEY_REMEMBERED";
    public static final String KEY_CUSID                        = "KEY_CUSID";
    public static final String KEY_NAME                         = "KEY_NAME";
    public static final String KEY_ACCOUNT_TYPE                 = "KEY_ACCOUNT_TYPE";
    public static final String KEY_FORGOT_TYPE                  = "KEY_FORGOT_TYPE";
    public static final String KEY_TRANS_CUSTID                 = "KEY_TRANS_CUSTID";
    public static final String KEY_MERCHANT                     = "KEY_MERCHANT";
    public static final String KEY_TRANSACTION_AMOUNT           = "KEY_TRANSACTION_AMOUNT";
    public static final String KEY_TRANSACTION_ID               = "KEY_TRANSACTION_ID";
    public static final String KEY_LOYALTY_EARNED               = "KEY_LOYALTY_EARNED";
    public static final String KEY_DESCRIPTION                  = "KEY_DESCRIPTION";
    public static final String KEY_MERCHANT_ID                  = "KEY_MERCHANT_ID";
    public static final String KEY_EDA                          = "KEY_EDA";
    public static final String KEY_EMAIL                        = "KEY_EMAIL";
    public static final String KEY_URL                          = "KEY_URL";
    public static final String KEY_TITLE                        = "KEY_TITLE";
    public static final String KEY_MERCHANT_CODE                = "KEY_MERCHANT_CODE";
    public static final String KEY_WEBVIEW_ACTION               = "com.edmark.loyaltyuser.webview:";
    public static final String KEY_CATEGORY                     = "KEY_CATEGORY";
    public static final String KEY_ADS_DETAILS                  = "KEY_ADS_DETAILS";
    public static final String KEY_PENDING_NOTIFICATION         = "KEY_PENDING_NOTIFICATION";
    public static final String KEY_FIRST_TIME_LAUNCH            = "KEY_FIRST_TIME_LAUNCH";
    public static final String KEY_COUNTRY_CODE                 = "KEY_COUNTRY_CODE";
    public static final String KEY_WALLET_COUNTRY_CODE          = "KEY_WALLET_COUNTRY_CODE";
    public static final String KEY_TRANS_DEVICEID               = "KEY_TRANS_DEVICEID";
    public static final String KEY_MASSAGE_DEVICEID             = "KEY_MASSAGE_DEVICEID";
    public static final String KEY_API                          = "KEY_API";
    public static final String KEY_SCAN_PROCESS                 = "KEY_SCAN_PROCESS";
    public static final String KEY_EDCOIN_BIND_KEY              = "KEY_EDCOIN_BIND_KEY";
    public static final String KEY_CUST_TYPE                    = "KEY_CUST_TYPE";
    public static final String KEY_WALLET_KEY                   = "KEY_WALLET_KEY";
    public static final String KEY_ENABLE_TIP_TRANSFER          = "KEY_ENABLE_TIP_TRANSFER";

    //Password/Pin page
    public static final String KEY_PASSWORD                     = "KEY_PASSWORD";
    public static final String KEY_PIN                          = "KEY_PIN";

    //FRAGMENT TAGS
    public static final String TAG_FRAGMENT_HOME                 = "TAG_FRAGMENT_HOME";
    public static final String TAG_FRAGMENT_MERCHANT             = "TAG_FRAGMENT_MERCHANT";
    public static final String TAG_FRAGMENT_LOCATION             = "TAG_FRAGMENT_LOCATION";
    public static final String TAG_HOME_PROFILE                  = "TAG_HOME_PROFILE";
    public static final String TAG_HOME_SHOW_QR                  = "TAG_HOME_SHOW_QR";
    public static final String TAG_HOME_LOG_OUT                  = "TAG_HOME_LOG_OUT";
    public static final String TAG_HOME_ACCOUNT                  = "TAG_HOME_ACCOUNT";
    public static final String TAG_HOME_SCAN                     = "TAG_HOME_SCAN";
    public static final String TAG_HOME_KEY_IN                   = "TAG_HOME_KEY_IN";
    public static final String TAG_HOME_LEDGER                   = "TAG_HOME_LEDGER";
    public static final String TAG_HOME_TRANSACTION              = "TAG_HOME_TRANSACTION";
    public static final String TAG_HOME_ADVERTISEMENT            = "TAG_HOME_ADVERTISEMENT";
    public static final String TAG_HOME_SUPPORT                  = "TAG_HOME_SUPPORT";
    public static final String TAG_HOME_UTILITIES                = "TAG_HOME_UTILITIES";
    public static final String TAG_MERCHANT_OFFLINE              = "TAG_MERCHANT_OFFLINE";
    public static final String TAG_MERCHANT_ONLINE               = "TAG_MERCHANT_ONLINE";
    public static final String TAG_HOME_STATISTICAL_ANALYSIS     = "TAG_HOME_STATISTICAL_ANALYSIS";
    public static final String TAG_HOME_ADVERTISEMENT_CONTENT    = "TAG_HOME_ADVERTISEMENT_CONTENT";
    public static final String TAG_MAP_DATA                      = "TAG_MAP_DATA";
    public static final String TAG_HOME_RELOAD                   = "TAG_HOME_RELOAD";
    public static final String TAG_HOME_TICKETING                = "TAG_HOME_TICKETING";
    public static final String TAG_HOME_COUNTRY                  = "TAG_HOME_COUNTRY";
    public static final String TAG_HOME_MASSAGE_CHAIR            = "TAG_HOME_MASSAGE_CHAIR";
    public static final String TAG_HOME_MASSAGE_CHAIR_USAGE      = "TAG_HOME_MASSAGE_CHAIR_USAGE";
    public static final String TAG_HOME_FAMILY_TREE              = "TAG_HOME_FAMILY_TREE";
    public static final String TAG_HOME_EDCOIN                   = "TAG_HOME_EDCOIN";
    public static final String TAG_HOME_POINT_TRANSFER           = "TAG_HOME_POINT_TRANSFER";

    //home fragment events
    public static final String EVENT_SET_ED_POINT                = "EVENT_SET_ED_POINT";
    public static final String EVENT_SET_INFO                    = "EVENT_SET_INFO";
    public static final String EVENT_SET_LOYALTY_POINT           = "EVENT_SET_LOYALTY_POINT";
    public static final String EVENT_SET_ADVERTISEMENT           = "EVENT_SET_ADVERTISEMENT";
    public static final String EVENT_SET_PROFILE_IMAGE           = "EVENT_SET_PROFILE_IMAGE";
    public static final String EVENT_SET_MASSAGE_CHAIR           = "EVENT_SET_MASSAGE_CHAIR";

    //merchant fragment events
    public static final String EVENT_VIEW_MERCHANT               = "EVENT_VIEW_MERCHANT";
    public static final String EVENT_VIEW_ONLINE_MERCHANT        = "EVENT_VIEW_ONLINE_MERCHANT";
    public static final String EVENT_VIEW_MERCHANT_LIST          = "EVENT_VIEW_MERCHANT_LIST";
    public static final String EVENT_VIEW_ONLINE_MERCHANT_LIST   = "EVENT_VIEW_ONLINE_MERCHANT_LIST";

    //map fragment events
    public static final String EVENT_VIEW_MAP                    = "EVENT_VIEW_MAP";
    public static final String EVENT_VIEW_MAP_MERCHANT           = "EVENT_VIEW_MAP_MERCHANT";

    //Ledger fragment events
    public static final String EVENT_INIT_API                    = "EVENT_INIT_API";
    public static final String EVENT_OPEN_DATE_PICKER            = "EVENT_OPEN_DATE_PICKER";
    public static final String EVENT_SET_DATE_DISPLAY            = "EVENT_SET_DATE_DISPLAY";
    public static final String EVENT_SET_DATA_DISPLAY            = "EVENT_SET_DATA_DISPLAY";
    public static final String EVENT_SET_NO_DATA_DISPLAY         = "EVENT_SET_NO_DATA_DISPLAY";
    public static final String EVENT_INIT_API_REFERRAL           = "EVENT_INIT_API_REFERRAL";
    public static final String EVENT_INIT_API_SPEND_EARN         = "EVENT_INIT_API_SPEND_EARN";

    //Dialog Request Code
    public static final int REQUEST_CODE_EXIT                    = 0;
    public static final int REQUEST_CODE_UPDATE                  = 1;
    public static final int REQUEST_CODE_LOGOUT                  = 2;
    public static final int REQUEST_CODE_MERCHANT_DETAILS        = 3;
    public static final int REQUEST_CODE_SWITCH                  = 4;
    public static final int REQUEST_CODE_REFRESH                 = 5;
    public static final int REQUEST_CODE_PIN_STATUS              = 6;
    public static final int REQUEST_CODE_OPEN_GPS                = 7;
    public static final int REQUEST_CODE_GOOGLE_LOCATION         = 8;
    public static final int REQUEST_CODE_EDA_NAME                = 9;

    //STATUS CODE
    public static final String STATUS_CODE_ACTIVE                = "STATUS_CODE_ACTIVE";
    public static final String STATUS_CODE_INACTIVE              = "STATUS_CODE_INACTIVE";

    //WebView URL
    public static final String URL_HEADER                        = "http://";
    public static final String URL_ABOUT_EDPOINTS                = "http://www.ed2e.network/about-us";
    public static final String URL_FAQ                           = "http://www.ed2e.network/faq";
    public static final String URL_MAPS                          = "http://maps.google.com/maps?q=";
    public static final String URL_FAIL                          = "file:///android_asset/fail.html";
    public static final String PRIVACY_POLICY                    = KEY_WEBVIEW_ACTION+"http://edmarker.com/ED2E-folder/User-Terms-and-Conditions-ED2E.html";
    public static final String TERMS_CONDITIONS                  = KEY_WEBVIEW_ACTION+"http://edmarker.com/ED2E-folder/Merchant-Terms-and-Conditions-ED2E.html";
    public static final String CONSENT_CLAUSE                    = KEY_WEBVIEW_ACTION+"http://edmarker.com/ED2E-folder/Consent-to-Obtain-Data-ED2E.html";
    public static final String URL_MERCHANT_FORM                 = "http://merchantregistration.ed2e.network/merchant_form/";

    //formats
    public static final String DATE_FORMAT                       = "MM-dd-yyyy";
    public static final String DATE_FORMAT_SERVER                = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_TIME                  = "HH:mm:ss";
    public static final String DATE_FORMAT_MONTH                 = "MMM";
    public static final String DECIMAL_FORMAT                    = "#,###,##0.00";
    public static final String INTEGER_FORMAT                    = "#,###,###";
    public static final String DATE_FORMAT_REVIEW                = "MM/dd/yyyy";
    public static final String DATE_FORMAT_TIME_12_HRS           = "hh:mm a";

    //Ledger Index
    public static final int FRAGMENT_REFERRAL                    = 0;
    public static final int FRAGMENT_SUMMARY                     = 1;
    public static final int FRAGMENT_TIER_ONE                    = 2;
    public static final int FRAGMENT_TIER_TWO                    = 3;
    public static final int FRAGMENT_TIER_THREE                  = 4;
    public static final int FRAGMENT_SPEND_EARN                  = 5;

    //Merchant Categories Index
    public static final int CATEGORY_FOOD_AND_BEVERAGES          = 1201;
    public static final int CATEGORY_TRAVELS                     = 1202;
    public static final int CATEGORY_LIFESTYLE                   = 1203;
    public static final int CATEGORY_ENTERTAINMENT               = 1204;
    public static final int CATEGORY_3C_PRODUCTS                 = 1205;
    public static final int CATEGORY_HEALTH_CARE                 = 1206;
    public static final int CATEGORY_SERVICES                    = 1207;
    public static final int CATEGORY_LIVING                      = 1208;

    //Firebase key
    public static final String FIREBASE_KEY_TOKEN                = "token";
    public static final String FIREBASE_KEY_APP_VER              = "app_ver";
    public static final String FIREBASE_KEY_ID                   = "id";
    public static final String FIREBASE_KEY_PICTURE              = "picture";
    public static final String FIREBASE_KEY_URL                  = "url";
    public static final String FIREBASE_KEY_MESSAGE              = "message";
    public static final String FIREBASE_KEY_NOTIFTYPE            = "notif_type";
    public static final String FIREBASE_KEY_CUSTID               = "custid";
    public static final String FIREBASE_KEY_TITLE                = "title";
    public static final String FIREBASE_KEY_ICON                 = "icon";
    public static final String FIREBASE_KEY_TICKETING            = "ticketing";
    public static final String FIREBASE_KEY_CATEGORY             = "category";

    //Notification Tag
    public static final String TAG_NOTIF_DASHBOARD               = "TAG_NOTIF_DASHBOARD";
    public static final String TAG_NOTIF_ADVERTISEMENT           = "TAG_NOTIF_ADVERTISEMENT";
    public static final String TAG_NOTIF_TRANSACTION_HISTORY     = "TAG_NOTIF_TRANSACTION_HISTORY";
    public static final String TAG_NOTIF_ANNOUNCEMENT            = "TAG_NOTIF_ANNOUNCEMENT";
    public static final String TAG_NOTIF_CONVERSION_HISTORY      = "TAG_NOTIF_CONVERSION_HISTORY";

    //Statistical analysis TAG
    public static final String TAG_STAT_TOTAL                    = "TAG_STAT_TOTAL";
    public static final String TAG_STAT_TODAY                    = "TAG_STAT_TODAY";
    public static final String TAG_STAT_YESTERDAY                = "TAG_STAT_YESTERDAY";
    public static final String TAG_STAT_LAST_WEEK                = "TAG_STAT_LAST_WEEK";
    public static final String TAG_STAT_LAST_MONTH               = "TAG_STAT_LAST_MONTH";

    //Identifier defType
    public static final String DEFTYPE_ARRAY                     = "array";
    public static final String SUPPORT_ARRAY_HEADER              = "support_items_";

    //Request Permission
    public static final int LOCATION_PERMISSION_REQUEST_CODE     = 101;
    public static final int REQUEST_PERMISSION_SETTING           = 102;

    //Splash Screen
    public static final int SPLASH_SCREEN_TIME                   = 2 * 1000;

    //Current Location is not visible
    public static final String LOCATION_NOT_AVAILABLE            = "LOCATION_NOT_AVAILABLE";

    //Merchant category
    public static final String CAT_3C_PRODUCTS                   = "3C Products";
    public static final String CAT_ENTERTAINMENT                 = "Entertainment";
    public static final String CAT_FOOD_BEV                      = "F&B";
    public static final String CAT_HEALT_CARE                    = "Health Care";
    public static final String CAT_LIFESTYLE                     = "Lifestyle";
    public static final String CAT_LIVING                        = "Living";
    public static final String CAT_SERVICES                      = "Services";
    public static final String CAT_TRAVELS                       = "Travels";

    public static final String STATUS_ACTIVE                     = "active";
    public static final String STATUS_INACTIVE                   = "inactive";

    public static final String FONT_CALIBRI                      = "fonts/Calibri.otf";
    public static final String FONT_CALIBRI_BOLD                 = "fonts/Calibri Bold.ttf";

    public static final String EDA_BYPASS                        = "dev-zafyraminx";

    //Utilities Module
    public static final String EPI_API_USERNAME                  = "testEdmark";
    public static final String EPI_API_SECRETKEY                 = "aBcAGFcfFTLhvD87";
    public static final String EPI_API_KEY                       = "expresspayapikey";

    //Family Tree Module
    public static final String TOTAL_DOWNLINE                    = "total_downline";
    public static final String CUSTSERIALNO                      = "custserialno";
    public static final String CUSTEDA                           = "custeda";
    public static final String CUSTNAME                          = "custname";
    public static final String CUSTTYPE                          = "custtype";
    public static final String CUSTJOINDATE                      = "custjoindate";

    public static final String DOWNLINE                          = "downline";
    public static final String DOWNLINE_TOTALDONWLINES           = "downline_totaldownlines";
    public static final String DOWNLINE_CUSTID                   = "downline_custid";
    public static final String DOWNLINE_CUSTSERIALNO             = "downline_custserialno";
    public static final String DOWNLINE_CUSTEDA                  = "downline_custeda";
    public static final String DOWNLINE_CUSTNAME                 = "downline_custname";
    public static final String DOWNLINE_CUSTTYPE                 = "downline_custtype";
    public static final String DOWNLINE_CUSTJOINDATE             = "downline_custjoindate";

    public static final String KEY_FAMILY_TREE_CUSTID            = "KEY_FAMILY_TREE_CUSTID";
    public static final String KEY_FAMILY_TREE_COUNT             = "KEY_FAMILY_TREE_COUNT";

    public static final String KEY_UTILITY_BILLERID              = "KEY_UTILITY_BILLERID";
    public static final String KEY_UTILITY_BILLERNAME            = "KEY_UTILITY_BILLERNAME";

    //EDCoins Module
    public static final String EDA_NUMBER                        = "eda_number";
    public static final String WALLET_BALANCE                    = "wallet_balance";
    public static final String WALLET_LOCK_BALANCE               = "wallet_lock_balance";
    public static final String WALLET_AVAILABLE_BALANCE          = "wallet_available_balance";
    public static final String TOTAL_CONVERTED_EDPOINTS          = "total_converted_edpoints";
    public static final String REFERRAL_BONUS                    = "referral_bonus";
    public static final String RATE                              = "rate";
    public static final String COIN_AMOUNT                       = "coin_amount";
    public static final String RECEIVED_AMOUNT                   = "received_amount";
    public static final String REFERENCE_NO                      = "reference_no";
    public static final String DATE_TIME                         = "date_time";
    public static final String EDC_AMOUNT                        = "edc_amount";
    public static final String EDP_AMOUNT                        = "edp_amount";
    public static final String DESCRIPTION                       = "description";
    public static final String RECORD                            = "record";
    public static final String TOTAL_POINT                       = "total_point";
    public static final String EDCOIN_AVAILABLE_BALANCE          = "edcoin_available_balance";

    public static final String KEY_EDC_BALANCE                   = "KEY_EDC_BALANCE";
    public static final String KEY_COIN_AMOUNT                   = "KEY_COIN_AMOUNT";
    public static final String KEY_RECEIVED_AMOUNT               = "KEY_RECEIVED_AMOUNT";
    public static final String KEY_REFERENCE_NO                  = "KEY_REFERENCE_NO";
    public static final String KEY_DATE_TIME                     = "KEY_DATE_TIME";
    public static final String KEY_EDA_NO_VALUE                  = "KEY_EDA_NO_VALUE";
    public static final String KEY_BIND_KEY                      = "KEY_BIND_KEY";
    public static final String KEY_CUSTID                        = "KEY_CUSTID";
    public static final String KEY_RATE                          = "KEY_RATE";

    public static final String KEY_EDC_VALUE                     = "KEY_EDC_VALUE";
    public static final String KEY_EDP_VALUE                     = "KEY_EDP_VALUE";

    //Transfer Auto-Converted Point

    public static final String COUNTRY_CODE                      = "country_code";

    public static final String KEY_AUTO_CONVERTED_EP             = "KEY_AUTO_CONVERTED_EP";
    public static final String KEY_TRANSFER_APP                  = "KEY_TRANSFER_APP";
    public static final String KEY_ACCOUNT_NAME                  = "KEY_ACCOUNT_NAME";
    public static final String KEY_EDA_NUMBER                    = "KEY_EDA_NUMBER";
    public static final String KEY_EDP_AMOUNT                    = "KEY_EDP_AMOUNT";
    public static final String KEY_EDP_AMOUNT_FROM               = "KEY_EDP_AMOUNT_FROM";
    public static final String KEY_EDP_AMOUNT_TO                 = "KEY_EDP_AMOUNT_TO";
    public static final String KEY_CUST_ID                       = "KEY_CUST_ID";
    public static final String KEY_AVAILABLE_EDP                 = "KEY_AVAILABLE_EDP";


    //Transfer Logs - API
    public static final String TRANSFER_DATA                     = "transfer_data";
    public static final String ACCOUNT_ID                        = "account_id";
    public static final String TOPUP_ID                          = "topup_id";
    public static final String APP_TYPE                          = "app_type";
    public static final String PREVIOUS_BALANCE                  = "previous_balance";
    public static final String TRANSFERRED_AMOUNT                = "transferred_amount";
    public static final String CURRENT_BALANCE                   = "current_balance";
    public static final String PURCHASER_NAME                    = "purchaser_name";
    public static final String PURCHASER_COUNTRY                 = "purchaser_country";
    public static final String CONVERTED_AMOUNT                  = "converted_amount";



}
