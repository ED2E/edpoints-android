package com.edmark.loyaltyuser.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


/**
 * Created by DEV-Michael-ED2E on 05/23/2018.
 */
public final class PermissionUtils {

    public static final String PER_GET_ACCOUNTS = Manifest.permission.GET_ACCOUNTS;
    public static final String PER_WAKE_LOCK = Manifest.permission.WAKE_LOCK;
    public static final String PER_CAMERA = Manifest.permission.CAMERA;
    public static final String PER_WRITE_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final String PER_READ_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    public static final int FEEDBACK_PERMISSION_REQUEST_CODE = 1001;


    private PermissionUtils() {
    }

    public static boolean checkPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (!checkPermission(context, permission)) {
                return false;
            }
        }
        return true;
    }

    public static boolean AllPermissionsGranted(Context context) {
        return IsAccountsInfoGranted(context) && IsCameraGranted(context);
    }

    public static boolean checkPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    //define functions for each permission
    public static boolean IsWakeLockGranted(Context context) {
        return checkPermission(context, PER_WAKE_LOCK);
    }

    public static boolean IsAccountsInfoGranted(Context context) {
        return checkPermission(context, PER_GET_ACCOUNTS);
    }

    public static boolean IsCameraGranted(Context context) {
        return checkPermission(context, PER_CAMERA);
    }

    public static boolean PermissionsGranted(@NonNull int[] permissionResults) {
        if (permissionResults.length > 0) {
            for (int permission : permissionResults) {
                if (permission != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
            return true;
        } else return false;
    }

    public static void RequestPermissions(Activity activity, int permissionId, String... permissions) {
        ActivityCompat.requestPermissions(activity, permissions, permissionId);
    }
}