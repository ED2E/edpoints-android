package com.edmark.loyaltyuser.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/*
 * Created by DEV-Michael-ED2E on 04/05/2018.
 */

public class InternetUtils extends BroadcastReceiver
{
    public static ConnectivityReceiverListener connectivityReceiverListener;

    public InternetUtils() {
        super();
    }

    public final boolean isInternetOn(Context context)
    {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec != null) {
            if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                    connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                    connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                    connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) {

                // if connected with internet
                return true;

            } else if (
                    connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
                            connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) {

                return false;
            }
        }
        return false;
    }

    @Override
    public void onReceive(Context context, Intent arg1) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();

        if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }
    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }
}
