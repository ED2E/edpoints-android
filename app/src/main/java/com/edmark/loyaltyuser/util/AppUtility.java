package com.edmark.loyaltyuser.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.SslErrorHandler;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.model.Categories;
import com.edmark.loyaltyuser.model.Country;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;

import static com.edmark.loyaltyuser.constant.Common.AUTH_KEY;
import static com.edmark.loyaltyuser.constant.Common.AUTH_KEY_ED2E;
import static com.edmark.loyaltyuser.constant.Common.BRONZE;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_ENTERTAINMENT;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_FOOD_AND_BEVERAGES;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_HEALTH_CARE;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_LIFESTYLE;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_LIVING;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_SERVICES;
import static com.edmark.loyaltyuser.constant.Common.CATEGORY_TRAVELS;
import static com.edmark.loyaltyuser.constant.Common.DATE_FORMAT_SERVER;
import static com.edmark.loyaltyuser.constant.Common.DECIMAL_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.FONT_CALIBRI;
import static com.edmark.loyaltyuser.constant.Common.GOLD;
import static com.edmark.loyaltyuser.constant.Common.INTEGER_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.ORDINARY;
import static com.edmark.loyaltyuser.constant.Common.ORDINARYP;
import static com.edmark.loyaltyuser.constant.Common.SILVER;
import static com.edmark.loyaltyuser.constant.Common.USERNAME_STR;
import static java.lang.Math.toRadians;

//import com.bumptech.glide.load.resource.drawable.GlideDrawable;
//import com.bumptech.glide.request.RequestListener;
//import com.bumptech.glide.request.animation.GlideAnimation;
//import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

public class AppUtility {
    private static final String PHONE_LAYOUT = "androidphone";
    private static final String TABLET_LAYOUT = "androidtablet";
    public static double latitude = 0.0;
    public static double longitude = 0.0;
    public static Gson g = new Gson();
    public static String id;
    private static String merchantProfile;
    private static int position;
    public static String customerOrMerchant = "";
    public static int transferPermission = 1;        // default is send only
    public static String transId = "";
    public static String eda = "";
    private static MediaPlayer mediaPlayer;

    /**
     * @param context - context
     * @return true, if Internet available otherwise false
     */
    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] networkInfos = connectivity.getAllNetworkInfo();
            if (networkInfos != null)
                for (NetworkInfo info : networkInfos) {
                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
        }
        return false;
    }

    /**
     * Alert dialog to show information
     *
     * @param context - context
     * @param title   - dialog title
     * @param message - dialog message
     */
    public static void showAlertDialog(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create().show();
    }

    /**
     * @return true if email address is valid otherwise false
     */
    public static boolean isInvalidEmail(String email) {
        return TextUtils.isEmpty(email) || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * Hide keyboard method
     *
     * @param activity - activity class
     */
    public static void hideKeyboard(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //now tell the IMM to hide the keyboard FROM whatever has focus in the activity
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), 0);
        }
    }

    private static final AppUtility ourInstance = new AppUtility();

    public static AppUtility getInstance() {
        return ourInstance;
    }

    /**
     * set Position of Drawer Item
     */
    public void setPosition(int pos) {
        position = pos;
    }

    /**
     * Get Position of Drawer Item
     */
    public static int getPosition() {
        return position;
    }

    /**
     * set Merchant Profile Image
     */
    public static void setMerchantProfileImage(String pic) {
        merchantProfile = pic;
    }

    /**
     * Get IMEI of device
     *
     * @param activity - class activity
     * @return phone IMEI
     */
//    public static String getIMEI(Activity activity) {
//        String imeiStr = "";
//        try {
//            TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
//            imeiStr = telephonyManager.getDeviceId();
//            if (imeiStr == null) {
//                imeiStr = "";
//            }
//        } catch (Exception ex) {
//        }
//        return imeiStr;
//    }

    /**
     * set user user name
     */
    public static void setUserName(Context context, TextView userName) {
        userName.setText(AppPreference.getInstance(context).getString(USERNAME_STR, ""));
    }

    /**
     * Text watcher to set remove error text
     *
     * @param editText - text to be edited
     */

    public static void textWatcherEditText(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    editText.setError(null);
                }
            }
        });
    }

    /**
     * Getting title from Url
     *
     * @param url - full url
     */
    public static String getUrlTitle(String url) {
        if (url.contains("title=")) {
            String[] couple = url.split("title=");
            String itemValue = "";
            for (String aCouple : couple) {
                itemValue = couple[1];
            }
            return itemValue.replace("%20", " ");
        } else {
            return "";
        }
    }

    /**
     * Getting url to email
     *
     * @param url - email url
     * @return email
     */
    /*public static String getUrlEmail(String url) {
        if (url.contains("&emailNeeded")) {
            String[] couple = url.split("&emailNeeded");
            String itemValue = "";
            for (String aCouple : couple) {
                itemValue = couple[0];
            }
            return itemValue.replace("%20", " ");
        } else {
            return "";
        }
    }*/

    /*public static String getLatitude(String url) {
        if (url.contains("&mapsNeeded")) {
            String[] couple = url.split("mapsNeeded&lat=");
            String itemValue = "";
            for (String aCouple : couple) {
                itemValue = couple[1];
                String[] coupleStr = itemValue.split("&lng=");
                itemValue = "";
                for (String aCoupleStr : coupleStr) {
                    itemValue = coupleStr[0];
                }
            }
            return itemValue;
        } else {
            return "";
        }
    }*/

    /*public static String getLongitude(String url) {
        if (url.contains("&mapsNeeded")) {
            String[] couple = url.split("mapsNeeded&lat=");
            String itemValue = "";
            for (String aCouple : couple) {
                itemValue = couple[1];
                String[] coupleStr = itemValue.split("&lng=");
                itemValue = "";
                for (String aCoupleStr : coupleStr) {
                    itemValue = coupleStr[1];
                }
            }
            return itemValue;
        } else {
            return "";
        }
    }*/

    /**
     * Getting image name from the returned full name with path and image name
     *
     * @param name - full name
     * @return icon name
     */
    public static String getIconName(String name) {
        try {
            if (name.contains("/")) {
                String[] couple = name.split("/");
                String itemValue = "";
                for (String aCouple : couple) {
                    itemValue = couple[2];
                }

                String[] coupleStr = itemValue.split(".png");
                itemValue = "";
                for (String aCoupleStr : coupleStr) {
                    itemValue = coupleStr[0];
                }

                return itemValue;
            } else {
                return "";
            }
        } catch (Exception e) {

        }
        return "";
    }

    /**
     * Getting unix time stamp
     *
     * @return unix time stamp
     */
    public static long unixTimeStamp() {
        return System.currentTimeMillis() / 1000L;
    }

    /**
     * For API authentication we need to send to server
     *
     * @return VC key
     */
    public static String getVCKey() {
        String decryptKey = AUTH_KEY + unixTimeStamp();
        Log.v("decrypt key ", decryptKey);
        Log.v("decrypt key ", getHash(decryptKey));
        return getHash(decryptKey);
    }

    public static String getVCKeyED2E() {
        String decryptKey = AUTH_KEY_ED2E + unixTimeStamp();
        Log.v("decrypt key ", decryptKey);
        Log.v("decrypt key ", getHash(decryptKey));
        return getHash(decryptKey);
    }

    /**
     * Gettting md5 hash key
     *
     * @param str - String to be hashed
     * @return md5 hash key
     */
    public static String getHash(String str) {
        MessageDigest digest = null;
        byte[] input = null;

        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            input = digest.digest(str.getBytes("UTF-8"));

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return convertToHex(input);
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Check password length and alphanumberic string
     *
     * @param password - password
     * @return checked password
     */
    public static boolean checklength(String password) {
        //    String pattern= "^(?:[0-9]+[A-Za-z]|[A-Za-z]+[0-9])[A-Za-z0-9A-Z]*$";

        return password.trim().length() < 6;
    }

    public static boolean checkpinlength(String password) {
        //String pattern= "[0-9]";

        return password.trim().length() < 4;
    }

    /**
     * Checking mobile number length
     *
     * @param mobile - phone number
     * @return checked phone number
     */
    public static boolean checkMobilelength(String mobile) {
        return (mobile.trim().length() < 8 || mobile.trim().length() > 12);
    }

    /**
     * Check pin length
     *
     * @param pin - pin number
     * @return checked pin number
     */
    public static boolean checklengthPin(String pin) {
        return pin.trim().length() < 4;
    }

    /**
     * Loading image with centre crop
     *
     * @param context   - context
     * @param glideUrl  - image url
     * @param imageView - image view
     */
//    public static void loadImageUsingGlide(final Context context, String glideUrl, final ImageView imageView, final ProgressBar progressBar) {
//
//        Glide.with(context).load(glideUrl).crossFade().diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        progressBar.setVisibility(View.GONE);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        progressBar.setVisibility(View.GONE);
//                        return false;
//                    }
//                })
//                .into(new GlideDrawableImageViewTarget(imageView) {
//                    @Override
//                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
//                        super.onResourceReady(drawable, anim);
//                        //progressBar.setVisibility(View.GONE);
//                    }
//                });
//    }

    /*public static String getFormattedDate(String text, Context context) {
        String datePattern = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(datePattern);

        Date dateStr = null;
        String formattedDate = text;
        try {
            dateStr = sdf.parse(text);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

            //    DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(context);
            formattedDate = dateFormat.format(dateStr);
        } catch (Exception e) {
        }
        return formattedDate;
    }*/

    /**
     * Date format for dashboard
     *
     * @param text    - date string
     * @param context - context
     * @return
     */
    /*public static String getFormattedDateDashboard(String text, Context context) {
        String datePattern = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(datePattern);

        Date dateStr = null;
        String formattedDate = text;
        try {
            dateStr = sdf.parse(text);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

            //    DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(context);
            formattedDate = dateFormat.format(dateStr);
        } catch (Exception e) {
        }
        return formattedDate;
    }*/

    /**
     * Returning month as "Jan"
     *
     * @param text    - date string
     * @param context - context
     * @return
     */
    /*public static String getFormattedDateMonth(String text, Context context) {
        String datePattern = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(datePattern);

        Date dateStr = null;
        String formattedDate = text;
        try {
            dateStr = sdf.parse(text);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

            //    DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(context);
            formattedDate = dateFormat.format(dateStr);
        } catch (Exception e) {
        }
        return formattedDate;
    }*/

    /**
     * getBitmap from url
     *
     * @param src - image source
     * @return Bitmap image
     */
    /*public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }*/

    /**
     * Unique id of Android phone or tablet
     *
     * @param context - context
     * @return android ID
     */
    public static String androidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * Detecting whether its phone or tablet. If 0 its phone else tablet
     *
     * @param context - context
     * @return device type
     */
    public static String getDeviceLayoutType(Context context) {
        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        if (dpWidth >= 600) {
            return TABLET_LAYOUT;
        } else {
            return PHONE_LAYOUT;
        }
    }

    /**
     * Getting OS version of the device
     *
     * @param context - context
     * @return Android OS version
     */
    public static String getOsVersion(Context context) {
//        String appversion = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
//            appversion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("NameNotFound", e.toString());
        }

        String osVersion = Build.VERSION.RELEASE;
//        String manufacturer = Build.MANUFACTURER;
//        String model = Build.MODEL;

        return osVersion;
    }

    /**
     * Getting version of the device
     *
     * @param context - context
     * @return device version
     */
    public static String getVersion(Context context) {
        String appVersion = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }

        return "Version : " + appVersion;
    }

    /**
     * Getting device name having manufacture and model
     *
     * @param context - context
     * @return device name
     */
    public static String getDeviceName(Context context) {
        return Build.MANUFACTURER + " " + Build.MODEL;
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {

        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static String bitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static Bitmap stringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static Bitmap fastblur(Bitmap sentBitmap) {
        int radius = 50;
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    /**
     * Opening Map and passing address
     *
     * @param context - contex
     * @param address - address to be searched
     */
    public static void openMapDialogWithAddress(Context context, String address) {
        Log.v("Address", address);
        String geoUri = "http://maps.google.com/maps?q=" + address;
        Uri geoDat = Uri.parse(geoUri);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoDat);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    /**
     * Haversine formula to calculate distance between 2 locations
     *
     * @param sourceLat      - source latitude
     * @param sourceLon      - source longitude
     * @param destinationLat - destination latitude
     * @param destinationLon - destinateion longitude
     * @return
     */
    public static double calculateDistance(double sourceLat, double sourceLon, double destinationLat, double destinationLon) {
        if (sourceLat == 0.0) {
            sourceLat = 3.13;
            sourceLon = 101.68;
        }
        double latDiff = toRadians(destinationLat - sourceLat);
        double longDiff = toRadians(destinationLon - sourceLon);
        double earthRadius = 6371; //In Km if you want the distance in km

        double distance = 2 * earthRadius * Math.asin(Math.sqrt(Math.pow(Math.sin(latDiff / 2.0), 2) + Math.cos(sourceLat) * Math.cos(destinationLat) * Math.pow(Math.sin(longDiff / 2), 2)));

        DecimalFormat df = new DecimalFormat("0.00");
        String formate = df.format(distance);
        Log.v("Finder ", formate);
        if (formate != null && formate.trim().length() > 0) {
            try {
                distance = (double) df.parse(formate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return (distance);
    }

    /**
     * Alert dialog to show SSL handling in webview
     *
     * @param handler - ssl error handler
     * @param context - context
     */
    public static void sslMessage(final SslErrorHandler handler, Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.webview_ssl);
        builder.setPositiveButton(R.string.webview_proceed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.proceed();
            }
        });
        builder.setNegativeButton(R.string.webview_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.cancel();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();  //⁠ Ignore SSL certificate errors
    }

    public static Bitmap encodeAsBitmap(String str) throws WriterException {
        Bitmap result = null;

        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(str, BarcodeFormat.QR_CODE, 300, 300);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            result = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    result.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }

        } catch (WriterException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void playQr(Context context, int volume_level) {
        try {
            if (volume_level != 0) {
                mediaPlayer = MediaPlayer.create(context, R.raw.qrcode);
                mediaPlayer.start();
            }
        } catch (Exception e) {
            Log.e("SongsError", e.toString());
        }
    }

    public static void playTrans(Context context, int volume_level) {
        try {
            if (volume_level != 0) {
                mediaPlayer = MediaPlayer.create(context, R.raw.transaction);
                mediaPlayer.start();
            }
        } catch (Exception e) {
            Log.e("SongsError", e.toString());
        }
    }

    /**
     * return formatted string of selected date
     *
     * @param date    selected date
     * @param pattern pattern that you want
     * @return formatted date string
     */
    public static String getFormattedDate(Date date, String pattern) {
        String dateString;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        dateString = simpleDateFormat.format(date);
        return dateString;
    }

    /**
     * return formatted string of selected date
     *
     * @param dateString selected date
     * @param pattern    pattern that you want
     * @return formatted date string
     */
    public static Date getFormattedDate(String dateString, String pattern) throws ParseException {
        Date date;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        date = simpleDateFormat.parse(dateString);
        return date;
    }

    public static String getTimeinMilli(String dateTime, String pattern) throws ParseException {
        Date date = new SimpleDateFormat(pattern, Locale.getDefault()).parse(dateTime);
        long milliseconds = date.getTime() / 1000L;
        return String.valueOf(milliseconds);
    }

    public static String getDate(String dateString, String pattern) throws ParseException {
        Date date;
        String dateStringOutput;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_SERVER);
        date = simpleDateFormat.parse(dateString);
        SimpleDateFormat outputformat = new SimpleDateFormat(pattern);
        dateStringOutput = outputformat.format(date);
        return dateStringOutput;
    }

    public static String getTime(String dateString, String pattern) throws ParseException {
        Date date;
        String dateStringOutput;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_SERVER);
        date = simpleDateFormat.parse(dateString);
        SimpleDateFormat outputformat = new SimpleDateFormat(pattern);
        dateStringOutput = outputformat.format(date);
        return dateStringOutput;
    }

    public static String decimalOutputFormat(String value) {
        if (value != null) {
            double amount = Double.parseDouble(value.replaceAll(" ", ""));
            //checker if decimal has value or none
            if (amount % 1 == 0) {
                DecimalFormat formatter = new DecimalFormat(INTEGER_FORMAT);
                return formatter.format(amount).replace(" ", "");
            } else {
                DecimalFormat formatter = new DecimalFormat(DECIMAL_FORMAT);
                return formatter.format(amount).replace(" ", "");
            }
        } else {
            return value;
        }
    }

    public static ArrayList<String> getDateSection(ArrayList<String> dates) {
        HashSet<String> hashSet = new HashSet<>(dates);
        dates.clear();
        dates.addAll(hashSet);
        return dates;
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public static Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static class XAxisValueFormatter implements IAxisValueFormatter {

        private String[] mValues;
        private DecimalFormat mFormat;

        public XAxisValueFormatter(String[] values) {
            this.mValues = values;
        }

        public XAxisValueFormatter() {
            mFormat = new DecimalFormat(INTEGER_FORMAT); // use one decimal
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            if (mValues != null) {
                return mValues[(int) value];
            } else {
                return mFormat.format(value);
            }
        }
    }

    public static String formatDate_yyyyMMdd(String stringOfDate){
        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("MM-dd-yyyy");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
            // format the date into another format
            dateStr = destDf.format(date);

            return dateStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static class ValueFormatter implements IValueFormatter {

        private DecimalFormat mFormat;

        public ValueFormatter() {
            mFormat = new DecimalFormat(INTEGER_FORMAT); // use one decimal
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mFormat.format(value);
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

//    public static Bitmap encodeAsBitmap(String str, Bitmap icon) throws WriterException {
//        Bitmap result = null;
//
//        QRCodeWriter writer = new QRCodeWriter();
//        try {
//            BitMatrix bitMatrix = writer.encode(str, BarcodeFormat.QR_CODE, 300, 300);
//            int width = bitMatrix.getWidth();
//            int height = bitMatrix.getHeight();
//            result = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
//            for (int x = 0; x < width; x++) {
//                for (int y = 0; y < height; y++) {
//                    result.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
//                }
//            }
//
////            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_resource);
//            result = mergeBitmaps(icon, result);
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }

    public static Bitmap encodeAsBitmap(String qrCodeData, Bitmap overlay) throws WriterException {
        Bitmap result = null;

        Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<>();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        try {
            //generating qr code.
            BitMatrix matrix = new MultiFormatWriter().encode(new String(qrCodeData.getBytes("UTF-8"), "UTF-8"),
                    BarcodeFormat.QR_CODE, 300, 300, hintMap);
            //converting bitmatrix to bitmap

            int width = matrix.getWidth();
            int height = matrix.getHeight();
            int[] pixels = new int[width * height];
            // All are 0, or black, by default
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    //for black and white
                    //pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
                    //for custom color
                    pixels[offset + x] = matrix.get(x, y) ? Color.BLACK : Color.WHITE;
                }
            }
            //creating bitmap
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

            //getting the logo
//            Bitmap overlay = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
//            //setting bitmap to image view
//            imageViewBitmap.setImageBitmap(mergeBitmaps(overlay,bitmap));
            result = mergeBitmaps(overlay, bitmap);
        } catch (Exception er) {
            Log.e("QrGenerate", er.getMessage());
        }

        return result;
    }

    private static Bitmap mergeBitmaps(Bitmap overlay, Bitmap bitmap) {

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        overlay = Bitmap.createScaledBitmap(overlay, 72, 72, true);
//        overlay = getScaledDownBitmap(overlay,72,true);
//        overlay = getResizedBitmap(overlay, 72, 72);
//        overlay = scaleBitmap(overlay, 58, 58);
        Bitmap combined = Bitmap.createBitmap(width, height, bitmap.getConfig());
        Canvas canvas = new Canvas(combined);
        int canvasWidth = canvas.getWidth();
        int canvasHeight = canvas.getHeight();

        canvas.drawBitmap(bitmap, new Matrix(), null);

        int centreX = (canvasWidth - overlay.getWidth()) / 2;
        int centreY = (canvasHeight - overlay.getHeight()) / 2;
        canvas.drawBitmap(overlay, centreX, centreY, null);

        return combined;
    }

    public static Bitmap getScaledDownBitmap(Bitmap bitmap, int threshold, boolean isNecessaryToKeepOrig) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = width;
        int newHeight = height;

        if (width > height && width > threshold) {
            newWidth = threshold;
            newHeight = (int) (height * (float) newWidth / width);
        }

        if (width > height && width <= threshold) {
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        if (width < height && height > threshold) {
            newHeight = threshold;
            newWidth = (int) (width * (float) newHeight / height);
        }

        if (width < height && height <= threshold) {
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        if (width == height && width > threshold) {
            newWidth = threshold;
            newHeight = newWidth;
        }

        if (width == height && width <= threshold) {
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        return getResizedBitmap(bitmap, newWidth, newHeight, isNecessaryToKeepOrig);
    }

    private static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight, boolean isNecessaryToKeepOrig) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        if (!isNecessaryToKeepOrig) {
            bm.recycle();
        }
        return resizedBitmap;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public static Bitmap getBase64Bitmap(String base64String) {
        byte[] decodedString;
        if (base64String.contains("data:image/")) {
            String base64Image = base64String.split(",")[1];//to remove data:image/png;base64
            decodedString = Base64.decode(base64Image, Base64.DEFAULT);
        } else {
            decodedString = Base64.decode(base64String, Base64.DEFAULT);
        }
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static String getBase64String(ImageView imageView) {
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, Base64.NO_WRAP);
    }


    public static String getRealPathFromUri(Context context, final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    public static ArrayList<Country> getCountry(Context context) {
        ArrayList<Country> countries = new ArrayList<>();
        List<String> nationality = Arrays.asList(context.getResources().getStringArray(R.array.nationality_array));
        List<String> nationality_code = Arrays.asList(context.getResources().getStringArray(R.array.nationality_code_array));
        for (int i = 0; i < nationality.size(); i++) {
            Country country = new Country(nationality.get(i), nationality_code.get(i));
            countries.add(country);
        }
        return countries;
    }

    public static void playBeep(Context context, String beepSoundFile) {
        MediaPlayer m = new MediaPlayer();
        try {
            if (m.isPlaying()) {
                m.stop();
                m.release();
                m = new MediaPlayer();
            }
            AssetFileDescriptor descriptor = context.getAssets().openFd(beepSoundFile != null ? beepSoundFile : "beep.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            m.setAudioStreamType(AudioManager.STREAM_ALARM);
            m.prepare();
            m.setVolume(1f, 1f);
            m.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static LatLng getLatLongFromGivenAddress(Context context, String address) {
        AtomicReference<LatLng> latLng = new AtomicReference<>(new LatLng(0.0, 0.0));
        new Thread(() -> {
            // do something here
            Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addresses = geoCoder.getFromLocationName(address, 1);
                if (addresses.size() > 0) {
//                GeoPoint p = new GeoPoint(
//                        (int) (addresses.get(0).getLatitude() * 1E6),
//                        (int) (addresses.get(0).getLongitude() * 1E6));
//
//                lat=p.getLatitudeE6()/1E6;
//                lng=p.getLongitudeE6()/1E6;

                    double lat = 0.0;
                    double lng = 0.0;
                    lat = (addresses.get(0).getLatitude() * 1E6);
                    lng = (addresses.get(0).getLongitude() * 1E6);
//                Log.d("Latitude", ""+lat);
//                Log.d("Longitude", ""+lng);
                    Log.d("Latitude2", "" + lat / 1E6);
                    Log.d("Longitude2", "" + lng / 1E6);
                    latLng.set(new LatLng(lat, lng));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        return latLng.get();
    }

    public static int getCardDrawable(String card) {
        int drawable = 0;
        switch (card) {
            case ORDINARY:
                drawable = R.drawable.ic_card_ordinary;
                break;
            case ORDINARYP:
                drawable = R.drawable.ic_card_ordinary;
                break;
            case GOLD:
                drawable = R.drawable.ic_card_gold;
                break;
            case SILVER:
                drawable = R.drawable.ic_card_silver;
                break;
            case BRONZE:
                drawable = R.drawable.ic_card_bronze;
                break;
        }
        return drawable;
    }

    public static int getCompanyFlagDrawable(Context context, String company_code) {
        int resID;
        //Saudi (ME) and Saudi has the same flag
        if (company_code.equals("ME")) {
            resID = context.getResources().getIdentifier("sa", "drawable", Objects.requireNonNull(context).getPackageName());
        } else {
            resID = context.getResources().getIdentifier(company_code.toLowerCase(), "drawable", Objects.requireNonNull(context).getPackageName());
        }

        return resID;
    }

    public static ArrayList<Country> getCompanyCountry(Context context) {
        ArrayList<Country> countries = new ArrayList<>();
        List<String> companycode = Arrays.asList(context.getResources().getStringArray(R.array.company_code_array));
        for (int i = 0; i < companycode.size(); i++) {
            String[] separated = companycode.get(i).split("-");
            Country country = new Country(separated[1], separated[0]);
            countries.add(country);
        }
        return countries;
    }

    public static ArrayList<Country> getMerchantCountry(Context context) {
        ArrayList<Country> countries = new ArrayList<>();
        List<String> companycode = Arrays.asList(context.getResources().getStringArray(R.array.merchant_country_array));
        for (int i = 0; i < companycode.size(); i++) {
            String[] separated = companycode.get(i).split("-");
            Country country = new Country(separated[1], separated[0]);
            countries.add(country);
        }
        return countries;
    }

    public static File saveBitmapToFile(File file) {
        try {
            int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
            Log.d("saveBitmapToFile", "file name:" + file.getName() + " file_size:" + file_size);

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 50;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public static ArrayList<Categories> getCategories(Context context) {
        ArrayList<Categories> categories = new ArrayList<>();
        categories.add(new Categories(CATEGORY_FOOD_AND_BEVERAGES, context.getResources().getString(R.string.label_merchant_category_food)));
        categories.add(new Categories(CATEGORY_TRAVELS, context.getResources().getString(R.string.label_merchant_category_travels)));
        categories.add(new Categories(CATEGORY_LIFESTYLE, context.getResources().getString(R.string.label_merchant_category_life_style)));
        categories.add(new Categories(CATEGORY_ENTERTAINMENT, context.getResources().getString(R.string.label_merchant_category_entertainment)));
//        categories.add(new Categories(CATEGORY_3C_PRODUCTS,context.getResources().getString(R.string.label_merchant_category_3c_products)));
        categories.add(new Categories(CATEGORY_HEALTH_CARE, context.getResources().getString(R.string.label_merchant_category_health_care)));
        categories.add(new Categories(CATEGORY_SERVICES, context.getResources().getString(R.string.label_merchant_category_services)));
        categories.add(new Categories(CATEGORY_LIVING, context.getResources().getString(R.string.label_merchant_category_living)));
        return categories;
    }

    public static String formatDate_MMMMDDYYYY(String stringOfDate){
        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("MMMM dd, yyyy");
            // format the date into another format
            dateStr = destDf.format(date);

            return dateStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatTime_HHMMAAA(String stringOfDate){
        String timeStr = "";

        try {
            timeStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(timeStr);

            DateFormat destDf = new SimpleDateFormat("hh:mm aaa");
            // format the date into another format
            timeStr = destDf.format(date);

            return timeStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatDate_yyyy(String stringOfDate){
        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("MM-dd-yyyy");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("yyyy");
            // format the date into another format
            dateStr = destDf.format(date);

            return dateStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatDate_MM(String stringOfDate){
        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("MM-dd-yyyy");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("MM");
            // format the date into another format
            dateStr = destDf.format(date);

            return dateStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatDate_dd(String stringOfDate){
        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("MM-dd-yyyy");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("dd");
            // format the date into another format
            dateStr = destDf.format(date);

            return dateStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatDate_MMDDYYYY(String stringOfDate){
        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("MM-dd-yyyy");
            // format the date into another format
            dateStr = destDf.format(date);

            return dateStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String get30DaysBeforeCurrentDate_YYYYMMDD(){
        String dateStr = "";

        // get current date
        Date date = Calendar.getInstance().getTime();
        Date dateLess30 = new Date(date.getTime() - 30 * 24 * 3600 * 1000l);

        SimpleDateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
        // format the date into another format
        dateStr = destDf.format(dateLess30);

        return dateStr;
    }

    public static String getCurrentDate_YYYYMMDD(){
        String dateStr = "";

        // get current date
        Date date = Calendar.getInstance().getTime();

        SimpleDateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
        // format the date into another format
        dateStr = destDf.format(date);

        return dateStr;
    }

    public static String formatDate_YYYYMMDD(String stringOfDate){
        String dateStr = "";
        String timeStr = "";

        try {
            dateStr = stringOfDate.replace("/", "-");

            TimeZone tz = TimeZone.getDefault();
            DateFormat srcDf = new SimpleDateFormat("MM-dd-yyyy");
            Log.d("timezone value","TimeZone: "+ tz.getDisplayName(false, TimeZone.SHORT) + " | TimeZone ID: " + tz.getID());
            srcDf.setTimeZone(TimeZone.getTimeZone(tz.getDisplayName()));

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
            // format the date into another format
            dateStr = destDf.format(date);

            return dateStr;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int compareVersionNames(String oldVersionName, String newVersionName) {
        int res = 0;

        String[] oldNumbers = oldVersionName.split("\\.");
        String[] newNumbers = newVersionName.split("\\.");

        // To avoid IndexOutOfBounds
        int maxIndex = Math.min(oldNumbers.length, newNumbers.length);

        for (int i = 0; i < maxIndex; i++) {
            int oldVersionPart = Integer.valueOf(oldNumbers[i]);
            int newVersionPart = Integer.valueOf(newNumbers[i]);

            if (oldVersionPart < newVersionPart) {
                res = -1;
                break;
            } else if (oldVersionPart > newVersionPart) {
                res = 1;
                break;
            }
        }

        // If versions are the same so far, but they have different length...
        if (res == 0 && oldNumbers.length != newNumbers.length) {
            res = (oldNumbers.length > newNumbers.length) ? 1 : -1;
        }

        return res;
    }

    public static SpannableStringBuilder setHintFont(Context context, String msg, String fontPath) {
        SpannableStringBuilder sBuilder = new SpannableStringBuilder();
        sBuilder.append(msg); // Default TextView font.
        // Create the Typeface you want to apply to certain text
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), fontPath));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sBuilder.setSpan(typefaceSpan, 0, msg.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sBuilder;
    }
}
