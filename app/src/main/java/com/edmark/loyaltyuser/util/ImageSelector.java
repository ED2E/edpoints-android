package com.edmark.loyaltyuser.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;

import androidx.core.content.FileProvider;

import com.edmark.loyaltyuser.BuildConfig;
import com.edmark.loyaltyuser.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by AkkiSingh on 06/12/16.
 * Modifies by DEV-Michael-ED2E on 05/23/2018.
 */
public class ImageSelector {

    public static final int CHOICE_AVATAR_FROM_CAMERA = 1;
    public static final int CHOICE_AVATAR_FROM_GALLERY = 2;
    public static final int CHOICE_AVATAR_REMOVE_PHOTO = 6;

    private Context mContext;
    private String mCurrentPhotoPath;
    private AppPreference mPreferences;

    public ImageSelector(Context mContext) {
        this.mContext = mContext;
        mPreferences = AppPreference.getInstance(mContext);
    }

    //Allow user to choose whether to use camera or select picture from gallery
    public void selectImageWithRemove(final ImageView imageView) {
        final CharSequence[] items = {mContext.getString(R.string.take_photo), getString(R.string.choose_from_gallery),
                getString(R.string.remove_photo), getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    //Take picture using device camera
                    takePicture();
                } else if (items[item].equals(getString(R.string.choose_from_gallery))) {
                    //Choose picture from Gallery
                    chooseFromGallery();
                } else if (items[item].equals(getString(R.string.remove_photo))) {
                    // remove selected photo
                    imageView.setImageResource(R.drawable.ic_default_user);
//                    mPreferences.putString(PrefConstant.USER_IMAGE, "");
//                    mPreferences.putString(PrefConstant.BLUR_BACKGROUND, "");
                    dialog.dismiss();

                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void selectImage(final ImageView imageView) {
        final CharSequence[] items = {mContext.getString(R.string.take_photo), getString(R.string.choose_from_gallery), getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals(getString(R.string.take_photo))) {
                //Take picture using device camera
                takePicture();
            } else if (items[item].equals(getString(R.string.choose_from_gallery))) {
                //Choose picture from Gallery
                chooseFromGallery();
            } else if (items[item].equals(getString(R.string.cancel))) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private String getString(int ID) {
        return mContext.getString(ID);
    }

    public Bitmap rotateBitmapOrientation(Uri uri) {
        String photoFilePath = AppUtility.getRealPathFromURI(uri, mContext);

        int rotate = 0;
        ExifInterface exifInterf;
        int exifOrientation = 0;
        try {
            exifInterf = new ExifInterface(photoFilePath);
            exifOrientation = exifInterf.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        File image = new File(photoFilePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, (bitmap.getWidth()), (bitmap.getHeight()), matrix, true);
        return bitmap;
    }

    private void takePicture() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(mContext,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                ((Activity) mContext).startActivityForResult(takePictureIntent, CHOICE_AVATAR_FROM_CAMERA);
            }
        }
    }

    private void chooseFromGallery() {
        Intent intent;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        } else {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        }
        intent.putExtra("return-data", true);
        intent.setType("image/*");
        ((Activity) mContext).startActivityForResult(intent, CHOICE_AVATAR_FROM_GALLERY);
    }

    public void doCropRectangle(Activity context, Uri picUri, Activity a, int x, int y) {
//        UCrop.of(picUri, picUri)
//                .withMaxResultSize(x, y)
//                .start(a);

        CropImage.activity(picUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(context);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents

        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        System.out.println("Path is  " + mCurrentPhotoPath);
        return image;
    }

    public String getmCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }
}

