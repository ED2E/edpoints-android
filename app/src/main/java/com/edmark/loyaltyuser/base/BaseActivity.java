package com.edmark.loyaltyuser.base;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edmark.loyaltyuser.R;
import com.edmark.loyaltyuser.util.dialog.DialogFactory;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

import static com.edmark.loyaltyuser.constant.Common.LOCATION_PERMISSION_REQUEST_CODE;

/*
 * Created by DEV-Michael-ED2E on 02/04/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected Dialog progressDialog;
    protected Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        runOnUiThread(() ->
        {
            // Initialize the progress dialog
            //Default progress
//            progressDialog = DialogFactory.createProgressDialog(this, getString(R.string.dialog_loading));
//            progressDialog.setCancelable(false);

            progressDialog = new Dialog(getContext(), android.R.style.Theme_Black_NoTitleBar);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (progressDialog.getWindow() != null)
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.dialog_progress_loading);
            ImageView imageView_progress = progressDialog.findViewById(R.id.dialog_progress_loading_imageview);

            Glide.with(getContext())
                    .load(R.drawable.ic_loading)
                    .into(imageView_progress);

            dialog = new Dialog(getContext(), R.style.DialogTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (dialog.getWindow() != null)
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_main);

        });
    }

    public Context getContext() {
        return this;
    }

    public Toolbar setupToolBar(int id) {
        Toolbar toolbar = findViewById(id);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
            toolbar.setNavigationOnClickListener(v -> onToolbarBackPress());
        }
        return toolbar;
    }

    public void showProgress() {
        if (getContext() != null) {
            if (progressDialog != null) {
                try {
                    runOnUiThread(() -> progressDialog.show());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void hideProgress() {
        if (getContext() != null) {
            if (progressDialog != null) {
                if (progressDialog.isShowing())
                    try {
                        progressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    public void showError(String message) {
        DialogFactory.createGenericErrorDialog(this, message).show();
    }

    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void showDialog(int requestCode, String message, boolean isSingleButton) {
        try {
            TextView textmsg = dialog.findViewById(R.id.dialog_main_textview_message);
            textmsg.setText(message);

            Button declineButton = dialog.findViewById(R.id.dialog_main_button_cancel);
            Button okeyButton = dialog.findViewById(R.id.dialog_main_button_ok);

            declineButton.setOnClickListener(v1 -> hideDialog());
            okeyButton.setVisibility(View.VISIBLE);

            if (isSingleButton) declineButton.setVisibility(View.GONE);
            else declineButton.setVisibility(View.VISIBLE);

            okeyButton.setOnClickListener(v1 ->
            {
                onDialogDismiss(requestCode);
                hideDialog();
            });
            declineButton.setOnClickListener(v ->
                    hideDialog());
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideDialog() {
        try {
            dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    protected void onDialogDismiss(int requestCode) {
        //superclass implemention does nothing
    }

    protected void onToolbarBackPress() {
        //superclass implemention does nothing
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    enableMyLocationIfPermitted();
                } else {
//                    showDefaultLocation();
                }
                return;

            }

        }
    }
}
