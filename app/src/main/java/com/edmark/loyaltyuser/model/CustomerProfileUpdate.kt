package com.edmark.loyaltyuser.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
 * Created by DEV-Michael-ED2E on 05/25/2018.
 */

class CustomerProfileUpdate(var status: String?, var message: String?, var custid: String?, var f_name: String?, var website_url: String?, var email: String?, var phoneno: String?, var DOB: String?)