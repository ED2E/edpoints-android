package com.edmark.loyaltyuser.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
 * Created by DEV-Michael-ED2E on 05/22/2018.
 */

class Country(var name: String, var code: String)