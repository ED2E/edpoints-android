package com.edmark.loyaltyuser.model

import android.util.Log
import com.edmark.loyaltyuser.constant.Common.DATE_FORMAT_REVIEW
import com.edmark.loyaltyuser.util.AppUtility.getDate

class Reviews(_date: String, var transaction_id: String, var review_star: Int, var remark: String, var dist_name: String, var merchant_name: String) {
    var date: String = _date
        get() = getDate(field, DATE_FORMAT_REVIEW)
}
