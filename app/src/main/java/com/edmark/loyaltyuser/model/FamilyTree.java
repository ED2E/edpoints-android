package com.edmark.loyaltyuser.model;

import java.util.ArrayList;
import java.util.List;

public class FamilyTree {

    private String downline_totaldownlines;
    private String downline_custid;
    private String downline_custserialno;
    private String downline_custeda;
    private String downline_custname;
    private String downline_custtype;
    private String downline_custjoindate;


    public FamilyTree(String downline_totaldownlines, String downline_custid, String downline_custserialno, String downline_custeda, String downline_custname, String downline_custtype, String downline_custjoindate) {
        this.downline_totaldownlines = downline_totaldownlines;
        this.downline_custid = downline_custid;
        this.downline_custserialno = downline_custserialno;
        this.downline_custeda = downline_custeda;
        this.downline_custname = downline_custname;
        this.downline_custtype = downline_custtype;
        this.downline_custjoindate = downline_custjoindate;
    }

    public String getDownline_totaldownlines() {
        return downline_totaldownlines;
    }

    public void setDownline_totaldownlines(String downline_totaldownlines) {
        this.downline_totaldownlines = downline_totaldownlines;
    }

    public String getDownline_custid() {
        return downline_custid;
    }

    public void setDownline_custid(String downline_custid) {
        this.downline_custid = downline_custid;
    }

    public String getDownline_custserialno() {
        return downline_custserialno;
    }

    public void setDownline_custserialno(String downline_custserialno) {
        this.downline_custserialno = downline_custserialno;
    }

    public String getDownline_custeda() {
        return downline_custeda;
    }

    public void setDownline_custeda(String downline_custeda) {
        this.downline_custeda = downline_custeda;
    }

    public String getDownline_custname() {
        return downline_custname;
    }

    public void setDownline_custname(String downline_custname) {
        this.downline_custname = downline_custname;
    }

    public String getDownline_custtype() {
        return downline_custtype;
    }

    public void setDownline_custtype(String downline_custtype) {
        this.downline_custtype = downline_custtype;
    }

    public String getDownline_custjoindate() {
        return downline_custjoindate;
    }

    public void setDownline_custjoindate(String downline_custjoindate) {
        this.downline_custjoindate = downline_custjoindate;
    }
}
