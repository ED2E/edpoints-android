package com.edmark.loyaltyuser.model;


import java.text.DecimalFormat;

import static com.edmark.loyaltyuser.constant.Common.DECIMAL_FORMAT;
import static com.edmark.loyaltyuser.constant.Common.INTEGER_FORMAT;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;

/*
 * Created by DEV-Michael-ED2E on 04/13/2018.
 */
public class TierSummary {

    private String level;
    private String spending_amount;
    private String percentage;
    private String bonus_amount;

    public TierSummary() {
    }

    public TierSummary(String level, String spending_amount, String percentage, String bonus_amount) {
        this.level = level;
        this.spending_amount = spending_amount;
        this.percentage = percentage;
        this.bonus_amount = bonus_amount;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSpending_amount() {
        return decimalOutputFormat(spending_amount);
    }

    public void setSpending_amount(String spending_amount) {
        this.spending_amount = spending_amount;
    }

    public String getPercentage() {
        return decimalOutputFormat(percentage);
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getBonus_amount() {
        return decimalOutputFormat(bonus_amount);
    }

    public void setBonus_amount(String bonus_amount) {
        this.bonus_amount = bonus_amount;
    }
}
