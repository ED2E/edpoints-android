package com.edmark.loyaltyuser.model


class Merchant(var company_code:String?, var merchant_code: String, var merchant_name: String, var contact_no: String, var email: String, var image_url:String,var markerImageStatus:Boolean, var latitude:String, var longitude: String, var address:String?, var contenturl:String, var description:String, var websiteurl:String, var merchant_category:String)