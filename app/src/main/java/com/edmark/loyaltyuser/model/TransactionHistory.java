package com.edmark.loyaltyuser.model;


import com.edmark.loyaltyuser.constant.Common;

import java.text.DecimalFormat;
import java.text.ParseException;

import static com.edmark.loyaltyuser.constant.Common.DECIMAL_FORMAT;
import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;
import static com.edmark.loyaltyuser.util.AppUtility.getTime;

/*
 * Created by DEV-Michael-ED2E on 04/19/2018.
 */
public class TransactionHistory {

    private String date;
    private String transaction_id;
    private String dist_name;
    private String merchant_name;
    private String description;
    private String total_spending_amount;
    private String loyalty_earn_point;
    private String type;

    public TransactionHistory() {
    }

    public TransactionHistory(String date, String transaction_id, String dist_name, String merchant_name, String description, String total_spending_amount, String loyalty_earn_point) {
        this.date = date;
        this.transaction_id = transaction_id;
        this.dist_name = dist_name;
        this.merchant_name = merchant_name;
        this.description = description;
        this.total_spending_amount = total_spending_amount;
        this.loyalty_earn_point = loyalty_earn_point;
    }

    public TransactionHistory(String date, String transaction_id, String dist_name, String merchant_name, String description, String total_spending_amount, String loyalty_earn_point, String type) {
        this.date = date;
        this.transaction_id = transaction_id;
        this.dist_name = dist_name;
        this.merchant_name = merchant_name;
        this.description = description;
        this.total_spending_amount = total_spending_amount;
        this.loyalty_earn_point = loyalty_earn_point;
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getDist_name() {
        return dist_name;
    }

    public void setDist_name(String dist_name) {
        this.dist_name = dist_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotal_spending_amount() {
        return decimalOutputFormat(total_spending_amount);
    }

    public String getTotal_spending_amount_2() {
        return total_spending_amount;
    }

    public void setTotal_spending_amount(String total_spending_amount) {
        this.total_spending_amount = total_spending_amount;
    }

    public String getLoyalty_earn_point() {
        return decimalOutputFormat(loyalty_earn_point);
    }

    public String getLoyalty_earn_point_2() {
        return loyalty_earn_point;
    }

    public void setLoyalty_earn_point(String loyalty_earn_point) {
        this.loyalty_earn_point = loyalty_earn_point;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimeFormatted() {
        try {
            return getTime(date, Common.DATE_FORMAT_TIME);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }
}
