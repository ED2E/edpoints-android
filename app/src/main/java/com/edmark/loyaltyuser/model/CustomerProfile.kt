package com.edmark.loyaltyuser.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
 * Created by DEV-Michael-ED2E on 05/25/2018.
 */

class CustomerProfile(var status: String?, var eda: String?, var name: String?, var email: String?, var telno: String?, var websiteurl: String?, var image_url: String?, var DOB: String?)