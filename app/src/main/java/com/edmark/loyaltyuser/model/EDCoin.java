package com.edmark.loyaltyuser.model;

public class EDCoin {

    private String reference_no;
    private String edc_amount;
    private String edp_amount;
    private String description;
    private String created_date;

    public EDCoin(String reference_no, String edc_amount, String edp_amount, String description, String created_date) {
        this.reference_no = reference_no;
        this.edc_amount = edc_amount;
        this.edp_amount = edp_amount;
        this.description = description;
        this.created_date = created_date;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    public String getEdc_amount() {
        return edc_amount;
    }

    public void setEdc_amount(String edc_amount) {
        this.edc_amount = edc_amount;
    }

    public String getEdp_amount() {
        return edp_amount;
    }

    public void setEdp_amount(String edp_amount) {
        this.edp_amount = edp_amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }
}
