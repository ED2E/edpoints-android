package com.edmark.loyaltyuser.model;

import android.widget.ImageView;

import com.bumptech.glide.Glide;



/*
 * Created by DEV-Michael-ED2E on 04/12/2018.
 */
public class SupportItem
{
    private String display_msg;
    private int res;

    public SupportItem() {
    }

    public SupportItem(String display_msg, int res) {
        this.display_msg = display_msg;
        this.res = res;
    }

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public String getDisplay_msg() {
        return display_msg;
    }

    public void setDisplay_msg(String display_msg) {
        this.display_msg = display_msg;
    }
}
