package com.edmark.loyaltyuser.model;


/*
 * Created by DEV-Michael-ED2E on 04/18/2018.
 */
public class FirebaseToken {

    private String token;
    private String custid;
    private String eda;
    private String email;
    private String deviceid;
    private String deviceOSver;
    private String date;
    private String deviceName;
    private String type;
    private String company_code;

    public FirebaseToken() {
    }

    public FirebaseToken(String token, String custid, String eda, String email, String deviceid, String deviceOSver, String date, String deviceName, String type, String company_code) {

        this.token = token;
        this.custid = custid;
        this.eda = eda;
        this.email = email;
        this.deviceid = deviceid;
        this.deviceOSver = deviceOSver;
        this.date = date;
        this.deviceName = deviceName;
        this.type = type;
        this.company_code = company_code;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getEda() {
        return eda;
    }

    public void setEda(String eda) {
        this.eda = eda;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getDeviceOSver() {
        return deviceOSver;
    }

    public void setDeviceOSver(String deviceOSver) {
        this.deviceOSver = deviceOSver;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }
}
