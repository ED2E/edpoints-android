package com.edmark.loyaltyuser.model.sugardb;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name="firebase_notif")
public class FirebasePendingNotif extends SugarRecord {

    private String custId;
    private String data;
    private String time;

    public FirebasePendingNotif() {
    }

    public FirebasePendingNotif(String custId, String data, String time) {
        this.custId = custId;
        this.data = data;
        this.time = time;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
