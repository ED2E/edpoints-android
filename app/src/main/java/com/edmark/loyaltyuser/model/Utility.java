package com.edmark.loyaltyuser.model;

import com.google.gson.annotations.SerializedName;

public class Utility {

    private int billerID;
    private String billerName;

    public Utility(int billerID, String billerName) {
        this.billerID = billerID;
        this.billerName = billerName;
    }

    public int getBillerID() {
        return billerID;
    }

    public void setBillerID(int billerID) {
        this.billerID = billerID;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

}
