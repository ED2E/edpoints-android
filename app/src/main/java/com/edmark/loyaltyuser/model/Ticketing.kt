package com.edmark.loyaltyuser.model

import com.edmark.loyaltyuser.constant.Common
import com.edmark.loyaltyuser.util.AppUtility
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
 * Created by DEV-Michael-ED2E on 04/02/2018.
 */

data class Ticketing(var custid: String = "",
                     var date: String = "",
                     var event_name: String = "",
                     var image_url: String = "",
                     var merchantid: String = "",
                     var rate: String = "",
                     var time: String = "",
                     var company_code: String = "",
                     var status: String = "")
