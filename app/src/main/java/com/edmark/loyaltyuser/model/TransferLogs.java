package com.edmark.loyaltyuser.model;

public class TransferLogs {

    private String id;
    private String account_id;
    private String topup_id;
    private String app_type;
    private String previous_balance;
    private String transferred_amount;
    private String current_balance;
    private String rate;
    private String converted_amount;
    private String description;
    private String created_date;
    private String purchaser_name;
    private String purchaser_country;


    public TransferLogs(String id, String account_id, String topup_id, String app_type, String previous_balance, String transferred_amount, String current_balance, String rate, String converted_amount, String description, String created_date, String purchaser_name, String purchaser_country) {
        this.id = id;
        this.account_id = account_id;
        this.topup_id = topup_id;
        this.app_type = app_type;
        this.previous_balance = previous_balance;
        this.transferred_amount = transferred_amount;
        this.current_balance = current_balance;
        this.rate = rate;
        this.converted_amount = converted_amount;
        this.description = description;
        this.created_date = created_date;
        this.purchaser_name = purchaser_name;
        this.purchaser_country = purchaser_country;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getTopup_id() {
        return topup_id;
    }

    public void setTopup_id(String topup_id) {
        this.topup_id = topup_id;
    }

    public String getApp_type() {
        return app_type;
    }

    public void setApp_type(String app_type) {
        this.app_type = app_type;
    }

    public String getPrevious_balance() {
        return previous_balance;
    }

    public void setPrevious_balance(String previous_balance) {
        this.previous_balance = previous_balance;
    }

    public String getTransferred_amount() {
        return transferred_amount;
    }

    public void setTransferred_amount(String transferred_amount) {
        this.transferred_amount = transferred_amount;
    }

    public String getCurrent_balance() {
        return current_balance;
    }

    public void setCurrent_balance(String current_balance) {
        this.current_balance = current_balance;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getConverted_amount() {
        return converted_amount;
    }

    public void setConverted_amount(String converted_amount) {
        this.converted_amount = converted_amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getPurchaser_name() {
        return purchaser_name;
    }

    public void setPurchaser_name(String purchaser_name) {
        this.purchaser_name = purchaser_name;
    }

    public String getPurchaser_country() {
        return purchaser_country;
    }

    public void setPurchaser_country(String purchaser_country) {
        this.purchaser_country = purchaser_country;
    }
}
