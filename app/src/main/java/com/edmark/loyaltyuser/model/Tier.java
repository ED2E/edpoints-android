package com.edmark.loyaltyuser.model;


import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;

/*
 * Created by DEV-Michael-ED2E on 04/16/2018.
 */
public class Tier {

    private String passup_serialno;
    private String spending_amount;
    private String percentage;
    private String bonus_amount;
    private String passup_date;

    public Tier() {
    }

    public Tier(String passup_serialno, String spending_amount, String percentage, String bonus_amount, String passup_date) {
        this.passup_serialno = passup_serialno;
        this.spending_amount = spending_amount;
        this.percentage = percentage;
        this.bonus_amount = bonus_amount;
        this.passup_date = passup_date;
    }

    public Tier(String passup_serialno, String spending_amount, String percentage, String bonus_amount) {
        this.passup_serialno = passup_serialno;
        this.spending_amount = spending_amount;
        this.percentage = percentage;
        this.bonus_amount = bonus_amount;
    }

    public String getPassup_serialno() {
        return passup_serialno;
    }

    public void setPassup_serialno(String passup_serialno) {
        this.passup_serialno = passup_serialno;
    }

    public String getSpending_amount() {
        return decimalOutputFormat(spending_amount);
    }

    public void setSpending_amount(String spending_amount) {
        this.spending_amount = spending_amount;
    }

    public String getPercentage() {
        return decimalOutputFormat(percentage);
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getBonus_amount() {
        return decimalOutputFormat(bonus_amount);
    }

    public void setBonus_amount(String bonus_amount) {
        this.bonus_amount = bonus_amount;
    }

    public String getPassup_date() {
        return passup_date;
    }

    public void setPassup_date(String passup_date) {
        this.passup_date = passup_date;
    }
}
