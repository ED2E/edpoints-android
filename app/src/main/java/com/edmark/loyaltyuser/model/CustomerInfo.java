package com.edmark.loyaltyuser.model;

/*
 * Created by DEV-Michael-ED2E on 04/04/2018.
 */
public class CustomerInfo {

    private String status;
    private String package_type;
    private String eda;
    private String name;
    private String email;
    private String phone;
    private String address;
    private String gender;
    private String company_code;

    public CustomerInfo() {
    }

    public CustomerInfo(String status, String package_type, String eda, String name, String email, String phone, String address, String gender, String company_code) {
        this.status = status;
        this.package_type = package_type;
        this.eda = eda;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.gender = gender;
        this.company_code = company_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getEda() {
        return eda;
    }

    public void setEda(String eda) {
        this.eda = eda;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }
}
