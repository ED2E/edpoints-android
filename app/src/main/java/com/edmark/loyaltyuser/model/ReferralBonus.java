package com.edmark.loyaltyuser.model;


import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;

/*
 * Created by DEV-Michael-ED2E on 04/13/2018.
 */
public class ReferralBonus {

    private String passup_date;
    private String passup_serialno;
    private String point;

    public ReferralBonus() {
    }

    public ReferralBonus(String passup_date, String passup_serialno, String point) {
        this.passup_date = passup_date;
        this.passup_serialno = passup_serialno;
        this.point = point;
    }

    public String getPassup_date() {
        return passup_date;
    }

    public void setPassup_date(String passup_date) {
        this.passup_date = passup_date;
    }

    public String getPassup_serialno() {
        return passup_serialno;
    }

    public void setPassup_serialno(String passup_serialno) {
        this.passup_serialno = passup_serialno;
    }

    public String getPoint() {
        return decimalOutputFormat(point);
    }

    public void setPoint(String point) {
        this.point = point;
    }

//    @Override

    public ReferralBonus(String passup_date) {
        this.passup_date = passup_date;
    }
//    public String toString() {
//        return "Empno is "+ empno + " Empname is " + empName;
//    }

    @Override
    public boolean equals(Object arg0) {
        boolean flag = false;
        ReferralBonus employee = (ReferralBonus) arg0;
        if(null!= employee && employee.getPassup_date().equalsIgnoreCase(passup_date)){
            flag = true;
        }
        return flag;
    }
}
