package com.edmark.loyaltyuser.model;


import static com.edmark.loyaltyuser.util.AppUtility.decimalOutputFormat;

/*
 * Created by DEV-Michael-ED2E on 04/16/2018.
 */
public class SpendAndEarn {

    private String merchant_name;
    private String total_spend_point;
    private String earn_lep_point;
    private String name;
    private String date;

    public SpendAndEarn() {
    }

    public SpendAndEarn(String f_name, String merchant_name, String total_spend_point, String earn_lep_point) {
        this.name = name;
        this.merchant_name = merchant_name;
        this.total_spend_point = total_spend_point;
        this.earn_lep_point = earn_lep_point;
    }

    public SpendAndEarn(String name, String merchant_name, String total_spend_point, String earn_lep_point, String date) {
        this.name = name;
        this.merchant_name = merchant_name;
        this.total_spend_point = total_spend_point;
        this.earn_lep_point = earn_lep_point;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date.substring(0, date.indexOf(' '));
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getTotal_spend_point() {
        return decimalOutputFormat(total_spend_point);
    }

    public void setTotal_spend_point(String total_spend_point) {
        this.total_spend_point = total_spend_point;
    }

    public String getEarn_lep_point() {
        return decimalOutputFormat(earn_lep_point);
    }

    public void setEarn_lep_point(String earn_lep_point) {
        this.earn_lep_point = earn_lep_point;
    }
}
