package com.edmark.loyaltyuser.model;

import android.util.Log;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.edmark.loyaltyuser.R;

import static com.edmark.loyaltyuser.constant.Common.BRONZE;
import static com.edmark.loyaltyuser.constant.Common.GOLD;
import static com.edmark.loyaltyuser.constant.Common.ORDINARY;
import static com.edmark.loyaltyuser.constant.Common.SILVER;
import static com.edmark.loyaltyuser.constant.Common.STATUS_CODE_ACTIVE;
import static com.edmark.loyaltyuser.constant.Common.STATUS_CODE_INACTIVE;

public class CardType {

    private String package_type;
    private String eda;
    private String name;
    private int custid;
    private String status;

    public CardType(String package_type, String eda, String name, int custid) {
        this.package_type = package_type;
        this.eda = eda;
        this.name = name;
        this.custid = custid;
    }

    public CardType(String package_type, String eda, String name, int custid, String status) {

        this.package_type = package_type;
        this.eda = eda;
        this.name = name;
        this.custid = custid;
        this.status = status;
    }

    public CardType() {

    }


    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getEda() {
        return eda;
    }

    public void setEda(String eda) {
        this.eda = eda;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }

    public String isStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
