package com.edmark.loyaltyuser.model

class DateRange(var tag: String, var dateFrom: String, var dateTo: String, var total_sales: Int)