package com.edmark.loyaltyuser.model;

public class PointTransfer {

    private String id;
    private String ed2e_id;
    private String edp_amount;
    private String status;
    private String created_date;

    public PointTransfer(String id, String ed2e_id, String edp_amount, String status, String created_date) {
        this.id = id;
        this.ed2e_id = ed2e_id;
        this.edp_amount = edp_amount;
        this.status = status;
        this.created_date = created_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEd2e_id() {
        return ed2e_id;
    }

    public void setEd2e_id(String ed2e_id) {
        this.ed2e_id = ed2e_id;
    }

    public String getEdp_amount() {
        return edp_amount;
    }

    public void setEdp_amount(String edp_amount) {
        this.edp_amount = edp_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }
}
